package com.just.bio.structure.pdb;


import com.just.bio.structure.pdb.pojo.PdbSeqres;

import java.util.List;


/**
 * represents the Primary Structure Section of a PDB file
 *
 * Full PDB Contents: http://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html
 * Primary Contents: http://www.wwpdb.org/documentation/file-format-content/format33/sect3.html
 *
 * The primary structure section of a PDB formatted file contains the sequence of residues in each
 * chain of the macromolecule(s). Embedded in these records are chain identifiers and sequence
 * numbers that allow other records to link into the sequence.
 *
 *     DBREF (standard format)
 *     DBREF1 / DBREF2
 *     SEQADV
 *     SEQRES
 *     MODRES
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbSectionPrimary {



    //###########################################################################
    // DATA MEMBERS
    //###########################################################################

    //after the data is parsed
    private List<PdbSeqres> mPdbSeqress;



    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbSectionPrimary(){
    }

    public PdbSectionPrimary(List<PdbSeqres> inPdbSeqress){
        this.mPdbSeqress = inPdbSeqress;
    }



    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################


    // --------------------------------------------------------------------------
    public List<PdbSeqres> getPdbSeqress() { return mPdbSeqress; }
    public void setPdbSeqress (List<PdbSeqres> inPdbSeqress) { this.mPdbSeqress = inPdbSeqress; }






}
