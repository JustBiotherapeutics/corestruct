package com.just.bio.structure.pdb;

import java.util.List;

import com.just.bio.structure.pdb.pojo.PdbRemark;


/**
 * represents the Remark (REMARK) portion of a PDB file
 * <p>
 * Full PDB Contents: http://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html
 * Remark Contents: http://www.wwpdb.org/documentation/file-format-content/format33/remarks.html
 * <p>
 * REMARK records present experimental details, annotations, comments, and information not included in other records.
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbSectionRemark
{

   //###########################################################################
   // DATA MEMBERS
   //###########################################################################

   private List<PdbRemark> mRemarks;
   private String mResolutionLine;
   private String mResolutionUnit; //probably always 'ANGSTROMS'
   private Double mResolution;


   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   public PdbSectionRemark()
   {
   }


   //###########################################################################
   // GETTERS/SETTERS
   //###########################################################################


   /**
    * Remarks as parsed LinkedHashMap (key = RemName)
    *
    * @return the list of parsed remarks
    */
   public List<PdbRemark> getRemarks()
   {
      return mRemarks;
   }

   public void setRemarks(List<PdbRemark> remarks)
   {
      this.mRemarks = remarks;
   }


   /**
    * Full Resolution line (REMARK 2 line)
    *
    * @return the remark line containing resolution information
    */
   public String getResolutionLine()
   {
      return mResolutionLine;
   }

   public void setResolutionLine(String resolutionLine)
   {
      this.mResolutionLine = resolutionLine;
   }

   /**
    * Resolution units identified on REMARK 2 line
    *
    * @return the resolution units
    */
   public String getResolutionUnit()
   {
      return mResolutionUnit;
   }

   public void setResolutionUnit(String resolutionUnit)
   {
      this.mResolutionUnit = resolutionUnit;
   }

   /**
    * Resolution measurement identified on REMARK 2 line
    *
    * @return the resolution value
    */
   public Double getResolution()
   {
      return mResolution;
   }

   public void setResolution(Double resolution)
   {
      this.mResolution = resolution;
   }


   //###########################################################################
   // PUBLIC METHODS
   //###########################################################################


}
