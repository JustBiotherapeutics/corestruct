package com.just.bio.structure.pdb;


import com.hfg.util.StringUtil;

import com.just.bio.structure.enums.StructureFormatType;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class StructureUtils
{


   private final static Pattern RTRIM = Pattern.compile("\\s+$");
   private final static Pattern WHTE_SPCE = Pattern.compile("\\s+");
   private final static Pattern LAST_INT = Pattern.compile("[0-9]+$");


   //###########################################################################
   // STATIC PUBLIC METHODS
   //###########################################################################


   //--------------------------------------------------------------------------
   /**
    * Parses PDB date strings.
    * @param inDate the date to be parsed
    * @return the parsed date
    */
   public static Date parse_dd_MMM_yy_Date(String inDate)
   {
      //http://stackoverflow.com/questions/29490893/parsing-string-to-local-date-doesnt-use-desired-century
      Date date = null;
      if (StringUtil.isSet(inDate))
      {
         DateTimeFormatter formatter = new DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern("dd-MMM-").appendValueReduced(ChronoField.YEAR, 2, 2, Year.now().getValue() - 90).toFormatter(Locale.US);
         LocalDate obsDate = LocalDate.parse(inDate, formatter);
         Instant instant = obsDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
         date = Date.from(instant);
      }

      return date;

   }


   //--------------------------------------------------------------------------
   /**
    * Parses CIF date strings.
    * @param inDate the date to be parsed
    * @return the parsed date
    */
   public static Date parse_yyyy_MM_dd_Date(String inDate)
   {
      //http://stackoverflow.com/questions/29490893/parsing-string-to-local-date-doesnt-use-desired-century
      Date date = null;
      if (StringUtil.isSet(inDate))
      {
         DateTimeFormatter formatter = new DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern("yyyy-MM-dd").toFormatter(Locale.US);
         LocalDate obsDate = LocalDate.parse(inDate, formatter);
         Instant instant = obsDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
         date = Date.from(instant);
      }

      return date;
   }


   //--------------------------------------------------------------------------
   /**
    * Right trim whitespace from a string
    *
    * @param s the string to be trimmed
    * @return the trimmed string
    */
   public static String rightTrim(String s)
   {
      return RTRIM.matcher(s).replaceAll("");
   }


   /**
    * condense all whitespace to single space
    *
    * @param s the string to be operated on
    * @return the string with processed whitespace
    */
   public static String collapseWhitespace(String s)
   {
      return WHTE_SPCE.matcher(s).replaceAll(" ");
   }


   /**
    * split a string on any whitespace - return an array
    *
    * @param s the string to be split
    * @return an array of split fields
    */
   public static String[] splitOnWhitespace(String s)
   {
      if (!StringUtil.isSet(s))
      {
         return null;
      }
      return s.split("\\s+");
   }


   /**
    * split a string on commas - return an array where each element is already trimmed
    *
    * @param s the string to be split
    * @return an array of split and trimmed fields
    */
   public static String[] splitOnCommaAndTrim(String s)
   {
      if (!StringUtil.isSet(s))
      {
         return null;
      }
      return s.split("\\s*,\\s*");
   }


   /**
    * @param i the integer to be converted into a string
    * @return the string representation of the integer or null if the integer was null
    */
   public static String integerToString(Integer i)
   {
      if (null == i)
      {
         return null;
      }
      return i.toString();
   }


   public static Character stringToCharacter(String s)
   {
      if (!StringUtil.isSet(s))
      {
         return null;
      }
      return s.toCharArray()[0];
   }

   /**
    * parse a string into an Integer; return null if the string is not set
    * set param subdueErrors to true if all errors thrown should return null instead
    *
    * @param s the string to parse
    * @param subdueErrors whether or not all errors should result in a null result instead
    * @return the integer representation of the specified string
    */
   public static Integer stringToInteger(String s, boolean subdueErrors)
   {
      try
      {
         if (!StringUtil.isSet(s))
         {
            return null;
         }
         return Integer.parseInt(s.trim());
      }
      catch (Exception e)
      {
         if (subdueErrors)
         {
            return null;
         }
         else
         {
            throw e;
         }
      }
   }

   /**
    * parse a string into an Integer; return null if the string is not set
    *
    * @param s the string to parse
    * @return the integer representation of the specified string
    */
   public static Integer stringToInteger(String s)
   {
      if (!StringUtil.isSet(s))
      {
         return null;
      }
      return Integer.parseInt(s.trim());
   }


   /**
    * parse a string into an Double; return null if the string is not set
    * set param subdueErrors to true if all errors thrown should return null instead - ug, this seems sketchy.
    *
    * @param s the string to parse
    * @param subdueErrors whether or not all errors should result in a null result instead
    * @return the Double representation of the specified string
    */
   public static Double stringToDouble(String s, boolean subdueErrors)
   {
      try
      {
         if (!StringUtil.isSet(s))
         {
            return null;
         }
         return Double.parseDouble(s.trim());
      } catch (Exception e)
      {
         if (subdueErrors)
         {
            return null;
         }
         else
         {
            throw e;
         }
      }
   }


   /**
    * parse a string into an Double; return null if the string is not set
    *
    * @param s the string to parse
    * @return the Double representation of the specified string
    */
   public static double stringToDouble(String s)
   {
      return Double.parseDouble(s.trim());
   }


   /**
    * parse a string into an Float; return null if the string is not set
    * set param subdueErrors to true if all errors thrown should return null instead
    *
    * @param s the string to parse
    * @param subdueErrors whether or not all errors should result in a null result instead
    * @return the Float representation of the specified string
    */
   public static Float stringToFloat(String s, boolean subdueErrors)
   {
      try
      {
         if (!StringUtil.isSet(s))
         {
            return null;
         }
         return Float.parseFloat(s.trim());
      } catch (Exception e)
      {
         if (subdueErrors)
         {
            return null;
         }
         else
         {
            throw e;
         }
      }
   }


   //--------------------------------------------------------------------------
   /**
    * parse a string into an Float; return null if the string is not set
    *
    * @param s the string to parse
    * @return a Float object or null if the string was not set
    */
   public static Float stringToFloat(String s)
   {
      return stringToFloat(s, false);
   }


   //--------------------------------------------------------------------------
   /**
    * parse a string into a BigDecimal; return null if the string is not set
    *
    * @param s the string to parse
    * @return a BigDecimal object or null if the string was not set
    */
   public static BigDecimal stringToBigDecimal(String s)
   {
      return StringUtil.isSet(s) ? new BigDecimal(s) : null;
   }


   //--------------------------------------------------------------------------
   /**
    * find the last integer in a string, if no Integer return null
    * note: the integer must be at the END of the string
    *
    * @param s string to search
    * @return location of last integer in a string
    */
   public static Integer findLastInteger(String s)
   {

      Integer foundInteger = null;

      if (!StringUtil.isSet(s))
      {
         return null;
      }
      Matcher m = LAST_INT.matcher(s);
      if (m.find())
      {
         String foundMatch = m.group();
         foundInteger = stringToInteger(foundMatch);
      }
      return foundInteger;
   }


   //--------------------------------------------------------------------------
   /**
    * based on a PDB file name, return the 4 letter PDB Identifier.
    * file name could be in one of many formats:
    * - [pdb]2rs3.ent[.gz]
    * - [pdb]2RS3.pdb[.gz]
    * - 1VY7.cif[.gz]
    *
    * @param inPdbFileName the file filename to examine
    * @return the four character PDB id or null if none could be determined
    */
   public static String parsePdbIdentifierFromFileName(String inPdbFileName)
   {
      String pdbId = null;

      if (StringUtil.isSet(inPdbFileName))
      {
         String ucName = inPdbFileName.toUpperCase();

         // Remove gzipped extension if present at the end of the name
         if (ucName.endsWith(".GZ"))
         {
            ucName = ucName.substring(0, ucName.length() - 3);
         }

         //remove extension
         if(ucName.endsWith(".ENT") || ucName.endsWith(".PDB") || ucName.endsWith(".CIF"))
         {
            ucName = ucName.substring(0, ucName.length() - 4);
         }

         //last 4 characters outta be the PDB identifier
         if (ucName.length() >= 4)
         {
            pdbId = ucName.substring(ucName.length() - 4);
         }
      }

      return pdbId;
   }


   /**
    * determine file format based on the file extension. this could come from URL, full path, or just a file name.
    * whichever format is sent, we need the actual file to include an extension. examples:
    * - file names:  pdbXXXX.ent.gz,  XXXX.pdb
    * - urls:        http://www.rcsb.org/pdb/files/XXXX.pdb,  http://www.rcsb.org/pdb/files/XXXX.cif?headerOnly=YES
    * - path:        /data/pdb/mirror/.../structures/divided/pdb/zz/pdbXXXX.ent.gz
    *
    * @param inNameOrUrlOrPath the name, URL, or path to be examined
    * @return the structure format type likely represented by the specified file
    */
   public static StructureFormatType determineFileFormatByNameUrlOrPath(String inNameOrUrlOrPath)
   {

      if (!StringUtil.isSet(inNameOrUrlOrPath))
      {
         return null;
      }
      //strip gz if it ends with it
      if (inNameOrUrlOrPath.toLowerCase().endsWith(".gz"))
      {
         inNameOrUrlOrPath = inNameOrUrlOrPath.substring(0, inNameOrUrlOrPath.length() - 3);
      }

      //get everything past last .
      if (!inNameOrUrlOrPath.contains("."))
      {
         return null;
      }
      String fileExt = inNameOrUrlOrPath.substring(StringUtils.lastIndexOf(inNameOrUrlOrPath, ".") + 1);

      //if this is a url, strip any params
      if (fileExt.contains("?"))
      {
         fileExt = fileExt.substring(0, fileExt.indexOf("?"));
      }

      return determineFileFormatByExtension(fileExt);

   }


   public static StructureFormatType determineFileFormatByExtension(String inExtension)
   {
      if (!StringUtil.isSet(inExtension))
      {
         return null;
      }

      if (inExtension.equalsIgnoreCase(StructureFormatType.PDB.extension()))
      {
         return StructureFormatType.PDB;
      }
      else if (inExtension.equalsIgnoreCase(StructureFormatType.ENT.extension()))
      {
         return StructureFormatType.ENT;
      }
      else if (inExtension.equalsIgnoreCase(StructureFormatType.CIF.extension()))
      {
         return StructureFormatType.CIF;
      }
      else if (inExtension.equalsIgnoreCase(StructureFormatType.MOE.extension()))
      {
         return StructureFormatType.MOE;
      }

      return null;
   }


   /**
    * retrieve the filename from a string path (everything after last / separator)
    *
    * @param fullPathWithFileName the path or URL to be examined
    * @return the filename extracted from the end of the specified path
    */
   public static String getFileName(String fullPathWithFileName)
   {
      if (!StringUtil.isSet(fullPathWithFileName))
      {
         return null;
      }
      String[] tokens = fullPathWithFileName.split("[\\\\|/]");

      String maybeHasParams = tokens[tokens.length - 1];
      if (maybeHasParams.contains("?"))
      {
         return maybeHasParams.substring(0, maybeHasParams.indexOf("?"));
      }
      return maybeHasParams;
   }

}
