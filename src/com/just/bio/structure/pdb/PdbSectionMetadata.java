package com.just.bio.structure.pdb;

import com.just.bio.structure.pdb.pojo.*;
import java.util.Date;
import java.util.List;


/**
 * represents the metadata portion of a PDB file
 *
 * Full PDB Contents: http://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html
 * Metadata Contents (aka 'Title Section') http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html
 *
 * This section contains records used to describe the experiment and the biological macromolecules present in the entry:
 *     HEADER (contains classification, deposition date, and pdb identifier)
 *     OBSLTE
 *     TITLE
 *     SPLT
 *     CAVEAT
 *     COMPND
 *     SOURCE
 *     KEYWDS
 *     EXPDTA
 *     NUMMDL
 *     MDLTYP
 *     AUTHOR
 *     REVDAT
 *     SPRSDE
 *     JRNL
 *     REMARKS --- see PdbSectionRemarkJava class
 *
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbSectionMetadata {



    //###########################################################################
    // DATA MEMBERS
    //###########################################################################

    private String mPdbIdentifier;
    private String mClassification;
    private Date mDepositionDate;
    private String mTitle;
    private String mCaveat;
    private String mExpdta;
    private Long mNummdl;
    private String mMdltyp;

    private List<String> mKeyWrds;
    private List<String> mAuthors;

    private List<PdbCompnd> mCompnds;
    private List<PdbSource> mSources;

    private List<PdbRevdat> mPdbRevdats;

    private List<PdbJrnl> mPdbJrnls;

    private List<PdbSplit> mPdbSplits;

    private Date mObslteDate;
    private List<String> mObsltes;

    private Date mSprsdeDate;
    private List<String> mSprsdes;




    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbSectionMetadata(){

    }



    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################

    public String getPdbIdentifier() {return mPdbIdentifier;}
    public void setPdbIdentifier(String pdbIdentifier) {this.mPdbIdentifier = pdbIdentifier;}

    public String getClassification() {return mClassification;}
    public void setClassification(String classification) {this.mClassification = classification;}

    public Date getDepositionDate() {return mDepositionDate;}
    public void setDepositionDate(Date depositionDate) {this.mDepositionDate = depositionDate;}

    public String getTitle() {return mTitle;}
    public void setTitle(String title) {this.mTitle = title;}

    public String getCaveat() {return mCaveat;}
    public void setCaveat(String caveat) {this.mCaveat = caveat;}

    public String getExpdta() {return mExpdta;}
    public void setExpdta(String expdta) {this.mExpdta = expdta;}

    public Long getNummdl() {return mNummdl;}
    public void setNummdl(Long nummdl) {this.mNummdl = nummdl;}

    public String getMdltyp() {return mMdltyp;}
    public void setMdltyp(String mdltyp) {this.mMdltyp = mdltyp;}

    public List<PdbSplit> getSplits() {return mPdbSplits;}
    public void setSplits(List<PdbSplit> pdbSplits) {this.mPdbSplits = pdbSplits;}

    public List<String> getKeyWrds() {return mKeyWrds;}
    public void setKeyWrds(List<String> keyWrds) {this.mKeyWrds = keyWrds;}

    public List<String> getAuthors() {return mAuthors;}
    public void setAuthors(List<String> authors) {this.mAuthors = authors;}

    public List<PdbCompnd> getCompnds() {return mCompnds;}
    public void setCompnds(List<PdbCompnd> compnds) {this.mCompnds = compnds;}

    public List<PdbSource> getSources() {return mSources;}
    public void setSources(List<PdbSource> sources) {this.mSources = sources;}

    public List<PdbRevdat> getPdbRevdats() {return mPdbRevdats;}
    public void setPdbRevdats(List<PdbRevdat> pdbRevdats) {this.mPdbRevdats = pdbRevdats;}

    public List<PdbJrnl> getPdbJrnls() {return mPdbJrnls;}
    public void setPdbJrnls(List<PdbJrnl> pdbJrnls) {this.mPdbJrnls = pdbJrnls;}

    public List<String> getObsltes() {return mObsltes;}
    public void setObsltes(List<String> obsltes) {this.mObsltes = obsltes;}

    public Date getObslteDate() {return mObslteDate;}
    public void setObslteDate(Date obslteDate) {this.mObslteDate = obslteDate;}

    public List<String> getSprsdes() {return mSprsdes;}
    public void setSprsdes(List<String> sprsdes) {this.mSprsdes = sprsdes;}

    public Date getSprsdeDate() {return mSprsdeDate;}
    public void setSprsdeDate(Date sprsdeDate) {this.mSprsdeDate = sprsdeDate;}




}
