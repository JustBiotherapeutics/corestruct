package com.just.bio.structure.pdb;


import java.util.List;

import com.just.bio.structure.pdb.pojo.PdbSsbond;

/**
 * represents the Connectivity Annotation Section of a PDB file
 * <p>
 * Full PDB Contents: http://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html
 * Connectivity Contents: http://www.wwpdb.org/documentation/file-format-content/format33/sect6.html
 * <p>
 * The connectivity annotation section allows the depositors to specify the existence and location of disulfide bonds and other linkages.
 * SSBOND
 * LINK
 * CISPEP
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbSectionConnectivity
{
   //###########################################################################
   // DATA MEMBERS
   //###########################################################################

   private List<PdbSsbond> mPdbSsbonds;


   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   public PdbSectionConnectivity()
   {
   }


   //###########################################################################
   // GETTERS/SETTERS
   //###########################################################################

   /**
    * Ssbonds as list
    *
    * @return the Ssbond records
    */
   public List<PdbSsbond> getPdbSsbonds()
   {
      return mPdbSsbonds;
   }

   public void setPdbSsbonds(List<PdbSsbond> pdbSsbonds)
   {
      this.mPdbSsbonds = pdbSsbonds;
   }


}
