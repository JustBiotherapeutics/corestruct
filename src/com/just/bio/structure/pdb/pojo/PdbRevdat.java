package com.just.bio.structure.pdb.pojo;

import com.hfg.xml.XMLTag;

import java.util.Date;


/**
 * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#REVDAT
 * part of the PdbSectionMetada pojo
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbRevdat {


    private int mModnum;
    private Date mModdate;
    private String mModId;
    private int mModType;
    private String mDetail;

    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbRevdat(){
    }

    public PdbRevdat(int inModnum, Date inModdate, String inModId, int inModType, String inDetail) {
        this.mModnum = inModnum;
        this.mModdate = inModdate;
        this.mModId = inModId;
        this.mModType = inModType;
        this.mDetail = inDetail;
    }

    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################

    //--------------------------------------------------------------------------------------------------
    public int getModnum() {
        return mModnum;
    }

    public void setModnum(int inModnum) {
        this.mModnum = inModnum;
    }

    //--------------------------------------------------------------------------------------------------
    public Date getModdate() {
        return mModdate;
    }

    public void setModdate(Date inModdate) {
        this.mModdate = inModdate;
    }

    //--------------------------------------------------------------------------------------------------
    public String getModId() {
        return mModId;
    }

    public void setModId(String inModId) {
        this.mModId = inModId;
    }

    //--------------------------------------------------------------------------------------------------
    public int getModType() {
        return mModType;
    }

    public void setModType(int inModType) {
        this.mModType = inModType;
    }

    //--------------------------------------------------------------------------------------------------
    public String getDetail() {
        return mDetail;
    }

    public void setDetail(String inDetail) {
        this.mDetail = inDetail;
    }



    public XMLTag toXml(){
        XMLTag tag = new XMLTag(PdbRevdat.class.getSimpleName());
        tag.setAttribute("mModnum", mModnum);
        tag.setAttribute("mModdate",  mModdate);
        tag.setAttribute("mModId", mModId);
        tag.setAttribute("mModType", mModType);
        tag.setAttribute("mDetail", mDetail);
        return tag;
    }

    @Override
    public String toString(){
        return toXml().toXML();
    }
    
    
    
    
    
    
    
    
}
