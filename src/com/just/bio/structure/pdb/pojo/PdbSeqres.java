package com.just.bio.structure.pdb.pojo;


import com.hfg.xml.XMLTag;

/**
 * http://www.wwpdb.org/documentation/file-format-content/format33/sect3.html#SEQRES
 * part of the PdbSectionPrimary pojo (SEQRES)
 *
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbSeqres 
{
    private String  mChainId;
    private String  mChainName;
    private String  mChainDescription;
    private Integer mNumRes;
    private String  mResidues;


    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbSeqres()
    {
    }

    public PdbSeqres(String inChainId, String inChainName, Integer inNumRes, String inResidues) 
    {
        this.mChainId = inChainId;
        this.mChainName = inChainName;
        this.mNumRes = inNumRes;
        this.mResidues = inResidues;
    }

    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################


    public String getChainId() {
        return mChainId;
    }

    public void setChainId(String inChainId) {
        this.mChainId = inChainId;
    }

    public String getChainName() {
        return mChainName;
    }

    public void setChainName(String inChainName) {
        this.mChainName = inChainName;
    }

    public String getDescription()
    {
        return mChainDescription;
    }

    public PdbSeqres setDescription(String inValue)
    {
        this.mChainDescription = inValue;
        return this;
    }

    public Integer getNumRes() {
        return this.mNumRes;
    }

    public void setNumRes(Integer inNumRes) {
        this.mNumRes = inNumRes;
    }


    public String getResidues() {
        return this.mResidues;
    }

    public void setResidues(String inResidues) {
        this.mResidues = inResidues;
    }






    public XMLTag toXml(){
        XMLTag tag = new XMLTag(PdbSeqres.class.getSimpleName());
        tag.setAttribute("mChainId", mChainId);
        tag.setAttribute("mChainName",  mChainName);
        tag.setAttribute("mNumRes", mNumRes);
        tag.setAttribute("mResidues", mResidues);
        return tag;
    }

    @Override
    public String toString(){
        return toXml().toXML();
    }


}
