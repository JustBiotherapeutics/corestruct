package com.just.bio.structure.pdb.pojo;


import com.hfg.xml.XMLTag;

/**
 * http://www.wwpdb.org/documentation/file-format-content/format33/sect6.html#SSBOND
 * part of the PdbSectionConnectivity pojo (SSBOND)
 *
 * The SSBOND record identifies each disulfide bond in protein and polypeptide
 * structures by identifying the two residues involved in the bond.
 *
     COLUMNS        DATA  TYPE     FIELD            DEFINITION
     --------------------------------------------------------------------------------
      1 -  6        Record name    "SSBOND"
      8 - 10        Integer        serNum           Serial number.
     12 - 14        LString(3)     "CYS"            Residue name.
     16             Character      chainID1         Chain identifier.
     18 - 21        Integer        seqNum1          Residue sequence number.
     22             AChar          icode1           Insertion code.
     26 - 28        LString(3)     "CYS"            Residue name.
     30             Character      chainID2         Chain identifier.
     32 - 35        Integer        seqNum2          Residue sequence number.
     36             AChar          icode2           Insertion code.
     60 - 65        SymOP          sym1             Symmetry operator for residue 1.
     67 - 72        SymOP          sym2             Symmetry operator for residue 2.
     74 - 78        Real(5.2)      Length           Disulfide bond distance
 *
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbSsbond {


    private Integer mSerNum;

    private String mResidue1;
    private String mChainId1;
    private Integer mSeqNum1;
    private String mIcode1;

    private String mResidue2;
    private String mChainId2;
    private Integer mSeqNum2;
    private String mIcode2;

    private String mSym1;
    private String mSym2;
    private Double mBondLength;

    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbSsbond(){
    }

    public PdbSsbond(Integer serNum, String residue1, String chainId1, Integer seqNum1, String icode1, String residue2, String chainId2, Integer seqNum2, String icode2, String sym1, String sym2, Double bondLength) {
        this.mSerNum = serNum;
        this.mResidue1 = residue1;
        this.mChainId1 = chainId1;
        this.mSeqNum1 = seqNum1;
        this.mIcode1 = icode1;
        this.mResidue2 = residue2;
        this.mChainId2 = chainId2;
        this.mSeqNum2 = seqNum2;
        this.mIcode2 = icode2;
        this.mSym1 = sym1;
        this.mSym2 = sym2;
        this.mBondLength = bondLength;
    }

    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################


    public Integer getSerNum() {
        return this.mSerNum;
    }

    public void setSerNum(Integer serNum) {
        this.mSerNum = serNum;
    }

    public String getResidue1() {
        return this.mResidue1;
    }

    public void setResidue1(String residue1) {
        this.mResidue1 = residue1;
    }

    public String getChainId1() {
        return this.mChainId1;
    }

    public void setChainId1(String chainId1) {
        this.mChainId1 = chainId1;
    }

    public Integer getSeqNum1() {
        return this.mSeqNum1;
    }

    public void setSeqNum1(Integer seqNum1) {
        this.mSeqNum1 = seqNum1;
    }

    public String getIcode1() {
        return this.mIcode1;
    }

    public void setIcode1(String icode1) {
        this.mIcode1 = icode1;
    }

    public String getResidue2() {
        return this.mResidue2;
    }

    public void setResidue2(String residue2) {
        this.mResidue2 = residue2;
    }

    public String getChainId2() {
        return this.mChainId2;
    }

    public void setChainId2(String chainId2) {
        this.mChainId2 = chainId2;
    }

    public Integer getSeqNum2() {
        return this.mSeqNum2;
    }

    public void setSeqNum2(Integer seqNum2) {
        this.mSeqNum2 = seqNum2;
    }

    public String getIcode2() {
        return this.mIcode2;
    }

    public void setIcode2(String icode2) {
        this.mIcode2 = icode2;
    }

    public String getSym1() {
        return this.mSym1;
    }

    public void setSym1(String sym1) {
        this.mSym1 = sym1;
    }

    public String getSym2() {
        return this.mSym2;
    }

    public void setSym2(String sym2) {
        this.mSym2 = sym2;
    }

    public Double getBondLength() {
        return this.mBondLength;
    }

    public void setBondLength(Double bondLength) {
        this.mBondLength = bondLength;
    }




    public XMLTag toXml(){
        XMLTag tag = new XMLTag(PdbSeqres.class.getSimpleName());
        tag.setAttribute("mSerNum", mSerNum);
        tag.setAttribute("mResidue1",  mResidue1);
        tag.setAttribute("mChainId1", mChainId1);
        tag.setAttribute("mSeqNum1", mSeqNum1);
        tag.setAttribute("mIcode1", mIcode1);
        tag.setAttribute("mResidue2", mResidue2);
        tag.setAttribute("mChainId2", mChainId2);
        tag.setAttribute("mSeqNum2", mSeqNum2);
        tag.setAttribute("mIcode2", mIcode2);
        tag.setAttribute("mSym1", mSym1);
        tag.setAttribute("mSym2", mSym2);
        tag.setAttribute("mBondLength", mBondLength);
        return tag;
    }

    @Override
    public String toString(){
        return toXml().toXML();
    }




}
