package com.just.bio.structure.pdb.pojo;

/**
 *
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbLine {




    private String lineKey;
    private String lineValue;




    public PdbLine(String inLineKey, String inLineValue) {
        this.lineKey = inLineKey;
        this.lineValue = inLineValue;
    }




    public String getLineKey() {return lineKey;}
    public void setLineKey(String lineKey) {this.lineKey = lineKey;}

    public String getLineValue() {return lineValue;}
    public void setLineValue(String lineValue) {this.lineValue = lineValue;}





}
