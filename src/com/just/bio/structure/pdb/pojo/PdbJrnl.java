package com.just.bio.structure.pdb.pojo;


import com.hfg.xml.XMLTag;

/**
 * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#JRNL
 * part of the PdbSectionMetada pojo
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbJrnl {



    private String mAuth;
    private String mTitl;
    private String mEdit;
    private String mRef;
    private String mPubl;
    private String mRefn;
    private String mPmid;
    private String mDoi;
    private boolean mIsPrimary;
    private String mSection;

    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbJrnl(){
    }

    public PdbJrnl(String inAuth, String inTitl, String inEdit, String inRef, String inPubl, String inRefn, String inPmid, String inDoi, boolean isPrimary, String section) {
        this.mAuth = inAuth;
        this.mTitl = inTitl;
        this.mEdit = inEdit;
        this.mRef = inRef;
        this.mPubl = inPubl;
        this.mRefn = inRefn;
        this.mPmid = inPmid;
        this.mDoi = inDoi;
        this.mIsPrimary = isPrimary;
        this.mSection = section;
    }

    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################



    public String getAuth() {
        return mAuth;
    }

    public void setAuth(String inAuth) {
        this.mAuth = inAuth;
    }

    public String getTitl() {
        return mTitl;
    }

    public void setTitl(String inTitl) {
        this.mTitl = inTitl;
    }

    public String getEdit() {
        return mEdit;
    }

    public void setEdit(String inEdit) {
        this.mEdit = inEdit;
    }

    public String getRef() {
        return mRef;
    }

    public void setRef(String inRef) {
        this.mRef = inRef;
    }

    public String getPubl() {
        return mPubl;
    }

    public void setPubl(String inPubl) {
        this.mPubl = inPubl;
    }

    public String getRefn() {
        return mRefn;
    }

    public void setRefn(String inRefn) {
        this.mRefn = inRefn;
    }

    public String getPmid() {
        return mPmid;
    }

    public void setPmid(String inPmid) {
        this.mPmid = inPmid;
    }

    public String getDoi() {
        return mDoi;
    }

    public void setDoi(String inDoi) {
        this.mDoi = inDoi;
    }

    public boolean getIsPrimary() {
        return mIsPrimary;
    }

    public void setIsPrimary(boolean inIsPrimary) {
        this.mIsPrimary = inIsPrimary;
    }

    public String getSection() {
        return mSection;
    }

    public void setSection(String section) {
        this.mSection = section;
    }





    public XMLTag toXml(){
        XMLTag tag = new XMLTag(PdbJrnl.class.getSimpleName());
        tag.setAttribute("mAuth", mAuth);
        tag.setAttribute("mTitl",  mTitl);
        tag.setAttribute("mEdit", mEdit);
        tag.setAttribute("mRef", mRef);
        tag.setAttribute("mPubl", mPubl);
        tag.setAttribute("mRefn", mRefn);
        tag.setAttribute("mPmid", mPmid);
        tag.setAttribute("mDoi", mDoi);
        tag.setAttribute("mIsPrimary", mIsPrimary);
        tag.setAttribute("mSection", mSection);
        return tag;
    }

    @Override
    public String toString(){
        return toXml().toXML();
    }




}
