package com.just.bio.structure.pdb.pojo;


import com.hfg.xml.XMLTag;

import java.util.LinkedList;

/**
 * http://www.wwpdb.org/documentation/file-format-content/format33/remarks.html
 * part of the PdbSectionRemark pojo (REMARK)
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbRemark {


    private Integer mRemNumber;
    private String mRemName;
    private LinkedList<String> mLines;


    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbRemark(){
    }

    public PdbRemark(Integer inRemNumber, String inRemName, LinkedList<String> inLines) {
        this.mRemNumber = inRemNumber;
        this.mRemName = inRemName;
        this.mLines = inLines;
    }

    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################


    public Integer getRemNumber() {return mRemNumber;}
    public void setRemNumber(Integer inRemNumber) {this.mRemNumber = inRemNumber;}

    public String getRemName() {return mRemName;}
    public void setRemName(String inRemName) {this.mRemName = inRemName;}

    public LinkedList<String> getLines() {return mLines;}
    public void setLines(LinkedList<String> inLines) {this.mLines = inLines;}




    public XMLTag toXml(){
        XMLTag tag = new XMLTag(PdbRemark.class.getSimpleName());
        tag.setAttribute("mRemNumber", mRemNumber);
        tag.setAttribute("mRemName",  mRemName);
        tag.setAttribute("countOfLines", mLines.size());
        return tag;
    }

    @Override
    public String toString(){
        return toXml().toXML();
    }


}
