package com.just.bio.structure.pdb.pojo;


import com.hfg.util.StringUtil;
import com.hfg.xml.XMLTag;

/**
 * PDB format:
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#SPLIT
 *
 * CIF format:
 *   http://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v40.dic/Categories/pdbx_database_related.html
 *
 * part of the PdbSectionMetada pojo
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbSplit {


    private String mPdbIdentifier;
    private String mDbName;
    private String mContentType;
    private String mDetail;


    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbSplit(){
    }

    public PdbSplit(String inPdbIdentifier, String inDbName, String inContentType, String inDetail) {
        this.mPdbIdentifier = inPdbIdentifier.trim();
        if(StringUtil.isSet(inDbName)) {
            this.mDbName = inDbName.trim();
        }
        if(StringUtil.isSet(inContentType)) {
            this.mContentType = inContentType.trim();
        }
        if(StringUtil.isSet(inDetail)) {
            this.mDetail = inDetail.trim();
        }

    }

    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################


    //-----------------------------------------------------------
    public String getPdbIdentifier() {
        return mPdbIdentifier;
    }

    public void setPdbIdentifier(String inPdbIdentifier) {
        this.mPdbIdentifier = inPdbIdentifier;
    }

    //-----------------------------------------------------------
    public String getDbName() {
        return mDbName;
    }

    public void setDbName(String inDbName) {
        this.mDbName = inDbName;
    }

    //-----------------------------------------------------------
    public String getContentType() {
        return mContentType;
    }

    public void setContentType(String inContentType) {
        this.mContentType = inContentType;
    }

    //-----------------------------------------------------------
    public String getDetail() {
        return mDetail;
    }

    public void setDetail(String inDetail) {
        this.mDetail = inDetail;
    }



    public XMLTag toXml(){
        XMLTag tag = new XMLTag(PdbSplit.class.getSimpleName());
        tag.setAttribute("mPdbIdentifier", mPdbIdentifier);
        tag.setAttribute("mDbName",  mDbName);
        tag.setAttribute("mContentType", mContentType);
        tag.setAttribute("mDetail", mDetail);
        return tag;
    }

    @Override
    public String toString(){
        return toXml().toXML();
    }


}
