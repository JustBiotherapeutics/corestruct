package com.just.bio.structure.pdb.pojo;


import com.hfg.xml.XMLTag;

import java.util.List;

/**
 * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#COMPND
 * part of the PdbSectionMetadata pojo (COMPND)
 *
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbCompnd {


    private Integer mMolId;
    private String mName;
    private List<String> mLines;


    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public PdbCompnd(){
    }

    public PdbCompnd(Integer inMolId, String inName, List<String> inLines) {
        this.mMolId = inMolId;
        this.mName = inName;
        this.mLines = inLines;
    }

    //###########################################################################
    // GETTERS/SETTERS
    //###########################################################################


    public Integer getMolId() {
        return mMolId;
    }

    public void setMolId(Integer inMolId) {
        this.mMolId = inMolId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String inName) {
        this.mName = inName;
    }

    public List<String> getLines() {
        return mLines;
    }

    public void setLines(List<String> inLines) {
        this.mLines = inLines;
    }



    public XMLTag toXml(){
        XMLTag tag = new XMLTag(PdbCompnd.class.getSimpleName());
        tag.setAttribute("mMolId", mMolId);
        tag.setAttribute("mName",  mName);
        tag.setAttribute("countOfLines", mLines.size());
        return tag;
    }

    @Override
    public String toString(){
        return toXml().toXML();
    }


}
