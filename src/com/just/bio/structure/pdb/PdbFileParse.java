package com.just.bio.structure.pdb;

import java.io.*;
import java.util.zip.GZIPInputStream;

import com.hfg.util.StringUtil;
import com.hfg.util.io.FlowMeterInputStream;
import com.hfg.util.io.MD5InputStream;

import com.just.bio.structure.Structure;
import com.just.bio.structure.StructureImpl;
import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.exceptions.StructureParseException;
import com.just.bio.structure.format.BufferedStructureReader;
import com.just.bio.structure.format.ReadableStructureFormat;
import com.just.bio.structure.format.StructureIOException;
import com.just.bio.structure.services.RcsbRestService;
import com.just.bio.structure.services.RemoteFileRepoService;

import org.apache.commons.collections4.map.SingletonMap;


/**
 * Contains the core functionality for parsing data from a Structure (PDB,CIF,MOE) file
 * <p>
 * the classes PdbSection* contain sections of the PDB file -
 * not all PDB sections are currently implemented, not all classes represent fully completed sections
 * <p>
 * http://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PdbFileParse
{
   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   /**
    * empty constructor will use the default amino acid map when parsing chain/sequences in structure files (see AminoAcid class)
    */
   public PdbFileParse()
   {
   }


   //###########################################################################
   // PUBLIC METHODS TO EXECUTE THE PARSING
   //###########################################################################


   /**
    * parse a PDB structure file based on a PDB Identifier - this will retrieve it from RCSB via REST services
    *
    * @param inPdbIdentifier the PDB identifier whose data is to be retrieved
    * @param inRestUrlFile the URL for viewing the file
    * @param inRestUrlDownload the URL for downloading the file
    * @return a parsed Structure object
    * @throws StructureIOException thrown if an error is encountered during structure retrieval
    */
   public Structure executeParseByRestServices(String inPdbIdentifier, String inRestUrlFile, String inRestUrlDownload)
         throws StructureParseException
   {

      if (!StringUtil.isSet(inPdbIdentifier))
      {
         throw new NullPointerException("PDB Identifier input to executeParseByRestServices() is null!");
      }

      Structure structure;

      try
      {

         //based on the PDB ID, get the structure file from the REST service (PDB format or CIF format)
         //this will also copy the file to a local temporary path
         SingletonMap<String, File> mapPdbUrlFile = RcsbRestService.getInstance(inRestUrlFile, inRestUrlDownload)
                                                                   .retrievePdbContentByPdbIdentifier(inPdbIdentifier);

         //parse temp file into structure object
         structure = parseStructureFromFile(mapPdbUrlFile.getValue());

         //set the url that was used to get this file into the structure object
         structure.setFileRestUrl(mapPdbUrlFile.getKey());

      } catch (IOException ioe)
      {
         throw new StructureParseException("Error parsing structure file: " + inPdbIdentifier + " (" + ioe.getMessage() + ")", ioe);
      }

      return structure;
   }


   /**
    * parse a PDB structure file - this will retrieve it as a file from a local directory (e.g. manually uploaded, or from a mirror)
    *
    * @param originalFile the structure file to be parsed
    * @return a parsed Structure object
    * @throws StructureIOException thrown if an error is encountered during structure retrieval
    */
   public Structure executeParseByLocalFile(File originalFile)
         throws StructureParseException
   {

      if (null == originalFile)
      {
         throw new NullPointerException("File input to executeParseByLocalFile() is null!");
      }

      Structure structure;

      try
      {

         //copy file to temp directory
         File tempFile = RemoteFileRepoService.getInstance().downloadFileToLocal(originalFile);

         //parse temp file into structure object
         structure = parseStructureFromFile(tempFile);

         //track the original path this file initially came from
         structure.setFilePath(originalFile.getAbsolutePath());

      } catch (IOException ioe)
      {
         throw new StructureParseException("Error parsing structure file: " + originalFile.getAbsolutePath(), ioe);
      }

      return structure;
   }


   /**
    * @param tempFile the structure file to be parsed
    * @return a parsed Structure object
    * @throws StructureParseException thrown if an error is encountered during structure retrieval
    */
   private Structure parseStructureFromFile(File tempFile)
         throws StructureParseException
   {

      if (null == tempFile)
      {
         throw new NullPointerException("File input to parseStructureFromFile() is null!");
      }

      Structure structure;

      try
      {

         StructureFormatType structureFormatType = StructureUtils.determineFileFormatByNameUrlOrPath(tempFile.getAbsolutePath());

         ReadableStructureFormat formatObj = ReadableStructureFormat.allocateReader(structureFormatType);

         InputStream fileStream = new FileInputStream(tempFile);
         if (tempFile.getName().endsWith(".gz"))
         {
            fileStream = new GZIPInputStream(fileStream);
         }

         //these will calculate the byte count and the checksum on the uncompressed bytes
         FlowMeterInputStream flowStream = new FlowMeterInputStream(fileStream);
         MD5InputStream md5Stream = new MD5InputStream(flowStream);

         BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(md5Stream));
         structure = parseStructureFromReader(formatObj, bufferedReader);

         structure.setPdbIdentifier(StructureUtils.parsePdbIdentifierFromFileName(tempFile.getName()));
         structure.setFileFormat(structureFormatType);
         structure.setLocalFile(tempFile);
         structure.setUncompressedSizeInBytes(flowStream.getNumBytes());
         structure.setMd5Checksum(md5Stream.getChecksum());

      } catch (IOException ioe)
      {
         throw new StructureIOException("Error parsing structure file: " + tempFile.getAbsolutePath(), ioe);
      }

      return structure;
   }


   /**
    * @param formatObj the structure format object to use for parsing
    * @param inReader the structure data
    * @return a parsed Structure object
    * @throws StructureParseException thrown if an error is encountered during structure retrieval
    */
   public Structure parseStructureFromReader(ReadableStructureFormat formatObj, BufferedReader inReader)
         throws StructureParseException
   {

      Structure structure;

      try
      {

         if (formatObj != null)
         {
            BufferedStructureReader structureReader = new BufferedStructureReader(inReader, formatObj);
            structure = structureReader.next();
         }
         else
         {
            //if there is no format object, we can't really parse the file, but we can read it
            // in order to gather some additional metadata (see flowStream/size and md5Stream/checksum)
            structure = new StructureImpl();
            while (inReader.readLine() != null)
            {
               //if the format is not parsable (PDB/CIF/MOE) then just read through the file so we can calculate size and checksum
            }
            inReader.close();
         }


      } catch (IOException ioe)
      {
         throw new StructureIOException("Error parsing structure data!", ioe);
      }

      return structure;

   }


}
