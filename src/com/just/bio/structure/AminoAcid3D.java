package com.just.bio.structure;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hfg.bio.AminoAcid;
import com.just.bio.structure.format.pdb.PDB_AtomName;
import static com.just.bio.structure.format.pdb.PDB_AtomName.*;



// file:///apps/moe/moe2015/html/proteins/fcnref/pro_lett.htm
// http://www.rcsb.org/pdb/ligand/ligandsummary.do?hetId=ALA


//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class AminoAcid3D
{

    private AminoAcid mAA;
    private String m3LetterCode;
    private boolean mStandard;
    private List<PDB_AtomName> mAtomNames;

    private static Map<String, AminoAcid3D> sValueMap = new HashMap<>(50);
    private static Map<String, AminoAcid3D> sLookupMap = new HashMap<>(50);

    //##########################################################################
    // PUBLIC FIELDS
    //##########################################################################

    public static final AminoAcid3D GLYCINE       = new AminoAcid3D(AminoAcid.GLYCINE,       true, Arrays.asList(N, CA, C, O));
    public static final AminoAcid3D ALANINE       = new AminoAcid3D(AminoAcid.ALANINE,       true, Arrays.asList(N, CA, C, O, CB));
    public static final AminoAcid3D SERINE        = new AminoAcid3D(AminoAcid.SERINE,        true, Arrays.asList(N, CA, C, O, CB, OG));
    public static final AminoAcid3D CYSTEINE      = new AminoAcid3D(AminoAcid.CYSTEINE,      true, Arrays.asList(N, CA, C, O, CB, SG));
    public static final AminoAcid3D ASPARTIC_ACID = new AminoAcid3D(AminoAcid.ASPARTIC_ACID, true, Arrays.asList(N, CA, C, O, CB, CG, OD1, OD2));
    public static final AminoAcid3D GLUTAMIC_ACID = new AminoAcid3D(AminoAcid.GLUTAMIC_ACID, true, Arrays.asList(N, CA, C, O, CB, CG, CD, OE1, OE2));
    public static final AminoAcid3D PHENYLALANINE = new AminoAcid3D(AminoAcid.PHENYLALANINE, true, Arrays.asList(N, CA, C, O, CB, CG, CD1, CE1, CZ, CE2, CD2));
    public static final AminoAcid3D HISTIDINE     = new AminoAcid3D(AminoAcid.HISTIDINE,     true, Arrays.asList(N, CA, C, O, CB, CG, ND1, CE1, NE2, CD2));
    public static final AminoAcid3D ISOLEUCINE    = new AminoAcid3D(AminoAcid.ISOLEUCINE,    true, Arrays.asList(N, CA, C, O, CB, CG2, CG1, CD1));
    public static final AminoAcid3D LEUCINE       = new AminoAcid3D(AminoAcid.LEUCINE,       true, Arrays.asList(N, CA, C, O, CB, CG, CD1, CD2));
    public static final AminoAcid3D LYSINE        = new AminoAcid3D(AminoAcid.LYSINE,        true, Arrays.asList(N, CA, C, O, CB, CG, CD, CE, NZ));
    public static final AminoAcid3D METHIONINE    = new AminoAcid3D(AminoAcid.METHIONINE,    true, Arrays.asList(N, CA, C, O, CB, CG, SD, CE));
    public static final AminoAcid3D ASPARAGINE    = new AminoAcid3D(AminoAcid.ASPARAGINE,    true, Arrays.asList(N, CA, C, O, CB, CG, OD1, ND2));
    public static final AminoAcid3D PROLINE       = new AminoAcid3D(AminoAcid.PROLINE,       true, Arrays.asList(N, CA, C, O, CB, CG, CD));
    public static final AminoAcid3D GLUTAMINE     = new AminoAcid3D(AminoAcid.GLUTAMINE,     true, Arrays.asList(N, CA, C, O, CB, CG, CD, OE1, NE2));
    public static final AminoAcid3D ARGININE      = new AminoAcid3D(AminoAcid.ARGININE,      true, Arrays.asList(N, CA, C, O, CB, CG, CD, NE, CZ, NH1, NH2));
    public static final AminoAcid3D THREONIE      = new AminoAcid3D(AminoAcid.THREONIE,      true, Arrays.asList(N, CA, C, O, CB, CG2, OG1));
    public static final AminoAcid3D VALINE        = new AminoAcid3D(AminoAcid.VALINE,        true, Arrays.asList(N, CA, C, O, CB, CG1, CG2));
    public static final AminoAcid3D TRYPTOPHAN    = new AminoAcid3D(AminoAcid.TRYPTOPHAN,    true, Arrays.asList(N, CA, C, O, CB, CG, CD1, NE1, CE2, CZ2, CH2, CZ3, CE3, CD2));
    public static final AminoAcid3D TYROSINE      = new AminoAcid3D(AminoAcid.TYROSINE,      true, Arrays.asList(N, CA, C, O, CB, CG, CD1, CE1, CZ, OH, CE2, CD2));

    public static final AminoAcid3D GLU_GLN_AVG     = new AminoAcid3D(AminoAcid.GLU_GLN_AVG,     false, Arrays.asList(N, CA, C, O, CB, CG, CD, XE1, XE2));
    public static final AminoAcid3D ASP_ASN_AVG     = new AminoAcid3D(AminoAcid.ASP_ASN_AVG,     false, Arrays.asList(N, CA, C, O, CB, CG, XD1, XD2));

    public static final AminoAcid3D D_ALANINE =         new AminoAcid3D(new AminoAcid(AminoAcid.D_ALANINE.name(),       AminoAcid.D_ALANINE.getThreeLetterCode(),       'A'), false, Arrays.asList(N, CA, C, O, CB, CG, CD, NE, CZ, NH1, NH2));
    public static final AminoAcid3D D_ARGININE =        new AminoAcid3D(new AminoAcid(AminoAcid.D_ARGININE.name(),      AminoAcid.D_ARGININE.getThreeLetterCode(),      'R'), false, Arrays.asList(N, CA, C, O, CB, CG, CD, NE, CZ, NH1, NH2));
    public static final AminoAcid3D D_ASPARAGINE =      new AminoAcid3D(new AminoAcid(AminoAcid.D_ASPARAGINE.name(),    AminoAcid.D_ASPARAGINE.getThreeLetterCode(),    'N'), false, Arrays.asList(N, CA, C, O, CB, CG, OD1, ND2));
    public static final AminoAcid3D D_ASPARTIC_ACID =   new AminoAcid3D(new AminoAcid(AminoAcid.D_ASPARTIC_ACID.name(), AminoAcid.D_ASPARTIC_ACID.getThreeLetterCode(), 'D'), false, Arrays.asList(N, CA, C, O, CB, CG, OD1, OD2));
    public static final AminoAcid3D D_CYSTEINE =        new AminoAcid3D(new AminoAcid(AminoAcid.D_CYSTEINE.name(),      AminoAcid.D_CYSTEINE.getThreeLetterCode(),      'C'), false, Arrays.asList(N, CA, C, O, CB, SG));
    public static final AminoAcid3D D_GLUTAMIC_ACID =   new AminoAcid3D(new AminoAcid(AminoAcid.D_GLUTAMIC_ACID.name(), AminoAcid.D_GLUTAMIC_ACID.getThreeLetterCode(), 'E'), false, Arrays.asList(N, CA, C, O, CB, CG, CD, OE1, OE2));
    public static final AminoAcid3D D_GLUTAMINE =       new AminoAcid3D(new AminoAcid(AminoAcid.D_GLUTAMINE.name(),     AminoAcid.D_GLUTAMINE.getThreeLetterCode(),     'Q'), false, Arrays.asList(N, CA, C, O, CB, CG, CD, OE1, NE2));
    public static final AminoAcid3D D_HISTIDINE =       new AminoAcid3D(new AminoAcid(AminoAcid.D_HISTIDINE.name(),     AminoAcid.D_HISTIDINE.getThreeLetterCode(),     'H'), false, Arrays.asList(N, CA, C, O, CB, CG, ND1, CE1, NE2, CD2));
    public static final AminoAcid3D D_ISOLEUCINE =      new AminoAcid3D(new AminoAcid(AminoAcid.D_ISOLEUCINE.name(),    AminoAcid.D_ISOLEUCINE.getThreeLetterCode(),    'I'), false, Arrays.asList(N, CA, C, O, CB, CG2, CG1, CD1));
    public static final AminoAcid3D D_ISOVALINE =       new AminoAcid3D(new AminoAcid(AminoAcid.D_ISOVALINE.name(),     AminoAcid.D_ISOVALINE.getThreeLetterCode(),     'X'), false, Arrays.asList(N, CA, C, O, CB1, CB2, CG1));
    public static final AminoAcid3D D_LEUCINE =         new AminoAcid3D(new AminoAcid(AminoAcid.D_LEUCINE.name(),       AminoAcid.D_LEUCINE.getThreeLetterCode(),       'L'), false, Arrays.asList(N, CA, C, O, CB, CG, CD1, CD2));
    public static final AminoAcid3D D_LYSINE =          new AminoAcid3D(new AminoAcid(AminoAcid.D_LYSINE.name(),        AminoAcid.D_LYSINE.getThreeLetterCode(),        'K'), false, Arrays.asList(N, CA, C, O, CB, CG, CD, CE, NZ));
    public static final AminoAcid3D D_METHIONINE =      new AminoAcid3D(new AminoAcid(AminoAcid.D_METHIONINE.name(),    AminoAcid.D_METHIONINE.getThreeLetterCode(),    'M'), false, Arrays.asList(N, CA, C, O, CB, CG, SD, CE));
    public static final AminoAcid3D D_PHENYLALANINE =   new AminoAcid3D(new AminoAcid(AminoAcid.D_PHENYLALANINE.name(), AminoAcid.D_PHENYLALANINE.getThreeLetterCode(), 'F'), false, Arrays.asList(N, CA, C, O, CB, CG, CD1, CE1, CZ, CE2, CD2));
    public static final AminoAcid3D D_PROLINE =         new AminoAcid3D(new AminoAcid(AminoAcid.D_PROLINE.name(),       AminoAcid.D_PROLINE.getThreeLetterCode(),       'P'), false, Arrays.asList(N, CA, C, O, CB, CG, CD));
    public static final AminoAcid3D D_SERINE =          new AminoAcid3D(new AminoAcid(AminoAcid.D_SERINE.name(),        AminoAcid.D_SERINE.getThreeLetterCode(),        'S'), false, Arrays.asList(N, CA, C, O, CB, OG));
    public static final AminoAcid3D D_THREONIE =        new AminoAcid3D(new AminoAcid(AminoAcid.D_THREONIE.name(),      AminoAcid.D_THREONIE.getThreeLetterCode(),      'T'), false, Arrays.asList(N, CA, C, O, CB, CG2, OG1));
    public static final AminoAcid3D D_TRYPTOPHAN =      new AminoAcid3D(new AminoAcid(AminoAcid.D_TRYPTOPHAN.name(),    AminoAcid.D_TRYPTOPHAN.getThreeLetterCode(),    'W'), false, Arrays.asList(N, CA, C, O, CB, CG, CD1, NE1, CE2, CZ2, CH2, CZ3, CE3, CD2));
    public static final AminoAcid3D D_TYROSINE =        new AminoAcid3D(new AminoAcid(AminoAcid.D_TYROSINE.name(),      AminoAcid.D_TYROSINE.getThreeLetterCode(),      'Y'), false, Arrays.asList(N, CA, C, O, CB, CG, CD1, CE1, CZ, OH, CE2, CD2));
    public static final AminoAcid3D D_VALINE =          new AminoAcid3D(new AminoAcid(AminoAcid.D_VALINE.name(),        AminoAcid.D_VALINE.getThreeLetterCode(),        'V'), false, Arrays.asList(N, CA, C, O, CB, CG1, CG2));

    public static final AminoAcid3D HISTIDINE_HID   = new AminoAcid3D(AminoAcid.HISTIDINE_HID,   false, Arrays.asList(N, CA, C, O, CB, CG, ND1, CE1, NE2, CD2));
    public static final AminoAcid3D HISTIDINE_HIE   = new AminoAcid3D(AminoAcid.HISTIDINE_HIE,   false, Arrays.asList(N, CA, C, O, CB, CG, ND1, CE1, NE2, CD2));
    public static final AminoAcid3D HISTIDINE_HIP   = new AminoAcid3D(AminoAcid.HISTIDINE_HIP,   false, Arrays.asList(N, CA, C, O, CB, CG, ND1, CE1, NE2, CD2));

    public static final AminoAcid3D SARCOSINE                      = new AminoAcid3D(AminoAcid.SARCOSINE,                       false, Arrays.asList(N, CA, C, O, CN));
    public static final AminoAcid3D NORVALINE                      = new AminoAcid3D(AminoAcid.NORVALINE,                       false, Arrays.asList(N, CA, C, O, CB, CG, CD));
    public static final AminoAcid3D NORLEUCINE                     = new AminoAcid3D(AminoAcid.NORLEUCINE,                      false, Arrays.asList(N, CA, C, O, CB, CG, CD, CE));
    public static final AminoAcid3D THREE_SULFINOALANINE           = new AminoAcid3D(AminoAcid.THREE_SULFINOALANINE,            false, Arrays.asList(N, CA, C, O, CB, SG, OD1, OD2));
    public static final AminoAcid3D CYS_2_HYDROXYETHYL_THIOCYSTEINE = new AminoAcid3D(AminoAcid.CYS_2_HYDROXYETHYL_THIOCYSTEINE,false, Arrays.asList(N, CA, C, O, CB, SG, SD, CE, CZ, OH));
    public static final AminoAcid3D CYS_S_HYDROXY                  = new AminoAcid3D(AminoAcid.CYS_S_HYDROXY,                   false, Arrays.asList(N, CA, C, O, CB, SG, OD));
    public static final AminoAcid3D CYS_S_DIOXIDE                  = new AminoAcid3D(AminoAcid.CYS_S_DIOXIDE,                   false, Arrays.asList(N, CA, C, O, CB, SG, OD1, OD2));
    public static final AminoAcid3D CYS_S_PHOSPHO                  = new AminoAcid3D(AminoAcid.CYS_S_PHOSPHO,                   false, Arrays.asList(N, CA, C, O, CB, SG, P, O1P, O2P, O3P));
    public static final AminoAcid3D CYS_SULFONIC_ACID              = new AminoAcid3D(AminoAcid.CYS_SULFONIC_ACID,               false, Arrays.asList(N, CA, C, O, CB, SG, OD1, OD3, OD2));
    public static final AminoAcid3D SELENOCYSTEINE                 = new AminoAcid3D(AminoAcid.SELENOCYSTEINE,                  false, Arrays.asList(N, CA, C, O, CB, SE));
    public static final AminoAcid3D HIS_4_METHYL                   = new AminoAcid3D(AminoAcid.HIS_4_METHYL,                    false, Arrays.asList(N, CA, C, O, CB, CG, ND1, CE1, CZ, NE2, CD2));
    public static final AminoAcid3D PRO_HYDROXY                    = new AminoAcid3D(AminoAcid.PRO_HYDROXY,                     false, Arrays.asList(N, CA, C, O, CB, CG, CD, OD));
    public static final AminoAcid3D LYS_ACETYLATED                 = new AminoAcid3D(AminoAcid.LYS_ACETYLATED,                  false, Arrays.asList(N, CA, C, O, CB, CG, CD, CE, NZ, CH, OH, CH3));
    public static final AminoAcid3D LYS_NZ_CARBOXYLIC_ACID         = new AminoAcid3D(AminoAcid.LYS_NZ_CARBOXYLIC_ACID,          false, Arrays.asList(N, CA, C, O, CB, CG, CD, CD, NZ, CX, OQ1, OQ2));
    public static final AminoAcid3D LYS_N_TRIMETHYL                = new AminoAcid3D(AminoAcid.LYS_N_TRIMETHYL,                 false, Arrays.asList(N, CA, C, O, CB, CG, CD, CE, NZ, CM1, CM2, CM3));
    public static final AminoAcid3D LYS_N_DIMETHYL                 = new AminoAcid3D(AminoAcid.LYS_N_DIMETHYL,                  false, Arrays.asList(N, CA, C, O, CB, CG, CD, CE, NZ, CH1, CH2));
    public static final AminoAcid3D MET_S_DIXOY                    = new AminoAcid3D(AminoAcid.MET_S_DIXOY,                     false, Arrays.asList(N, CA, C, O, CB, CG, SD, CE, OD1, OD2));
    public static final AminoAcid3D SELENOMETHIONINE               = new AminoAcid3D(AminoAcid.SELENOMETHIONINE,                false, Arrays.asList(N, CA, C, O, CB, CG, SE, CE));
    public static final AminoAcid3D SER_PHOSPHO                    = new AminoAcid3D(AminoAcid.SER_PHOSPHO,                     false, Arrays.asList(N, CA, C, O, CB, OG, P, O1P, O2P, O3P));
    public static final AminoAcid3D THR_PHOSPHO                    = new AminoAcid3D(AminoAcid.THR_PHOSPHO,                     false, Arrays.asList(N, CA, C, O, CB, CG2, OG1, P, O1P, O2P, O3P));
    public static final AminoAcid3D TYR_PHOSPHO                    = new AminoAcid3D(AminoAcid.TYR_PHOSPHO,                     false, Arrays.asList(N, CA, C, O, CB, CG, CD1, CE1, CZ, OH, CE2, CD2, P, O1P, O2P, O3P));
    public static final AminoAcid3D TYR_O_SULFO                    = new AminoAcid3D(AminoAcid.TYR_O_SULFO,                     false, Arrays.asList(N, CA, C, O, CB, CG, CD1, CE1, CZ, OH, CE2, CD2, S, O1, O2, O3));
    public static final AminoAcid3D ASN_N_METHYL                   = new AminoAcid3D(AminoAcid.ASN_N_METHYL,                    false, Arrays.asList(N, CA, C, O, CB, CG, OD1, ND2, CE2));
    public static final AminoAcid3D ORNITHINE                      = new AminoAcid3D(AminoAcid.ORNITHINE,                       false, Arrays.asList(N, CA, C, O, CB, CG, CD, NE));
    public static final AminoAcid3D ALPHA_AMINOBUTYRIC_ACID        = new AminoAcid3D(AminoAcid.ALPHA_AMINOBUTYRIC_ACID,         false, Arrays.asList(N, CA, C, O, CB, CG));
    public static final AminoAcid3D ALPHA_AMINOISOBUTYRIC_ACID     = new AminoAcid3D(AminoAcid.ALPHA_AMINOISOBUTYRIC_ACID,      false, Arrays.asList(N, CA, C, O, CB1, CB2));
    public static final AminoAcid3D AMINO_MERCAPTO_BUTYRIC_ACID    = new AminoAcid3D(AminoAcid.AMINO_MERCAPTO_BUTYRIC_ACID,     false, Arrays.asList(N, CA, C, O, CB, CG, SD));
    public static final AminoAcid3D LLP                            = new AminoAcid3D(AminoAcid.LLP,                             false, Arrays.asList(N, CA, C, O, CB, CG, N1, C2, C2_PRIME, C3, O3, C4, C4_PRIME, C5, C6, C5_PRIME, O4P, P, O1P, O2P, O3P, CD, CE, NZ));

    // Some PDB AA codes refer to a termini-modified version of an amino acid. Treat these separately
    public static final AminoAcid3D VAL_N_METHYL                      = new AminoAcid3D(AminoAcid.VALINE,        "MVA", false, Arrays.asList(N, CA, C, O, CB, CN, CG1, CG2));
    public static final AminoAcid3D MET_N_CARBOXY                     = new AminoAcid3D(AminoAcid.METHIONINE,    "CXM", false, Arrays.asList(N, CA, C, O, CB, CG, SD, CE, CN, ON1, ON2));
    public static final AminoAcid3D MET_N_FORMYL                      = new AminoAcid3D(AminoAcid.METHIONINE,    "FME", false, Arrays.asList(N, CA, C, O, CB, CG, SD, CE, CN, O1));
    public static final AminoAcid3D ALA_N_METHYL                      = new AminoAcid3D(AminoAcid.ALANINE,       "MAA", false, Arrays.asList(N, CA, C, O, CB, CM));
    public static final AminoAcid3D LEU_N_METHYL                      = new AminoAcid3D(AminoAcid.LEUCINE,       "MLE", false, Arrays.asList(N, CA, C, O, CB, CG, CN, CD1, CD2));
    public static final AminoAcid3D PYROGLUTAMIC_ACID                 = new AminoAcid3D(AminoAcid.GLUTAMINE,     "PCA", false, Arrays.asList(N, CA, C, O, CB, CG, CD, OE));
    public static final AminoAcid3D THR_4_METHYL_4_BUTENYL_4_N_METHYL = new AminoAcid3D(null,                    "BMT", false, Arrays.asList(N, CA, C, O, CB, CG2, CN, OG1, CD1, CD2, CE, CZ, CH));
    public static final AminoAcid3D ISOVALERIC_ACID                   = new AminoAcid3D(null,                    "IVA", false, Arrays.asList(CA, C, O, CB, CG1, CG2));



    //##########################################################################
    // CONSTRUCTORS
    //##########################################################################

    //--------------------------------------------------------------------------
    public AminoAcid3D(AminoAcid inAA, boolean inStandard, List<PDB_AtomName> inAtomNames) {
        this(inAA, inAA.getThreeLetterCode(), inStandard, inAtomNames);
    }

    //--------------------------------------------------------------------------
    public AminoAcid3D(AminoAcid inAA, String in3LetterCode, boolean inStandard, List<PDB_AtomName> inAtomNames) {
        mAA = inAA;
        m3LetterCode = in3LetterCode;
        mStandard = inStandard;
        mAtomNames = inAtomNames;

        sValueMap.put(m3LetterCode, this);
        sLookupMap.put(m3LetterCode.toUpperCase(), this);
    }


    //##########################################################################
    // PUBLIC FUNCTIONS
    //##########################################################################

    //--------------------------------------------------------------------------
    public static AminoAcid3D valueOf(String inThreeLetterCode)
    {
        return sLookupMap.get(inThreeLetterCode.toUpperCase());
    }

    //##########################################################################
    // GETTERS
    //##########################################################################

    //--------------------------------------------------------------------------
    public AminoAcid getAminoAcid() {
        return mAA;
    }

    //--------------------------------------------------------------------------
    public String getName() {
        if(mAA!=null) {
            return mAA.name();
        }
        return null;
    }

    //--------------------------------------------------------------------------
    public String getThreeLetterCode() {
        return m3LetterCode;
    }

    //--------------------------------------------------------------------------
    public boolean isStandard() {
        return mStandard;
    }

    //--------------------------------------------------------------------------
    public List<PDB_AtomName> getAtomNames() {
        return mAtomNames;
    }


}
