package com.just.bio.structure;

import com.just.bio.structure.AminoAcid3D;
import com.just.bio.structure.ElementMap;
import com.just.bio.structure.enums.ResidueType;



/**
 * a list of residues defined in the SEQRES portion of a file - contain NO atomic coordinates.
 * these evaluate to the theoretical sequence, of which there is only ONE per chain.
 *
 *
 * each SeqRes consists of:
 *   -- name     - 3 letter abbreviation
 *   -- ordinal  - order which the residues are found
 *
 <div>
 @author Russell Elbert Williams
 </div>
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class SeqResidue {



    // #################################################################################################################
    // data members

    private String mName;
    private Integer mOrdinal;
    private ResidueType mResidueType = ResidueType.UNKNOWN;



    // #################################################################################################################
    // constructors

    public SeqResidue(String inName, Integer inOrdinal)
    {
        this.mName = inName;
        this.mOrdinal = inOrdinal;
        init();
    }


    private void init()
    {
        //determine if this residue is:
        //   - one of the standard (20) amino acid residues based on the fullAminoAcid3D
        //   - a non-standard amino acid
        //   - a nucleotide

        ElementMap elementMap = ElementMap.getInstance();
        AminoAcid3D fullAminoAcid3D = elementMap.getAaMap().get(mName);

        if(fullAminoAcid3D != null){
            //determine if this residue is one of the standard (20) amino acid residues based on the fullAminoAcid3D
            mResidueType = fullAminoAcid3D.isStandard() ? ResidueType.STANDARD_AMINO_ACID : ResidueType.NON_STANDARD_AMINO_ACID;
        }
        else if(elementMap.getNaMap().containsKey(mName))
        {
            //set this residue to a nucleotide (as compared to an amino acid)
            mResidueType = ResidueType.NUCLEOTIDE;
        }
        //else the residue type remains as unknown


    }



    //--------------------------------------------------------------------------
    @Override
    public String toString()
    {
        return name() + " " + getOrdinal();
    }



    // #################################################################################################################
    // getters


    public String name() {return mName;}

    public Integer getOrdinal() {return mOrdinal;}

    public ResidueType getResidueType() {return mResidueType;}







}
