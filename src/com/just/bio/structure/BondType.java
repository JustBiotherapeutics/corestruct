package com.just.bio.structure;

/** ----------------------------------------------------------------------------------------------------
 * Enumeration of the types of bonds found in biological structures.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public enum BondType
{
   COVALENT,
   IONIC,
   HYDROGEN,
   VAN_DER_WAALS;
}
