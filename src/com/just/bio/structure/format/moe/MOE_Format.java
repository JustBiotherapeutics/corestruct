package com.just.bio.structure.format.moe;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

import com.hfg.math.Range;
import com.hfg.util.StringUtil;

import com.just.bio.structure.*;
import com.just.bio.structure.enums.AtomType;
import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.exceptions.StructureParseException;
import com.just.bio.structure.format.ReadableStructureFormatBase;
import com.just.bio.structure.format.StructureFactory;
import com.just.bio.structure.format.StructureIOException;
import com.just.bio.structure.format.pdb.PDB_AtomName;
import com.just.bio.structure.pdb.StructureUtils;
import com.just.bio.structure.pdb.pojo.PdbSeqres;


// local MOE info:
//    file:///apps/moe/moe2015/html/index.htm

// MOE parsing info:
//    file://<site>/html/moe/fcnref/moefmt.htm

/**
 * Structure format object for MOE-formatted files.
 * @param <T> The Structure class that will result from parsing
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class MOE_Format<T extends Structure> extends ReadableStructureFormatBase<T>
{
    //##################################################################################################################
    // data members for initially reading in the file

    //these three come from the #molecule section
    private int mTotalChainCount;
    private int mTotalResidueCount;
    private int mTotalAtomCount;

    //these come from their respective ParaTables (#attr) - they contain ALL rows
    private MOE_ParaTable<ChainParaTableRecord>   mAllChainData;
    private MOE_ParaTable<ResidueParaTableRecord> mAllResidueData;
    private MOE_ParaTable<AtomParaTableRecord>    mAllAtomData;

    private static final MOE_ParaTableCol sBackboneCol = new MOE_ParaTableCol("aBackbone", MOE_ParaTableColFormat.type_integer, 1);

    private static final Map<String, PDB_AtomName> sAtomNameConversionMap = new HashMap<>();
    static
    {
       sAtomNameConversionMap.put("C1$n", PDB_AtomName.C1_PRIME);
       sAtomNameConversionMap.put("C2$n", PDB_AtomName.C2_PRIME);
       sAtomNameConversionMap.put("C3$n", PDB_AtomName.C3_PRIME);
       sAtomNameConversionMap.put("C4$n", PDB_AtomName.C4_PRIME);
       sAtomNameConversionMap.put("C5$n", PDB_AtomName.C5_PRIME);
       sAtomNameConversionMap.put("H1$n", PDB_AtomName.H1_PRIME);
       sAtomNameConversionMap.put("H2$n", PDB_AtomName.H2_PRIME);
       sAtomNameConversionMap.put("H2$n$n", PDB_AtomName.H2_DOUBLE_PRIME);
       sAtomNameConversionMap.put("H3$n", PDB_AtomName.H3_PRIME);
       sAtomNameConversionMap.put("H4$n", PDB_AtomName.H4_PRIME);
       sAtomNameConversionMap.put("H5$n", PDB_AtomName.H5_PRIME);
       sAtomNameConversionMap.put("H5$n$n", PDB_AtomName.H5_DOUBLE_PRIME);
       sAtomNameConversionMap.put("HO3$n", PDB_AtomName.HO3_PRIME);
       sAtomNameConversionMap.put("HO5$n", PDB_AtomName.HO5_PRIME);
       sAtomNameConversionMap.put("O3$n", PDB_AtomName.O3_PRIME);
       sAtomNameConversionMap.put("O4$n", PDB_AtomName.O4_PRIME);
       sAtomNameConversionMap.put("O5$n", PDB_AtomName.O5_PRIME);
    }

    //##################################################################################################################
    // CONSTRUCTORS
    //##################################################################################################################

    //---------------------------------------------------------------------------
    public MOE_Format(StructureFactory<T> inStructureFactory)
    {
        super(inStructureFactory);
    }


    //##################################################################################################################
    // PUBLIC METHODS
    //##################################################################################################################

    //---------------------------------------------------------------------------
    public StructureFormatType getStructureFormatType()
    {
        return StructureFormatType.MOE;
    }

    //---------------------------------------------------------------------------
    @Override
    public T readRecord(BufferedReader inReader)
          throws StructureIOException
    {
        if (null == getStructureFactory())
        {
            throw new StructureIOException("No Structure factory has been specified!");
        }

        T structure;

        try
        {
            //read and parse all sections of the file that we care about; then amalgamate that data into our structure
            structure = readAndParseMoeFile(inReader);
        }
        catch (Exception e)
        {
            throw new StructureIOException(e);
        }

        return structure;
    }

    //---------------------------------------------------------------------------
    private boolean containsChainColumns(Collection<MOE_ParaTableCol> inCols)
    {
        boolean result = false;
        for (MOE_ParaTableCol col : inCols)
        {
            if (col.name().startsWith("c"))
            {
                result = true;
            }
        }
        return result;
    }

    //---------------------------------------------------------------------------
    /**
     * iterate through the file reading in the sections (ParaTables) that we want to parse
     * for each section create the related objects that contain lists of data
     * @param inReader
     * @throws StructureParseException
     */
    private T readAndParseMoeFile(BufferedReader inReader)
          throws StructureParseException
    {
        T structure;

        try
        {
            structure = getStructureFactory().createStructureObj();
            AtomicCoordinates atomicCoordinates = new AtomicCoordinates();

            structure.setAtomicCoordinates(atomicCoordinates);

            String line;
            while ((line = inReader.readLine()) != null)
            {
                //System.out.println(line);

                // Is this the #molecule section?
                if (line.startsWith("#molecule "))
                {
                    parseParaTableMolecule(inReader);
                }
                else if (line.startsWith("#attr "))
                {
                    MOE_ParaTable<MOE_ParaTableRecord> paraTable = new MOE_ParaTable<>();
                    List<MOE_ParaTableCol> cols = paraTable.parseCols(line);
                    int rowCount = paraTable.getNumRows();

                    // Is this the chains attrs ParaTable?
         //           if (cols.containsAll(ChainParaTable.getExpectedCols()))
                    if (containsChainColumns(cols))
                    {
                        /*
                        // chainCount from #molecule must be the same as the chainCount identified in the #attr header for chain
                        if(mTotalChainCount != rowCount)
                        {
                            throw new StructureParseException("Count of Chains from #molecule section (" + mTotalChainCount
                                                              + ") does not equal the count of rows in the #attr line for the Chain section ("
                                                              + rowCount + ").");
                        }
                        */


                        if (null == mAllChainData)
                        {
                            mAllChainData = new ChainParaTable(paraTable);
                            // Delaying parsing until after it is created as a ChainParaTable
                            // so the that records will be ChainParaTableRecords.
                            mAllChainData.parseRecords(inReader);
                        }
                        else
                        {
                            paraTable.parseRecords(inReader);
                            for (MOE_ParaTableRecord record : paraTable.getRecords())
                            {
                                int id = Integer.parseInt(record.getField("ID").getValue().toString());
                                MOE_ParaTableRecord masterRecord = mAllChainData.getRecord(id);
                                if (masterRecord != null)
                                {
                                    for (MOE_ParaTableField field : record.getFields())
                                    {
                                        if (! masterRecord.getCols().contains(field.getCol()))
                                        {
                                            masterRecord.addField(field);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (cols.containsAll(ResidueParaTable.getExpectedCols()))
                    {
                        // residueCount from #molecule must be the same as the residueCount identified in the #attr header for chain
                        if (mTotalResidueCount != rowCount)
                        {
                            throw new StructureParseException("Count of Residues from #molecule section (" + mTotalResidueCount
                                                              + ") does not equal the count of rows in the #attr line for the Residue section ("
                                                              + rowCount + ").");
                        }

                        mAllResidueData = new ResidueParaTable(paraTable);
                        mAllResidueData.parseRecords(inReader);
                    }
                    else if (cols.containsAll(AtomParaTable.getExpectedCols()))
                    {
                        // atomCount from #molecule must be the same as the atomCount identified in the #attr header for chain
                        if (mTotalAtomCount != rowCount)
                        {
                            throw new StructureParseException("Count of Atoms from #molecule section (" + mTotalAtomCount
                                                              + ") does not equal the count of rows in the #attr line for the Atom section ("
                                                              + rowCount + ").");
                        }

                        mAllAtomData = new AtomParaTable(paraTable);
                        mAllAtomData.parseRecords(inReader);
                    }
                    else if (cols.contains(sBackboneCol))
                    {
                        // This ParaTable contains a list of the atom ids that make up the backbone
                        paraTable.parseRecords(inReader);
                        labelBackboneAtoms(paraTable);
                    }
                }
                else if (line.startsWith("#endmolecule"))
                {
                    atomicCoordinates.addModel(getModel(mAllChainData.getRecords(), mAllResidueData.getRecords()));

                    atomicCoordinates.setSeqRes(getPdbSeqress(mAllChainData.getRecords()));
                }
            }
        }
        catch (IOException ioe)
        {
            throw new StructureParseException("Error reading MOE file!",ioe);
        }

        return structure;
    }


    //------------------------------------------------------------------------------------------------------------------
    // SUB-METHODS of readAndParseMoeFile()

    //---------------------------------------------------------------------------
    private void labelBackboneAtoms(MOE_ParaTable<MOE_ParaTableRecord> inBackboneParaTable)
    {
        List<MOE_ParaTableRecord> records = inBackboneParaTable.getRecords();

        for (AtomParaTableRecord atomRecord : mAllAtomData.getRecords())
        {
            for (int i = 0; i < records.size(); i++)
            {
                MOE_ParaTableRecord record = records.get(i);
                int backboneAtomId = (int) record.getField(0).getValue();
                if (atomRecord.getId() == backboneAtomId)
                {
                    atomRecord.setIsBackbone(true);
                    records.remove(i);
                    break;
                }

                if (atomRecord.getId() < backboneAtomId)
                {
                    break;
                }
            }
        }
    }

    //---------------------------------------------------------------------------
    /**
     * get the counts of atoms, residues, and chains from the #molecule section
     * @param inReader
     * @throws StructureParseException
     */
    private void parseParaTableMolecule(BufferedReader inReader)
          throws StructureParseException
    {
        String line = "";
        try
        {
            line = inReader.readLine();
            String[] words = line.split(" ");

            int chainsIndex = Arrays.asList(words).indexOf("chains");
            int residuesIndex = Arrays.asList(words).indexOf("residues");
            int atomIndex = Arrays.asList(words).indexOf("atoms");

            mTotalChainCount = StructureUtils.stringToInteger(words[chainsIndex + 2]);
            mTotalResidueCount = StructureUtils.stringToInteger(words[residuesIndex + 2]);
            mTotalAtomCount = StructureUtils.stringToInteger(words[atomIndex + 2]);

        }
        catch (IOException ioe)
        {
            throw new StructureParseException("Error parsing #molecule line information: " + line, ioe);
        }
    }


    //##################################################################################################################
    // 2 of 2 PRIMARY, HIGH-LEVEL METHODS - see readRecord(...)
    //##################################################################################################################



    /**
     * create the PdbSeqress list (for PdbSectionPrimary) based on the mAllResidueData ResidueParaTable
     * @param chainList
     * @return
     */
    private List<PdbSeqres> getPdbSeqress(List<ChainParaTableRecord> chainList)
    {
        List<PdbSeqres> pdbSeqress = new ArrayList<>(mTotalChainCount);

        int residueIndex = 0;
        int currentChainIndex = 0;
        ChainParaTableRecord currentChain = chainList.get(currentChainIndex);

        List<String> seq = new ArrayList<>();

        for(ResidueParaTableRecord residueRecord : mAllResidueData.getRecords())
        {
            //append one-char amino acid to the sequence
            //seq.append(translateAminoAcid3to1(residueParaLine.getName()));
            seq.add(residueRecord.getName());

            residueIndex++;
            if(residueIndex == currentChain.getResidueCount())
            {
                //done building this seqres, put it together and add to list
                pdbSeqress.add(new PdbSeqres(StructureUtils.integerToString(chainList.get(currentChainIndex).getId()),
                                             currentChain.getName(),
                                             currentChain.getResidueCount(),
                                             StringUtil.join(seq, " ")).setDescription(currentChain.getDescription()));

                //setup for next chain, but only if there are still chains to do (if that was last chain, the loop will end)
                currentChainIndex++;
                if(currentChainIndex < mTotalChainCount)
                {
                    //seq.setLength(0);
                    seq.clear();
                    residueIndex = 0;
                    currentChain = chainList.get(currentChainIndex);
                }

            }

        }
        return pdbSeqress;
    }


    //--------------------------------------------------------------------------
    private void addAdditionalAttributesToChain(ChainParaTableRecord inChainInfo, Chain inChain)
    {
        for (MOE_ParaTableCol col : inChainInfo.getCols())
        {
            inChain.setAdditionalFormatAttribute(col.name(), inChainInfo.getField(col).getValue());
        }
    }

    //--------------------------------------------------------------------------
    private Model getModel(List<ChainParaTableRecord> inChainList, List<ResidueParaTableRecord> inResidueList)
    {
        Model model = new Model();

        int chainIndex = 0;

        ChainParaTableRecord chainInfo = inChainList.get(chainIndex);
        Chain currentChain = new Chain(chainInfo.getId() + "")
              .setName(chainInfo.getName());

        addAdditionalAttributesToChain(chainInfo, currentChain);

        model.addChain(currentChain);


        Range<Float> xRange = new Range<>();
        Range<Float> yRange = new Range<>();
        Range<Float> zRange = new Range<>();

        ResidueParaTableRecord residue = null;
        AtomGroup currentGroup = null;
        AtomGroupType atomGroupType;
        int atomCount = 0;
        int chainResidueCount = 0;
        int residueIndex = -1;
        int residueAtomCount;

        do
        {
            residueAtomCount = 0;
            residueIndex++;
            if (residueIndex == inResidueList.size())
            {
                break; //break if that was our last residue (aka last atom of last residue)
            }

            residue = inResidueList.get(residueIndex);

            //if this is the last residue in this chain, go to next
            if (chainResidueCount == chainInfo.getResidueCount())
            {
                chainIndex++;

                chainInfo = inChainList.get(chainIndex);
                currentChain = new Chain(chainInfo.getId() + "")
                      .setName(chainInfo.getName());
                addAdditionalAttributesToChain(chainInfo, currentChain);

                model.addChain(currentChain);

                chainResidueCount = 0;
            }

            if (residue.getAtomType().equals(AtomType.ATOM))
            {
                if (AminoAcid3D.valueOf(residue.getName()) != null)
                {
                    atomGroupType = AtomGroupType.AMINO_ACID;
                }
                else
                {
                    atomGroupType = AtomGroupType.NUCLEOTIDE;
                }
            }
            else
            {
                atomGroupType = AtomGroupType.HETERO_ATOM;
            }

            currentGroup = new AtomGroup(residue.getName(), atomGroupType).setResidueNum(residueIndex + 1);

            currentChain.addAtomGroup(currentGroup);
            chainResidueCount++;
        }
        while (0 == residue.getAtomCount());


        for(AtomParaTableRecord atomRecord : mAllAtomData.getRecords())
        {
            atomCount++;

            residueAtomCount++;

            updateRange(xRange, atomRecord.getPosX());
            updateRange(yRange, atomRecord.getPosY());
            updateRange(zRange, atomRecord.getPosZ());

            PDB_AtomName atomName = sAtomNameConversionMap.get(atomRecord.getName());
            if (null == atomName)
            {
                atomName = PDB_AtomName.getInstance(atomRecord.getName(), atomRecord.getElement());
            }

            currentGroup.addAtom(new AtomImpl(atomName)
                  .setElement(atomRecord.getElement())
                  .setGeometry(atomRecord.getGeometry())
                  .setCoords(atomRecord.getPosX(), atomRecord.getPosY(), atomRecord.getPosZ())
                  .setInsertCode(residue.getIns())
//                  .setSerialNum(atomParaLine.getId()));
                  .setSerialNum(atomCount)
                  .setIsBackbone(atomRecord.isBackbone()));
/*
            pdbAtoms.add(new PdbAtom(
                                    residue.getAtomType(),                          // group  ATOM or HETATM
                                    atomParaLine.getId(),                           // serial (id)
                                    atomParaLine.getName(),                         // atom name   - N, CA, CB, NH1, ...
                                    residue.getName(),                              // res name    - GLY, ARG, HIS, ...     - from RESIDUE
                                    StructureUtils.integerToString(chain.getId()),    // chain id    - 123, 124, ...          - from CHAIN
                                    chain.getName(),                                // chain name  - A, B, H, L, ...        - from CHAIN
                                    residue.getUid(),                               // residue sequence                     - from RESIDUE
                                    residue.getIns(),                               // iCode
                                    atomParaLine.getPosX(),                         // coordinates X
                                    atomParaLine.getPosY(),                         // coordinates Y
                                    atomParaLine.getPosZ(),                         // coordinates Z
                                    atomParaLine.getElement()                       // element   - N, C, O, ...
            ));
*/

            //if this is the last atom in this residue, go to next
            if(residueAtomCount == residue.getAtomCount())
            {
                do
                {
                    residueAtomCount = 0;
                    residueIndex++;
                    if (residueIndex == inResidueList.size())
                    {
                        break; //break if that was our last residue (aka last atom of last residue)
                    }

                    residue = inResidueList.get(residueIndex);

                    //if this is the last residue in this chain, go to next
                    if (chainResidueCount == chainInfo.getResidueCount())
                    {
                        chainIndex++;

                        chainInfo = inChainList.get(chainIndex);
                        currentChain = new Chain(chainInfo.getId() + "")
                              .setName(chainInfo.getName());
                        addAdditionalAttributesToChain(chainInfo, currentChain);

                        model.addChain(currentChain);

                        atomCount++; // The TER gets a serial #

                        chainResidueCount = 0;
                    }

                    if (residue.getAtomType().equals(AtomType.ATOM))
                    {
                        if (AminoAcid3D.valueOf(residue.getName()) != null)
                        {
                            atomGroupType = AtomGroupType.AMINO_ACID;
                        }
                        else
                        {
                            atomGroupType = AtomGroupType.NUCLEOTIDE;
                        }
                    }
                    else
                    {
                        atomGroupType = AtomGroupType.HETERO_ATOM;
                    }

                    currentGroup = new AtomGroup(residue.getName(), atomGroupType).setResidueNum(residueIndex + 1);

                    currentChain.addAtomGroup(currentGroup);
                    chainResidueCount++;
                }
                while (0 == residue.getAtomCount());
            }
        }
        
        model.setXRange(xRange);
        model.setYRange(yRange);
        model.setZRange(zRange);

        return model;
    }

    //##################################################################################################################
    // HELPER METHODS for READING LINES/WORDS of a FILE
    //##################################################################################################################

    //--------------------------------------------------------------------------
    private void updateRange(Range<Float> inRange, double inValue)
    {
        updateRange(inRange, (float) inValue);
    }

    //--------------------------------------------------------------------------
    private void updateRange(Range<Float> inRange, float inValue)
    {
        if (null == inRange.getStart()
            || inValue < inRange.getStart())
        {
            inRange.setStart(inValue);
        }

        if (null == inRange.getEnd()
            || inValue > inRange.getEnd())
        {
            inRange.setEnd(inValue);
        }
    }

    //##################################################################################################################
    // OVERRIDES
    //##################################################################################################################

    //---------------------------------------------------------------------------
    @Override
    public boolean isEndOfRecord(String inLine)
    {
        return (inLine.startsWith("#endsystem"));
    }

    //---------------------------------------------------------------------------
    @Override
    public boolean hasJanusDelimiter()
    {
        return false;
    }
}
