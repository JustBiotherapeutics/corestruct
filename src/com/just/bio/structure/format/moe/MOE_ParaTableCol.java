package com.just.bio.structure.format.moe;

/**
  * Container representing a column in a ParaTable.
  */
 //------------------------------------------------------------------------------------------------------
 // Copyright (c) Just Biotherapeutics, Inc.
 // All rights reserved.
 // See the LICENSE.txt file included with this library for license terms and conditions.
 //------------------------------------------------------------------------------------------------------

public class MOE_ParaTableCol implements Comparable
{
   private String mName;
   private MOE_ParaTableColFormat mType;
   private int mIndex;
   private Object mImpliedValue;

   //---------------------------------------------------------------------------
   public MOE_ParaTableCol(String inName, MOE_ParaTableColFormat inType, int inIndex)
   {
      mName = inName;
      mType = inType;
      mIndex = inIndex;
   }

   //---------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return name();
   }

   //---------------------------------------------------------------------------
   public String name()
   {
      return mName;
   }

   //---------------------------------------------------------------------------
   public MOE_ParaTableColFormat getType()
   {
      return mType;
   }

   //---------------------------------------------------------------------------
   public int getIndex()
   {
      return mIndex;
   }

   //---------------------------------------------------------------------------
   public boolean hasImpliedValue()
   {
      return mImpliedValue != null;
   }

   //---------------------------------------------------------------------------
   public Object getImpliedValue()
   {
      return mImpliedValue;
   }

   //---------------------------------------------------------------------------
   public void setImpliedValueFromString(String inValue)
   {
      mImpliedValue = getType().covertValueFromString(inValue);
   }

   //---------------------------------------------------------------------------
   @Override
   public int hashCode()
   {
      return mName.hashCode();
   }

   //---------------------------------------------------------------------------
   @Override
   public boolean equals(Object inObj2)
   {
      return 0 == compareTo(inObj2);
   }

   //---------------------------------------------------------------------------
   @Override
   public int compareTo(Object inObj2)
   {
      int result = -1;

      if (inObj2 != null
          && inObj2 instanceof MOE_ParaTableCol)
      {
         result = name().compareTo(((MOE_ParaTableCol) inObj2).name());
      }

      return result;
   }
}
