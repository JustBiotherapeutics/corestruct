package com.just.bio.structure.format.moe;


import com.hfg.chem.Element;
import com.just.bio.structure.OrbitalHybridizationGeometry;

/**
 * representation of a single Atom in a MOE file
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class AtomParaTableRecord extends MOE_ParaTableRecord
{
    private boolean mIsBackbone;

    //##########################################################################
    // CONSTRUCTORS
    //##########################################################################

    //--------------------------------------------------------------------------
    public AtomParaTableRecord()
    {

    }


    //##########################################################################
    // PUBLIC METHODS
    //##########################################################################


    //---------------------------------------------------------------------------
    public int getId()
    {
        return (Integer) getField(AtomParaTable.HEADER_ID).getValue();
    }

    //---------------------------------------------------------------------------
    public String getName()
    {
        return (String) getField(AtomParaTable.HEADER_NAME).getValue();
    }

    //--------------------------------------------------------------------------
    public Element getElement()
    {
        String stringValue = (String) getField(AtomParaTable.HEADER_ELEMENT).getValue();
        return Element.valueOf(stringValue);
    }

    //--------------------------------------------------------------------------
    public OrbitalHybridizationGeometry getGeometry()
    {
        String stringValue = (String) getField(AtomParaTable.HEADER_GEOMETRY).getValue();
        return OrbitalHybridizationGeometry.valueOf(stringValue);
    }


    //---------------------------------------------------------------------------
    public Double getPosX()
    {
        return (Double) getField(AtomParaTable.HEADER_POS_X).getValue();
    }

    //---------------------------------------------------------------------------
    public Double getPosY()
    {
        return (Double) getField(AtomParaTable.HEADER_POS_Y).getValue();
    }

    //---------------------------------------------------------------------------
    public Double getPosZ()
    {
        return (Double) getField(AtomParaTable.HEADER_POS_Z).getValue();
    }

    //---------------------------------------------------------------------------
    public void setIsBackbone(boolean inValue)
    {
        mIsBackbone = true;
    }

    //---------------------------------------------------------------------------
    public boolean isBackbone()
    {
        return mIsBackbone;
    }

}
