package com.just.bio.structure.format.moe;

import java.util.Collection;
import java.util.Map;

import com.hfg.util.collection.OrderedMap;

/**
 * Enumeration of MOE's format types for ParaTable columns.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class MOE_ParaTableColFormat
{
   private static Map<String, MOE_ParaTableColFormat> sValueMap = new OrderedMap<>(15);

   public static final MOE_ParaTableColFormat type_integer = new MOE_ParaTableColFormat("i");
   public static final MOE_ParaTableColFormat type_hexInteger = new MOE_ParaTableColFormat("ix");
   public static final MOE_ParaTableColFormat type_real = new MOE_ParaTableColFormat("r");
   public static final MOE_ParaTableColFormat type_char = new MOE_ParaTableColFormat("c");
   public static final MOE_ParaTableColFormat type_string = new MOE_ParaTableColFormat("ss");
   public static final MOE_ParaTableColFormat type_token = new MOE_ParaTableColFormat("tt");

   public static final MOE_ParaTableColFormat type_integerVector = new MOE_ParaTableColFormat("i*");
   public static final MOE_ParaTableColFormat type_hexIntegerVector = new MOE_ParaTableColFormat("ix*");
   public static final MOE_ParaTableColFormat type_realVector = new MOE_ParaTableColFormat("r*");
   public static final MOE_ParaTableColFormat type_longToken = new MOE_ParaTableColFormat("t");
   public static final MOE_ParaTableColFormat type_tokenVector = new MOE_ParaTableColFormat("t*");
   public static final MOE_ParaTableColFormat type_longCharVector = new MOE_ParaTableColFormat("s");
   public static final MOE_ParaTableColFormat type_stringVector = new MOE_ParaTableColFormat("s*");
   public static final MOE_ParaTableColFormat type_variableFormat = new MOE_ParaTableColFormat("*");
   // TODO: The behavior for implied format needs to be fleshed out
   public static final MOE_ParaTableColFormat type_impliedFormat = new MOE_ParaTableColFormat(null);

   private String mAbbrev;

   //---------------------------------------------------------------------------
   private MOE_ParaTableColFormat(String inAbbrev)
   {
      mAbbrev = inAbbrev;
      sValueMap.put(inAbbrev, this);
   }

   //---------------------------------------------------------------------------
   public String getAbbrev()
   {
      return mAbbrev;
   }

   //---------------------------------------------------------------------------
   public Object covertValueFromString(String inStringValue)
   {
      Object objValue = inStringValue;

      if (this.equals(type_integer))
      {
         objValue = Integer.parseInt(inStringValue);
      }
      else if (this.equals(type_hexInteger))
      {
         objValue = Integer.parseInt(inStringValue, 16);
      }
      else if (this.equals(type_real))
      {
         objValue = Double.parseDouble(inStringValue);
      }
      else if (this.equals(type_char))
      {
         objValue = inStringValue.charAt(0);
      }
      else if (this.equals(type_string)
               || this.equals(type_token)
               || this.equals(type_longToken))
      {
         objValue = MOE_IO_Util.escapeString(inStringValue);
      }

      return objValue;
   }


   //---------------------------------------------------------------------------
   public static Collection<MOE_ParaTableColFormat> values()
   {
      return sValueMap.values();
   }

   //---------------------------------------------------------------------------
   public static MOE_ParaTableColFormat valueOf(String inAbbrev)
   {
      MOE_ParaTableColFormat result = null;

      if (inAbbrev.indexOf("=") > 0)
      {
         result = type_impliedFormat;
         // TODO:
      }
      else
      {
         for (MOE_ParaTableColFormat value : values())
         {
            if (value.getAbbrev() != null
                && value.getAbbrev().equalsIgnoreCase(inAbbrev))
            {
               result = value;
               break;
            }
         }
      }

      return result;
   }
}
