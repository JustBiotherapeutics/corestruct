package com.just.bio.structure.format.moe;

import com.just.bio.structure.enums.AtomType;

/**
 * representation of a single Residue in a MOE file
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class ResidueParaTableRecord extends MOE_ParaTableRecord
{
    //##################################################################################################################
    // CONSTRUCTORS

    //---------------------------------------------------------------------------
    public ResidueParaTableRecord()
    {
    }


    //##################################################################################################################
    // getters / setters

    //---------------------------------------------------------------------------
    public int getId()
    {
        return (Integer) getField(ResidueParaTable.HEADER_ID).getValue();
    }

    //---------------------------------------------------------------------------
    public int getAtomCount()
    {
        return (Integer) getField(ResidueParaTable.HEADER_ATOM_COUNT).getValue();
    }

    //---------------------------------------------------------------------------
    public String getName()
    {
        return (String) getField(ResidueParaTable.HEADER_NAME).getValue();
    }

    //---------------------------------------------------------------------------
    public String getUID()
    {
        return (String) getField(ResidueParaTable.HEADER_UID).getValue();
    }

    //---------------------------------------------------------------------------
    public Character getIns()
    {
        Character ins = (Character) getField(ResidueParaTable.HEADER_INS).getValue();

        return (ins != null && ins.equals('!') ? null : ins);
    }

    //---------------------------------------------------------------------------
    public int getPos()
    {
        return (Integer) getField(ResidueParaTable.HEADER_POS).getValue();
    }

    //---------------------------------------------------------------------------
    public AtomType getAtomType()
    {
        String stringValue = (String) getField(ResidueParaTable.HEADER_TYPE).getValue();

        // we'll assume that 'none' == HETATM,  and 'amino' == ATOM (actually everything that isn't 'none' == ATOM)

        AtomType atomType = AtomType.ATOM;
        if (stringValue.equalsIgnoreCase("none"))
        {
            atomType = AtomType.HETATM;
        }

        return atomType;
    }
}
