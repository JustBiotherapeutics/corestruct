package com.just.bio.structure.format.moe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * representation of the ParaTable related to residues in a MOE file
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class ResidueParaTable extends MOE_ParaTable<ResidueParaTableRecord>
{
    //##################################################################################################################
    // public static final stuff

    public static final String HEADER_ID = "ID";
    public static final String HEADER_ATOM_COUNT = "rAtomCount";
    public static final String HEADER_NAME = "rName";
    public static final String HEADER_UID = "rUID";
    public static final String HEADER_INS = "rINS";
    public static final String HEADER_POS = "rPos";
    public static final String HEADER_TYPE = "rType";

    private static List<MOE_ParaTableCol> sCols;
    static
    {
        sCols = new ArrayList<>(7);
        sCols.add(new MOE_ParaTableCol(HEADER_ID, MOE_ParaTableColFormat.type_integer, 0));
        sCols.add(new MOE_ParaTableCol(HEADER_ATOM_COUNT, MOE_ParaTableColFormat.type_integer, 1));
        sCols.add(new MOE_ParaTableCol(HEADER_NAME, MOE_ParaTableColFormat.type_longToken, 2));
        sCols.add(new MOE_ParaTableCol(HEADER_UID, MOE_ParaTableColFormat.type_integer, 3));
        sCols.add(new MOE_ParaTableCol(HEADER_INS, MOE_ParaTableColFormat.type_char, 4));
        sCols.add(new MOE_ParaTableCol(HEADER_POS, MOE_ParaTableColFormat.type_integer, 5));
        sCols.add(new MOE_ParaTableCol(HEADER_TYPE, MOE_ParaTableColFormat.type_longToken, 6));
    }

    //##################################################################################################################
    // CONSTRUCTORS

    //---------------------------------------------------------------------------
    public ResidueParaTable(MOE_ParaTable inParaTable)
    {
        super(inParaTable);
    }

    //##################################################################################################################
    // PUBLIC FUNCTIONS AND METHODS

    //---------------------------------------------------------------------------
    public static List<MOE_ParaTableCol> getExpectedCols()
    {
        return Collections.unmodifiableList(sCols);
    }

    //---------------------------------------------------------------------------
    public ResidueParaTableRecord allocateRecord()
    {
       return new ResidueParaTableRecord();
    }
}
