package com.just.bio.structure.format.moe;

import java.awt.*;

import com.hfg.html.attribute.HTMLColor;

/**
 * representation of a single chain in a MOE file
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class ChainParaTableRecord extends MOE_ParaTableRecord
{
    //##################################################################################################################
    // constructors

    public ChainParaTableRecord()
    {

    }

    //##################################################################################################################
    // getters / setters

    //---------------------------------------------------------------------------
    public int getId()
    {
        return (Integer) getField(ChainParaTable.HEADER_ID).getValue();
    }

    //---------------------------------------------------------------------------
    public int getResidueCount()
    {
        return (Integer) getField(ChainParaTable.HEADER_RESIDUE_COUNT).getValue();
    }

    //---------------------------------------------------------------------------
    public String getName()
    {
        return (String) getField(ChainParaTable.HEADER_NAME).getValue();
    }

    //---------------------------------------------------------------------------
    public String getDescription()
    {
        return (String) getField(ChainParaTable.HEADER_HEADER).getValue();
    }

    //---------------------------------------------------------------------------
    public String getTag()
    {
        return (String) getField(ChainParaTable.HEADER_TAG).getValue();
    }

    //---------------------------------------------------------------------------
    public String getColorBy()
    {
        return (String) getField(ChainParaTable.HEADER_COLOR_BY).getValue();
    }

    //---------------------------------------------------------------------------
    public Color getRGB()
    {
        Color color = null;
        Integer intValue = (Integer) getField(ChainParaTable.HEADER_RGB).getValue();
        if (intValue != null)
        {
            color = HTMLColor.valueOf(Integer.toHexString(intValue));
        }

        return color;
    }









}
