package com.just.bio.structure.format.moe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * representation of the ParaTable related to atoms in a MOE file
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class AtomParaTable extends MOE_ParaTable<AtomParaTableRecord>
{

    //##################################################################################################################
    // public static final stuff

    public static final String HEADER_ID = "ID";
    public static final String HEADER_NAME = "aName";
    public static final String HEADER_ELEMENT = "aElement";
    public static final String HEADER_GEOMETRY = "aGeometry";
    public static final String HEADER_POS_X = "aPosX";
    public static final String HEADER_POS_Y = "aPosY";
    public static final String HEADER_POS_Z = "aPosZ";

    private static List<MOE_ParaTableCol> sCols;
    static {
        sCols = new ArrayList<>(7);
        sCols.add(new MOE_ParaTableCol(HEADER_ID, MOE_ParaTableColFormat.type_integer, 0));
        sCols.add(new MOE_ParaTableCol(HEADER_NAME, MOE_ParaTableColFormat.type_longToken, 1));
        sCols.add(new MOE_ParaTableCol(HEADER_ELEMENT, MOE_ParaTableColFormat.type_longToken, 2));
        sCols.add(new MOE_ParaTableCol(HEADER_GEOMETRY, MOE_ParaTableColFormat.type_longToken, 3));
        sCols.add(new MOE_ParaTableCol(HEADER_POS_X, MOE_ParaTableColFormat.type_real, 4));
        sCols.add(new MOE_ParaTableCol(HEADER_POS_Y, MOE_ParaTableColFormat.type_real, 5));
        sCols.add(new MOE_ParaTableCol(HEADER_POS_Z, MOE_ParaTableColFormat.type_real, 6));
    }

    //##########################################################################
    // CONSTRUCTORS
    //##########################################################################

    //---------------------------------------------------------------------------
    public AtomParaTable(MOE_ParaTable inParaTable)
    {
        super(inParaTable);
    }

    //##########################################################################
    // PUBLIC METHODS
    //##########################################################################

    //---------------------------------------------------------------------------
    public static List<MOE_ParaTableCol> getExpectedCols()
    {
        return Collections.unmodifiableList(sCols);
    }

    //---------------------------------------------------------------------------
    public AtomParaTableRecord allocateRecord()
    {
       return new AtomParaTableRecord();
    }

}
