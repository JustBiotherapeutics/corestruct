package com.just.bio.structure.format.moe;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.hfg.util.io.RuntimeIOException;

/**
 * IO utilities for parsing MOE files.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class MOE_IO_Util
{
   private static LinkedList bucket = new LinkedList<>();

   private static final Set<Character> PREFIXES = new HashSet<>(4);
   private static final Character[] PREFIX_ARRAY = new Character[] {'$', '%', '&', '~'};
   // ENCODE = [A-Za-z0-9+/]
   private static final Character[] ENCODE_ARRAY = new Character[64];
   static
   {
       PREFIXES.add('$');
       PREFIXES.add('%');
       PREFIXES.add('&');
       PREFIXES.add('~');

       int index = 0;
       for (int i = 'A'; i <= 'Z'; i++)
       {
           ENCODE_ARRAY[index++] = (char) i;
       }

       for (int i = 'a'; i <= 'z'; i++)
       {
           ENCODE_ARRAY[index++] = (char) i;
       }

       for (int i = '0'; i <= '9'; i++)
       {
           ENCODE_ARRAY[index++] = (char) i;
       }

       ENCODE_ARRAY[index++] = '+';
       ENCODE_ARRAY[index++] = '/';
   }

   private static Map<String, Character> sDecodeMap;


   //---------------------------------------------------------------------------
   /*
   ENCODE = [A-Za-z0-9+/]   PREFIX = [$%&~]   RAW = [A-Za-z0-9 ()+-*.,/:;=?[_]{|}^]

   The following algorithm is used to decode strings that have been encoded by the previous scheme:

     1. Remove all characters not in the union of the RAW, ENCODE, PREFIX and {"!"} sets of characters.
     2. Replace all two character combinations AB in which A is in the PREFIX set with either a) the corresponding
        ASCII character if B is in the ENCODE set, or b) the character "A" if B is not in the ENCODE set.
     3. If the resulting string is exactly "$" (octal 044) then return the empty string.
     4. Replace any "!" (octal 041) characters with blank (octal 040) and return the resulting string.
    */
   public static String escapeString(String inValue)
   {
       StringBuilderPlus value = new StringBuilderPlus("");

       if (inValue != null
           && ! inValue.equals("$"))
       {
           // Replace any "!" (octal 041) characters with blank (octal 040)
           value = new StringBuilderPlus(StringUtil.replaceAll(inValue, "!", " "));

           for (int i = 0; i < value.length() - 1; i++)
           {
               if (PREFIXES.contains(value.charAt(i)))
               {
                   value.replace(i, i + 2, decode(value.substring(i, i + 2)) + "");
               }
           }

       }

       return value.toString();
   }

   //---------------------------------------------------------------------------
   /**
    * get the next N words from the file.
    * @param inReader content stream
    * @param inReader the source of data
    * @param inRecord the record to return fields in
    * @return boolean whether a complete record was parsed
    * @throws IOException if an error occurs while processing content
    */
   public static boolean getNextWords(BufferedReader inReader, String[] inRecord)
         throws IOException
   {
      boolean result = true;

      for (int i = 0; i < inRecord.length; i++)
      {
         String nextWord = getWordFromBucket(inReader);
         if (! StringUtil.isSet(nextWord))
         {
            result = false;
         }
         else if (nextWord.equals("~"))
         {
            // If the first word is tilde (~) then the next word is taken to be a word count, k (in i format).
            // The next k words are concatenated.
            int wordCount = Integer.parseInt(getWordFromBucket(inReader));
            StringBuilderPlus buffer = new StringBuilderPlus();
            for (int tokenCount = 0; tokenCount < wordCount; tokenCount++)
            {
               buffer.append(getWordFromBucket(inReader));
            }

            nextWord = buffer.toString();
         }

          inRecord[i] = nextWord;
      }

      return result;
   }

   /**
    * get a single word from the bucket.. if the bucket is empty, refill it with the next line from the file
    * @param inReader
    * @return
    * @throws IOException
    */
   private static String getWordFromBucket(BufferedReader inReader)
         throws IOException
   {
      if(!CollectionUtil.hasValues(bucket))
      {
         refillBucket(inReader);
      }
      return (String) bucket.poll(); //note: poll will return null if bucket is empty
   }


   //---------------------------------------------------------------------------
   /**
    * refill bucket one line at at time
    * @param inReader
    * @throws IOException
    */
   private static void refillBucket(BufferedReader inReader)
         throws IOException
   {
      inReader.mark(5000);
      String line = inReader.readLine();
      if(line.startsWith("#"))
      {
         inReader.reset();
      }
      else
      {
         String[] words = line.split(" ");
         bucket = new LinkedList<>();
         for(String word : words)
         {
            bucket.add(word);
         }
      }
   }


   //---------------------------------------------------------------------------
   private static char decode(String inToken)
   {
       if (null == sDecodeMap)
       {
           Map<String, Character> map = new HashMap<>();
           Pattern rawPattern = Pattern.compile("[A-Za-z0-9 ()+\\-*.,/:;=?[_]{|}^]");
           for (int i = 1; i < 256; i++)
           {
               // Every character k not in the RAW set is replaced by the two character combination PREFIX(i) ENCODE(j)
               // where i = 1 + (k >> 6) and j = 1 + (k & 0x3F).
               char theChar = (char) i;
               if (! rawPattern.matcher(theChar + "").matches())
               {
                   int prefixIdx = (i >> 6);
                   int encodeIdx = (i & 0x3F);
                   String token = "" + PREFIX_ARRAY[prefixIdx] + ENCODE_ARRAY[encodeIdx];
                   map.put(token, theChar);
               }
           }

           sDecodeMap = map;
       }

       return sDecodeMap.get(inToken);
   }


}
