package com.just.bio.structure.format.moe;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.hfg.util.StringUtil;

import com.just.bio.structure.exceptions.StructureParseException;
import com.just.bio.structure.format.StructureIOException;
import com.just.bio.structure.pdb.StructureUtils;

/**
 * Container for ParaTable data.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class MOE_ParaTable<T extends MOE_ParaTableRecord>
{
   private List<MOE_ParaTableCol> mCols;
   private Integer mRowCount;
   private List<T> mRecords;

   //---------------------------------------------------------------------------
   public MOE_ParaTable()
   {

   }

   //---------------------------------------------------------------------------
   public MOE_ParaTable(MOE_ParaTable inParaTable)
   {
      mCols = inParaTable.getCols();
      mRowCount = inParaTable.getNumRows();
      setRecords(inParaTable.getRecords());
   }

   //---------------------------------------------------------------------------
   public List<MOE_ParaTableCol> parseCols(String inLine)
         throws IOException
   {
      String[] fields = inLine.split("\\s+");
      mRowCount = StructureUtils.stringToInteger(fields[1]);

      mCols = parseParaTableColumnDefs(fields);

      return mCols;
   }

   //---------------------------------------------------------------------------
   protected void setNumRows(Integer inValue)
   {
      mRowCount = inValue;
   }

   //---------------------------------------------------------------------------
   public Integer getNumRows()
   {
      return mRowCount;
   }

   //---------------------------------------------------------------------------
   public List<MOE_ParaTableCol> getCols()
   {
      return mCols;
   }

   //---------------------------------------------------------------------------
   public T allocateRecord()
   {
      return (T) new MOE_ParaTableRecord();
   }

   //---------------------------------------------------------------------------
   public List<T> parseRecords(BufferedReader inReader)
         throws IOException
   {
      mRecords = new ArrayList<>(mRowCount);

      int numNonImpliedCols = 0;
      for (MOE_ParaTableCol col : mCols)
      {
         if (! col.hasImpliedValue())
         {
            numNonImpliedCols++;
         }
      }

      String[] wordsForRecord = new String[numNonImpliedCols];
      // Get the next N words from the file (where N = number of fields in each row)
      while (MOE_IO_Util.getNextWords(inReader, wordsForRecord))
      {
         try
         {
            T record = allocateRecord();

            for (MOE_ParaTableCol col : mCols)
            {
               MOE_ParaTableField field = new MOE_ParaTableField(col);
               if (! col.hasImpliedValue())
               {
                  String word = wordsForRecord[col.getIndex()];
                  field.setValueFromString(word);
               }

               record.addField(field);
            }

            mRecords.add(record);
         }
         catch (Exception e)
         {
            throw new StructureParseException("Error parsing #attr row information: " + StringUtil.join(wordsForRecord, " "), e);
         }
      }

      return mRecords;
   }

   //---------------------------------------------------------------------------
   public void setRecords(List<T> inValue)
   {
      mRecords = inValue;
   }

   //---------------------------------------------------------------------------
   public List<T> getRecords()
   {
      return mRecords;
   }

   //---------------------------------------------------------------------------
   public T getRecord(int inRecordId)
   {
      T requestedRecord = null;

      for (T record : getRecords())
      {
         if (record.getID() == inRecordId)
         {
            requestedRecord = record;
            break;
         }
      }

      return requestedRecord;
   }

   //---------------------------------------------------------------------------
   private List<MOE_ParaTableCol> parseParaTableColumnDefs(String[] inFields)
   {
       List<MOE_ParaTableCol> cols = new ArrayList<>(inFields.length / 2);

       // Skip the first '#attr' token and the second row count token
       for (int i = 2; i < inFields.length; i += 2)
       {
          MOE_ParaTableCol col;

           String formatString = inFields[i + 1];
           if (formatString.indexOf("=") > 0)
           {
              // Implied format
              String[] pieces  = formatString.split("=");
              formatString = pieces[0];
              String impliedValueString = pieces[1];
              col = new MOE_ParaTableCol(inFields[i], MOE_ParaTableColFormat.valueOf(formatString), cols.size());
              col.setImpliedValueFromString(impliedValueString);
           }
           else
           {
              col = new MOE_ParaTableCol(inFields[i], MOE_ParaTableColFormat.valueOf(formatString), cols.size());
           }

          cols.add(col);
       }

       return cols;
   }

}
