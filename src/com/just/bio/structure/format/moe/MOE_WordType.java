package com.just.bio.structure.format.moe;


import java.util.Collection;
import java.util.Map;

import com.hfg.util.collection.OrderedMap;

/**
 * Enumeration of MOE's word types.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class MOE_WordType
{
   private static Map<String, MOE_WordType> sValueMap = new OrderedMap<>(6);

   public static final MOE_WordType type_integer = new MOE_WordType("i");
   public static final MOE_WordType type_hexInteger = new MOE_WordType("ix");
   public static final MOE_WordType type_real = new MOE_WordType("r");
   public static final MOE_WordType type_char = new MOE_WordType("c");
   public static final MOE_WordType type_string = new MOE_WordType("ss");
   public static final MOE_WordType type_token = new MOE_WordType("tt");

   private String mAbbrev;

   //---------------------------------------------------------------------------
   private MOE_WordType(String inAbbrev)
   {
      mAbbrev = inAbbrev;
      sValueMap.put(inAbbrev, this);
   }

   //---------------------------------------------------------------------------
   public String getAbbrev()
   {
      return mAbbrev;
   }

   //---------------------------------------------------------------------------
   public static Collection<MOE_WordType> values()
   {
      return sValueMap.values();
   }

   //---------------------------------------------------------------------------
   public static MOE_WordType valueOf(String inAbbrev)
   {
      MOE_WordType result = null;
      for (MOE_WordType value : values())
      {
         if (value.getAbbrev().equalsIgnoreCase(inAbbrev))
         {
            result = value;
            break;
         }
      }

      return result;
   }
}
