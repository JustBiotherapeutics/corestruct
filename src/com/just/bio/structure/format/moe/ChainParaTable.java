package com.just.bio.structure.format.moe;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.format.StructureIOException;
import com.just.bio.structure.pdb.StructureUtils;

import java.util.*;

/**
 * representation of the ParaTable related to chains in a MOE file
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class ChainParaTable extends MOE_ParaTable<ChainParaTableRecord>
{
    //##################################################################################################################
    // public static final stuff

    //string 'header' names associated to each field that we want to get
    public static final String HEADER_ID = "ID";
    public static final String HEADER_RESIDUE_COUNT = "cResidueCount";
    public static final String HEADER_NAME = "cName";
    public static final String HEADER_HEADER = "cHeader";
    public static final String HEADER_TAG = "cTag";
    public static final String HEADER_COLOR_BY = "cColorBy";
    public static final String HEADER_RGB = "cRGB";

    private static List<MOE_ParaTableCol> sCols;
    static
    {
        sCols = new ArrayList<>(7);
        sCols.add(new MOE_ParaTableCol(HEADER_ID,            MOE_ParaTableColFormat.type_integer,    0));
        sCols.add(new MOE_ParaTableCol(HEADER_RESIDUE_COUNT, MOE_ParaTableColFormat.type_integer,    1));
        sCols.add(new MOE_ParaTableCol(HEADER_NAME,          MOE_ParaTableColFormat.type_longToken,  2));
        sCols.add(new MOE_ParaTableCol(HEADER_HEADER,        MOE_ParaTableColFormat.type_longToken,  3));
        sCols.add(new MOE_ParaTableCol(HEADER_TAG,           MOE_ParaTableColFormat.type_longToken,  4));
        sCols.add(new MOE_ParaTableCol(HEADER_COLOR_BY,      MOE_ParaTableColFormat.type_longToken,  5));
        sCols.add(new MOE_ParaTableCol(HEADER_RGB,           MOE_ParaTableColFormat.type_hexInteger, 6));
    }


    //##################################################################################################################
    // CONSTRUCTORS

    //---------------------------------------------------------------------------
    public ChainParaTable(MOE_ParaTable inParaTable)
    {
        super(inParaTable);
    }

    //##################################################################################################################
    // PUBLIC METHODS

    //---------------------------------------------------------------------------
    public static List<MOE_ParaTableCol> getExpectedCols()
    {
        return Collections.unmodifiableList(sCols);
    }

    //---------------------------------------------------------------------------
    public ChainParaTableRecord allocateRecord()
    {
       return new ChainParaTableRecord();
    }

}
