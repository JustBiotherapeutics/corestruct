package com.just.bio.structure.format.moe;

import java.util.ArrayList;
import java.util.List;

/**
 * Record (row) from a ParaTable.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class MOE_ParaTableRecord
{
   private List<MOE_ParaTableField> mFields;
   private Integer mID;

   //--------------------------------------------------------------------------
   public Integer getID()
   {
      return mID;
   }


   //--------------------------------------------------------------------------
   public List<MOE_ParaTableCol> getCols()
   {
      List<MOE_ParaTableCol> cols = new ArrayList<>(size());
      for (MOE_ParaTableField field : mFields)
      {
         cols.add(field.getCol());
      }

      return cols;
   }

   //--------------------------------------------------------------------------
   public void addField(MOE_ParaTableField inValue)
   {
      if (null == mFields)
      {
         mFields = new ArrayList<>(10);
      }

      mFields.add(inValue);

      if (inValue.getCol().name().equalsIgnoreCase("ID"))
      {
         mID = Integer.parseInt(inValue.getValue().toString());
      }
   }

   //--------------------------------------------------------------------------
   public List<MOE_ParaTableField> getFields()
   {
      return mFields;
   }

   //--------------------------------------------------------------------------
   public MOE_ParaTableField getField(int inFieldIndex)
   {
      return mFields.get(inFieldIndex);
   }

   //--------------------------------------------------------------------------
   public MOE_ParaTableField getField(String inFieldName)
   {
      MOE_ParaTableField requestedField = null;
      for (MOE_ParaTableField field : mFields)
      {
         if (field.getCol().name().equals(inFieldName))
         {
            requestedField = field;
            break;
         }
      }

      return requestedField;
   }

   //--------------------------------------------------------------------------
   public MOE_ParaTableField getField(MOE_ParaTableCol inColumn)
   {
      MOE_ParaTableField requestedField = null;
      for (MOE_ParaTableField field : mFields)
      {
         if (field.getCol().equals(inColumn))
         {
            requestedField = field;
            break;
         }
      }

      return requestedField;
   }

   //--------------------------------------------------------------------------
   public int size()
   {
      return mFields.size();
   }
}
