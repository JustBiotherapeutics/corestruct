package com.just.bio.structure.format.moe;

/**
 * Field from a ParaTable record.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class MOE_ParaTableField
{
   private MOE_ParaTableCol mCol;

   private Object mValue;

   //--------------------------------------------------------------------------
   public MOE_ParaTableField(MOE_ParaTableCol inCol)
   {
      mCol = inCol;

      if (mCol.getImpliedValue() != null)
      {
         mValue = mCol.getImpliedValue();
      }
   }

   //--------------------------------------------------------------------------
   public MOE_ParaTableField(MOE_ParaTableCol inCol, Object inValue)
   {
      this(inCol);
      mValue = inValue;
   }

   //--------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return mCol.name() + ": " + getValue();
   }

   //--------------------------------------------------------------------------
   public MOE_ParaTableCol getCol()
   {
      return mCol;
   }

   //--------------------------------------------------------------------------
   public void setValueFromString(String inStringValue)
   {
      mValue = mCol.getType().covertValueFromString(inStringValue);
   }

   //--------------------------------------------------------------------------
   public void setValue(Object inValue)
   {
      mValue = inValue;
   }

   //--------------------------------------------------------------------------
   public Object getValue()
   {
      return mValue;
   }
}
