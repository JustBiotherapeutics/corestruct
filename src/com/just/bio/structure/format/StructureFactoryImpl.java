package com.just.bio.structure.format;


import com.just.bio.structure.StructureImpl;

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class StructureFactoryImpl implements StructureFactory<StructureImpl>
{
    //---------------------------------------------------------------------------
    public StructureImpl createStructureObj() {
        return new StructureImpl();
    }

}
