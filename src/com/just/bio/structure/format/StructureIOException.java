package com.just.bio.structure.format;

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class StructureIOException extends RuntimeException
{
   //---------------------------------------------------------------------------
   public StructureIOException(String inMessage)
   {
      super(inMessage);
   }

   //---------------------------------------------------------------------------
   public StructureIOException(String inMessage, Throwable inException)
   {
      super(inMessage, inException);
   }

   //---------------------------------------------------------------------------
   public StructureIOException(Throwable inException)
   {
      super(inException);
   }

}