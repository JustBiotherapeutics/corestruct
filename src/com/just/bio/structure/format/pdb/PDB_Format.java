package com.just.bio.structure.format.pdb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

import com.hfg.chem.Element;
import com.hfg.util.Case;
import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;

import com.just.bio.structure.*;
import com.just.bio.structure.Chain;
import com.just.bio.structure.enums.AtomType;
import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.format.ReadableStructureFormat;
import com.just.bio.structure.format.ReadableStructureFormatBase;
import com.just.bio.structure.format.StructureFactory;
import com.just.bio.structure.format.StructureIOException;
import com.just.bio.structure.pdb.*;
import com.just.bio.structure.pdb.pojo.*;


/**
 * Structure format object for PDB-formatted files.
 * @param <T> The Structure class that will result from parsing
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class PDB_Format<T extends Structure> extends ReadableStructureFormatBase<T>
{

   //Metadata section Record Name headers (first 6 characters of every line)
   public static String PDB_KEY_HEADER   = "HEADER";
   public static String PDB_KEY_OBSLTE   = "OBSLTE";
   public static String PDB_KEY_TITLE    = "TITLE";
   public static String PDB_KEY_SPLIT    = "SPLIT";
   public static String PDB_KEY_CAVEAT   = "CAVEAT";
   public static String PDB_KEY_COMPND   = "COMPND";
   public static String PDB_KEY_SOURCE   = "SOURCE";
   public static String PDB_KEY_KEYWDS   = "KEYWDS";
   public static String PDB_KEY_EXPDTA   = "EXPDTA";
   public static String PDB_KEY_NUMMDL   = "NUMMDL";
   public static String PDB_KEY_MDLTYP   = "MDLTYP";
   public static String PDB_KEY_AUTHOR   = "AUTHOR";
   public static String PDB_KEY_REVDAT   = "REVDAT";
   public static String PDB_KEY_SPRSDE   = "SPRSDE";
   public static String PDB_KEY_JRNL     = "JRNL";

   //Remark section Record Name headers
   public static String PDB_KEY_REMARK   = "REMARK";

   //Connectivity section Record Name headers
   public static String PDB_KEY_SSBOND   = "SSBOND";

   //Primary section Record Name headers
   public static String PDB_KEY_SEQRES   = "SEQRES";

   //Coordinate section Record Name headers
   public static String PDB_KEY_ATOM     = "ATOM";
   public static String PDB_KEY_HETATM   = "HETATM";
   public static String PDB_KEY_TER      = "TER";
   public static String PDB_KEY_MODEL    = "MODEL"; //MODEL and ENDMDL are optional and only identified if there are more than one models in the file
   public static String PDB_KEY_ENDMDL   = "ENDMDL";



   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   //---------------------------------------------------------------------------
   public PDB_Format(StructureFactory<T> inStructureFactory)
   {
      super(inStructureFactory);
   }


   //###########################################################################
   // PUBLIC METHODS
   //###########################################################################

   //---------------------------------------------------------------------------
   public StructureFormatType getStructureFormatType()
   {
      return StructureFormatType.PDB;
   }

   //---------------------------------------------------------------------------
   @Override
   public T readRecord(BufferedReader inReader)
         throws StructureIOException
   {
      if (null == getStructureFactory())
      {
         throw new StructureIOException("No Structure factory has been specified!");
      }

      //the entire contents of the PDB file as a [LinkedHash]Map (key=record name header; value=related lines)
      Map<String, LinkedList<String>> pdbMap = new LinkedHashMap<>();

      //additionally the ATOM and HETATM records will be combined together into it's own list
      //note, sometimes (rarely) there is more than one model, if so these will be border by MODE/ENDMDL keywords
      List<PdbLine> atomLines = new ArrayList<>();
      List<List<PdbLine>> atomModels = new ArrayList<>();


      T structure;
      try {
         structure = getStructureFactory().createStructureObj();

         String line;
         while ((line = inReader.readLine()) != null)
         {
            String entryKey = line.substring(0, Math.min(6, line.length())).trim();
            if (pdbMap.containsKey(entryKey))
            {
               LinkedList foundEntry = pdbMap.get(entryKey);
               foundEntry.add(line); //don't trim; we want character locations when parsing the individual lines
            }
            else
            {
               LinkedList<String> newEntry = new LinkedList<>();
               newEntry.add(line);
               pdbMap.put(entryKey, newEntry);

            }

            //ATOM and HETATM records should be in the same list for processing together. in addition to the map above, we'll also add them to this
            //we also need the TER lines that identify when a chain is 'terminated' (even if that chain id repeats again)
            if(entryKey.equalsIgnoreCase(PDB_KEY_ATOM) ||
                  entryKey.equalsIgnoreCase(PDB_KEY_HETATM) ||
                  entryKey.equalsIgnoreCase(PDB_KEY_TER))
            {
               atomLines.add(new PdbLine(entryKey, line));
            }

            //
            if(entryKey.equalsIgnoreCase(PDB_KEY_ENDMDL))
            {
               atomModels.add(atomLines);
               atomLines = new ArrayList<>();
            }

         }

         if(CollectionUtil.hasValues(atomLines))
         {
            atomModels.add(atomLines);
         }

         //now that the entire file is parsed, pass the map to our functions that create each section of the structure pojo
         structure.setMetadata(parseSectionMetadata(pdbMap));
         structure.setRemark(parseSectionRemark(pdbMap));
         structure.setPrimary(parseSectionPrimary(pdbMap));
         structure.setConnectivity(parseSectionConnectivity(pdbMap));

         AtomicCoordinates atomicCoordinates = parseSectionCoordinate(atomModels);
         atomicCoordinates.setSeqRes(structure.getPrimary().getPdbSeqress());

         structure.setAtomicCoordinates(atomicCoordinates);
      }
      catch (Exception e)
      {
         if (e instanceof StructureIOException)
         {
            throw (StructureIOException) e;
         }
         else
         {
            throw new StructureIOException(e);
         }
      }

      return structure;
   }


   //---------------------------------------------------------------------------
   @Override
   public boolean isEndOfRecord(String inLine)
   {
      return (inLine.equals("END"));
   }

   //---------------------------------------------------------------------------
   @Override
   public boolean hasJanusDelimiter()
   {
      return false;
   }

   //---------------------------------------------------------------------------
   /*
    SEQRES Overview

    SEQRES records contain a listing of the consecutive chemical components covalently
    linked in a linear fashion to form a polymer. The chemical components included in this
    listing may be standard or modified amino acid and nucleic acid residues. It may also
    include other residues that are linked to the standard backbone in the polymer. Chemical
    components or groups covalently linked to side-chains (in peptides) or sugars and/or bases
    (in nucleic acid polymers) will not be listed here.

    Record Format

    COLUMNS        DATA TYPE      FIELD        DEFINITION
    -------------------------------------------------------------------------------------
     1 -  6        Record name    "SEQRES"
     8 - 10        Integer        serNum       Serial number of the SEQRES record for  the
                                               current  chain. Starts at 1 and increments
                                               by one  each line. Reset to 1 for each chain.
    12             Character      chainID      Chain identifier. This may be any single
                                               legal  character, including a blank which is
                                               is  used if there is only one chain.
    14 - 17        Integer        numRes       Number of residues in the chain.
                                               This  value is repeated on every record.
    20 - 22        Residue name   resName      Residue name.
    24 - 26        Residue name   resName      Residue name.
    28 - 30        Residue name   resName      Residue name.
    32 - 34        Residue name   resName      Residue name.
    36 - 38        Residue name   resName      Residue name.
    40 - 42        Residue name   resName      Residue name.
    44 - 46        Residue name   resName      Residue name.
    48 - 50        Residue name   resName      Residue name.
    52 - 54        Residue name   resName      Residue name.
    56 - 58        Residue name   resName      Residue name.
    60 - 62        Residue name   resName      Residue name.
    64 - 66        Residue name   resName      Residue name.
    68 - 70        Residue name   resName      Residue name.
    Verification/Validation/Value Authority Control

    The residues presented in the ATOM records must agree with those on the SEQRES records.

    The SEQRES records are checked using sequence databases and information provided by the depositor.

    SEQRES is compared to the ATOM records during processing, and both are checked against the sequence databases.
    All discrepancies are either resolved or annotated appropriately in the entry.

    The ribo- and deoxyribonucleotides in the SEQRES records are distinguished.  The ribo- forms of these residues
    are identified with the residue names A, C, G, U and I. The deoxy- forms of these residues are identified with
     the residue names DA, DC, DG, DT and DI. Modified nucleotides in the sequence are identified by separate 3-letter
     residue codes.  The plus character prefix to label modified nucleotides (e.g. +A, +C, +T) is no longer used.

    Example

             1         2         3         4         5         6         7         8
    12345678901234567890123456789012345678901234567890123456789012345678901234567890
    SEQRES   1 A   21  GLY ILE VAL GLU GLN CYS CYS THR SER ILE CYS SER LEU
    SEQRES   2 A   21  TYR GLN LEU GLU ASN TYR CYS ASN
    SEQRES   1 B   30  PHE VAL ASN GLN HIS LEU CYS GLY SER HIS LEU VAL GLU
    SEQRES   2 B   30  ALA LEU TYR LEU VAL CYS GLY GLU ARG GLY PHE PHE TYR
    SEQRES   3 B   30  THR PRO LYS ALA
    SEQRES   1 C   21  GLY ILE VAL GLU GLN CYS CYS THR SER ILE CYS SER LEU
    SEQRES   2 C   21  TYR GLN LEU GLU ASN TYR CYS ASN
    SEQRES   1 D   30  PHE VAL ASN GLN HIS LEU CYS GLY SER HIS LEU VAL GLU
    SEQRES   2 D   30  ALA LEU TYR LEU VAL CYS GLY GLU ARG GLY PHE PHE TYR
                                                                                                                                                      SEQRES   3 D   30   THR PRO LYS ALA
    SEQRES   1 A    8   DA  DA  DC  DC  DG  DG  DT  DT
    SEQRES   1 B    8   DA  DA  DC  DC  DG  DG  DT  DT

    SEQRES   1 X   39    U   C   C   C   C   C   G   U   G   C   C   C   A
    SEQRES   2 X   39    U   A   G   C   G   G   C   G   U   G   G   A   A
    SEQRES   3 X   39    C   C   A   C   C   C   G   U   U   C   C   C   A

    Known Problems

    Polysaccharides do not lend themselves to being represented in SEQRES.

    There is no mechanism provided to describe the sequence order if their starting position is unknown.

    For cyclic peptides, a residue is arbitrarily assigned as the N-terminus.
    */
   public void writeSeqResSection(Structure inStructure, Writer inWriter)
      throws IOException
   {
      Model model = inStructure.getAtomicCoordinates().getModels().get(0);

      for (Chain chain : model.getChains())
      {
         String chainDesignator = chain.getUnqualifiedName();

         int chainRowIndex = 1;
         for (List<SeqResidue> residueChunk : CollectionUtil.chunkList(chain.getSeqResidues(), 13))
         {
            StringBuilderPlus concatResidues = new StringBuilderPlus().setDelimiter(" ");
            for (SeqResidue seqResidue : residueChunk)
            {
               concatResidues.delimitedAppend(String.format("%3.3s", seqResidue.name()));
            }

            inWriter.write(composePaddedPdbLine(String.format("SEQRES %3d %1.1s %4d  %s",
                                         chainRowIndex, chainDesignator, chain.getSeqResidues().size(), concatResidues)));

            chainRowIndex++;
         }
      }
   }


   /*
   The Coordinate Section contains the collection of atomic coordinates as well as the MODEL and ENDMDL records.
   See https://www.wwpdb.org/documentation/file-format-content/format33/sect9.html

   MODEL
   Overview

   The MODEL record specifies the model serial number when multiple models of the same structure are presented in
   a single coordinate entry, as is often the case with structures determined by NMR.

   Record Format

   COLUMNS        DATA  TYPE    FIELD          DEFINITION
   ---------------------------------------------------------------------------------------
    1 -  6        Record name   "MODEL "
   11 - 14        Integer       serial         Model serial number.
   Details

   This record is used only when more than one model appears in an entry. Generally, it is employed mainly
   for NMR structures. The chemical connectivity should be the same for each model. ATOM, HETATM, ANISOU,
   and TER records for each model structure and are interspersed as needed between MODEL and ENDMDL records.
   The numbering of models is sequential, beginning with 1.
   All models in a deposition should be superimposed in an appropriate author determined manner and only one
   superposition method should be used. Structures from different experiments, or different domains of a structure
   should not be superimposed and deposited as models of a deposition.
   All models in an NMR ensemble should be homogeneous – each model should have the exact same atoms (hydrogen
   and heavy atoms), sequence and chemistry.
   All models in an NMR entry should have hydrogen atoms.
   Deposition of minimized average structure must be accompanied with ensemble and must be homogeneous with ensemble.
   A model cannot have more than 99,999 atoms. Where the entry does not contain an ensemble of models, then
   the entry cannot have more than 99,999 atoms. Entries that go beyond this atom limit must be split into multiple
   entries, each containing no more than the limits specified above.
   Verification/Validation/Value Authority Control
   Entries with multiple models in the NUMMDL record are checked for corresponding pairs of MODEL/ ENDMDL records,
   and for consecutively numbered models.

   Relationships to Other Record Types

   Each MODEL must have a corresponding ENDMDL record.

   Examples

            1         2         3         4         5         6         7         8
   12345678901234567890123456789012345678901234567890123456789012345678901234567890
   MODEL        1
   ATOM      1  N   ALA A   1      11.104   6.134  -6.504  1.00  0.00           N
   ATOM      2  CA  ALA A   1      11.639   6.071  -5.147  1.00  0.00           C
   ...
   ...
   ...
   ATOM    293 1HG  GLU A   18    -14.861  -4.847   0.361  1.00  0.00           H
   ATOM    294 2HG  GLU A   18    -13.518  -3.769   0.084  1.00  0.00           H
   TER     295      GLU A   18
   ENDMDL
   MODEL        2
   ATOM    296  N   ALA  A   1     10.883   6.779  -6.464  1.00  0.00           N
   ATOM    297  CA  ALA  A   1     11.451   6.531  -5.142  1.00  0.00           C
   ...
   ...
   ATOM    588 1HG  GLU A   18    -13.363  -4.163  -2.372  1.00  0.00           H
   ATOM    589 2HG  GLU A   18    -12.634  -3.023  -3.475  1.00  0.00           H
   TER     590      GLU A   18
   ENDMDL
            1         2         3         4         5         6         7         8
   12345678901234567890123456789012345678901234567890123456789012345678901234567890
   MODEL        1
   ATOM      1  N  AALA A   1      72.883  57.697  56.410  0.50 83.80           N
   ATOM      2  CA AALA A   1      73.796  56.531  56.644  0.50 84.78           C
   ATOM      3  C  AALA A   1      74.549  56.551  57.997  0.50 85.05           C
   ATOM      4  O  AALA A   1      73.951  56.413  59.075  0.50 84.77           O
   ...
   ...
   ...
   HETATM37900  O  AHOH   490     -24.915 147.513  36.413  0.50 41.86           O
   HETATM37901  O  AHOH   491     -28.699 130.471  22.248  0.50 36.06           O
   HETATM37902  O  AHOH   492     -33.309 184.488  26.176  0.50 15.00           O
   ENDMDL
   MODEL        2
   ATOM      1  N  BALA A   1      72.883  57.697  56.410  0.50 83.80           N
   ATOM      2  CA BALA A   1      73.796  56.531  56.644  0.50 84.78           C
   ATOM      3  C  BALA A   1      74.549  56.551  57.997  0.50 85.05           C
   ATOM      4  O  BALA A   1      73.951  56.413  59.075  0.50 84.77           O
   ATOM      5  CB BALA A   1      74.804  56.369  55.453  0.50 84.29           C
   ATOM      6  N  BASP A   2      75.872  56.703  57.905  0.50 85.59           N
   ATOM      7  CA BASP A   2      76.801  56.651  59.048  0.50 85.67           C
   ATOM      8  C  BASP A   2      76.283  57.361  60.309  0.50 84.80           C
   ...


   ATOM Overview

   The ATOM records present the atomic coordinates for standard amino acids and nucleotides. They also present the
   occupancy and temperature factor for each atom. Non-polymer chemical coordinates use the HETATM record type.
   The element symbol is always present on each ATOM record; charge is optional.

   Changes in ATOM/HETATM records result from the standardization atom and residue nomenclature. This nomenclature
   is described in the Chemical Component Dictionary (ftp://ftp.wwpdb.org/pub/pdb/data/monomers).

   Record Format

   COLUMNS        DATA  TYPE    FIELD        DEFINITION
   -------------------------------------------------------------------------------------
    1 -  6        Record name   "ATOM  "
    7 - 11        Integer       serial       Atom  serial number.
   13 - 16        Atom          name         Atom name.
   17             Character     altLoc       Alternate location indicator.
   18 - 20        Residue name  resName      Residue name.
   22             Character     chainID      Chain identifier.
   23 - 26        Integer       resSeq       Residue sequence number.
   27             AChar         iCode        Code for insertion of residues.
   31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
   39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
   47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
   55 - 60        Real(6.2)     occupancy    Occupancy.
   61 - 66        Real(6.2)     tempFactor   Temperature  factor.
   77 - 78        LString(2)    element      Element symbol, right-justified.
   79 - 80        LString(2)    charge       Charge  on the atom.
   Details

   ATOM records for proteins are listed from amino to carboxyl terminus.
   Nucleic acid residues are listed from the 5' to the 3' terminus.
   Alignment of one-letter atom name such as C starts at column 14, while two-letter atom name such as FE starts at column 13.
   Atom nomenclature begins with atom type.
   No ordering is specified for polysaccharides.
   Non-blank alphanumerical character is used for chain identifier.
   The list of ATOM records in a chain is terminated by a TER record.
   If more than one model is present in the entry, each model is delimited by MODEL and ENDMDL records.
   AltLoc is the place holder to indicate alternate conformation. The alternate conformation can be in the entire
   polymer chain, or several residues or partial residue (several atoms within one residue). If an atom is provided
   in more than one position, then a non-blank alternate location indicator must be used for each of the atomic
   positions. Within a residue, all atoms that are associated with each other in a given conformation are assigned
   the same alternate position indicator. There are two ways of representing alternate conformation- either at atom
   level or at residue level (see examples).
   For atoms that are in alternate sites indicated by the alternate site indicator, sorting of atoms in the ATOM/HETATM
   list uses the following general rules:
   - In the simple case that involves a few  atoms or a few residues with alternate sites,
     the coordinates occur one after  the other in the entry.
   - In the case of a large heterogen groups  which are disordered, the atoms for each conformer
     are listed together.
   Alphabet letters are commonly used for insertion code. The insertion code is used when two residues have the same
   numbering. The combination of residue numbering and insertion code defines the unique residue.
   If the depositor provides the data, then the isotropic B value is given for the temperature factor.
   If there are neither isotropic B values from the depositor, nor anisotropic temperature factors in ANISOU, then the
   default value of 0.0 is used for the temperature factor.
   Columns 79 - 80 indicate any charge on the atom, e.g., 2+, 1-. In most cases, these are blank.
   For refinements with program REFMAC prior 5.5.0042 which use TLS refinement, the values of B may include only the
   TLS contribution to the isotropic temperature factor rather than the full isotropic value.
   Verification/Validation/Value Authority Control

   The ATOM/HETATM records are checked for PDB file format, sequence information, and packing.

   Relationships to Other Record Types

   The ATOM records are compared to the corresponding sequence database. Sequence discrepancies appear in the SEQADV
   record. Missing atoms are annotated in the remarks. HETATM records are formatted in the same way as ATOM records.
   The sequence implied by ATOM records must be identical to that given in SEQRES, with the exception that residues
   that have no coordinates, e.g., due to disorder, must appear in SEQRES.

   Example

            1         2         3         4         5         6         7         8
   12345678901234567890123456789012345678901234567890123456789012345678901234567890
   ATOM     32  N  AARG A  -3      11.281  86.699  94.383  0.50 35.88           N
   ATOM     33  N  BARG A  -3      11.296  86.721  94.521  0.50 35.60           N
   ATOM     34  CA AARG A  -3      12.353  85.696  94.456  0.50 36.67           C
   ATOM     35  CA BARG A  -3      12.333  85.862  95.041  0.50 36.42           C
   ATOM     36  C  AARG A  -3      13.559  86.257  95.222  0.50 37.37           C
   ATOM     37  C  BARG A  -3      12.759  86.530  96.365  0.50 36.39           C
   ATOM     38  O  AARG A  -3      13.753  87.471  95.270  0.50 37.74           O
   ATOM     39  O  BARG A  -3      12.924  87.757  96.420  0.50 37.26           O
   ATOM     40  CB AARG A  -3      12.774  85.306  93.039  0.50 37.25           C
   ATOM     41  CB BARG A  -3      13.428  85.746  93.980  0.50 36.60           C
   ATOM     42  CG AARG A  -3      11.754  84.432  92.321  0.50 38.44           C
   ATOM     43  CG BARG A  -3      12.866  85.172  92.651  0.50 37.31           C
   ATOM     44  CD AARG A  -3      11.698  84.678  90.815  0.50 38.51           C
   ATOM     45  CD BARG A  -3      13.374  85.886  91.406  0.50 37.66           C
   ATOM     46  NE AARG A  -3      12.984  84.447  90.163  0.50 39.94           N
   ATOM     47  NE BARG A  -3      12.644  85.487  90.195  0.50 38.24           N
   ATOM     48  CZ AARG A  -3      13.202  84.534  88.850  0.50 40.03           C
   ATOM     49  CZ BARG A  -3      13.114  85.582  88.947  0.50 39.55           C
   ATOM     50  NH1AARG A  -3      12.218  84.840  88.007  0.50 40.76           N
   ATOM     51  NH1BARG A  -3      14.338  86.056  88.706  0.50 40.23           N
   ATOM     52  NH2AARG A  -3      14.421  84.308  88.373  0.50 40.45           N
    */
   public void writeCoordinateSection(Structure inStructure, Writer inWriter)
         throws IOException
   {
      int modelIdx = 1;
      for (Model model : inStructure.getAtomicCoordinates().getModels())
      {
         if (inStructure.getAtomicCoordinates().getModels().size() > 1)
         {
            // Only write the model tags if there is more than one model
            inWriter.write(composePaddedPdbLine(String.format("MODEL     %4d", modelIdx)));
         }

         // First, write the ATOM records
         for (Chain chain : model.getChains())
         {
            String chainDesignator = chain.getUnqualifiedName();

            Atom lastAtom = null;
            for (AtomGroup atomGroup : chain.getAtomGroups())
            {
               if (!atomGroup.getType().equals(AtomGroupType.HETERO_ATOM))
               {
                  lastAtom = writeLinesForAtomGroup(atomGroup, inWriter);
               }
            }

            /*
             The TER record indicates the end of a list of ATOM/HETATM records for a chain.

             COLUMNS        DATA  TYPE    FIELD           DEFINITION
             -------------------------------------------------------------------------
              1 -  6        Record name   "TER   "
              7 - 11        Integer       serial          Serial number.
             18 - 20        Residue name  resName         Residue name.
             22             Character     chainID         Chain identifier.
             23 - 26        Integer       resSeq          Residue sequence number.
             27             AChar         iCode           Insertion code.
             */
            inWriter.write(composePaddedPdbLine(String.format("%-6.6s%5d      %3.3s %1.1s%4d%c",
                  "TER",
                  lastAtom.getSerialNum() + 1,
                  lastAtom.getAtomGroup().name(),
                  chainDesignator,
                  lastAtom.getAtomGroup().getResidueNum(),
                  lastAtom.getInsertCode() != null ? lastAtom.getInsertCode() : ' ')));
         }

         // Now write the non-water HETATOM lines
         for (Chain chain : model.getChains())
         {
            for (AtomGroup atomGroup : chain.getAtomGroups())
            {
               if (atomGroup.getType().equals(AtomGroupType.HETERO_ATOM)
                     && ! atomGroup.name().equalsIgnoreCase("HOH"))
               {
                  writeLinesForAtomGroup(atomGroup, inWriter);
               }
            }
         }

         // Now write the water HETATOM lines
         for (Chain chain : model.getChains())
         {
            for (AtomGroup atomGroup : chain.getAtomGroups())
            {
               if (atomGroup.getType().equals(AtomGroupType.HETERO_ATOM)
                   && atomGroup.name().equalsIgnoreCase("HOH"))
               {
                  writeLinesForAtomGroup(atomGroup, inWriter);
               }
            }
         }

         if (inStructure.getAtomicCoordinates().getModels().size() > 1)
         {
            // Only write the model tags if there is more than one model
            inWriter.write(composePaddedPdbLine(String.format("ENDMDL")));
         }

         modelIdx++;
      }
   }

   //--------------------------------------------------------------------------
   private Atom writeLinesForAtomGroup(AtomGroup inAtomGroup, Writer inWriter)
      throws IOException
   {
      Atom lastAtom = null;
      for (Atom atom : inAtomGroup.getAtoms())
      {
                  /*
                     COLUMNS        DATA  TYPE    FIELD        DEFINITION
                     -------------------------------------------------------------------------------------
                      1 -  6        Record name   "ATOM  "
                      7 - 11        Integer       serial       Atom  serial number.
                     13 - 16        Atom          name         Atom name.
                     17             Character     altLoc       Alternate location indicator.
                     18 - 20        Residue name  resName      Residue name.
                     22             Character     chainID      Chain identifier.
                     23 - 26        Integer       resSeq       Residue sequence number.
                     27             AChar         iCode        Code for insertion of residues.
                     31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
                     39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
                     47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
                     55 - 60        Real(6.2)     occupancy    Occupancy.
                     61 - 66        Real(6.2)     tempFactor   Temperature  factor.
                     77 - 78        LString(2)    element      Element symbol, right-justified.
                     79 - 80        LString(2)    charge       Charge  on the atom.

                     ATOM      4  O  AALA A   1      73.951  56.413  59.075  0.50 84.77           O
                   */
         inWriter.write(composePaddedPdbLine(String.format("%-6.6s%5d %-4.4s%c%3.3s %1.1s%4.4s%c   %8.3f%8.3f%8.3f%6.6s%6.6s          %2.2s%2.2s",
               inAtomGroup.getType().equals(AtomGroupType.HETERO_ATOM) ? "HETATM" : "ATOM",
               atom.getSerialNum(),
               // The crazy logic on the next line is only to be able to reproduce the spacing seen in PDB files
               (atom.name().getName().length() < 4 && atom.getElement().getSymbol().length() < 2 ? " " : "") + atom.name().getName(),
               ' ', // TODO: Alternate location indicator.
               atom.getAtomGroup().name(),
               atom.getAtomGroup().getParentChain().getUnqualifiedName(),
               atom.getAtomGroup().getResidueNum(),
               atom.getInsertCode() != null ? atom.getInsertCode() : ' ',
               atom.getCoords()[0],
               atom.getCoords()[1],
               atom.getCoords()[2],
               atom.getOccupancy() != null ? String.format("%6.2f", atom.getOccupancy()) : " ",
               atom.getTempFactor() != null ? String.format("%6.2f", atom.getTempFactor()) : " ",
               atom.getElement().getSymbol().toUpperCase(),
               atom.getCharge() != null ? atom.getCharge().toString() : " ")));

         lastAtom = atom;
      }

      return lastAtom;
   }

   //##################################################################################################################
   // FUNCTIONS THAT CREATE EACH 'SECTION' OF THE STRUCTURE POJO
   //##################################################################################################################

   /**
    *
    * @param pdbMap
    * @return
    */
   private PdbSectionMetadata parseSectionMetadata(Map<String, LinkedList<String>> pdbMap){

      PdbSectionMetadata pdbSectionMetadata = new PdbSectionMetadata();

      //create lists of lines for each metadata section of this PDB file
      String headerLine              = pdbMap.get(PDB_KEY_HEADER).get(0);
      LinkedList<String> titleLines  = pdbMap.get(PDB_KEY_TITLE);
      LinkedList<String> obslteLines = pdbMap.get(PDB_KEY_OBSLTE);
      LinkedList<String> splitLines  = pdbMap.get(PDB_KEY_SPLIT);
      LinkedList<String> caveatLines = pdbMap.get(PDB_KEY_CAVEAT);
      LinkedList<String> compndLines = pdbMap.get(PDB_KEY_COMPND);
      LinkedList<String> sourceLines = pdbMap.get(PDB_KEY_SOURCE);
      LinkedList<String> keywdsLines = pdbMap.get(PDB_KEY_KEYWDS);
      LinkedList<String> expdtaLines = pdbMap.get(PDB_KEY_EXPDTA);
      LinkedList<String> nummdlLines = pdbMap.get(PDB_KEY_NUMMDL);
      LinkedList<String> mdltypLines = pdbMap.get(PDB_KEY_MDLTYP);
      LinkedList<String> authorLines = pdbMap.get(PDB_KEY_AUTHOR);
      LinkedList<String> revdatLines = pdbMap.get(PDB_KEY_REVDAT);
      LinkedList<String> sprsdeLines = pdbMap.get(PDB_KEY_SPRSDE);
      LinkedList<String> jrnlLines   = pdbMap.get(PDB_KEY_JRNL);

      //call each of the metadata parsing functions to store the fully fleshed-out data
      pdbSectionMetadata.setPdbIdentifier(this.parsePdbIdentifier(headerLine));
      pdbSectionMetadata.setClassification(this.parseClassification(headerLine));
      pdbSectionMetadata.setDepositionDate(this.parseDepositionDate(headerLine));
      pdbSectionMetadata.setTitle(this.parseTitle(titleLines));
      pdbSectionMetadata.setObsltes(this.parseObsltes(obslteLines));
      pdbSectionMetadata.setObslteDate(this.parseObslteDate(obslteLines));
      pdbSectionMetadata.setCaveat(this.parseCaveat(caveatLines));
      pdbSectionMetadata.setSplits(this.parseSplits(splitLines));
      pdbSectionMetadata.setCompnds(this.parseCompnds(compndLines));
      pdbSectionMetadata.setSources(this.parseSources(sourceLines));
      pdbSectionMetadata.setKeyWrds(this.parseKeywords(keywdsLines));
      pdbSectionMetadata.setExpdta(this.parseExpdta(expdtaLines));
      pdbSectionMetadata.setNummdl(this.parseNummdl(nummdlLines));
      pdbSectionMetadata.setMdltyp(this.parseMdltyp(mdltypLines));
      pdbSectionMetadata.setAuthors(this.parseAuthors(authorLines));
      pdbSectionMetadata.setPdbRevdats(this.parseRevdats(revdatLines));
      pdbSectionMetadata.setSprsdes(this.parseSprsdes(sprsdeLines));
      pdbSectionMetadata.setSprsdeDate(this.parseSprsdeDate(sprsdeLines));
      pdbSectionMetadata.setPdbJrnls(this.parsePdbJrnl(jrnlLines));

      return pdbSectionMetadata;

   }


   /**
    *
    * @param pdbMap
    * @return
    */
   private PdbSectionRemark parseSectionRemark(Map<String, LinkedList<String>> pdbMap){

      PdbSectionRemark pdbSectionRemark = new PdbSectionRemark();

      //create list of lines for the remark section of this PDB file
      LinkedList<String> remarkLines  = pdbMap.get(PDB_KEY_REMARK);

      //call the parsing functions to store the fully fleshed-out remark data...
      pdbSectionRemark.setRemarks(this.parseRemarks(remarkLines));
      //...and the specific line related to resolution data
      String resolutionLine = this.parseRemarkResolutionLine(remarkLines);
      if (StringUtil.isSet(resolutionLine))
      {
         String resolutionUnit = resolutionLine.substring(31, 41).trim();
         Double resolution = StructureUtils.stringToDouble(resolutionLine.substring(23, 30).trim(), true);
         pdbSectionRemark.setResolutionLine(resolutionLine);
         pdbSectionRemark.setResolution(resolution);
         pdbSectionRemark.setResolutionUnit(resolutionUnit);
      }

      return pdbSectionRemark;

   }


   /**
    *
    * @param pdbMap
    * @return
    */
   private PdbSectionPrimary parseSectionPrimary(Map<String, LinkedList<String>> pdbMap){

      PdbSectionPrimary pdbSectionPrimary = new PdbSectionPrimary();

      //create list of lines for the seqres section of this PDB file
      LinkedList<String> seqresLines  = pdbMap.get(PDB_KEY_SEQRES);

      //call the parsing function to store the fully fleshed-out seqres data
      pdbSectionPrimary.setPdbSeqress(this.parseSeqress(seqresLines));

      return pdbSectionPrimary;

   }




   /**
    *
    * @param pdbMap
    * @return
    */
   private PdbSectionConnectivity parseSectionConnectivity(Map<String, LinkedList<String>> pdbMap){

      PdbSectionConnectivity pdbSectionConnectivity = new PdbSectionConnectivity();

      //create list of lines for the ssbond section of this PDB file
      LinkedList<String> ssbondLines  = pdbMap.get(PDB_KEY_SSBOND);

      //call the parsing function to store the fully fleshed-out ssbond data
      pdbSectionConnectivity.setPdbSsbonds(this.parseSsbonds(ssbondLines));

      return pdbSectionConnectivity;

   }






   //--------------------------------------------------------------------------
   /**
    *
    * @param atomModels
    * @return
    */
   private AtomicCoordinates parseSectionCoordinate(List<List<PdbLine>> atomModels)
   {
      AtomicCoordinates atomicCoordinates = new AtomicCoordinates();
      for (List<PdbLine> modelLines : atomModels)
      {
         atomicCoordinates.addModel(parseModel(modelLines));
      }

      return atomicCoordinates;
   }









   //##################################################################################################################
   // PARSING METADATA SECTION
   //##################################################################################################################

   //--------------------------------------------------------------------------
   /**
    * Parse the first (and only) HEADER line from the [LinkedHash]Map - find the 'pdb identifier' location (63-66)
    * @return the four character PDB identifier or null if no HEADER line is defined
    */
   private String parsePdbIdentifier(String headerLine)
   {
      return StringUtil.isSet(headerLine) && headerLine.length() > 66 ? headerLine.substring(62,66) : null;
   }


   //--------------------------------------------------------------------------
   /**
    * Parse the first (and only) HEADER line from the [LinkedHash]Map - find the 'classification' location (11-50)
    * @return
    */
   private String parseClassification(String headerLine)
   {
      return StringUtil.isSet(headerLine) && headerLine.length() > 50 ? headerLine.substring(10, 50) : null;
   }


   //--------------------------------------------------------------------------
   /**
    * Parse the first (and only) HEADER line from the [LinkedHash]Map - find the 'depdate' location (51-59)
    * @return
    */
   private Date parseDepositionDate(String headerLine)
   {
      return StringUtil.isSet(headerLine) && headerLine.length() > 59 ? StructureUtils.parse_dd_MMM_yy_Date(headerLine.substring(50, 59)) : null;
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#TITLE
    * @return
    */
   private String parseTitle(LinkedList<String> titleLines)
   {
      if (!CollectionUtil.hasValues(titleLines))
      {
         return null;
      }

      StringBuilder sb = new StringBuilder();

      //and create the full title (location: 11 to end)
      for (String line : titleLines)
      {
         sb.append(StructureUtils.rightTrim(line.substring(10)));
      }

      return sb.toString();
   }



   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#OBSLTE
    * @return
    */
   private List<String> parseObsltes(LinkedList<String> obslteLines)
   {

      if (!CollectionUtil.hasValues(obslteLines))
      {
         return null;
      }

      List<String> obsltes = new ArrayList<>();

      //add all pdb_identifiers to our obsoletes list
      for (String line : obslteLines)
      {
         String[] pdbIds = StructureUtils.splitOnWhitespace(line.substring(31));
         if(null == pdbIds)
         {
            return null;
         }

         for(String pbdId : pdbIds)
         {
            obsltes.add(pbdId);
         }
      }

      return obsltes;
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#OBSLTE
    * @return
    */
   private Date parseObslteDate(LinkedList<String> obslteLines)
   {
      if (!CollectionUtil.hasValues(obslteLines))
      {
         return null;
      }

      String firstObslteLine = obslteLines.getFirst();
      Date obslteDate = StructureUtils.parse_dd_MMM_yy_Date(firstObslteLine.substring(11, 20));
      return obslteDate;
   }



   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#SPLIT
    * @return
    */
   private List<PdbSplit> parseSplits(LinkedList<String> splitLines)
   {
      if (!CollectionUtil.hasValues(splitLines))
      {
         return null;
      }

      List<PdbSplit> pdbSplits = new ArrayList<>();

      //add all pdb_identifiers to our splits list
      for (String line : splitLines) {
         String[] pdbIds = StructureUtils.splitOnWhitespace(line.substring(11));
         for(String pbdId : pdbIds){
            pdbSplits.add(new PdbSplit(pbdId, "PDB", null, null));
         }
      }

      return pdbSplits;
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#CAVEAT
    * @return
    */
   private String parseCaveat(LinkedList<String> caveatLines)
   {

      if (!CollectionUtil.hasValues(caveatLines))
      {
         return null;
      }

      StringBuilder sb = new StringBuilder();

      //and create the full caveat (location: 20 to end)
      for (String line : caveatLines)
      {
         sb.append(StructureUtils.rightTrim(line.substring(19)));
      }

      return sb.toString();
   }




   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#COMPND
    * @return
    */
   private List<PdbCompnd> parseCompnds(LinkedList<String> compndLines)
   {

      if (!CollectionUtil.hasValues(compndLines))
      {
         return null;
      }

      Map<String, LinkedList<String>> pdbCompndDatas = new LinkedHashMap<>();

      final String sectionSplitter = "MOL_ID:";
      String molKey0 = sectionSplitter + " 0";
      String molKey = molKey0;
      pdbCompndDatas.put(molKey, new LinkedList<>());

      //first parse all compnd lines into another [LinkedHash]Map
      for (String line : compndLines)
      {
         //get just the data line (no 'COMPND' key)
         String dataLine = line.substring(10);

         //if starting a new MOL_ID sub section, create a new entry
         if (dataLine.trim().startsWith(sectionSplitter))
         {
            molKey = dataLine.trim().replaceAll(";$", ""); //trim and remove trailing semicolon
            pdbCompndDatas.put(molKey, new LinkedList<>());
         }
         //else, just keep adding lines to the current entries value
         else
         {
            List<String> values = pdbCompndDatas.get(molKey);
            values.add(StructureUtils.rightTrim(dataLine));
         }
      }

      //if our first one (molKey0) never got any lines, remove it
      //(it was only in place in case the line block started without a mol_id number)
      List<String> molKey0Values = pdbCompndDatas.get(molKey0);
      if (!CollectionUtil.hasValues(molKey0Values))
      {
         pdbCompndDatas.remove(molKey0);
      }

      //next, create list of PdbCompnd objects from the [LinkedHash]Map
      List<PdbCompnd> pdbCompnds = new ArrayList<>(pdbCompndDatas.size());
      if(CollectionUtil.hasValues(pdbCompndDatas)){
         for (Map.Entry<String, LinkedList<String>> compnd : pdbCompndDatas.entrySet())
         {
            String molIdStr = StructureUtils.collapseWhitespace(compnd.getKey().substring(7).trim());
            Integer molId = StructureUtils.stringToInteger(molIdStr);

            pdbCompnds.add(new PdbCompnd(molId, compnd.getKey(), compnd.getValue()));
         }
      }

      return pdbCompnds;
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#SOURCE
    * @return
    */
   private List<PdbSource> parseSources(LinkedList<String> sourceLines)
   {

      if (!CollectionUtil.hasValues(sourceLines))
      {
         return null;
      }

      Map<String, LinkedList<String>> pdbSourceDatas = new LinkedHashMap<>();

      final String sectionSplitter = "MOL_ID:";
      String molKey0 = sectionSplitter + " 0";
      String molKey = molKey0;
      pdbSourceDatas.put(molKey, new LinkedList<>());

      //parse all source lines into another [LinkedHash]Map
      for (String line : sourceLines)
      {
         //get just the data line (no 'SOURCE' key)
         String dataLine = line.substring(10);

         //if starting a new MOL_ID sub section, create a new entry
         if (dataLine.trim().startsWith(sectionSplitter))
         {
            molKey = dataLine.trim().replaceAll(";$", ""); //trim and remove trailing semicolon
            pdbSourceDatas.put(molKey, new LinkedList<>());
         }
         //else, just keep adding lines to the current entries value
         else
         {
            List<String> values = pdbSourceDatas.get(molKey);
            values.add(StructureUtils.rightTrim(dataLine));
         }
      }

      //if our first one (molKey0) never got any lines, remove it
      //(it was only in place in case the line block started without a mol_id number)
      List<String> molKey0Values = pdbSourceDatas.get(molKey0);
      if (!CollectionUtil.hasValues(molKey0Values))
      {
         pdbSourceDatas.remove(molKey0);
      }

      //next, create list of PdbSource objects from the [LinkedHash]Map
      List<PdbSource> pdbSources = new ArrayList<>(pdbSourceDatas.size());
      if(CollectionUtil.hasValues(pdbSourceDatas))
      {
         for (Map.Entry<String, LinkedList<String>> source : pdbSourceDatas.entrySet())
         {
            String molIdStr = StructureUtils.collapseWhitespace(source.getKey().substring(7).trim());
            Integer molId = StructureUtils.stringToInteger(molIdStr);

            pdbSources.add(new PdbSource(molId, source.getKey(), source.getValue()));
         }
      }

      return pdbSources;
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#KEYWDS
    * @return
    */
   private List<String> parseKeywords(LinkedList<String> keywdsLines)
   {
      if (!CollectionUtil.hasValues(keywdsLines))
      {
         return null;
      }

      //add all keywords to our splits list
      StringBuilder sb = new StringBuilder();
      for (String line : keywdsLines)
      {
         sb.append(StructureUtils.rightTrim(line.substring(10)));
      }

      List<String> pdbKeywords = new ArrayList<>();

      //split the long string into individual keywords by commas
      String[] allKeywords = StructureUtils.splitOnCommaAndTrim(sb.toString());
      for (String keyword : allKeywords)
      {
         pdbKeywords.add(keyword);
      }

      return pdbKeywords;
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#EXPDTA
    * @return
    */
   private String parseExpdta(LinkedList<String> expdtaLines)
   {
      if (!CollectionUtil.hasValues(expdtaLines))
      {
         return null;
      }

      StringBuilder sb = new StringBuilder();

      //create the full expdta line (location: 11 to end)
      for (String line : expdtaLines)
      {
         sb.append(StructureUtils.rightTrim(line.substring(10)));
      }

      return sb.toString();
   }



   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#NUMMDL
    * @return
    */
   private Long parseNummdl(LinkedList<String> nummdlLines)
   {
      if (!CollectionUtil.hasValues(nummdlLines))
      {
         return null;
      }

      String strNummdl = nummdlLines.get(0).substring(10).trim();
      return Long.parseLong(strNummdl);

   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#MDLTYP
    * @return
    */
   private String parseMdltyp(LinkedList<String> mdltypLines)
   {
      if (!CollectionUtil.hasValues(mdltypLines))
      {
         return null;
      }

      StringBuilder sb = new StringBuilder();

      //create the full mdltyp line (location: 11 to end)
      for (String line : mdltypLines)
      {
         sb.append(StructureUtils.rightTrim(line.substring(10)));
      }

      return sb.toString();
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#AUTHOR
    * @return
    */
   private List<String> parseAuthors(LinkedList<String> authorLines)
   {
      if (!CollectionUtil.hasValues(authorLines))
      {
         return null;
      }

      List<String> pdbAuthors = new ArrayList<>();

      //add all authors to our authors list
      StringBuilder sb = new StringBuilder();
      for (String line : authorLines)
      {
         sb.append(StructureUtils.rightTrim(line.substring(10)));
      }

      //split the long string into individual authors by commas
      String[] allAuthors = StructureUtils.splitOnCommaAndTrim(sb.toString());
      for (String author : allAuthors)
      {
         pdbAuthors.add(author);
      }

      return pdbAuthors;
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#REVDAT
    * @return
    */
   private List<PdbRevdat> parseRevdats(LinkedList<String> revdatLines)
   {
      if (!CollectionUtil.hasValues(revdatLines))
      {
         return null;
      }

      //parse REVDAT lines into another [LinkedHash]Map
      Map<Integer, LinkedList<String>> revdatData = new LinkedHashMap<>();
      for (String line : revdatLines)
      {
         int revdatNum = Integer.parseInt(line.substring(8, 10).trim());

         if(revdatData.containsKey(revdatNum))
         {
            LinkedList foundEntry = revdatData.get(revdatNum);
            foundEntry.add(line);
         }
         else
         {
            LinkedList<String> newEntry = new LinkedList<>();
            newEntry.add(line);
            revdatData.put(revdatNum, newEntry);
         }
      }

      //iterate over our [LinkedHash]Map and create a PdbRevdats collection
      List<PdbRevdat> pdbRevdats = new ArrayList<>(revdatData.size());
      for (Map.Entry<Integer, LinkedList<String>> rev : revdatData.entrySet())
      {
         Integer remNum = rev.getKey();
         String revFirstLine = rev.getValue().get(0);
         Date revdatDate = StructureUtils.parse_dd_MMM_yy_Date(revFirstLine.substring(13, 22));
         String revModId = revFirstLine.substring(23, 27).trim();
         int revdatType = Integer.parseInt(revFirstLine.substring(31, 33).trim());
         StringBuilder sb = new StringBuilder();
         for (String line : rev.getValue())
         {
            sb.append(line.substring(39));
         }
         String detail = StructureUtils.collapseWhitespace(sb.toString());
         pdbRevdats.add(new PdbRevdat(remNum, revdatDate, revModId, revdatType, detail));
      }

      return pdbRevdats;
   }



   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#SPRSDE
    * @return
    */
   private List<String> parseSprsdes(LinkedList<String> sprsdeLines)
   {
      if (!CollectionUtil.hasValues(sprsdeLines))
      {
         return null;
      }

      List<String> sprsdes = new ArrayList<>(sprsdeLines.size());

      //add all pdb_identifiers to our sprsdes list
      for (String line : sprsdeLines)
      {
         String[] pdbIds = StructureUtils.splitOnWhitespace(line.substring(31));
         for(String pbdId : pdbIds)
         {
            sprsdes.add(pbdId);
         }
      }

      return sprsdes;
   }


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#SPRSDE
    * @param inSprsdeLines the list of supersedes lines
    * @return the supersedes date
    */
   private Date parseSprsdeDate(LinkedList<String> inSprsdeLines)
   {
      Date sprsdeDate = null;
      if (CollectionUtil.hasValues(inSprsdeLines))
      {
         String firstSprsdeLine = inSprsdeLines.getFirst();
         sprsdeDate = StructureUtils.parse_dd_MMM_yy_Date(firstSprsdeLine.substring(11, 20));
      }

      return sprsdeDate;
   }



   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#JRNL
    * @param inJrnlLines the list of journal lines
    * @return the journal section objects
    */
   private List<PdbJrnl> parsePdbJrnl(LinkedList<String> inJrnlLines)
   {
      if (!CollectionUtil.hasValues(inJrnlLines))
      {
         return null;
      }

      Map<String, LinkedList<String>> jrnlData = new LinkedHashMap<>();

      //parse JRNL lines into another [LinkedHash]Map
      for (String line : inJrnlLines)
      {
         String jrnlSubKey = line.substring(12,16).trim();
         String jrnlValue = StructureUtils.rightTrim(line.substring(19));

         if(jrnlData.containsKey(jrnlSubKey))
         {
            LinkedList foundEntry = jrnlData.get(jrnlSubKey);
            foundEntry.add(jrnlValue.trim());
         }
         else
         {
            LinkedList<String> newEntry = new LinkedList<>();
            newEntry.add(jrnlValue.trim());
            jrnlData.put(jrnlSubKey, newEntry);
         }
      }

      String auth = StringUtil.join(jrnlData.get("AUTH"), " ");
      String titl = StringUtil.join(jrnlData.get("TITL"), " ");
      String edit = StringUtil.join(jrnlData.get("EDIT"), " ");
      String ref  = StringUtil.join(jrnlData.get("REF"),  " ");
      String publ = StringUtil.join(jrnlData.get("PUBL"), " ");
      String refn = StringUtil.join(jrnlData.get("REFN"), " ");
      String pmid = StringUtil.join(jrnlData.get("PMID"), " ");
      String doi  = StringUtil.join(jrnlData.get("DOI"),  " ");

      List<PdbJrnl> pdbJrnls = new ArrayList<>(1);
      pdbJrnls.add(new PdbJrnl(auth, titl, edit, ref, publ, refn, pmid, doi, true, "primary"));

      return pdbJrnls;
   }


   //##################################################################################################################
   // PARSING REMARK SECTION
   //##################################################################################################################


   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/remarks.html
    * @param inRemarkLines the list of remark lines
    * @return the REMARK section objects
    */
   private List<PdbRemark> parseRemarks(LinkedList<String> inRemarkLines)
   {
      if (!CollectionUtil.hasValues(inRemarkLines))
      {
         return null;
      }

      //parse REMARK lines into another [LinkedHash]Map
      Map<String, LinkedList<String>> remarkData = new LinkedHashMap<>();
      for (String line : inRemarkLines)
      {
         String remarkName = StructureUtils.collapseWhitespace(line.substring(0, 10).trim());

         if (line.length() > 10)
         {
            String remarkValue = StructureUtils.rightTrim(line.substring(11));

            if (StringUtil.isSet(remarkValue.trim()))
            { //if remark data is blank, skip it

               if (remarkData.containsKey(remarkName))
               {
                  LinkedList foundEntry = remarkData.get(remarkName);
                  foundEntry.add(remarkValue);
               }
               else
               {
                  LinkedList<String> newEntry = new LinkedList<>();
                  newEntry.add(remarkValue);
                  remarkData.put(remarkName, newEntry);
               }
            }
         }
      }

      //iterate over our [LinkedHash]Map and create a PdbRemark collection
      List<PdbRemark> pdbRemarks = new ArrayList<>(remarkData.size());
      for (Map.Entry<String, LinkedList<String>> rem : remarkData.entrySet())
      {
         //String firstLine = rem.getValue().get(0);
         String remNum = StructureUtils.collapseWhitespace(rem.getKey().substring(7).trim());
         Integer remarkNumber = StructureUtils.stringToInteger(remNum);

         pdbRemarks.add(new PdbRemark(remarkNumber, rem.getKey(), rem.getValue()));
      }

      return pdbRemarks;
   }

   //--------------------------------------------------------------------------
   /**
    * Extracts the line containing the structure's resolution data.
    * @param inRemarkLines the list of remark lines
    * @return the resolution line from the specified list of remark lines
    */
   private String parseRemarkResolutionLine(LinkedList<String> inRemarkLines)
   {
      String result = null;

      if (CollectionUtil.hasValues(inRemarkLines))
      {
         //find appropriate line that contains resolution data (REMARK 2)
         for (String line : inRemarkLines)
         {
            if (line.length() > 10)
            {
               String remarkName = StructureUtils.collapseWhitespace(line.substring(0, 10).trim());
               String remarkValue = StructureUtils.rightTrim(line.substring(11));

               if (StringUtil.isSet(remarkValue.trim())) //if remark data is blank, skip it
               {
                  //REMARK 2 line contains resolution data - only one line
                  if ((PDB_KEY_REMARK + " 2").equalsIgnoreCase(remarkName))
                  {
                     if (!remarkValue.toUpperCase().contains("NOT APPLICABLE"))
                     {
                        //return remark 2 line
                        result = line;
                     }
                     else
                     {
                        //remark 2 is there, but data is not applicable
                     }

                     break;
                  }
               }
            }
         }
      }

      return result;
   }



   //##################################################################################################################
   // PARSING PRIMARY SECTION (SEQRES)
   //##################################################################################################################

   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect3.html#SEQRES
    * @return the SEQRES section objects
    */
   private List<PdbSeqres> parseSeqress(LinkedList<String> seqresLines)
   {
      if (!CollectionUtil.hasValues(seqresLines))
      {
         return null;
      }

      //parse SEQRES lines into another [LinkedHash]Map (for each chain)
      Map<String, LinkedList<String>> seqresData = new LinkedHashMap<>();
      for (String line : seqresLines)
      {
         String chainId = line.substring(11, 12).trim();

         if(seqresData.containsKey(chainId))
         {
            LinkedList foundEntry = seqresData.get(chainId);
            foundEntry.add(line);
         }
         else
         {
            LinkedList<String> newEntry = new LinkedList<>();
            newEntry.add(line);
            seqresData.put(chainId, newEntry);
         }
      }

      //iterate over our [LinkedHash]Map and create a PdbSeqres collection
      List<PdbSeqres> pdbSeqress = new ArrayList<>(seqresData.size());
      for (Map.Entry<String, LinkedList<String>> rev : seqresData.entrySet())
      {

         String chainId = rev.getKey();
         String firstLine = rev.getValue().get(0);
         int numbResidues = Integer.parseInt(firstLine.substring(13,17).trim());
         StringBuilder sb = new StringBuilder();
         for (String line : rev.getValue())
         {
            sb.append(line.substring(19));
         }

         pdbSeqress.add(new PdbSeqres(chainId, chainId, numbResidues, sb.toString().replaceAll("\\s+", " ").trim()));
      }

      return pdbSeqress;
   }






   //##################################################################################################################
   // PARSING CONNECTIVITY SECTION (SSBONDS)
   //##################################################################################################################

   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect6.html#SSBOND
    *
    * @return the SSBONDS section object
    */
   private List<PdbSsbond> parseSsbonds(LinkedList<String> ssbondLines)
   {
      if (!CollectionUtil.hasValues(ssbondLines))
      {
         return null;
      }

      List<PdbSsbond> ssBondData = new ArrayList<>(ssbondLines.size());

      //parse each line into it's own object
      for (String line : ssbondLines)
      {
         Integer serNum = StructureUtils.stringToInteger(line.substring(7, 10).trim(), true);
         String residue1 = line.substring(11, 14).trim();
         String chainId1 = line.substring(15, 16).trim();
         Integer seqNum1 = StructureUtils.stringToInteger(line.substring(16, 21).trim(), true);
         String icode1 = line.substring(21, 22).trim();
         String residue2 = line.substring(25, 28).trim();
         String chainId2 = line.substring(29, 30).trim();
         Integer seqNum2 = StructureUtils.stringToInteger(line.substring(31, 35).trim(), true);
         String icode2 = line.substring(35, 36).trim();
         String sym1 = line.substring(59, 65).trim();
         String sym2 = line.substring(66, 72).trim();
         Double bondLength = StructureUtils.stringToDouble(line.substring(73, 78).trim(), true);

         PdbSsbond pdbSsbond = new PdbSsbond(serNum, residue1, chainId1, seqNum1, icode1, residue2, chainId2, seqNum2, icode2, sym1, sym2, bondLength);
         ssBondData.add(pdbSsbond);

      }

      return ssBondData;
   }





   //##################################################################################################################
   // PARSING COORDINATE SECTION (ATOM)
   //##################################################################################################################

   //--------------------------------------------------------------------------
   /**
    * http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM
    * @return the Model represented by the atom lines
    */
   private Model parseModel(List<PdbLine> inModelLines)
   {
      Model model = null;

      if (CollectionUtil.hasValues(inModelLines))
      {
         model = new Model();

         AtomGroup currentGroup = null;

         // Parse each line
         for (PdbLine pdbLine : inModelLines)
         {
            String key = pdbLine.getLineKey();
            String line = pdbLine.getLineValue();

            Integer serial = StructureUtils.stringToInteger(line.substring(6, 11).trim());

            String atomNameString = line.substring(12, 16).trim();
            PDB_AtomName atomName = PDB_AtomName.valueOf(atomNameString);
            if (null == atomName)
            {
               // Don't let non-standard atom names ruin the fun
               String elementString = line.substring(76, 78).trim();
               atomName = new PDB_AtomName(atomNameString, Element.valueOf(elementString, Case.INSENSITIVE));
            }

            String resName = line.substring(17, 20).trim();
            String chainName = line.substring(21, 22).trim();
            Integer resSeq = StructureUtils.stringToInteger(line.substring(22, 26).trim());
            Character iCode = StructureUtils.stringToCharacter(line.substring(26, 27));

            if (! AtomType.TER.toString().equalsIgnoreCase(key))
            {
               double coordinateX = StructureUtils.stringToDouble(line.substring(30, 38).trim());
               double coordinateY = StructureUtils.stringToDouble(line.substring(38, 46).trim());
               double coordinateZ = StructureUtils.stringToDouble(line.substring(46, 54).trim());
               float  occupancy   = StructureUtils.stringToFloat(line.substring(54, 60).trim());
               float  tempFactor  = StructureUtils.stringToFloat(line.substring(60, 66).trim());

               String element = line.substring(76, 78).trim();

               Chain chain = model.getChain(chainName);
               if (null == chain)
               {
                  chain = new Chain(chainName);
                  model.addChain(chain);
               }

               if (null == currentGroup
                   || ! currentGroup.getResidueNum().equals(resSeq))
               {
                  AtomGroupType groupType;
                  if (AminoAcid3D.valueOf(resName) != null)
                  {
                     groupType = AtomGroupType.AMINO_ACID;
                  }
                  else if (key.equalsIgnoreCase("ATOM"))
                  {
                     groupType = AtomGroupType.NUCLEOTIDE;
                  }
                  else
                  {
                     groupType = AtomGroupType.HETERO_ATOM;
                  }

                  currentGroup = new AtomGroup(resName, groupType)
                        .setResidueNum(resSeq)
                        .setInsertCode(iCode);

                  chain.addAtomGroup(currentGroup);
               }

               currentGroup.addAtom(new AtomImpl(atomName)
                     .setAtomGroup(currentGroup)
                     .setSerialNum(serial)
                     .setCoords(coordinateX, coordinateY, coordinateZ)
                     .setOccupancy(occupancy)
                     .setTempFactor(tempFactor)
                     .setElement(Element.valueOf(element, Case.INSENSITIVE)));
            }
         }
      }

      return model;
   }


   //--------------------------------------------------------------------------
   // Pads the PDB line out to 80 columns
   private String composePaddedPdbLine(String inLine)
   {
      int padSize = 80 - inLine.length();
      return inLine + StringUtil.polyChar(' ', padSize) + "\n";
   }

}


















