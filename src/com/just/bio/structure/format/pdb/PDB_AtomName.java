package com.just.bio.structure.format.pdb;

import java.util.HashMap;
import java.util.Map;

import com.hfg.chem.Element;
import com.hfg.util.CompareUtil;

//======================================================================================================
/**
 An enumeration of structure-based names for atoms in a PDB file.
 <div>
 Notice that the first one or two characters of the atom name consists of the chemical symbol for the atom type.
 All the atom names beginning with C are carbon atoms; N indicates a nitrogen and O indicates oxygen.
 In amino acid residues, the next character is the remoteness indicator code, which is transliterated according to:
 <pre>
 α	A
 β	B
 γ	G
 δ	D
 ε	E
 ζ	Z
 η	H
 </pre>
 The next character of the atom name is a branch indicator, if required.
 </div>
 Atom names should be distinct within a residue but there are cases where the atom name needs an
 "alternate location indicator" to be disambiguated.
 <div>
 @author J. Alex Taylor
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class PDB_AtomName implements Comparable
{
   //##########################################################################
   // PRIVATE FIELDS
   //##########################################################################

   private String  mName;
   private Element mElement;
   private Character mAltLocationIndicator;

   // This declaration has to come before the public constants below.
   private static Map<String, PDB_AtomName> sValueMap = new HashMap<>();

   //##########################################################################
   // PUBLIC FIELDS
   //##########################################################################

   public static final PDB_AtomName B    = new PDB_AtomName("B",    Element.BORON);

   public static final PDB_AtomName C    = new PDB_AtomName("C",    Element.CARBON);
   public static final PDB_AtomName CA   = new PDB_AtomName("CA",   Element.CARBON);
   public static final PDB_AtomName CAA  = new PDB_AtomName("CAA",  Element.CARBON);
   public static final PDB_AtomName CAB  = new PDB_AtomName("CAB",  Element.CARBON);
   public static final PDB_AtomName CAC  = new PDB_AtomName("CAC",  Element.CARBON);
   public static final PDB_AtomName CAD  = new PDB_AtomName("CAD",  Element.CARBON);
   public static final PDB_AtomName CB   = new PDB_AtomName("CB",   Element.CARBON);
   public static final PDB_AtomName CBA  = new PDB_AtomName("CBA",  Element.CARBON);
   public static final PDB_AtomName CBB  = new PDB_AtomName("CBB",  Element.CARBON);
   public static final PDB_AtomName CBC  = new PDB_AtomName("CBC",  Element.CARBON);
   public static final PDB_AtomName CBD  = new PDB_AtomName("CBD",  Element.CARBON);
   public static final PDB_AtomName CB1  = new PDB_AtomName("CB1",  Element.CARBON);
   public static final PDB_AtomName CB2  = new PDB_AtomName("CB2",  Element.CARBON);
   public static final PDB_AtomName CD   = new PDB_AtomName("CD",   Element.CARBON);
   public static final PDB_AtomName CD1  = new PDB_AtomName("CD1",  Element.CARBON);
   public static final PDB_AtomName CD2  = new PDB_AtomName("CD2",  Element.CARBON);
   public static final PDB_AtomName CE   = new PDB_AtomName("CE",   Element.CARBON);
   public static final PDB_AtomName CE1  = new PDB_AtomName("CE1",  Element.CARBON);
   public static final PDB_AtomName CE2  = new PDB_AtomName("CE2",  Element.CARBON);
   public static final PDB_AtomName CE3  = new PDB_AtomName("CE3",  Element.CARBON);
   public static final PDB_AtomName CG   = new PDB_AtomName("CG",   Element.CARBON);
   public static final PDB_AtomName CGA  = new PDB_AtomName("CGA",  Element.CARBON);
   public static final PDB_AtomName CGD  = new PDB_AtomName("CGD",  Element.CARBON);
   public static final PDB_AtomName CG1  = new PDB_AtomName("CG1",  Element.CARBON);
   public static final PDB_AtomName CG2  = new PDB_AtomName("CG2",  Element.CARBON);
   public static final PDB_AtomName CHA  = new PDB_AtomName("CHA",  Element.CARBON);
   public static final PDB_AtomName CH   = new PDB_AtomName("CH",   Element.CARBON);
   public static final PDB_AtomName CHB  = new PDB_AtomName("CHB",  Element.CARBON);
   public static final PDB_AtomName CHC  = new PDB_AtomName("CHC",  Element.CARBON);
   public static final PDB_AtomName CHD  = new PDB_AtomName("CHD",  Element.CARBON);
   public static final PDB_AtomName CH1  = new PDB_AtomName("CH1",  Element.CARBON);
   public static final PDB_AtomName CH2  = new PDB_AtomName("CH2",  Element.CARBON);
   public static final PDB_AtomName CH3  = new PDB_AtomName("CH3",  Element.CARBON);
   public static final PDB_AtomName CM   = new PDB_AtomName("CM",   Element.CARBON);
   public static final PDB_AtomName CMA  = new PDB_AtomName("CMA",  Element.CARBON);
   public static final PDB_AtomName CMB  = new PDB_AtomName("CMB",  Element.CARBON);
   public static final PDB_AtomName CMC  = new PDB_AtomName("CMC",  Element.CARBON);
   public static final PDB_AtomName CMD  = new PDB_AtomName("CMD",  Element.CARBON);
   public static final PDB_AtomName CM1  = new PDB_AtomName("CM1",  Element.CARBON);
   public static final PDB_AtomName CM2  = new PDB_AtomName("CM2",  Element.CARBON);
   public static final PDB_AtomName CM3  = new PDB_AtomName("CM3",  Element.CARBON);
   public static final PDB_AtomName CN   = new PDB_AtomName("CN",   Element.CARBON);
   public static final PDB_AtomName CX   = new PDB_AtomName("CX",   Element.CARBON);
   public static final PDB_AtomName CZ   = new PDB_AtomName("CZ",   Element.CARBON);
   public static final PDB_AtomName CZ2  = new PDB_AtomName("CZ2",  Element.CARBON);
   public static final PDB_AtomName CZ3  = new PDB_AtomName("CZ3",  Element.CARBON);
   public static final PDB_AtomName C1A      = new PDB_AtomName("C1A",  Element.CARBON);
   public static final PDB_AtomName C1B      = new PDB_AtomName("C1B",  Element.CARBON);
   public static final PDB_AtomName C1C      = new PDB_AtomName("C1C",  Element.CARBON);
   public static final PDB_AtomName C1D      = new PDB_AtomName("C1D",  Element.CARBON);
   public static final PDB_AtomName C1_PRIME = new PDB_AtomName("C1'",  Element.CARBON);
   public static final PDB_AtomName C2       = new PDB_AtomName("C2",   Element.CARBON);
   public static final PDB_AtomName C2A      = new PDB_AtomName("C2A",  Element.CARBON);
   public static final PDB_AtomName C2B      = new PDB_AtomName("C2B",  Element.CARBON);
   public static final PDB_AtomName C2C      = new PDB_AtomName("C2C",  Element.CARBON);
   public static final PDB_AtomName C2D      = new PDB_AtomName("C2D",  Element.CARBON);
   public static final PDB_AtomName C2_PRIME = new PDB_AtomName("C2'",  Element.CARBON);
   public static final PDB_AtomName C3       = new PDB_AtomName("C3",   Element.CARBON);
   public static final PDB_AtomName C3A      = new PDB_AtomName("C3A",  Element.CARBON);
   public static final PDB_AtomName C3B      = new PDB_AtomName("C3B",  Element.CARBON);
   public static final PDB_AtomName C3C      = new PDB_AtomName("C3C",  Element.CARBON);
   public static final PDB_AtomName C3D      = new PDB_AtomName("C3D",  Element.CARBON);
   public static final PDB_AtomName C3_PRIME = new PDB_AtomName("C3'",  Element.CARBON);
   public static final PDB_AtomName C4       = new PDB_AtomName("C4",   Element.CARBON);
   public static final PDB_AtomName C4A      = new PDB_AtomName("C4A",  Element.CARBON);
   public static final PDB_AtomName C4B      = new PDB_AtomName("C4B",  Element.CARBON);
   public static final PDB_AtomName C4C      = new PDB_AtomName("C4C",  Element.CARBON);
   public static final PDB_AtomName C4D      = new PDB_AtomName("C4D",  Element.CARBON);
   public static final PDB_AtomName C4_PRIME = new PDB_AtomName("C4'",  Element.CARBON);
   public static final PDB_AtomName C5       = new PDB_AtomName("C5",   Element.CARBON);
   public static final PDB_AtomName C5_PRIME = new PDB_AtomName("C5'",  Element.CARBON);
   public static final PDB_AtomName C5A      = new PDB_AtomName("C5A",  Element.CARBON);
   public static final PDB_AtomName C6   = new PDB_AtomName("C6",   Element.CARBON);
   public static final PDB_AtomName C7   = new PDB_AtomName("C7",   Element.CARBON);
   public static final PDB_AtomName C8   = new PDB_AtomName("C8",   Element.CARBON);
   public static final PDB_AtomName C9   = new PDB_AtomName("C9",   Element.CARBON);

   public static final PDB_AtomName FE   = new PDB_AtomName("FE",   Element.IRON);

   public static final PDB_AtomName H    = new PDB_AtomName("H",    Element.HYDROGEN);
   public static final PDB_AtomName HA   = new PDB_AtomName("HA",   Element.HYDROGEN);
   public static final PDB_AtomName HA1  = new PDB_AtomName("HA1",  Element.HYDROGEN);
   public static final PDB_AtomName HA2  = new PDB_AtomName("HA2",  Element.HYDROGEN);
   public static final PDB_AtomName HA3  = new PDB_AtomName("HA3",  Element.HYDROGEN);
   public static final PDB_AtomName HB   = new PDB_AtomName("HB",   Element.HYDROGEN);
   public static final PDB_AtomName HB1  = new PDB_AtomName("HB1",  Element.HYDROGEN);
   public static final PDB_AtomName HB2  = new PDB_AtomName("HB2",  Element.HYDROGEN);
   public static final PDB_AtomName HB3  = new PDB_AtomName("HB3",  Element.HYDROGEN);
   public static final PDB_AtomName HD1  = new PDB_AtomName("HD1",  Element.HYDROGEN);
   public static final PDB_AtomName HD2  = new PDB_AtomName("HD2",  Element.HYDROGEN);
   public static final PDB_AtomName HD3  = new PDB_AtomName("HD3",  Element.HYDROGEN);
   public static final PDB_AtomName HD11 = new PDB_AtomName("HD11", Element.HYDROGEN);
   public static final PDB_AtomName HD12 = new PDB_AtomName("HD12", Element.HYDROGEN);
   public static final PDB_AtomName HD13 = new PDB_AtomName("HD13", Element.HYDROGEN);
   public static final PDB_AtomName HD21 = new PDB_AtomName("HD21", Element.HYDROGEN);
   public static final PDB_AtomName HD22 = new PDB_AtomName("HD22", Element.HYDROGEN);
   public static final PDB_AtomName HD23 = new PDB_AtomName("HD23", Element.HYDROGEN);
   public static final PDB_AtomName HE1  = new PDB_AtomName("HE1",  Element.HYDROGEN);
   public static final PDB_AtomName HE2  = new PDB_AtomName("HE2",  Element.HYDROGEN);
   public static final PDB_AtomName HE3  = new PDB_AtomName("HE3",  Element.HYDROGEN);
   public static final PDB_AtomName HE21 = new PDB_AtomName("HE21", Element.HYDROGEN);
   public static final PDB_AtomName HE22 = new PDB_AtomName("HE22", Element.HYDROGEN);
   public static final PDB_AtomName HG   = new PDB_AtomName("HG",   Element.HYDROGEN);
   public static final PDB_AtomName HG2  = new PDB_AtomName("HG2",  Element.HYDROGEN);
   public static final PDB_AtomName HG3  = new PDB_AtomName("HG3",  Element.HYDROGEN);
   public static final PDB_AtomName HG11 = new PDB_AtomName("HG11", Element.HYDROGEN);
   public static final PDB_AtomName HG12 = new PDB_AtomName("HG12", Element.HYDROGEN);
   public static final PDB_AtomName HG13 = new PDB_AtomName("HG13", Element.HYDROGEN);
   public static final PDB_AtomName HG21 = new PDB_AtomName("HG21", Element.HYDROGEN);
   public static final PDB_AtomName HG22 = new PDB_AtomName("HG22", Element.HYDROGEN);
   public static final PDB_AtomName HG23 = new PDB_AtomName("HG23", Element.HYDROGEN);
   public static final PDB_AtomName HH2  = new PDB_AtomName("HH2",  Element.HYDROGEN);
   public static final PDB_AtomName HN   = new PDB_AtomName("HN",   Element.HYDROGEN);
   public static final PDB_AtomName HN1  = new PDB_AtomName("HN1",  Element.HYDROGEN);
   public static final PDB_AtomName HN41 = new PDB_AtomName("HN41", Element.HYDROGEN);
   public static final PDB_AtomName HN42 = new PDB_AtomName("HN42", Element.HYDROGEN);
   public static final PDB_AtomName HO   = new PDB_AtomName("HO",   Element.HYDROGEN);
   public static final PDB_AtomName HO3_PRIME  = new PDB_AtomName("HO3'",   Element.HYDROGEN);
   public static final PDB_AtomName HO5_PRIME  = new PDB_AtomName("HO5'",   Element.HYDROGEN);
   public static final PDB_AtomName HZ1  = new PDB_AtomName("HZ1",  Element.HYDROGEN);
   public static final PDB_AtomName HZ   = new PDB_AtomName("HZ",   Element.HYDROGEN);
   public static final PDB_AtomName HZ2  = new PDB_AtomName("HZ2",  Element.HYDROGEN);
   public static final PDB_AtomName HZ3  = new PDB_AtomName("HZ3",  Element.HYDROGEN);
   public static final PDB_AtomName H1         = new PDB_AtomName("H1",    Element.HYDROGEN);
   public static final PDB_AtomName H1_PRIME   = new PDB_AtomName("H1'",   Element.HYDROGEN);
   public static final PDB_AtomName H2         = new PDB_AtomName("H2",    Element.HYDROGEN);
   public static final PDB_AtomName H2_PRIME   = new PDB_AtomName("H2'",   Element.HYDROGEN);
   public static final PDB_AtomName H2_DOUBLE_PRIME   = new PDB_AtomName("H2''",   Element.HYDROGEN);
   public static final PDB_AtomName H3         = new PDB_AtomName("H3",    Element.HYDROGEN);
   public static final PDB_AtomName H3_PRIME   = new PDB_AtomName("H3'",   Element.HYDROGEN);
   public static final PDB_AtomName H4         = new PDB_AtomName("H4",    Element.HYDROGEN);
   public static final PDB_AtomName H4_PRIME   = new PDB_AtomName("H4'",   Element.HYDROGEN);
   public static final PDB_AtomName H5         = new PDB_AtomName("H5",    Element.HYDROGEN);
   public static final PDB_AtomName H5_PRIME   = new PDB_AtomName("H5'",   Element.HYDROGEN);
   public static final PDB_AtomName H5_DOUBLE_PRIME   = new PDB_AtomName("H5''",   Element.HYDROGEN);
   public static final PDB_AtomName H5A1       = new PDB_AtomName("H5A1",  Element.HYDROGEN);
   public static final PDB_AtomName H5A2       = new PDB_AtomName("H5A2",  Element.HYDROGEN);
   public static final PDB_AtomName H5A3       = new PDB_AtomName("H5A3",  Element.HYDROGEN);
   public static final PDB_AtomName H6         = new PDB_AtomName("H6",    Element.HYDROGEN);
   public static final PDB_AtomName H6_PRIME   = new PDB_AtomName("H6'",   Element.HYDROGEN);
   public static final PDB_AtomName H7         = new PDB_AtomName("H7",    Element.HYDROGEN);
   public static final PDB_AtomName H8         = new PDB_AtomName("H8",    Element.HYDROGEN);
   public static final PDB_AtomName H9         = new PDB_AtomName("H9",    Element.HYDROGEN);
   public static final PDB_AtomName H21        = new PDB_AtomName("H21",   Element.HYDROGEN);
   public static final PDB_AtomName H22        = new PDB_AtomName("H22",   Element.HYDROGEN);
   public static final PDB_AtomName H41        = new PDB_AtomName("H41",   Element.HYDROGEN);
   public static final PDB_AtomName H42        = new PDB_AtomName("H42",   Element.HYDROGEN);
   public static final PDB_AtomName H61        = new PDB_AtomName("H61",   Element.HYDROGEN);
   public static final PDB_AtomName H62        = new PDB_AtomName("H62",   Element.HYDROGEN);
   public static final PDB_AtomName H71        = new PDB_AtomName("H71",   Element.HYDROGEN);
   public static final PDB_AtomName H72        = new PDB_AtomName("H72",   Element.HYDROGEN);
   public static final PDB_AtomName H73        = new PDB_AtomName("H73",   Element.HYDROGEN);

   public static final PDB_AtomName MG   = new PDB_AtomName("MG",   Element.MAGNESIUM);

   public static final PDB_AtomName N    = new PDB_AtomName("N",    Element.NITROGEN);
   public static final PDB_AtomName NA   = new PDB_AtomName("NA",   Element.NITROGEN);
   public static final PDB_AtomName NB   = new PDB_AtomName("NB",   Element.NITROGEN);
   public static final PDB_AtomName NC   = new PDB_AtomName("NC",   Element.NITROGEN);
   public static final PDB_AtomName ND   = new PDB_AtomName("ND",   Element.NITROGEN);
   public static final PDB_AtomName ND1  = new PDB_AtomName("ND1",  Element.NITROGEN);
   public static final PDB_AtomName ND2  = new PDB_AtomName("ND2",  Element.NITROGEN);
   public static final PDB_AtomName NE   = new PDB_AtomName("NE",   Element.NITROGEN);
   public static final PDB_AtomName NE1  = new PDB_AtomName("NE1",  Element.NITROGEN);
   public static final PDB_AtomName NE2  = new PDB_AtomName("NE2",  Element.NITROGEN);
   public static final PDB_AtomName NH1  = new PDB_AtomName("NH1",  Element.NITROGEN);
   public static final PDB_AtomName NH2  = new PDB_AtomName("NH2",  Element.NITROGEN);
   public static final PDB_AtomName NZ   = new PDB_AtomName("NZ",   Element.NITROGEN);
   public static final PDB_AtomName N1   = new PDB_AtomName("N1",   Element.NITROGEN);
   public static final PDB_AtomName N2   = new PDB_AtomName("N2",   Element.NITROGEN);
   public static final PDB_AtomName N3   = new PDB_AtomName("N3",   Element.NITROGEN);
   public static final PDB_AtomName N4   = new PDB_AtomName("N4",   Element.NITROGEN);
   public static final PDB_AtomName N5   = new PDB_AtomName("N5",   Element.NITROGEN);
   public static final PDB_AtomName N6   = new PDB_AtomName("N6",   Element.NITROGEN);
   public static final PDB_AtomName N7   = new PDB_AtomName("N7",   Element.NITROGEN);
   public static final PDB_AtomName N8   = new PDB_AtomName("N8",   Element.NITROGEN);
   public static final PDB_AtomName N9   = new PDB_AtomName("N9",   Element.NITROGEN);

   public static final PDB_AtomName O    = new PDB_AtomName("O",    Element.OXYGEN);
   public static final PDB_AtomName OD   = new PDB_AtomName("OD",   Element.OXYGEN);
   public static final PDB_AtomName OD1  = new PDB_AtomName("OD1",  Element.OXYGEN);
   public static final PDB_AtomName OD2  = new PDB_AtomName("OD2",  Element.OXYGEN);
   public static final PDB_AtomName OD3  = new PDB_AtomName("OD3",  Element.OXYGEN);
   public static final PDB_AtomName OE   = new PDB_AtomName("OE",   Element.OXYGEN);
   public static final PDB_AtomName OE1  = new PDB_AtomName("OE1",  Element.OXYGEN);
   public static final PDB_AtomName OE2  = new PDB_AtomName("OE2",  Element.OXYGEN);
   public static final PDB_AtomName OG   = new PDB_AtomName("OG",   Element.OXYGEN);
   public static final PDB_AtomName OG1  = new PDB_AtomName("OG1",  Element.OXYGEN);
   public static final PDB_AtomName OH   = new PDB_AtomName("OH",   Element.OXYGEN);
   public static final PDB_AtomName ON1  = new PDB_AtomName("ON1",  Element.OXYGEN);
   public static final PDB_AtomName ON2  = new PDB_AtomName("ON2",  Element.OXYGEN);
   public static final PDB_AtomName OP1  = new PDB_AtomName("OP1",  Element.OXYGEN);
   public static final PDB_AtomName OP2  = new PDB_AtomName("OP2",  Element.OXYGEN);
   public static final PDB_AtomName O1P  = new PDB_AtomName("O1P",  Element.OXYGEN);
   public static final PDB_AtomName O2P  = new PDB_AtomName("O2P",  Element.OXYGEN);
   public static final PDB_AtomName O3P  = new PDB_AtomName("O3P",  Element.OXYGEN);
   public static final PDB_AtomName O4P  = new PDB_AtomName("O4P",  Element.OXYGEN);
   public static final PDB_AtomName OQ1  = new PDB_AtomName("OQ1",  Element.OXYGEN);
   public static final PDB_AtomName OQ2  = new PDB_AtomName("OQ2",  Element.OXYGEN);
   public static final PDB_AtomName OXT  = new PDB_AtomName("OXT",  Element.OXYGEN);
   public static final PDB_AtomName O1         = new PDB_AtomName("O1",    Element.OXYGEN);
   public static final PDB_AtomName O1A        = new PDB_AtomName("O1A",   Element.OXYGEN);
   public static final PDB_AtomName O1D        = new PDB_AtomName("O1D",   Element.OXYGEN);
   public static final PDB_AtomName O2         = new PDB_AtomName("O2",    Element.OXYGEN);
   public static final PDB_AtomName O2_PRIME   = new PDB_AtomName("O2'",   Element.OXYGEN);
   public static final PDB_AtomName O2A        = new PDB_AtomName("O2A",   Element.OXYGEN);
   public static final PDB_AtomName O2D        = new PDB_AtomName("O2D",   Element.OXYGEN);
   public static final PDB_AtomName O3         = new PDB_AtomName("O3",    Element.OXYGEN);
   public static final PDB_AtomName O3_PRIME   = new PDB_AtomName("O3'",   Element.OXYGEN);
   public static final PDB_AtomName O4         = new PDB_AtomName("O4",    Element.OXYGEN);
   public static final PDB_AtomName O4_PRIME   = new PDB_AtomName("O4'",   Element.OXYGEN);
   public static final PDB_AtomName O5         = new PDB_AtomName("O5",    Element.OXYGEN);
   public static final PDB_AtomName O5_PRIME   = new PDB_AtomName("O5'",   Element.OXYGEN);
   public static final PDB_AtomName O6         = new PDB_AtomName("O6",    Element.OXYGEN);

   public static final PDB_AtomName P    = new PDB_AtomName("P",    Element.PHOSPHOROUS);
   public static final PDB_AtomName S    = new PDB_AtomName("S",    Element.SULFUR);
   public static final PDB_AtomName SD   = new PDB_AtomName("SD",   Element.SULFUR);
   public static final PDB_AtomName SG   = new PDB_AtomName("SG",   Element.SULFUR);
   public static final PDB_AtomName SE   = new PDB_AtomName("SE",   Element.SELENIUM);
   public static final PDB_AtomName CL1  = new PDB_AtomName("CL1",  Element.CHLORINE);
   public static final PDB_AtomName CL2  = new PDB_AtomName("CL2",  Element.CHLORINE);
   public static final PDB_AtomName XD1  = new PDB_AtomName("XD1",  null); // Ambiguous - no element can be assigned
   public static final PDB_AtomName XD2  = new PDB_AtomName("XD2",  null); // Ambiguous - no element can be assigned
   public static final PDB_AtomName XE1  = new PDB_AtomName("XE1",  null); // Ambiguous - no element can be assigned
   public static final PDB_AtomName XE2  = new PDB_AtomName("XE2",  null); // Ambiguous - no element can be assigned




   //##########################################################################
   // CONSTRUCTORS
   //##########################################################################

   //--------------------------------------------------------------------------
   public PDB_AtomName(String inName, Element inElement)
   {
      this(inName, null, inElement);
   }

   //--------------------------------------------------------------------------
   private PDB_AtomName(String inName, Character inAltLocationIndicator, Element inElement)
   {
      mName   = inName;
      mElement = inElement;
      mAltLocationIndicator = inAltLocationIndicator;

      if (sValueMap.containsKey(toKey()))
      {
         throw new RuntimeException("PDB atom name " + inName + " is already defined!");
      }

      sValueMap.put(toKey(), this);
   }

   //##########################################################################
   // PUBLIC METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   public static PDB_AtomName getInstance(String inName, Element inElement)
   {
      return getInstance(inName, null, inElement);
   }

   //--------------------------------------------------------------------------
   public static PDB_AtomName getInstance(String inName, Character inAltLocationIndicator, Element inElement)
   {
      String key = inName + (inAltLocationIndicator != null ? "." + inAltLocationIndicator : "");
      PDB_AtomName value = sValueMap.get(key);
      if (null == value)
      {
         value = new PDB_AtomName(inName, inAltLocationIndicator, inElement);
      }

      return value;
   }

   //--------------------------------------------------------------------------
   public static PDB_AtomName valueOf(String inString)
   {
      return sValueMap.get(inString.toUpperCase());
   }

   //--------------------------------------------------------------------------
   public static PDB_AtomName[] values()
   {
      return sValueMap.values().toArray(new PDB_AtomName[sValueMap.size()]);
   }

   //--------------------------------------------------------------------------
   public String getName()
   {
      return mName;
   }

   //--------------------------------------------------------------------------
   public Element getElement()
   {
      return mElement;
   }

   //--------------------------------------------------------------------------
   public Character getAltLocationIndicator()
   {
      return mAltLocationIndicator;
   }

   //--------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return getName();
   }

   //--------------------------------------------------------------------------
   @Override
   public int hashCode()
   {
      int hashcode = getName().hashCode();
      if (getAltLocationIndicator() != null)
      {
         hashcode += 31 * getAltLocationIndicator().hashCode();
      }

      return hashcode;
   }

   //--------------------------------------------------------------------------
   @Override
   public boolean equals(Object inObj2)
   {
      return inObj2 != null
             && 0 == compareTo(inObj2);
   }

   //--------------------------------------------------------------------------
   public int compareTo(Object inObj2)
   {
      int result = 1;

      if (inObj2 instanceof PDB_AtomName)
      {
         PDB_AtomName pdbName2 = (PDB_AtomName) inObj2;

         result = CompareUtil.compare(getName(), pdbName2.getName());

         if (0 == result)
         {
            result = CompareUtil.compare(mAltLocationIndicator, pdbName2.mAltLocationIndicator);
         }
      }

      return result;
   }

   //--------------------------------------------------------------------------
   private String toKey()
   {
      return getName() + (getAltLocationIndicator() != null ? "." + getAltLocationIndicator() : "");
   }

}

