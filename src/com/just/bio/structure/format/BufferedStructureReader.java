package com.just.bio.structure.format;


import com.hfg.util.StringBuilderPlus;
import com.hfg.util.collection.CollectionUtil;
import com.hfg.util.io.GZIP;
import com.just.bio.structure.Structure;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 Buffered molecule reader
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class BufferedStructureReader<T extends Structure>
{
   private ReadableStructureFormat<T> mFormatObj;
   private BufferedReader       mBufferedReader;
   private boolean              mEndOfContentReached;
   private String               mRecordStartLine;
   private int                  mNumRecordsParsed;

   private StringBuilderPlus    mUncompressedRecord = new StringBuilderPlus().setDelimiter("\n");
   private List<byte[]>         mCompressedRecordChunks;
   private int                  mCurrentRecordLength = 0;

   // How long the record should be before compression is used.
   private static int   sCompressionThreshold = 8 * 1024;

   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   //---------------------------------------------------------------------------
   public BufferedStructureReader(BufferedReader inReader, ReadableStructureFormat<T> inFormatObj)
   {
      mBufferedReader = inReader;
      mFormatObj      = inFormatObj;
   }

   //###########################################################################
   // PUBLIC METHODS
   //###########################################################################

   //---------------------------------------------------------------------------
   public void close()
         throws IOException
   {
      mBufferedReader.close();
   }

   //---------------------------------------------------------------------------
   public ReadableStructureFormat<T> getFormatObj()
   {
      return mFormatObj;
   }

   //---------------------------------------------------------------------------
   public synchronized boolean hasNext()
   {
      boolean result = false;
      if (! endOfContentReached())
      {
         if (0 == mCurrentRecordLength)
         {
            readNextRecord();
         }

         result = mCurrentRecordLength > 0;
      }

      return result;
   }

   //---------------------------------------------------------------------------
   public synchronized T next()
   {
      T nextSeq = null;
      if (0 == mCurrentRecordLength)
      {
         readNextRecord();
      }

      if (mCurrentRecordLength > 0)
      {
         nextSeq = mFormatObj.readRecord(getCurrentBufferedRecordReader());
//         mNextRecord.setLength(0); // Clear the raw record
         mCurrentRecordLength = 0;
      }

      return nextSeq;
   }

   //---------------------------------------------------------------------------
   public List<T> readAll()
   {
      List<T> seqs = new ArrayList<T>();
      while (hasNext())
      {
         seqs.add(next());
      }

      return seqs;
   }

   //--------------------------------------------------------------------------
   public BufferedReader getCurrentBufferedRecordReader()
   {
      InputStream seqStream = null;

      if (CollectionUtil.hasValues(mCompressedRecordChunks))
      {
         if (mUncompressedRecord.length() > 0)
         {
            mCompressedRecordChunks.add(GZIP.compress(mUncompressedRecord.toString()));
         }
         seqStream = new RecordStreamer();
      }
      else if (mUncompressedRecord.length() > 0)
      {
         seqStream = new ByteArrayInputStream(mUncompressedRecord.toString().getBytes());
      }

      return new BufferedReader(new InputStreamReader(seqStream));
   }

   //---------------------------------------------------------------------------
   protected boolean endOfContentReached()
   {
      return mEndOfContentReached;
   }

   //---------------------------------------------------------------------------
   private synchronized void readNextRecord()
   {
      if (! endOfContentReached())
      {
         // Start w/ a fresh record
         mUncompressedRecord = new StringBuilderPlus().setDelimiter("\n");
         mCompressedRecordChunks = null;
         mCurrentRecordLength = 0;

         if (mRecordStartLine != null)
         {
            mUncompressedRecord.appendln(mRecordStartLine);
         }

         try
         {
            String line;
            while ((line = mBufferedReader.readLine()) != null)
            {
               if (mFormatObj.isEndOfRecord(line))
               {
                  if (mFormatObj.hasJanusDelimiter())
                  {
                     if (0 == mNumRecordsParsed
                           && 0 == mCurrentRecordLength)
                     {
                        appendLineToCurrentRecord(line);
                     }
                     else
                     {
                        mRecordStartLine = line;
                        break;
                     }
                  }
                  else
                  {
                     appendLineToCurrentRecord(line);
                     break;
                  }
               }
               else
               {
                  appendLineToCurrentRecord(line);
               }
            }

            if (null == line)
            {
               mEndOfContentReached = true;
            }
         }
         catch (IOException e)
         {
            throw new StructureIOException(e);
         }
      }

      if (mCurrentRecordLength > 0)
      {
         mNumRecordsParsed++;
      }
   }

   //--------------------------------------------------------------------------
   // Note: inLine will not have a return at the end
   private void appendLineToCurrentRecord(String inLine)
         throws StructureIOException
   {
      mCurrentRecordLength += inLine.length() + 1;

      mUncompressedRecord.appendln(inLine);
      if (mUncompressedRecord.length() > sCompressionThreshold)
      {
         if (null == mCompressedRecordChunks)
         {
            mCompressedRecordChunks = new ArrayList<>();
         }

         mCompressedRecordChunks.add(GZIP.compress(mUncompressedRecord.toString()));
         mUncompressedRecord.setLength(0);
      }
   }

   //##########################################################################
   // INNER CLASSES
   //##########################################################################


   private class RecordStreamer extends InputStream
   {
      private String    mCurrentChunk;
      private int       mCurrentChunkIndex;
      private int       mCharIndex;
      private boolean   mDone = false;

      //-----------------------------------------------------------------------
      public RecordStreamer()
      {
         mCurrentChunkIndex = 0;
      }

      //-----------------------------------------------------------------------
      public int read()
      {
         return (mDone ? -1 : getNextChar());
      }

      //-----------------------------------------------------------------------
      private char getNextChar()
      {
         if (null == mCurrentChunk)
         {
            mCurrentChunk = GZIP.uncompressToString(mCompressedRecordChunks.get(mCurrentChunkIndex));

            mCharIndex = 0;
         }

         char nextChar = mCurrentChunk.charAt(mCharIndex++);

         if (mCharIndex >= mCurrentChunk.length())
         {
            // This is the last char in this chunk.
            mCurrentChunk = null;
            mCurrentChunkIndex++;
            if (mCurrentChunkIndex < 0 || mCurrentChunkIndex == mCompressedRecordChunks.size())
            {
               // This was the last chunk.
               mDone = true;
            }
         }

         return nextChar;
      }
   }
}
