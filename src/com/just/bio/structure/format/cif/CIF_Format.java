package com.just.bio.structure.format.cif;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

import com.globalphasing.startools.StarToken;
import com.globalphasing.startools.StarTokenTypes;
import com.globalphasing.startools.StarTokeniser;

import com.hfg.chem.Element;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;

import com.just.bio.structure.AminoAcid3D;
import com.just.bio.structure.AtomGroup;
import com.just.bio.structure.AtomGroupType;
import com.just.bio.structure.AtomImpl;
import com.just.bio.structure.AtomicCoordinates;
import com.just.bio.structure.Chain;
import com.just.bio.structure.Model;
import com.just.bio.structure.Structure;
import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.format.ReadableStructureFormatBase;
import com.just.bio.structure.format.StructureFactory;
import com.just.bio.structure.format.StructureIOException;
import com.just.bio.structure.format.pdb.PDB_AtomName;
import com.just.bio.structure.pdb.*;
import com.just.bio.structure.pdb.pojo.*;


//Self-Defining Text Archive and Retrieval (STAR):
//   https://en.wikipedia.org/wiki/Self-defining_Text_Archive_and_Retrieval
//   http://www.iucr.org/__data/assets/text_file/0014/11417/star.5.txt

//parsing, etc
//   http://mmcif.wwpdb.org/docs/pdb_to_pdbx_correspondences.html
//   http://mmcif.wwpdb.org/dictionaries/mmcif_pdbx_v40.dic/Index/
//   http://www.globalphasing.com/startools/
//   http://www.globalphasing.com/startools/javadoc/

//PDBx/mmCIF (cif)  format
//   http://mmcif.wwpdb.org/docs/tutorials/mechanics/pdbx-mmcif-syntax.html
//   https://en.wikipedia.org/wiki/Crystallographic_Information_File

/**
 * Structure format object for CIF-formatted files.
 * @param <T> The Structure class that will result from parsing
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class CIF_Format<T extends Structure> extends ReadableStructureFormatBase<T>
{


    //private static final String[] DESCRIPTIVE_TOKEN_TYPES = new String[]{
    // "MULTILINE", "COMMENT", "GLOBAL", "SAVE_FRAME", "SAVE_FRAME_REF", "LOOP_STOP", "DATA_BLOCK", "LOOP", "BAD_CONSTRUCT",
    // "DATA_NAME", "SQUOTE_STRING", "DQUOTE_STRING", "NULL", "UNKNOWN", "SQUARE_BRACKET", "STRING", "BAD_TOKEN"};



    //set to false in order to SKIP lines that have '?' as their value
    //http://mmcif.wwpdb.org/docs/tutorials/mechanics/pdbx-mmcif-syntax.html
    //    "The question mark (?) is used to mark an item value as missing. A period (.) may
    //    be used to identify that there is no appropriate value for the item or that a value
    //    has been intentionally omitted."
    //note: this is implemented for both lines and loops
    public static boolean includeLinesWithBlankValues = false;

    //some categories have values that are tooooo long to pull out of the file.
    //this list identifies the category.key of items to skip in both lines and loops.
    //note: it only excludes the field if it is longer than like 25 characters.
    //SEE transformCifValueRowToLinkedList() for where these are excluded
    public static Set<String> categoryKeyExcludeSet;
    static {
        categoryKeyExcludeSet = new HashSet<>();
        categoryKeyExcludeSet.add("_entity_poly.pdbx_seq_one_letter_code");
        categoryKeyExcludeSet.add("_entity_poly.pdbx_seq_one_letter_code_can");
        categoryKeyExcludeSet.add("_entity_poly.pdbx_strand_id");
    }

    private AtomicCoordinates mAtomicCoordinates;

    private Map<Integer, LinkedHashSet<String>> mChainIdsMap;

    //these two maps will be populated with the valid data from a PDBx/mmCIF file, after reading the
    //file into these maps they will be parsed by the second half ouf our logic. party on.
    private Map<String, CifLoop> mLoops = new LinkedHashMap<>();
    private Map<String, Map<String, String>> mItems = new LinkedHashMap<>();


    private static String COMMENT_CHAR = "#";
    private static String MULTILINE_CHAR = ";";
    private static String DATA_PREFIX_CHAR = "_";
    private static String NEWLINE = System.getProperty("line.separator");

    //list of categories parsed from the file. this ensures we don't spend time parsing categories that aren't used
    private static Set<String> categoryIncludeSet;

    static {
        categoryIncludeSet = new HashSet<>();

        //METADATA SECTION
        categoryIncludeSet.add("_entry");                      //HEADER    _entity.id                          (pdb identifier)
        categoryIncludeSet.add("_struct_keywords");            //HEADER    _entity.pdbx_keywords               (classification)
        categoryIncludeSet.add("_database_PDB_rev");           //HEADER    _database_PDB_rev.date_original     (deposit date)

        categoryIncludeSet.add("_struct");                     //TITLE    _struct.title   -&-   MDLTYP  _struct.pdbx_model_type_details
        categoryIncludeSet.add("_pdbx_database_PDB_obs_spr");  //OBSLTE
        categoryIncludeSet.add("_database_PDB_rev_record");    //REVDAT
        categoryIncludeSet.add("_pdbx_database_related");      //SPILT
        categoryIncludeSet.add("_database_PDB_caveat");        //CAVEAT
        categoryIncludeSet.add("_entity");                     //COMPND
        categoryIncludeSet.add("_entity_poly");                //COMPND

        categoryIncludeSet.add("_entity_src_nat");             //SOURCE
        categoryIncludeSet.add("_entity_src_gen");             //SOURCE
        categoryIncludeSet.add("_pdbx_entity_src_syn");        //SOURCE

        categoryIncludeSet.add("_struct_keywords");            //KEYWDS
        categoryIncludeSet.add("_exptl");                      //EXPDTA
        //categoryIncludeSet.add("");                          //NUMMDL       //todo
        //categoryIncludeSet.add("_struct");                     //MDLTYP     //same category as title
        categoryIncludeSet.add("_audit_author");               //AUTHOR
        categoryIncludeSet.add("_pdbx_database_PDB_obs_spr");  //SPRSDE

        categoryIncludeSet.add("_citation");                   //JRNL
        categoryIncludeSet.add("_citation_author");            //JRNL
        categoryIncludeSet.add("_citation_editor");            //JRNL

        //REMARK SECTION
        //categoryIncludeSet.add("????");                        //REMARKS  //todo ... somehow.

        //PRIMARY SECTION
        categoryIncludeSet.add("_entity_poly_seq");            //SEQRES
        categoryIncludeSet.add("_pdbx_poly_seq_scheme");       //SEQRES

        //CONNECTIVITY SECTION
        categoryIncludeSet.add("_struct_conn");                //SSBOND
        categoryIncludeSet.add("_struct_conn_type");           //SSBOND

        //COORDINATE SECTION
        categoryIncludeSet.add("_atom_site");                  //ATOM, HETATM
    }



    //##################################################################################################################
    // CONSTRUCTORS
    //##################################################################################################################

    //---------------------------------------------------------------------------
    public CIF_Format(StructureFactory<T> inStructureFactory) {
        super(inStructureFactory);
    }


    //##################################################################################################################
    // PUBLIC METHODS
    //##################################################################################################################

    //---------------------------------------------------------------------------
    public StructureFormatType getStructureFormatType()
    {
        return StructureFormatType.CIF;
    }

    //---------------------------------------------------------------------------
    @Override
    public T readRecord(BufferedReader inReader)
          throws StructureIOException
    {

        if (null == getStructureFactory())
        {
            throw new StructureIOException("No Structure factory has been specified!");
        }

        //read in file, than parse the data into a Structure object
        readEntireCifFile(inReader);
        T structure = parseEntireCifFile();

        return structure;
    }


    //##################################################################################################################
    // PRIMARY, HIGH-LEVEL METHODS (read file, parse file)
    //##################################################################################################################


    private void readEntireCifFile(BufferedReader inReader)
          throws StructureIOException
    {

        //typically only one datablock per file, but if we find another... well, for now throw an error (see while loop)
        List<String> dataBlocks = new ArrayList<>();

        try
        {
            List<StarToken> tokens;
            while ((tokens = nextTokens(inReader)) != null)
            {
                for (StarToken token : tokens)
                {
                    //for every token found in the file....

                    if (token.getType() == StarTokenTypes.TOKEN_LOOP)
                    {
                        //if we're starting a loop, get the entire loop (loops not in our 'include list' will not be returned)
                        CifLoop cifLoop = nextLoop(inReader);
                        if (cifLoop != null)
                        {
                            String loopCategory = cifLoop.getCategoryName();
                            mLoops.put(loopCategory, cifLoop);
                            //System.out.println("LOOP category: " + loopCategory);
                        }
                    }
                    else if (token.getType() == StarTokenTypes.TOKEN_DATA_NAME)
                    {
                        //else it's probably part of a key/value pair... (note: it's likely that multiple consecutive tokens are related by a category)
                        String[] split = token.getValue().split("\\.");
                        String category = split[0];

                        //categories not in our 'include list' will not be returned
                        Map<String, String> hm = nextCategoryLines(inReader, tokens);
                        if(hm != null)
                        {
                            if (mItems.containsKey(category))
                            {
                                Map<String, String> existing = mItems.get(category);
                                existing.putAll(hm);
                            }
                            else
                            {
                                mItems.put(category, hm);
                            }
                            //System.out.println("DATA category: " + category);
                        }
                        break;

                    }
                    else if (token.getType() == StarTokenTypes.TOKEN_DATA_BLOCK)
                    {
                        //it's possible to have multiple DATA_BLOCKs per file, but this is unlikely the with these xPDB/mmCIF files;
                        //however if we see more than one DATA_BLOCK token we'll throw an error and make future russ deal with it.
                        dataBlocks.add(token.getValue());
                        if(dataBlocks.size() > 1)
                        {
                            throw new StructureIOException("Multiple 'data blocks' (aka Structures) found in file [" + StringUtil.join(dataBlocks, ", ") +
                                    "]. Although this is valid syntax, the current version of corestruct does not yet support it.");
                        }

                    }
                    else if (StarTokenTypes.starErrorToken(token.getType()))
                    {
                        //error tokens are "BAD_CONSTRUCT" or "BAD_TOKEN
                        throw new StructureIOException("Error parsing structure file; bad token found: [" + token.toString() + "]");

                    }
                    else if (token.getType() == StarTokenTypes.TOKEN_GLOBAL
                            || token.getType() == StarTokenTypes.TOKEN_LOOP_STOP
                            || token.getType() == StarTokenTypes.TOKEN_SAVE_FRAME
                            || token.getType() == StarTokenTypes.TOKEN_SAVE_FRAME_REF
                            )
                    {
                        //http://www.globalphasing.com/startools/javadoc/index.html?constant-values.html
                        throw new StructureIOException("Error parsing structure file; invalid token found: [" + token.toString() + "]. Token type forbidden in CIF files");
                    }

                }
            }
        }
        catch (Exception e)
        {
            if (e instanceof StructureIOException)
            {
                throw (StructureIOException) e;
            }
            else
            {
                throw new StructureIOException(e);
            }
        }
    }


    private T parseEntireCifFile()
          throws StructureIOException
    {
        T structure;

        //first, lets setup our entityId to strandId [chain] map - this is specific to CIF files
        setupChainIdMap();

        try {
            //------------------------------------------------------------------------------------------
            //Metadata Section
            PdbSectionMetadata pdbSectionMetadata = new PdbSectionMetadata();

            pdbSectionMetadata.setPdbIdentifier(parseStringItemByCategoryKey("_struct", "entry_id"));
            pdbSectionMetadata.setClassification(parseStringItemByCategoryKey("_struct_keywords", "pdbx_keywords"));
            pdbSectionMetadata.setTitle(parseStringItemByCategoryKey("_struct", "title"));
            pdbSectionMetadata.setExpdta(parseStringItemByCategoryKey("_exptl", "method"));
            pdbSectionMetadata.setMdltyp(parseStringItemByCategoryKey("_struct", "pdbx_model_type_details"));

            pdbSectionMetadata.setDepositionDate(parseDepositionDate()); //_database_PDB_rev.date_original
            pdbSectionMetadata.setCaveat(parseStringFromItemOrLoop("_database_PDB_caveat", "text"));

            pdbSectionMetadata.setCompnds(parseCompnd());
            pdbSectionMetadata.setSources(parseSource());

            pdbSectionMetadata.setKeyWrds(parseKeywords());
            pdbSectionMetadata.setAuthors(parseAuthors());

            //pdbSectionMetadata.setNummdl(...); //todo

            Map.Entry<Date, List<String>> obsletData = parseLineage("OBSLTE");
            if(obsletData!=null)
            {
                pdbSectionMetadata.setObslteDate(obsletData.getKey());
                pdbSectionMetadata.setObsltes(obsletData.getValue());
            }

            Map.Entry<Date, List<String>> sprsdeData = parseLineage("SPRSDE");
            if(sprsdeData != null)
            {
                pdbSectionMetadata.setSprsdeDate(sprsdeData.getKey());
                pdbSectionMetadata.setSprsdes(sprsdeData.getValue());
            }

            pdbSectionMetadata.setPdbRevdats(parseRevdats());
            pdbSectionMetadata.setPdbJrnls(parseJrnls());
            pdbSectionMetadata.setSplits(parseSplits());

            //------------------------------------------------------------------------------------------
            //Remark Section
            PdbSectionRemark pdbSectionRemark = new PdbSectionRemark();
            //todo

            //------------------------------------------------------------------------------------------
            //Primary Section
            PdbSectionPrimary pdbSectionPrimary = new PdbSectionPrimary();
            pdbSectionPrimary.setPdbSeqress(parseSeqres());

            //------------------------------------------------------------------------------------------
            //Connectivity Section (todo rename linkage section everywhere)
            PdbSectionConnectivity pdbSectionConnectivity = new PdbSectionConnectivity();
            //todo


            //create the structure object
            structure = getStructureFactory().createStructureObj();
            structure.setMetadata(pdbSectionMetadata);
            structure.setRemark(pdbSectionRemark);
            structure.setPrimary(pdbSectionPrimary);
            structure.setConnectivity(pdbSectionConnectivity);

            if (mChainIdsMap.size() == mAtomicCoordinates.getModels().size())
            {
                for (int modelIdx = 0; modelIdx < mAtomicCoordinates.getModels().size(); modelIdx++)
                {
                    Model model = mAtomicCoordinates.getModels().get(modelIdx);
                    Iterator<String> chainIdIterator = mChainIdsMap.get(modelIdx + 1).iterator();

                    for (Chain chain : model.getChains())
                    {
                        if(chainIdIterator.hasNext())
                        {
                            chain.setName(chainIdIterator.next());
                        }
                    }
                }
            }
            else
            {
                for (int modelIdx = 0; modelIdx < mAtomicCoordinates.getModels().size(); modelIdx++)
                {
                    Model model = mAtomicCoordinates.getModels().get(modelIdx);
                    Iterator<Chain> chainModelIterator = model.getChains().iterator();

                    Iterator<LinkedHashSet<String>> chainIdIterator = mChainIdsMap.values().iterator();

                    while(chainModelIterator.hasNext() && chainIdIterator.hasNext())
                    {
                        LinkedHashSet<String> chainIdIteratorNext = chainIdIterator.next();

                        for (String chainId : chainIdIteratorNext)
                        {
                            Chain chain = chainModelIterator.next();
                            chain.setName(chainId);
                        }
                    }
                }
            }

            mAtomicCoordinates.setSeqRes(structure.getPrimary().getPdbSeqress());
            
            structure.setAtomicCoordinates(mAtomicCoordinates);

            // Clear to save space in the format obj.
            mAtomicCoordinates = null;
        }
        catch (Exception e)
        {
            if (e instanceof StructureIOException)
            {
                throw (StructureIOException) e;
            }
            else
            {
                throw new StructureIOException(e);
            }
        }

        return structure;
    }




    //##################################################################################################################
    // HELPER METHODS for READING FILE
    //##################################################################################################################

    /**
     * return a list of all tokens on a single line, OR a single element list containing a multi line token
     * (this is basically our interpreter between reading the file as lines and StarTokens)
     * @param inReader
     */
    private List<StarToken> nextTokens(BufferedReader inReader)
          throws IOException
    {

        String line;
        if((line = inReader.readLine()) == null)
        {
            return null;
        }

        //skip any comment lines
        if(line.startsWith(COMMENT_CHAR))
        {
            return nextTokens(inReader);
        }

        //process multiple line tokens (typically a single token of type MULTIPLINE)
        if(line.startsWith(MULTILINE_CHAR))
        {
            StringBuilder multiLine = new StringBuilder();
            do
            {
                multiLine.append(line + NEWLINE);
            } while (!(line = inReader.readLine()).equals(MULTILINE_CHAR));

            return tokensOfLine(multiLine.toString() + MULTILINE_CHAR + NEWLINE); //read until end character (semicolon)
        }

        return tokensOfLine(line);

    }



    /**
     * based on a 'line' return the token(s) that are a part of it
     * @param line
     * @return
     */
    private List<StarToken> tokensOfLine(String line) {

        StarTokeniser tokeniser = new StarTokeniser();
        tokeniser.startMatching(line);

        List<StarToken> starTokens = new ArrayList<>(1);

        while (tokeniser.hasMoreTokens()) {
            StarToken token = tokeniser.nextToken();
            starTokens.add(token);
        }
        return starTokens;

    }



    /**
     * the next N lines/tokens represent a loop (column name(s) first, then data rows)
     * @param inReader
     * @return
     */
    private CifLoop nextLoop(BufferedReader inReader)
          throws IOException
    {
        //get the column names for this loop block
        List<String> columnNames = nextColumnNames(inReader);

        String category = columnNames.get(0).split("\\.")[0];
        if(!categoryIncludeSet.contains(category))
        {
            skipLoop(inReader);
            return null;
        }

        //ATOM/HETATM records (category: _atom_site) are treated differently than other loops.
        if(category.equalsIgnoreCase("_atom_site"))
        {
            mAtomicCoordinates = addNextPdbAtomRows(inReader, columnNames);
            return null;
        }
        else
        {
            //create the loop block (starting with column names, then add in data values, aka rows)
            CifLoop cifLoop = new CifLoop(columnNames);
            addNextRows(inReader, cifLoop);
            return cifLoop;
        }
    }


    /**
     * read in all consecutive column names until the token type changes to data
     * @param inReader
     * @return
     * @throws IOException
     */
    private List<String> nextColumnNames(BufferedReader inReader) throws IOException {

        List<String> columnNames = new ArrayList<>();

        List<StarToken> tokens;

        while ((tokens = nextTokens(inReader)) != null) {
            for (StarToken token : tokens) {

                if(token.getValue().startsWith(DATA_PREFIX_CHAR)){
                    columnNames.add(token.getValue());
                    inReader.mark(5000);
                } else {
                    //end of column names
                    inReader.reset();
                    return columnNames;
                }
            }
        }

        return columnNames;
    }

    /**
     * after column headers are added, this will get all the data 'rows' until...
     * @param inReader
     * @param cifLoop
     * @throws IOException
     */
    private void addNextRows(BufferedReader inReader, CifLoop cifLoop) throws IOException {

        int columnCount = cifLoop.getColumnCount();

        List<String> rowValues = new ArrayList<>(columnCount);
        List<StarToken> tokens;

        while ((tokens = nextTokens(inReader)) != null) {
            for (StarToken token : tokens) {

                if(token.getValue().startsWith(DATA_PREFIX_CHAR)
                        || token.getType() == StarTokenTypes.TOKEN_LOOP
                        || token.getType() == StarTokenTypes.TOKEN_SAVE_FRAME){  //todo add in handling for BAD_TOKEN/CONSTRUCT.
                    //end of rows
                    inReader.reset();
                    return;

                } else {

                    //create rows, one token (aka column value) at a time.
                    rowValues.add(token.getValue());

                    //if we have the correct count of values for this row, add the row to the loop object (and reset the row)
                    if(rowValues.size() == columnCount){
                        cifLoop.addRow(rowValues);
                        rowValues = new ArrayList<>(columnCount);
                    }

                    inReader.mark(9999);
                }
            }
        }

    }


    /**
     * as we read thru the file, when we get to the ATOM/HETATM (category: _atom_site) it will be parsed differently than the other loops
     * instead of a temp place to hold the values, these go directly into a list of atoms.
     * <div>
     *    Ex:
     *    <pre>
     *     loop_
     *     _atom_site.group_PDB
     *     _atom_site.id
     *     _atom_site.type_symbol
     *     _atom_site.label_atom_id
     *     _atom_site.label_alt_id
     *     _atom_site.label_comp_id
     *     _atom_site.label_asym_id
     *     _atom_site.label_entity_id
     *     _atom_site.label_seq_id
     *     _atom_site.pdbx_PDB_ins_code
     *     _atom_site.Cartn_x
     *     _atom_site.Cartn_y
     *     _atom_site.Cartn_z
     *     _atom_site.occupancy
     *     _atom_site.B_iso_or_equiv
     *     _atom_site.pdbx_formal_charge
     *     _atom_site.auth_seq_id
     *     _atom_site.auth_comp_id
     *     _atom_site.auth_asym_id
     *     _atom_site.auth_atom_id
     *     _atom_site.pdbx_PDB_model_num
     *     ATOM   1     O "O5'"  . DG  A 1 1  ? 2.503   19.951  10.203 1.00 0.00 ? 1  DG  A "O5'"  1
     *     </pre>
     * </div>
     * @param inReader
     */
    private AtomicCoordinates addNextPdbAtomRows(BufferedReader inReader, List<String> columnNames)
          throws IOException
    {
        AtomicCoordinates atomicCoordinates = new AtomicCoordinates();

        int colIndexGroup = columnNames.indexOf("_atom_site.group_PDB");
        int colIndexSerial = columnNames.indexOf("_atom_site.id");
        int colIndexAtomName = columnNames.indexOf("_atom_site.label_atom_id");    //auth_atom_id ??
        int colIndexResName = columnNames.indexOf("_atom_site.label_comp_id");     //auth_comp_id ??
        int colIndexChainId = columnNames.indexOf("_atom_site.auth_asym_id");      // *** label_asym_id seems to be the wrong column.
        int colIndexResSeq = columnNames.indexOf("_atom_site.label_seq_id");
        int colIndexIcode = columnNames.indexOf("_atom_site.pdbx_PDB_ins_code");
        int colIndexCoordX = columnNames.indexOf("_atom_site.Cartn_x");
        int colIndexCoordY = columnNames.indexOf("_atom_site.Cartn_y");
        int colIndexCoordZ = columnNames.indexOf("_atom_site.Cartn_z");
        int colIndexElement = columnNames.indexOf("_atom_site.type_symbol");
        int colModelNum = columnNames.indexOf("_atom_site.pdbx_PDB_model_num");

        AtomGroup currentGroup = null;

        String line;
        while ((line = inReader.readLine()) != null)
        {
            if (StringUtil.isSet(line))
            {
                if(! line.startsWith("ATOM")
                   && ! line.startsWith("HETATM"))
                {
                    // Done with atom section
                    break;
                }

                //split each line, then grab the approriate fields to create a PdbAtom record for our mAtoms list
                String[] fields = line.split("\\s+");

                int modelNum = Integer.parseInt(fields[colModelNum]);
                Model model;
                if (modelNum > atomicCoordinates.getModels().size())
                {
                    model = new Model();
                    atomicCoordinates.addModel(model);
                }
                else
                {
                    model = atomicCoordinates.getModels().get(modelNum - 1);
                }



                Integer serial = StructureUtils.stringToInteger(fields[colIndexSerial]);
                PDB_AtomName atomName = PDB_AtomName.valueOf(fields[colIndexAtomName]);
                String resName = fields[colIndexResName];
                String chainName = fields[colIndexChainId];
                Integer resSeq = StructureUtils.stringToInteger(fields[colIndexResSeq], true);
                Character iCode = StructureUtils.stringToCharacter(fields[colIndexIcode]);
                double coordinateX = StructureUtils.stringToDouble(fields[colIndexCoordX]);
                double coordinateY = StructureUtils.stringToDouble(fields[colIndexCoordY]);
                double coordinateZ = StructureUtils.stringToDouble(fields[colIndexCoordZ]);
                String element = fields[colIndexElement];

                Chain chain = model.getChain(chainName);
                if (null == chain)
                {
                    chain = new Chain(chainName);
                    model.addChain(chain);
                }

                if (null == currentGroup
                    || ! currentGroup.name().equalsIgnoreCase(resName))
                {
                    AtomGroupType groupType;
                    if (AminoAcid3D.valueOf(resName) != null)
                    {
                        groupType = AtomGroupType.AMINO_ACID;
                    }
                    else if (fields[colIndexGroup].equalsIgnoreCase("ATOM"))
                    {
                        groupType = AtomGroupType.NUCLEOTIDE;
                    }
                    else
                    {
                        groupType = AtomGroupType.HETERO_ATOM;
                    }

                    currentGroup = new AtomGroup(resName, groupType)
                          .setResidueNum(resSeq)
                          .setInsertCode(iCode);

                    chain.addAtomGroup(currentGroup);
                }

                currentGroup.addAtom(new AtomImpl(atomName)
                                           .setElement(Element.valueOf(element))
                                           .setCoords(coordinateX, coordinateY, coordinateZ)
                                           .setAtomGroup(currentGroup)
                                           .setSerialNum(serial));

            }
        }

        return atomicCoordinates;
    }



    /**
     * this method is used to skip a section bordered by a token (aka the "SAVE_" token).
     * do no processing on any lines/tokens until the endToken is found
     *
     * @param inReader
     * @param endToken
     * @throws IOException
     */
    private void skipUntilTokenType(BufferedReader inReader, StarToken endToken) throws IOException {

        List<StarToken> tokens;

        while ((tokens = nextTokens(inReader)) != null) {
            for (StarToken token : tokens) {

                if(token.getType() == endToken.getType()){
                    //end of tokens we're skipping.
                    return;
                }
            }
        }
    }

    /**
     * used to skip to the end of a loop - note the column headers have been read, and now we're in the rows portion.
     * @param inReader
     * @throws IOException
     */
    private void skipLoop(BufferedReader inReader) throws IOException {

        List<StarToken> tokens;

        while ((tokens = nextTokens(inReader)) != null) {
            for (StarToken token : tokens) {

                if(token.getValue().startsWith(DATA_PREFIX_CHAR)
                        || token.getType() == StarTokenTypes.TOKEN_LOOP
                        || token.getType() == StarTokenTypes.TOKEN_SAVE_FRAME){  //todo add in handling for BAD_TOKEN/CONSTRUCT.
                    //end of rows
                    inReader.reset();
                    return;

                } else {
                    inReader.mark(5000);
                }

            }
        }
    }


    /**
     * skip all consecutive lines within the same category
     * @param inReader
     * @param unwantedCategory
     * @throws IOException
     */
    private void skipCategory(BufferedReader inReader, String unwantedCategory) throws IOException {

        List<StarToken> tokens;

        inReader.mark(5000);

        while ((tokens = nextTokens(inReader)) != null) {
            for (StarToken token : tokens) {

                String tokenValue = token.getValue();

                if(tokenValue.startsWith(DATA_PREFIX_CHAR)){
                    String category = tokenValue.split("\\.")[0];
                    if(!category.equalsIgnoreCase(unwantedCategory)){
                        inReader.reset();
                        return;
                    }
                }
                else if (token.getType() == StarTokenTypes.TOKEN_LOOP
                        || token.getType() == StarTokenTypes.TOKEN_SAVE_FRAME){  //todo add in handling for BAD_TOKEN/CONSTRUCT. TODO handle LOOP_END?
                    inReader.reset();
                    return;

                } else {
                    inReader.mark(5000);
                }

            }
        }
    }



    /**
     * based on a startTokens related to a category, return all items until the category end
     * @param inReader
     * @param startTokens
     * @return
     */
    private Map<String, String> nextCategoryLines(BufferedReader inReader, List<StarToken> startTokens) throws IOException {

        //it may be that the value token is on the next line (multi_line_token)
        if(startTokens.size() == 1){
            List<StarToken> tokens = nextTokens(inReader);
            startTokens.add(tokens.get(0));
        }

        StarToken categoryKeyToken = startTokens.get(0);
        StarToken valueToken = startTokens.get(1);

        String[] split = categoryKeyToken.getValue().split("\\.");
        String currentCategory = split[0];
        String category = split[0];
        String key = split[1];

        if(!categoryIncludeSet.contains(currentCategory)){
            skipCategory(inReader, currentCategory);
            return null;
        }

        //create hm and put first one in
        Map<String, String> hm = new LinkedHashMap<>();
        hm.put(key, valueToken.getValue());

        inReader.mark(5000);

        //loop thru tokens adding any that are the same category
        List<StarToken> tokens;
        while ((tokens = nextTokens(inReader)) != null) {
            for (StarToken token : tokens) {

                //category.key
                if (token.getType() == StarTokenTypes.TOKEN_DATA_NAME) {

                    split = token.getValue().split("\\.");
                    category = split[0];

                    //if this next 'line' is in the same category, just keep addin' 'em up
                     if(category.equalsIgnoreCase(currentCategory)) {
                         key = split[1];

                     } else {
                         inReader.reset();
                         return hm;
                     }

                } else if(StarTokenTypes.dataToken(token.getType())){

                    String value = token.getValue();
                    if(includeLinesWithBlankValues || (!value.equals("?")  && !value.equals("."))) {
                        hm.put(key, value);
                    }

                } else {
                    inReader.reset();
                    return hm;
                }
                inReader.mark(5000);
            }
        }
        inReader.reset();
        return hm;

    }



    //##################################################################################################################
    // HELPER METHODS for PARSING METHODS
    //##################################################################################################################




    /**
     * helper method to get all items if they are coming from more than one category
     * @param items
     * @param categories
     * @return
     */
    private Map<String, String> getAllItems(Map<String, LinkedHashMap<String, String>> items, List<String> categories){

        if(!CollectionUtil.hasValues(categories)){
            return null;
        }

        Map<String, String> theseItems = new LinkedHashMap<>();
        for(String category : categories){
            Map<String, String> foundItems = items.get(category);
            if(CollectionUtil.hasValues(foundItems)){
                theseItems.putAll(items.get(category));
            }
        }

        if(CollectionUtil.hasValues(theseItems)){
            return theseItems;
        } else {
            return null;
        }

    }


    /**
     *
     * @param category
     * @param key
     * @return
     */
    private String parseStringItemByCategoryKey(String category, String key){
        Map<String, String> foundItems = mItems.get(category);
        if(CollectionUtil.hasValues(foundItems)){
            String foundItem = foundItems.get(key);
            if(StringUtil.isSet(foundItem)){
                return foundItem;
            }
        }
        return null;
    }

    private Date parseDateItemByCategoryKey(String category, String key){
        String stringDate = parseStringItemByCategoryKey(category, key);
        if(StringUtil.isSet(stringDate)){
            return StructureUtils.parse_yyyy_MM_dd_Date(stringDate);
        }
        return null;
    }

    private CifLoop getLoopByCategory(String category){
        CifLoop loop = mLoops.get(category);
        return loop;
    }

    /**
     * parse a single string from an item based on it's category and key... or if it is found in a loop, concatenate
     * the key value into a single string for each of the 'rows'
     * @param category
     * @param key
     * @return
     */
    private String parseStringFromItemOrLoop(String category, String key){

        //first check for match in items
        String itemFound = parseStringItemByCategoryKey(category, key);
        if(StringUtil.isSet(itemFound)){
            return itemFound;
        }

        //if not yet found, look in loops - if key is found in the category, append each iteration of the value
        CifLoop loop = getLoopByCategory(category);
        if(loop != null){
            StringBuilder sb = new StringBuilder();
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){
                sb.append(getTokenValueFromRow(row, key));
            }
            if(StringUtil.isSet(sb.toString())){
                return sb.toString();
            }
        }
        return null;
    }


    /**
     * find deposition date based on first loop value: _database_PDB_rev.date_original
     * @return
     */
    private Date parseDepositionDate(){
        CifLoop revLoop = getLoopByCategory("_database_PDB_rev");
        if(null == revLoop){
            return null;
        }
        //iterate through the rows until we find the first that has a date value in the 'date_original' column
        List<LinkedHashMap<String, String>> rows = revLoop.getRows();
        for(Map<String, String> row : rows){
            Date d = StructureUtils.parse_yyyy_MM_dd_Date(getTokenValueFromRow(row, "date_original"));
            if(d!=null){
                return d;
            }
        }
        return null;
    }


    /**
     * lineage data may be stored in items or loops.
     * @param lineageType
     * @return
     */
    private Map.Entry<Date, List<String>> parseLineage(String lineageType){

        String pdbIdKey = "OBSLTE".equalsIgnoreCase(lineageType) ? "pdb_id" : "replace_pdb_id";
        String category = "_pdbx_database_PDB_obs_spr";

        //first see if lineage data (_pdbx_database_PDB_obs_spr) is stored in items

        String lineageTypeFound = parseStringItemByCategoryKey(category, "id");
        if(StringUtil.isSet(lineageTypeFound)){
            if(lineageTypeFound.equalsIgnoreCase(lineageType)){

                //found! parse the date and PDB IDs for this lineagetype
                Date lineageDate = parseDateItemByCategoryKey(category, "date");
                String pdbIdsString = parseStringItemByCategoryKey(category, pdbIdKey);

                //return found data
                if(StringUtil.isSet(pdbIdsString)){
                    List<String> pdbIds = Arrays.asList(pdbIdsString.split("\\s+"));
                    return new AbstractMap.SimpleEntry<>(lineageDate, pdbIds);
                } else {
                    return new AbstractMap.SimpleEntry<>(lineageDate, null);
                }

            } else {
                //lineage was found, but it's the wrong type (aka looking for OBSLTE, but found SPRSDE - or vise-versa)
                return null;
            }
        }

        //lineage data was NOT found in items.. next look for it in loops.
        CifLoop loop = getLoopByCategory(category);
        if(loop != null){
            //iterate through the rows until we find the first that has an id value that equals the lineageType
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){
                if(lineageType.equalsIgnoreCase(getTokenValueFromRow(row, "id"))){

                    //found! parse the date and PDB IDs for this lineagetype
                    Date lineageDate = StructureUtils.parse_yyyy_MM_dd_Date(getTokenValueFromRow(row, "date"));
                    String pdbIdsString = getTokenValueFromRow(row, pdbIdKey);

                    //return found data
                    if(StringUtil.isSet(pdbIdsString)){
                        List<String> pdbIds = Arrays.asList(pdbIdsString.split("\\w"));
                        return new AbstractMap.SimpleEntry<>(lineageDate, pdbIds);
                    } else {
                        return new AbstractMap.SimpleEntry<>(lineageDate, null);
                    }

                }
            }
        }

        //nope, no lineage data found
        return null;

    }


    /**
     * return a list of PDB IDs related to splits (_pdbx_database_related)
     * @return
     */
    private List<PdbSplit> parseSplits(){
        CifLoop loop  = getLoopByCategory("_pdbx_database_related");
        if(loop != null){

            List<PdbSplit> pdbSplits = new ArrayList<>();

            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){
                //if(getTokenValueFromRow(row, "db_name").equalsIgnoreCase("PDB")){
                    String dbId = getTokenValueFromRow(row, "db_id");
                    String dbName = getTokenValueFromRow(row, "db_name");
                    String dbContentType = getTokenValueFromRow(row, "content_type");
                    String dbDetail = getTokenValueFromRow(row, "detail");

                    pdbSplits.add(new PdbSplit(dbId, dbName, dbContentType, dbDetail));
                //}
            }
            if(CollectionUtil.hasValues(pdbSplits)){
                return pdbSplits;
            }
        }
        return null;
    }



    /**
     * find all authors (_audit_author.name) in either loops or items
     * @return
     */
    private List<String> parseAuthors(){
        List<String> lines = findAllForCategoryAndKey("_audit_author", "name");
        if(CollectionUtil.hasValues(lines)){
            return lines;
        }
        return null;
    }


    /**
     * parse PdbCompnds from either loops or items
     * @return
     */
    private List<PdbCompnd> parseCompnd(){

        //first create a LinkedHash[Map] of all PdbCompnds
        Map<String, LinkedList<String>> compndDatas = parseCategoryIntoMap("_entity", "id");

        Map<Integer, PdbCompnd> pdbCompnds = new LinkedHashMap<>();

        //transform the Linked HashMap (from loops or items) into list of PdbCompnd objects
        if(CollectionUtil.hasValues(compndDatas)){

            for (Map.Entry<String, LinkedList<String>> compnd : compndDatas.entrySet()) {
                Integer molId = StructureUtils.findLastInteger(compnd.getKey());
                pdbCompnds.put(molId, new PdbCompnd(molId, compnd.getKey(), compnd.getValue()));
            }

        }


        //CIF files have their 'COMPND' data in a couple locations.. so we'll append some of the values from entity_poly to the same 'id/entity_id
        Map<String, LinkedList<String>> polyDatas = parseCategoryIntoMap("_entity_poly", "entity_id");
        if(CollectionUtil.hasValues(polyDatas)){

            for (Map.Entry<String, LinkedList<String>> poly : polyDatas.entrySet()) {
                Integer molId = StructureUtils.findLastInteger(poly.getKey());

                if(pdbCompnds.containsKey(molId)) {
                    pdbCompnds.get(molId).getLines().addAll(poly.getValue());
                } else {
                    pdbCompnds.put(molId, new PdbCompnd(molId, poly.getKey(), poly.getValue()));
                }

            }

        }

        if(CollectionUtil.hasValues(pdbCompnds)){
            return new ArrayList<>(pdbCompnds.values());
        }
        return null;
    }


    /**
     * parse PdbSources from either loops or items
     * @return
     */
    private List<PdbSource> parseSource(){

        Map<String, LinkedList<String>> sources1 = parseCategoryIntoMap("_entity_src_nat", "entity_id");
        Map<String, LinkedList<String>> sources2 = parseCategoryIntoMap("_entity_src_gen", "entity_id");
        Map<String, LinkedList<String>> sources3 = parseCategoryIntoMap("_pdbx_entity_src_syn", "entity_id");
        Map<String, LinkedList<String>> allSources = new LinkedHashMap<>();
        if(CollectionUtil.hasValues(sources1)) {
            allSources.putAll(sources1);
        }
        if(CollectionUtil.hasValues(sources2)) {
            allSources.putAll(sources2);
        }
        if(CollectionUtil.hasValues(sources3)) {
            allSources.putAll(sources3);
        }

        //transform the Linked HashMap (form loops or items) into list of PdbSource objects
        if(CollectionUtil.hasValues(allSources)){

            List<PdbSource> pdbSources = new ArrayList<>(allSources.size());
            for (Map.Entry<String, LinkedList<String>> compnd : allSources.entrySet()) {
                Integer molId = StructureUtils.findLastInteger(compnd.getKey());
                pdbSources.add(new PdbSource(molId, compnd.getKey(), compnd.getValue()));
            }

            return pdbSources;
        }

        //none found.
        return null;
    }



    private List<PdbRevdat> parseRevdats(){

        List<PdbRevdat> revdats = new ArrayList<>();

        CifLoop loop = getLoopByCategory("_database_PDB_rev");
        if(loop != null){

            //found this category in loops
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){

                int revNum =  StructureUtils.stringToInteger(getTokenValueFromRow(row, "num"));
                Date revDate = StructureUtils.parse_yyyy_MM_dd_Date(getTokenValueFromRow(row, "date"));
                String revId = getTokenValueFromRow(row, "replaces");
                int revType = StructureUtils.stringToInteger(getTokenValueFromRow(row, "mod_type"));
                String revDetail = findDetailsForRevdat(getTokenValueFromRow(row, "num"));

                revdats.add(new PdbRevdat(revNum, revDate, revId, revType, revDetail));
            }

        } else {

            //found this category in items
            Map<String, String> foundItems = mItems.get("_database_PDB_rev");
            if(CollectionUtil.hasValues(foundItems)){
                int revNum =  StructureUtils.stringToInteger(foundItems.get("num"));
                Date revDate = StructureUtils.parse_yyyy_MM_dd_Date(foundItems.get("date"));
                String revId = foundItems.get("replaces");
                int revType = StructureUtils.stringToInteger(foundItems.get("mod_type"));
                String revDetail = findDetailsForRevdat(foundItems.get("num"));

                revdats.add(new PdbRevdat(revNum, revDate, revId, revType, revDetail));
            }

        }

        if(CollectionUtil.hasValues(revdats)){
            return revdats;
        }
        return null;
    }

    private String findDetailsForRevdat(String revNum){

        String category = "_database_PDB_rev_record";

        //first check for match in items
        String foundRevNum = parseStringItemByCategoryKey(category, "rev_num");
        if(StringUtil.isSet(foundRevNum)){
            if(foundRevNum.equalsIgnoreCase(revNum)){
                return parseStringItemByCategoryKey(category, "type");
            } else {
                return null;
            }
        }

        //if not yet found, look in loops - if key is found in the category, append each iteration of the value
        CifLoop loop = getLoopByCategory(category);
        if(loop != null){
            StringBuilder sb = new StringBuilder();
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){

                if(revNum.equalsIgnoreCase(getTokenValueFromRow(row, "rev_num"))){
                    sb.append(getTokenValueFromRow(row, "type") + " ");
                }

            }
            if(StringUtil.isSet(sb.toString())){
                return sb.toString().trim();
            }
        }
        return null;
    }


    /**
     *
     * @return
     */
    private List<PdbJrnl> parseJrnls(){

        List<PdbJrnl> jrnls = new ArrayList<>();

        CifLoop loop = getLoopByCategory("_citation");
        if(loop != null){

            //found this (citation/jrnl) category in loops
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){

                String section = getTokenValueFromRow(row, "id");
                String title = getTokenValueFromRow(row, "title");
                StringBuilder ref = new StringBuilder();
                ref.append(getTokenValueFromRow(row, "journal_abbrev")).append(" ");
                String refVer = getTokenValueFromRow(row, "journal_volume");
                if(StringUtil.isSet(refVer)) {
                    ref.append("V. ").append(refVer).append(" ");
                }
                ref.append(getTokenValueFromRow(row, "page_first")).append(" ");
                ref.append(getTokenValueFromRow(row, "year"));
                String publ = getTokenValueFromRow(row, "book_publisher");
                List<String> isbns = new ArrayList<>();
                String refnAstm = getTokenValueFromRow(row, "journal_id_ASTM");
                String refnIssn = getTokenValueFromRow(row, "journal_id_ISSN");
                String refnIsbn = getTokenValueFromRow(row, "journal_id_ISBN");
                if(StringUtil.isSet(refnAstm)) { isbns.add("ASTM " + refnAstm); }
                if(StringUtil.isSet(refnIssn)) { isbns.add("ISSN " + refnIssn); }
                if(StringUtil.isSet(refnIsbn)) { isbns.add("ISBN " + refnIsbn); }
                String refn = StringUtil.join(isbns, ", ");

                String pmid = getTokenValueFromRow(row, "pdbx_database_id_PubMed");
                String doi = getTokenValueFromRow(row, "pdbx_database_id_DOI");

                String auth = findAuthForJrnl(section);
                String editor = findEditorForJrnl(section);

                jrnls.add(new PdbJrnl(auth, title, editor, ref.toString(), publ, refn, pmid, doi, "primary".equalsIgnoreCase(section), section));
            }

        } else {

            //found this category in items
            Map<String, String> foundItems = mItems.get("_citation");
            if(CollectionUtil.hasValues(foundItems)){
                String section = foundItems.get("id");
                String title = foundItems.get("title");
                StringBuilder ref = new StringBuilder();
                ref.append(foundItems.get("journal_abbrev")).append(" ");
                String refVer = foundItems.get("journal_abbrev");
                if(StringUtil.isSet(refVer)) {
                    ref.append("V. ").append(refVer).append(" ");
                }
                ref.append(foundItems.get("page_first")).append(" ");
                ref.append(foundItems.get("year"));
                String publ = foundItems.get("book_publisher");
                List<String> isbns = new ArrayList<>();
                String refnAstm = foundItems.get("journal_id_ASTM");
                String refnIssn = foundItems.get("journal_id_ISSN");
                String refnIsbn = foundItems.get("journal_id_ISBN");
                if(StringUtil.isSet(refnAstm)) { isbns.add(refnAstm); }
                if(StringUtil.isSet(refnIssn)) { isbns.add(refnIssn); }
                if(StringUtil.isSet(refnIsbn)) { isbns.add(refnIsbn); }
                String refn = StringUtil.join(isbns, ", ");

                String pmid = foundItems.get("pdbx_database_id_PubMed");
                String doi = foundItems.get("pdbx_database_id_DOI");

                String auth = findAuthForJrnl(section);
                String editor = findEditorForJrnl(section);

                jrnls.add(new PdbJrnl(auth, title, editor, ref.toString(), publ, refn, pmid, doi, "primary".equalsIgnoreCase(section), section));
            }

        }

        if(CollectionUtil.hasValues(jrnls)){
            return jrnls;
        }
        return null;
    }


    private String findAuthForJrnl(String id){

        if(!StringUtil.isSet(id)){
            return null;
        }

        String category = "_citation_author";

        //first check for match in items
        String foundId = parseStringItemByCategoryKey(category, "citation_id");
        if(StringUtil.isSet(foundId)){
            if(foundId.equalsIgnoreCase(id)){
                return parseStringItemByCategoryKey(category, "name");
            } else {
                return null;
            }
        }

        //if not yet found, look in loops - if key is found in the category, append each iteration of the value
        CifLoop loop = getLoopByCategory(category);
        if(loop != null){
            List<String> authors = new ArrayList<>();
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){

                if(id.equalsIgnoreCase(getTokenValueFromRow(row, "citation_id"))){
                    authors.add(getTokenValueFromRow(row, "name"));
                }

            }
            if(CollectionUtil.hasValues(authors)){
                return StringUtil.join(authors, "; ");
            }
        }
        return null;
    }

    private String findEditorForJrnl(String id){

        if(!StringUtil.isSet(id)){
            return null;
        }

        String category = "_citation_editor";

        //first check for match in items
        String foundId = parseStringItemByCategoryKey(category, "citation_id");
        if(StringUtil.isSet(foundId)){
            if(foundId.equalsIgnoreCase(id)){
                return parseStringItemByCategoryKey(category, "name");
            } else {
                return null;
            }
        }

        //if not yet found, look in loops - if key is found in the category, append each iteration of the value
        CifLoop loop = getLoopByCategory(category);
        if(loop != null){
            List<String> editors = new ArrayList<>();
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){

                if(id.equalsIgnoreCase(getTokenValueFromRow(row, "citation_id"))){
                    editors.add(getTokenValueFromRow(row, "name"));
                }

            }
            if(CollectionUtil.hasValues(editors)){
                return StringUtil.join(editors, "; ");
            }
        }
        return null;
    }



    private List<PdbSeqres> parseSeqres(){

        List<PdbSeqres> tempSeqRess = new ArrayList<>();

        CifLoop loop = getLoopByCategory("_entity_poly_seq");
        if(loop != null){

            String prevChainNames = null;
            List<String> residues = new ArrayList<>();

            //found this (seqres) category in loops
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){

                String entityId = getTokenValueFromRow(row, "entity_id");    //entity id - this is the number, we gotta translate it to the 'official' chain identifier (e.g.: returns 1, we need A)
                String chainNames = getStrandIds(StructureUtils.stringToInteger(entityId));  //chainName
                String residue = getTokenValueFromRow(row, "mon_id");       //residue

                //if the chain id is different than last pass thru loop, save this seqres entry, clear it all out, and create new sequres entry
                if(prevChainNames != null && !prevChainNames.equalsIgnoreCase(chainNames)){
                    tempSeqRess.add(new PdbSeqres(prevChainNames, prevChainNames, residues.size(), StringUtil.join(residues, " ")));
                    residues = new ArrayList<>();
                }

                prevChainNames = chainNames;
                residues.add(residue);

            }

            //add the final chain now that we're outta the loop
            tempSeqRess.add(new PdbSeqres(prevChainNames, prevChainNames, residues.size(), StringUtil.join(residues, " ")));
        }

        //NOW, we check the strand id for each of these, if it contains a comma it's referring to more than one chain. we gotta break these apart
        List<PdbSeqres> finalSeqRess = new ArrayList<>();
        for(PdbSeqres pdbSeqres : tempSeqRess) {
            if(pdbSeqres.getChainName().contains(",")){
                String[] chainNames = pdbSeqres.getChainName().split(",");
                for(String chainName : chainNames){
                    finalSeqRess.add(new PdbSeqres(chainName, chainName, pdbSeqres.getNumRes(), pdbSeqres.getResidues()));
                }
            } else {
                finalSeqRess.add(pdbSeqres);
            }
        }


        if(CollectionUtil.hasValues(finalSeqRess)){
            return finalSeqRess;
        }
        return null;
    }



    /**
     * find all keywords (_struct_keywords.text) in either loops or items
     * @return
     */
    private List<String> parseKeywords(){
        List<String> lines = findAllForCategoryAndKey("_struct_keywords", "text");
        if(CollectionUtil.hasValues(lines)){
            List<String> keywords = new ArrayList<>();
            for(String line : lines){
                String[] split = StructureUtils.splitOnCommaAndTrim(line);
                List<String> keywordsOnSingleLine = Arrays.asList(split);
                keywords.addAll(keywordsOnSingleLine);
            }
            return keywords;
        }
        return null;
    }








    //##################################################################################################################
    //  general helper-ish methods
    //##################################################################################################################


    /**
     * based on a category and key, return all related lines from both items and loops
     * @param category
     * @param key
     * @return
     */
    private List<String> findAllForCategoryAndKey(String category, String key){

        List<String> foundValues = new ArrayList<>();

        //first look in items
        Map<String, String> foundItems = mItems.get(category);
        if(CollectionUtil.hasValues(foundItems)){
            String foundItem = foundItems.get(key);
            if(StringUtil.isSet(foundItem)){
                foundValues.add(foundItem);
            }
        }

        //next look in loops
        CifLoop loop = getLoopByCategory(category);
        if(loop != null){
            StringBuilder sb = new StringBuilder();
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){
                foundValues.add(getTokenValueFromRow(row, key));
            }
        }

        //return anything found that matches category.key
        if(CollectionUtil.hasValues(foundValues)){
            return foundValues;
        }
        return null;

    }


    /**
     * return a [LinkedHash]Map of all [from a specific category]
     * @param category
     * @param idKey
     * @return
     */
    private Map<String, LinkedList<String>> parseCategoryIntoMap(String category, String idKey){

        String idLabel = category + "." + idKey + " ";
        Map<String, LinkedList<String>> mappp = new LinkedHashMap<>();

        //first look in loops for our category
        CifLoop loop  = getLoopByCategory(category);
        if(loop != null){

            List<LinkedHashMap<String, String>> rows = loop.getRows();
            for(Map<String, String> row : rows){

                String id =  idLabel + getTokenValueFromRow(row, idKey);
                mappp.put(id, transformCifValueRowToLinkedList(category, row, true));

            }
            return mappp;

        }
        //couldn't find in loops, so now look in items
        else
        {
            Map<String, String> foundItems = mItems.get(category);
            if(CollectionUtil.hasValues(foundItems)) {
                String id = idLabel + foundItems.get(idKey);
                mappp.put(id, transformCifValuesToLinkedList(foundItems, true));
                return mappp;
            }

        }

        return null;
    }


    /**
     *
     * @param row
     * @param includeColumnName
     * @return
     */
    private LinkedList<String> transformCifValueRowToLinkedList(String category, Map<String, String> row, boolean includeColumnName){

        LinkedList<String> values = new LinkedList<>();

        for(Map.Entry<String, String> entry : row.entrySet()){
            StringBuilder sb = new StringBuilder();

            if(includeColumnName){
                sb.append(entry.getKey());
                sb.append(": ");
            }
            sb.append(entry.getValue());

            if(!categoryKeyExcludeSet.contains(category + "." + entry.getKey())) {
                values.add(sb.toString());
            } else if (entry.getValue().length() <=25 ) {
                values.add(sb.toString());
            }

        }
        return values;
    }

    private LinkedList<String> transformCifValuesToLinkedList(Map<String, String> inValues, boolean includeColumnName){

        LinkedList<String> outValues = new LinkedList<>();

        for(Map.Entry<String, String> entry : inValues.entrySet()){
            StringBuilder sb = new StringBuilder();
            if(includeColumnName){
                sb.append(entry.getKey());
                sb.append(": ");
            }
            sb.append(entry.getValue());
            outValues.add(sb.toString());
        }
        return outValues;
    }

    private String getTokenValueFromRow(Map<String, String> row, String key){
         if(row != null && StringUtil.isSet(key)) {
            String tokenVal = row.get(key);
            if (tokenVal != null) {
                return tokenVal;
            }
        }
        return null;
    }



    //##################################################################################################################
    // CIF specific chain id map stuff
    //##################################################################################################################

    private void setupChainIdMap()
    {
        mChainIdsMap = new HashMap<>();

        CifLoop loop = getLoopByCategory("_pdbx_poly_seq_scheme");
        if(loop != null)
        {
            List<LinkedHashMap<String, String>> rows = loop.getRows();
            LinkedHashMap<String, String> firstRow = rows.get(0);

            String firstEntityIdValue = getTokenValueFromRow(firstRow, "entity_id");
            String firstStrandIdValue = getTokenValueFromRow(firstRow, "pdb_strand_id");

            if(StringUtil.isSet(firstEntityIdValue) && StringUtil.isSet(firstStrandIdValue))
            {
                for(Map<String, String> row : rows)
                {
                    String entityIdValue = getTokenValueFromRow(row, "entity_id");
                    String strandIdValue = getTokenValueFromRow(row, "pdb_strand_id");

                    Integer entityId = StructureUtils.stringToInteger(entityIdValue);
                    String[] strandIds = strandIdValue.split(",");

                    putInChainIdsMap(entityId, strandIds);
                }
            }
        }
        else
        {
            // the entity_id and strand_id are not always in the first category we look (_pdbx_poly_seq_scheme) so lets try "_entity_poly"
            loop = getLoopByCategory("_entity_poly");
            if(loop != null)
            {
                List<LinkedHashMap<String, String>> rows = loop.getRows();
                LinkedHashMap<String, String> firstRow = rows.get(0);

                String firstEntityIdValue = getTokenValueFromRow(firstRow, "entity_id");
                String firstStrandIdValue = getTokenValueFromRow(firstRow, "pdbx_strand_id");

                if (StringUtil.isSet(firstEntityIdValue) && StringUtil.isSet(firstStrandIdValue))
                {
                    for (Map<String, String> row : rows)
                    {
                        String entityIdValue = getTokenValueFromRow(row, "entity_id");
                        String strandIdValue = getTokenValueFromRow(row, "pdbx_strand_id");

                        Integer entityId = StructureUtils.stringToInteger(entityIdValue);
                        String[] strandIds = strandIdValue.split(",");

                        putInChainIdsMap(entityId, strandIds);
                    }

                }
            }
            else   //no match in the loop - we can also check items
            {
                Map<String, String> foundItems = mItems.get("_entity_poly");
                if(CollectionUtil.hasValues(foundItems))
                {
                    String entityIdValue = foundItems.get("entity_id");
                    String strandIdValue = foundItems.get("pdbx_strand_id");

                    if(StringUtil.isSet(entityIdValue) && StringUtil.isSet(strandIdValue))
                    {
                        Integer entityId = StructureUtils.stringToInteger(entityIdValue);
                        String[] strandIds = strandIdValue.split(",");

                        putInChainIdsMap(entityId, strandIds);
                    }
                }
            }
        }
    }

    private void putInChainIdsMap(Integer entityId, List<String> strandIds)
    {
        for(String strandId : strandIds)
        {
            putInChainIdsMap(entityId, strandId);
        }
    }

    private void putInChainIdsMap(Integer entityId, String[] strandIds)
    {
        for(String strandId : strandIds)
        {
            putInChainIdsMap(entityId, strandId);
        }
    }

    private void putInChainIdsMap(Integer entityId, String strandId)
    {
        if(mChainIdsMap.containsKey(entityId))
        {
            mChainIdsMap.get(entityId).add(strandId);
        }
        else
        {
            LinkedHashSet<String> first = new LinkedHashSet<>();
            first.add(strandId);
            mChainIdsMap.put(entityId, first);
        }
    }



    private List<String> getStrandIdsList(Integer entityId){

        if(CollectionUtil.hasValues(mChainIdsMap)){
            List<String> strandIdsList = new ArrayList<>();
            strandIdsList.addAll(mChainIdsMap.get(entityId));
            return strandIdsList;
        }
        return null;
    }

    private String getStrandIds(Integer entityId)
    {
        if(CollectionUtil.hasValues(mChainIdsMap))
        {
            List<String> strandIdsList = new ArrayList<>();
            strandIdsList.addAll(mChainIdsMap.get(entityId));
            if(CollectionUtil.hasValues(strandIdsList))
            {
                return StringUtil.join(strandIdsList, ",");
            }
        }
        return null;
    }


    //##################################################################################################################
    // OVERRIDES
    //##################################################################################################################

    //---------------------------------------------------------------------------
    @Override
    public boolean isEndOfRecord(String inLine)
    {
        return (inLine.equals("END"));
    }

    //---------------------------------------------------------------------------
    @Override
    public boolean hasJanusDelimiter()
    {
        return false;
    }
}
