package com.just.bio.structure.format.cif;

import com.hfg.util.StringUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class CifLoop {


    private String categoryName;
    private List<String> columnNames = new ArrayList<>();                   //basically, this is the column headers
    private List<LinkedHashMap<String, String>> rows = new ArrayList<>();   //each map is a row...


    //###########################################################################
    // constructors
    //###########################################################################

    public CifLoop(List<String> columnNames) {
        //pull out category from column names
        this.categoryName = getCategoryFromColumnNames(columnNames.get(0));
        //strip category from column names
        for(String columnName : columnNames){
            String colNameKey = columnName.split("\\.")[1];
            addColumnName(colNameKey);
        }
    }



    //###########################################################################
    // all the helper stuff and things
    //###########################################################################

    public void addColumnName(String columnName){
        columnNames.add(columnName);
    }


    public void addRow(List<String> rowValues){

        int i = 0;
        LinkedHashMap<String, String> row = new LinkedHashMap<>(getColumnCount());
        for(String rowValue : rowValues){

            String columnName = columnNames.get(i);

            if(CIF_Format.includeLinesWithBlankValues || (!rowValue.equals("?")  && !rowValue.equals("."))) {
                row.put(columnName, rowValue);
            }
            i++;

        }
        rows.add(row);
    }


    public int getColumnCount(){
        return columnNames.size();
    }


    private String getCategoryFromColumnNames(String dataName){
        if(StringUtil.isSet(dataName) && dataName.contains(".")) {
            String[] data = dataName.split("\\.");
            return data[0];
        }
        return  null;
    }


    //###########################################################################
    // standard getters/setters
    //###########################################################################

    public String getCategoryName() {return categoryName;}

    public List<String> getColumnNames() {return columnNames;}

    public List<LinkedHashMap<String, String>> getRows() {return rows;}






}
