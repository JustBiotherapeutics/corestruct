package com.just.bio.structure.format;

import java.io.BufferedReader;
import java.io.File;
import java.util.List;

import com.just.bio.structure.Structure;
import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.format.cif.CIF_Format;
import com.just.bio.structure.format.moe.MOE_Format;
import com.just.bio.structure.format.pdb.PDB_Format;

/**
 * Interface for readable 3D protein structure formats.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public interface ReadableStructureFormat<T extends Structure>
{
    public StructureFormatType getStructureFormatType();

    public List<T> read(File inFile) throws StructureIOException;

    public List<T> read(BufferedReader inFile) throws StructureIOException;

    public T readRecord(CharSequence inString) throws StructureIOException;

    public T readRecord(BufferedReader inReader) throws StructureIOException;

    public boolean isEndOfRecord(String inLine);

    //--------------------------------------------------------------------------
    /**
     * A format has a Janus delimiter if the line that indicates that the previous record is
     * also the indicator for the start of the next record.
     *
     * @return whether this format's record delimiter is also part of the next record
     */
    public boolean hasJanusDelimiter();


    //--------------------------------------------------------------------------
    /**
     * Allocate a structure format object for the specified structure file by
     * examining the file extension.
     * @param inStructureFile the structure file for which a format object should be allocated
     * @return the structure format object suitable for reading a file of the specified structure format
     */
    public static ReadableStructureFormat allocateReader(File inStructureFile)
    {
        String filename = inStructureFile.getName().toLowerCase();
        if (filename.endsWith(".gz"))
        {
            filename = filename.substring(0, filename.length() - 3);
        }

        // Default to PDB format
        StructureFormatType structureFormatType = StructureFormatType.PDB;
        for (StructureFormatType value : StructureFormatType.values())
        {
            if (filename.endsWith("." + value.extension()))
            {
                structureFormatType = value;
                break;
            }
        }

        return allocateReader(structureFormatType);
    }

   //--------------------------------------------------------------------------
    /**
     * Allocate a structure format object for the specified structure format type.
     * @param structureFormatType the structure format time for which a format object should be allocated
     * @return the structure format object suitable for reading a file of the specified structure format
     */
    public static ReadableStructureFormat allocateReader(StructureFormatType structureFormatType)
    {
        ReadableStructureFormat structureFormatObj = null;

        if(structureFormatType != null)
        {
            if (structureFormatType.equals(StructureFormatType.PDB)
                || structureFormatType.equals(StructureFormatType.ENT))
            {
                structureFormatObj = new PDB_Format(new StructureFactoryImpl());

            } else if (structureFormatType.equals(StructureFormatType.CIF))
            {
                structureFormatObj = new CIF_Format(new StructureFactoryImpl());

            } else if (structureFormatType.equals(StructureFormatType.MOE))
            {
                structureFormatObj = new MOE_Format(new StructureFactoryImpl());
            }
        }
        return structureFormatObj;

    }
}
