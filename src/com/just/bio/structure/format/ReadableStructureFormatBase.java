package com.just.bio.structure.format;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import com.hfg.util.StringUtil;

import com.just.bio.structure.Structure;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public abstract class ReadableStructureFormatBase<T extends Structure> implements ReadableStructureFormat<T>
{

    private StructureFactory<T> mStructureFactory;


    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    //---------------------------------------------------------------------------
    public ReadableStructureFormatBase(StructureFactory<T> inStructureFactory)
    {
        mStructureFactory = inStructureFactory;
    }


    //###########################################################################
    // PUBLIC METHODS
    //###########################################################################


    //---------------------------------------------------------------------------
    public StructureFactory<T> getStructureFactory()
    {
        return mStructureFactory;
    }


    //---------------------------------------------------------------------------
    public List<T> read(File inFile)
          throws StructureIOException
    {
        try
        {
            BufferedStructureReader reader = new BufferedStructureReader(new BufferedReader(new FileReader(inFile)), this);

            return reader.readAll();
        }
        catch (StructureIOException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new StructureIOException("Problem reading structures from " + StringUtil.singleQuote(inFile.getPath()) + "!", e);
        }
    }

    //---------------------------------------------------------------------------
    public List<T> read(BufferedReader inReader)
          throws StructureIOException
    {
        try
        {
            BufferedStructureReader reader = new BufferedStructureReader<>(inReader, this);

            return reader.readAll();
        }
        catch (StructureIOException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new StructureIOException("Problem reading sequences!", e);
        }
    }

    //---------------------------------------------------------------------------
    public T readRecord(CharSequence inString)
          throws StructureIOException
    {
        T seq;

        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new StringReader(inString.toString()));
            seq = readRecord(reader);
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e)
                {
                    throw new StructureIOException(e);
                }
            }
        }

        return seq;
    }

}
