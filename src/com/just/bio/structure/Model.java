package com.just.bio.structure;

import java.util.Collection;
import java.util.Map;

import com.hfg.math.Range;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.hfg.util.collection.OrderedMap;


/** ----------------------------------------------------------------------------------------------------
 * Model is the highest level container for atomic coordinates. Models contain Chains.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class Model
{
   private String    mName;
   public Map<String, Chain> mChainMap = new OrderedMap<>(4);
   // Asymmetric unit coordinate range
   // See https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym
   public Range<Float> mXRange;
   public Range<Float> mYRange;
   public Range<Float> mZRange;

   //##########################################################################
   // CONSTRUCTORS
   //##########################################################################

   //--------------------------------------------------------------------------
   public Model()
   {
   }

   //--------------------------------------------------------------------------
   public Model(String inName)
   {
      setName(inName);
   }

   //##########################################################################
   // PUBLIC METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   public Model setName(String inValue)
   {
      mName = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   public String name()
   {
      return mName;
   }

   //--------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return name();
   }


   //--------------------------------------------------------------------------
   public Model addChain(Chain inValue)
   {
      mChainMap.put(inValue.getId(), inValue);

      inValue.setParentModel(this);

      return this;
   }

   //--------------------------------------------------------------------------
   public Collection<Chain> getChains()
   {
      return mChainMap.values();
   }

   //--------------------------------------------------------------------------
   public Chain getChain(String inId)
   {
      Chain chain = mChainMap.get(inId);
      if (null == chain)
      {
         if(CollectionUtil.hasValues(mChainMap.values()))
         {
            for (Chain chainValue : mChainMap.values())
            {
               if (StringUtil.isSet(chainValue.name()) && chainValue.name().equalsIgnoreCase(inId))
               {
                  chain = chainValue;
                  break;
               }
            }
         }
      }
      
      return chain;
   }


   //--------------------------------------------------------------------------
   /**
    * Sets the x range of the model's symmetric unit coordinates.
    * @see <a href='https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym'>
    *    https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym</a>
    * @param inValue the x range of the model's symmetric unit coordinates
    * @return this Model object to enable method chaining
    */
   public Model setXRange(Range<Float> inValue)
   {
      mXRange = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns the x range of the model's symmetric unit coordinates.
    * @see <a href='https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym'>
    *    https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym</a>
    * @return the x range of the model's symmetric unit coordinates
    */
   public Range<Float> getXRange()
   {
      return mXRange;
   }


   //--------------------------------------------------------------------------
   /**
    * Sets the y range of the model's symmetric unit coordinates.
    * @see <a href='https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym'>
    *    https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym</a>
    * @param inValue the y range of the model's symmetric unit coordinates
    * @return this Model object to enable method chaining
    */
   public Model setYRange(Range<Float> inValue)
   {
      mYRange = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns the y range of the model's symmetric unit coordinates.
    * @see <a href='https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym'>
    *    https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym</a>
    * @return the y range of the model's symmetric unit coordinates
    */
   public Range<Float> getYRange()
   {
      return mYRange;
   }


   //--------------------------------------------------------------------------
   /**
    * Sets the z range of the model's symmetric unit coordinates.
    * @see <a href='https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym'>
    *    https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym</a>
    * @param inValue the z range of the model's symmetric unit coordinates
    * @return this Model object to enable method chaining
    */
   public Model setZRange(Range<Float> inValue)
   {
      mZRange = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns the z range of the model's symmetric unit coordinates.
    * @see <a href='https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym'>
    *    https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies#Anchor-Asym</a>
    * @return the z range of the model's symmetric unit coordinates
    */
   public Range<Float> getZRange()
   {
      return mZRange;
   }

}
