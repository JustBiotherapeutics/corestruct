package com.just.bio.structure;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hfg.util.StringUtil;

import com.just.bio.structure.format.pdb.PDB_AtomName;

/**
 *
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class ElementMap
{

   //##################################################################################################################
   // data members
   //##################################################################################################################

   private HashMap<String, AminoAcid3D> aaMap;
   private HashMap<String, Character> naMap;

   private List<PDB_AtomName> mBkbnAtoms;
   private List<PDB_AtomName> mBkbnCbAtoms;


   //##################################################################################################################
   // Singleton Pattern constructor and static initialization block
   //##################################################################################################################

   private static ElementMap INSTANCE;

   private ElementMap()
   {
      try
      {
         init();
      } catch (Exception e)
      {
         throw new RuntimeException("Exception occurred creating 'ElementMap' singleton instance", e);
      }
   }

   private void init()
         throws IllegalAccessException
   {

      //get all the static fields from the AminoAcid3D class, and store 'em in our instances aaMap (amino acid map)
      Field[] declaredFields = AminoAcid3D.class.getDeclaredFields();

      aaMap = new HashMap<>(declaredFields.length);

      for (Field field : declaredFields)
      {
         if (java.lang.reflect.Modifier.isStatic(field.getModifiers()))
         {

            if (field.getType() == AminoAcid3D.class)
            {
               AminoAcid3D aa3d = (AminoAcid3D) field.get(this);
               aaMap.put(aa3d.getThreeLetterCode().toUpperCase(), aa3d);
            }

         }
      }

      //setup the nucleotideMap next
      naMap = new HashMap<>(12);
      naMap.put("DC", 'c');
      naMap.put("DG", 'g');
      naMap.put("DA", 'a');
      naMap.put("DU", 'u');
      naMap.put("DT", 't');
      naMap.put("DI", 'i');
      naMap.put("C", 'c');
      naMap.put("G", 'g');
      naMap.put("A", 'a');
      naMap.put("U", 'u');
      naMap.put("T", 't');
      naMap.put("I", 'i');

      //define backbone atoms
      mBkbnAtoms = new ArrayList<>(3);
      mBkbnAtoms.add(PDB_AtomName.CA);
      mBkbnAtoms.add(PDB_AtomName.N);
      mBkbnAtoms.add(PDB_AtomName.C);

      //define backbone/CB atoms
      mBkbnCbAtoms = new ArrayList<>(4);
      mBkbnCbAtoms.add(PDB_AtomName.CA);
      mBkbnCbAtoms.add(PDB_AtomName.N);
      mBkbnCbAtoms.add(PDB_AtomName.C);
      mBkbnCbAtoms.add(PDB_AtomName.CB);


   }


   //##################################################################################################################
   // public methods
   //##################################################################################################################


   public static ElementMap getInstance()
   {
      if (INSTANCE == null)
      {
         INSTANCE = new ElementMap();
      }
      return INSTANCE;
   }


   public HashMap<String, AminoAcid3D> getAaMap()
   {
      return aaMap;
   }

   public HashMap<String, Character> getNaMap()
   {
      return naMap;
   }

   public List<PDB_AtomName> getBkbnAtoms()
   {
      return mBkbnAtoms;
   }

   public List<PDB_AtomName> getBkbnCbAtoms()
   {
      return mBkbnCbAtoms;
   }


   /**
    * return the 1 character abbreviation of a Amino Acid based on the 3 character version
    * or, if this is a nucleotide, return the 1 Character based on the naMap
    * or, if it is only one character return the lower case version
    *
    * @param abbrev3char the three letter amino acid code to be converted
    * @return single Character identifier that maps to the 3 character abbreviation; or 'NO_MATCHING_RESIDUE_CHAR' if nothing matches.
    */
   public Character translateAminoAcid3to1(String abbrev3char)
   {

      if (StringUtil.isSet(abbrev3char))
      {

         AminoAcid3D aa = aaMap.get(abbrev3char);

         if (aa != null)
         {
            if (aa.getAminoAcid() != null && aa.getAminoAcid().getOneLetterCode() != null)
            {
               return aa.getAminoAcid().getOneLetterCode();
            }
            else
            {
               return Chain.NO_MATCHING_AMINO_ACID_CHAR;
            }

         }
         else
         {
            Character c = naMap.get(abbrev3char);
            if (c != null)
            {
               return c;
            }
            else
            {
               if (abbrev3char.length() == 1)
               {
                  return abbrev3char.toLowerCase().charAt(0);
               }
            }
         }

      }

      return Chain.NO_MATCHING_AMINO_ACID_CHAR;

   }


   //##################################################################################################################
   // private methods
   //##################################################################################################################


}
