package com.just.bio.structure;

import java.io.File;

import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.pdb.*;


//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class StructureImpl implements Structure
{

   //primary sections of a PDB/CIF Structure file
   private PdbSectionMetadata mPdbSectionMetadata;
   private PdbSectionRemark mPdbSectionRemark;
   private PdbSectionConnectivity mPdbSectionConnectivity;
   private PdbSectionPrimary mPdbSectionPrimary;

   //calculated information (after parsing file)

   //additional notes, annotations, and data about the Structure File this data was retrieved from
   private StructureFormatType mStructureFormatType;
   private String mPdbIdentifier;
   private String mFileRestUrl;
   private String mFilePath;

   //temporary location where local file is put so caller can upload as necessary
   private File mLocalFile;

   //values calculated on the uncompressed bytes as the file is initially being streamed in
   private Long mUncompressedSizeInBytes;
   private byte[] mMd5Checksum;

   private AtomicCoordinates mAtomicCoordinates;


   //===================================================================================================================
   // constructors
   //===================================================================================================================

   /**
    *
    */
   public StructureImpl()
   {
   }


   //===================================================================================================================
   // Sections of the file
   //===================================================================================================================

   //---------------------------------------------------------------------------

   /**
    * pojo representing the metadata portion of a PDB file
    *
    * @return metadata portion of a PDB file
    */
   public PdbSectionMetadata getMetadata()
   {
      return mPdbSectionMetadata;
   }

   public void setMetadata(PdbSectionMetadata inValue)
   {
      mPdbSectionMetadata = inValue;
   }

   //---------------------------------------------------------------------------

   /**
    * pojo representing the Remark portion of a PDB file
    *
    * @return Remark portion of a PDB file
    */
   public PdbSectionRemark getRemark()
   {
      return mPdbSectionRemark;
   }

   public void setRemark(PdbSectionRemark inValue)
   {
      mPdbSectionRemark = inValue;
   }

   //---------------------------------------------------------------------------

   /**
    * pojo representing the Connectivity portion of a PDB file
    *
    * @return Connectivity portion of a PDB file
    */
   public PdbSectionConnectivity getConnectivity()
   {
      return mPdbSectionConnectivity;
   }

   public void setConnectivity(PdbSectionConnectivity inValue)
   {
      mPdbSectionConnectivity = inValue;
   }

   //---------------------------------------------------------------------------

   /**
    * pojo representing the Primary portion of a PDB file
    *
    * @return Primary portion of a PDB file
    */
   public PdbSectionPrimary getPrimary()
   {
      return mPdbSectionPrimary;
   }

   public void setPrimary(PdbSectionPrimary inValue)
   {
      mPdbSectionPrimary = inValue;
   }


   //===================================================================================================================
   // additional calculated info
   //===================================================================================================================

   //---------------------------------------------------------------------------
   public Structure setAtomicCoordinates(AtomicCoordinates inValue)
   {
      mAtomicCoordinates = inValue;
      return this;
   }

   //---------------------------------------------------------------------------
   public AtomicCoordinates getAtomicCoordinates()
   {
      return mAtomicCoordinates;
   }


   //===================================================================================================================
   // File metadata
   //===================================================================================================================

   //---------------------------------------------------------------------------

   /**
    * enum describing the file format of this file - only a few file formats currently supported
    *
    * @return the structure format type that was the source of this structure data
    */
   public StructureFormatType getFileFormat()
   {
      return mStructureFormatType;
   }

   public void setFileFormat(StructureFormatType inValue)
   {
      mStructureFormatType = inValue;
   }

   //---------------------------------------------------------------------------

   /**
    * either the PDB Identifier that was passed in for the REST service, or a PDB Identifier that was parsed from the file name
    *
    * @return the PDB identifier associated with this structure
    */
   public String getPdbIdentifier()
   {
      return mPdbIdentifier;
   }

   public void setPdbIdentifier(String inValue)
   {
      mPdbIdentifier = inValue;
   }

   //---------------------------------------------------------------------------

   /**
    * the actual url that this file came from (for all possible URLs see RcsbRestService class)
    *
    * @return the URL where this structure was obtained
    */
   public String getFileRestUrl()
   {
      return mFileRestUrl;
   }

   public void setFileRestUrl(String inValue)
   {
      mFileRestUrl = inValue;
   }

   //---------------------------------------------------------------------------

   /**
    * the 'remote' file path that was passed in - used to parse the file
    *
    * @return the remote file path used to retrieve this structure
    */
   public String getFilePath()
   {
      return mFilePath;
   }

   public void setFilePath(String inValue)
   {
      mFilePath = inValue;
   }


   //---------------------------------------------------------------------------

   /**
    * the local "temporary" place the file was copied to
    *
    * @return the local file associated with this structure
    */
   public File getLocalFile()
   {
      return mLocalFile;
   }

   public void setLocalFile(File inValue)
   {
      mLocalFile = inValue;
   }

   //---------------------------------------------------------------------------

   /**
    * the size of the structure in bytes (this is calculated when the file is being read, on the uncompressed bytes)
    *
    * @return the size of the structure in bytes
    */
   public Long getUncompressedSizeInBytes()
   {
      return mUncompressedSizeInBytes;
   }

   public void setUncompressedSizeInBytes(Long inValue)
   {
      mUncompressedSizeInBytes = inValue;
   }


   //---------------------------------------------------------------------------

   /**
    * MD5 checksum of the file (this is calculated when the file is being read, on the uncompressed bytes)
    *
    * @return the MD5 checksum for this structure's file
    */
   public byte[] getMd5Checksum()
   {
      return mMd5Checksum;
   }

   public void setMd5Checksum(byte[] inValue)
   {
      mMd5Checksum = inValue;
   }


}
