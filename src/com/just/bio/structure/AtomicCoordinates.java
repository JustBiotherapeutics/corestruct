package com.just.bio.structure;

import java.util.ArrayList;
import java.util.List;

import com.hfg.util.collection.CollectionUtil;

import com.just.bio.structure.exceptions.StructureParseException;
import com.just.bio.structure.pdb.pojo.PdbSeqres;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class AtomicCoordinates
{
   public List<Model> mModels = new ArrayList<>(4);

   public static enum Flags
   {
      IGNORE_HETATM,
      IGNORE_WATER;
   }

   //##########################################################################
   // CONSTRUCTORS
   //##########################################################################

   //--------------------------------------------------------------------------
   public AtomicCoordinates()
   {
   }

   //##########################################################################
   // PUBLIC METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   public AtomicCoordinates addModel(Model inValue)
   {
      mModels.add(inValue);
      return this;
   }

   //--------------------------------------------------------------------------
   public List<Model> getModels()
   {
      return mModels;
   }

   //--------------------------------------------------------------------------
   public void setSeqRes(List<PdbSeqres> inSeqRes)
   {
      if (CollectionUtil.hasValues(inSeqRes))
      {
         for (PdbSeqres pdbSeqres : inSeqRes)
         {
            String[] residueNames = pdbSeqres.getResidues().split(" ");
            List<SeqResidue> seqResidues = new ArrayList<>(residueNames.length);
            int i = 1;
            for (String residueName : residueNames)
            {
               seqResidues.add(new SeqResidue(residueName, i++));
            }

            for (Model model : getModels())
            {
               Chain chain = model.getChain(pdbSeqres.getChainId());
               if (null == chain)
               {
                  throw new StructureParseException("No chain coordinates found from seqres chain id " + pdbSeqres.getChainId() + "!");
               }

               chain.setSeqResidues(seqResidues)
                 .setName(pdbSeqres.getChainName())
                 .setDescription(pdbSeqres.getDescription());
            }
         }
      }
   }
}
