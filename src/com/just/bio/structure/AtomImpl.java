package com.just.bio.structure;

import java.util.Arrays;
import java.util.List;

import com.hfg.chem.Charge;
import com.hfg.chem.Element;

import com.just.bio.structure.format.pdb.PDB_AtomName;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class AtomImpl implements Atom
{
   private PDB_AtomName mName;
   private int       mSerialNum;
   private AtomGroup mAtomGroup;
   private Character mInsertCode;
   private float[]   mCoords;
   private float     mOccupancy = -1;
   private float     mTempFactor = -1;
   private Element   mElement;
   private Charge    mCharge;
   private OrbitalHybridizationGeometry mGeometry;
   private CovalentBond[] mCovalentBonds;
   private boolean   mIsBackbone;

   //##########################################################################
   // CONSTRUCTORS
   //##########################################################################

   //--------------------------------------------------------------------------
   public AtomImpl(PDB_AtomName inName)
   {
      mName = inName;
   }

   //##########################################################################
   // PUBLIC METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   @Override
   public PDB_AtomName name()
   {
      return mName;
   }

   //--------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return name().toString();
   }

   //--------------------------------------------------------------------------
   public AtomImpl setSerialNum(int inValue)
   {
      mSerialNum = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public int getSerialNum()
   {
      return mSerialNum;
   }

   //--------------------------------------------------------------------------
   public AtomImpl setAtomGroup(AtomGroup inValue)
   {
      mAtomGroup = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public AtomGroup getAtomGroup()
   {
      return mAtomGroup;
   }


   //--------------------------------------------------------------------------
   public AtomImpl setInsertCode(Character inValue)
   {
      mInsertCode = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public Character getInsertCode()
   {
      return mInsertCode;
   }


   //--------------------------------------------------------------------------
   public AtomImpl setCoords(double inX, double inY, double inZ)
   {
      return setCoords((float)inX, (float)inY, (float)inZ);
   }

   //--------------------------------------------------------------------------
   public AtomImpl setCoords(float inX, float inY, float inZ)
   {
      mCoords = new float[] { inX, inY, inZ };
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public float[] getCoords()
   {
      return mCoords;
   }


   //--------------------------------------------------------------------------
   public AtomImpl setOccupancy(float inValue)
   {
      mOccupancy = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public Float getOccupancy()
   {
      return mOccupancy != -1 ? mOccupancy : null;
   }


   //--------------------------------------------------------------------------
   public AtomImpl setTempFactor(float inValue)
   {
      mTempFactor = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public Float getTempFactor()
   {
      return mTempFactor != -1 ? mTempFactor : null;
   }


   //--------------------------------------------------------------------------
   public AtomImpl setElement(Element inValue)
   {
      mElement = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public Element getElement()
   {
      return mElement;
   }


   //--------------------------------------------------------------------------
   public AtomImpl setCharge(Charge inValue)
   {
      mCharge = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public Charge getCharge()
   {
      return mCharge;
   }


   //--------------------------------------------------------------------------
   public AtomImpl setGeometry(OrbitalHybridizationGeometry inValue)
   {
      mGeometry = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public OrbitalHybridizationGeometry getGeometry()
   {
      return mGeometry;
   }


   //--------------------------------------------------------------------------
   @Override
   public List<CovalentBond> getCovalentBonds()
   {
      return mCovalentBonds != null ? Arrays.asList(mCovalentBonds) : null;
   }


   //--------------------------------------------------------------------------
   public AtomImpl setIsBackbone(boolean inValue)
   {
      mIsBackbone = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   @Override
   public boolean isBackbone()
   {
      return mIsBackbone;
   }

}
