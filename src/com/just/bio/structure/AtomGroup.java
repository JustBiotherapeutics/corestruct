package com.just.bio.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hfg.util.collection.CollectionUtil;

import com.just.bio.structure.format.pdb.PDB_AtomName;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class AtomGroup
{
   private String          mName;
   private AtomGroupType   mType;
   private Chain           mParentChain;
   private ArrayList<Atom> mAtoms;
   private Map<PDB_AtomName, Atom> mAtomLookup;
   private Integer         mResidueNum;
   private Character       mInsertCode;

   private Boolean mHasFullCoordinates;
   private Boolean mHasAnyCoordinates;
   private Boolean mHasCaCoordinates;
   private Boolean mHasBackboneCoordinates;
   private Boolean mHasBackboneCbCoordinates;

   private static Set<PDB_AtomName> sAA_BackboneAtomNames = new HashSet<>(3);
   private static Set<PDB_AtomName> sAA_BackbonePlusCbAtomNames = new HashSet<>(4);

   static
   {
      sAA_BackboneAtomNames.add(PDB_AtomName.CA);
      sAA_BackboneAtomNames.add(PDB_AtomName.N);
      sAA_BackboneAtomNames.add(PDB_AtomName.C);

      sAA_BackbonePlusCbAtomNames.addAll(sAA_BackboneAtomNames);
      sAA_BackbonePlusCbAtomNames.add(PDB_AtomName.CB);
   }


   //##########################################################################
   // CONSTRUCTORS
   //##########################################################################

   //--------------------------------------------------------------------------
   public AtomGroup(String inName, AtomGroupType inType)
   {
      mName = inName;
      mType = inType;
   }

   //##########################################################################
   // PUBLIC METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   public String name()
   {
      return mName;
   }

   //--------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return name();
   }

   //--------------------------------------------------------------------------
   public AtomGroupType getType()
   {
      return mType;
   }

   //--------------------------------------------------------------------------
   public AtomGroup setParentChain(Chain inValue)
   {
      mParentChain = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   public Chain getParentChain()
   {
      return mParentChain;
   }


   //--------------------------------------------------------------------------
   public AtomGroup setResidueNum(Integer inValue)
   {
      mResidueNum = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   public Integer getResidueNum()
   {
      return mResidueNum;
   }

   //--------------------------------------------------------------------------
   public AtomGroup setInsertCode(Character inValue)
   {
      mInsertCode = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   public Character getInsertCode()
   {
      return mInsertCode;
   }


   //--------------------------------------------------------------------------
   public AtomGroup addAtom(Atom inValue)
   {
      if (null == mAtoms)
      {
         mAtoms = new ArrayList<>(10);
      }

      mAtoms.add(inValue);

      inValue.setAtomGroup(this);

      return this;
   }

   //--------------------------------------------------------------------------
   public List<Atom> getAtoms()
   {
      return mAtoms;
   }

   //--------------------------------------------------------------------------
   public Atom getAtom(PDB_AtomName inName)
   {
      Map<PDB_AtomName, Atom> atomLookup = getAtomLookup();
      return atomLookup != null ? atomLookup.get(inName) : null;
   }

   //--------------------------------------------------------------------------
   public void clear()
   {
      mAtoms = null;
   }

   //--------------------------------------------------------------------------
   public int size()
   {
      return mAtoms != null ? mAtoms.size() : 0;
   }

   //--------------------------------------------------------------------------
   public void trimToSize()
   {
      if (mAtoms != null)
      {
         mAtoms.trimToSize();
      }
   }

   //--------------------------------------------------------------------------
   public Set<PDB_AtomName> getAtomNames()
   {
      Set<PDB_AtomName> atomNames = null;
      if (CollectionUtil.hasValues(getAtoms()))
      {
         atomNames = new HashSet<>(10);
         for (Atom atom : getAtoms())
         {
            atomNames.add(atom.name());
         }
      }

      return atomNames;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns whether the atom group contains all atoms that make up a 'full' amino acid or nucleotide.
    * (Nucleotide not currently supported.)
    * @return true if the atoms string contains all atoms for the full amino acid or nucleotide
    */
   public synchronized boolean hasFullCoordinates()
   {
      if (null == mHasFullCoordinates)
      {
         mHasFullCoordinates = false;
         if (getType().equals(AtomGroupType.AMINO_ACID))
         {
            Set<PDB_AtomName> atomNames = getAtomNames();
            if (CollectionUtil.hasValues(atomNames))
            {
               mHasFullCoordinates = atomNames.containsAll(AminoAcid3D.valueOf(name()).getAtomNames());
            }
         }
         // TODO: Add nucleotide support
      }

      return mHasFullCoordinates;
   }

   //--------------------------------------------------------------------------
   public boolean hasAnyCoordinates()
   {
      if (null == mHasAnyCoordinates)
      {
         mHasAnyCoordinates = CollectionUtil.hasValues(getAtoms());
      }

      return mHasAnyCoordinates;
   }

   //--------------------------------------------------------------------------
   public boolean hasCaCoordinates()
   {
      if (null == mHasCaCoordinates)
      {
         mHasCaCoordinates = hasAnyCoordinates() && getAtomNames().contains(PDB_AtomName.CA);
      }

      return mHasCaCoordinates;
   }

   //--------------------------------------------------------------------------
   public boolean hasBackboneCoordinates()
   {
      if (null == mHasBackboneCoordinates)
      {
         mHasBackboneCoordinates = hasAnyCoordinates() && getAtomNames().containsAll(sAA_BackboneAtomNames);
      }

      return mHasBackboneCoordinates;
   }

   //--------------------------------------------------------------------------
   public boolean hasBackboneCbCoordinates()
   {
      if (null == mHasBackboneCbCoordinates)
      {
         mHasBackboneCbCoordinates = hasAnyCoordinates() && getAtomNames().containsAll(sAA_BackbonePlusCbAtomNames);
      }

      return mHasBackboneCbCoordinates;
   }

   //##########################################################################
   // PRIVATE METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   private synchronized Map<PDB_AtomName, Atom> getAtomLookup()
   {
      if (null == mAtomLookup
          && CollectionUtil.hasValues(getAtoms()))
      {
         mAtomLookup = new HashMap<>(getAtoms().size());
         for (Atom atom : getAtoms())
         {
            mAtomLookup.put(atom.name(), atom);
         }
      }

      return mAtomLookup;
   }
}
