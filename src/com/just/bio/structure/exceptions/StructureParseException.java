package com.just.bio.structure.exceptions;

/**
 * Exception used when reading and parsing structure files
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------
public class StructureParseException extends RuntimeException
{
   //---------------------------------------------------------------------------
   public StructureParseException(String inMessage)
   {
      super(inMessage);
   }

   //---------------------------------------------------------------------------
   public StructureParseException(String inMessage, Throwable inException)
   {
      super(inMessage, inException);
   }

   //---------------------------------------------------------------------------
   public StructureParseException(Throwable inException)
   {
      super(inException);
   }
}
