package com.just.bio.structure.enums;


//======================================================================================================
/**
 line identifier from PDB or CIF files for the 'type' or 'grouping' of atom
 <div>
 @author Russell Elbert Williams
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================
public enum AtomType {

    ATOM,
    HETATM,
    TER;

}
