package com.just.bio.structure.enums;


//======================================================================================================
/**
 Enum for each Residue (Residue,java and SeqResidue.java)
 <div>
 @author Russell Elbert Williams
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================
public enum ResidueType {

    STANDARD_AMINO_ACID,
    NON_STANDARD_AMINO_ACID,
    NUCLEOTIDE,
    UNKNOWN;

}
