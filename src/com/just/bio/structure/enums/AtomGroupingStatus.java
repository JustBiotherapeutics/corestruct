package com.just.bio.structure.enums;


//======================================================================================================
/**
 Processing status for each AtomGrouping
 <div>
 @author Russell Elbert Williams
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================
public enum AtomGroupingStatus {

    NOT_YET_PROCESSED,
    FAIL,
    SUCCESS,
    NOT_APPLICABLE,
    NOTHING_TO_PROCESS;

}
