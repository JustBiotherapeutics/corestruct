package com.just.bio.structure.enums;


//======================================================================================================
/**
 Enum for each AtomGrouping - defines it's type based on what the atomGrouping is 'made of'
 <div>
 @author Russell Elbert Williams
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================
public enum ChainType
{
    PROTEIN,
    NUCLEIC_ACID,
    REPEATING_RESIDUE,
    UNKNOWN
}
