package com.just.bio.structure.enums;


//======================================================================================================
/**
 * Current structure format types (partially) supported by this parsing process:
 *      PDB -
 *          http://www.wwpdb.org/documentation/file-format  (NOTE: ENT is another extension for PDB files!)
 *      CIF -
 *          http://mmcif.wwpdb.org/
 *      MOE -
 *          https://www.chemcomp.com/MOE-Molecular_Operating_Environment.htm
 *          file:///apps/moe/moe2015/html/moe/fcnref/moefmt.htm
 *          file:///apps/moe/moe2015/html/index.htm
 *
 *      more structure/file format info: http://www.rcsb.org/pdb/static.do?p=file_formats/
 <div>
 @author Russell Elbert Williams
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================
public enum StructureFormatType
{

    PDB ("pdb"),
    ENT ("ent"),
    CIF ("cif"),
    MOE ("moe");

    private final String mExtension;

    //--------------------------------------------------------------------------
    StructureFormatType(String inExtension)
    {
        mExtension = inExtension;
    }

    //--------------------------------------------------------------------------
    public String extension()
    {
        return mExtension;
    }

}
