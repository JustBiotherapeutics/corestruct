package com.just.bio.structure;

import com.just.bio.structure.format.pdb.PDB_AtomName;

/** ----------------------------------------------------------------------------------------------------
 * Distance-related utility functions.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class DistanceUtil
{


   //--------------------------------------------------------------------------
   public static  Double distanceFromThisCarbonToThatNitrogen(AtomGroup inResidue1, AtomGroup inResidue2)
   {
      Double distance = null;

      Atom atom1Carbon   = inResidue1.getAtom(PDB_AtomName.C);
      Atom atom2Nitrogen = inResidue2.getAtom(PDB_AtomName.N);

      if(atom1Carbon != null
         && atom2Nitrogen != null)
      {
         distance = atom1Carbon.distanceTo(atom2Nitrogen);
      }

      return distance;
   }


   //--------------------------------------------------------------------------
   public static  Double distanceBetweenAlphaCarbons(AtomGroup inResidue1, AtomGroup inResidue2)
   {
      Double distance = null;

      Atom alphaCarbon1 = inResidue1.getAtom(PDB_AtomName.CA);
      Atom alphaCarbon2 = inResidue2.getAtom(PDB_AtomName.CA);

      if(alphaCarbon1 != null
            && alphaCarbon2 != null)
      {
         distance = alphaCarbon1.distanceTo(alphaCarbon2);
      }

      return distance;
   }

   //--------------------------------------------------------------------------
   public static Double minDistance(AtomGroup inResidue1, AtomGroup inResidue2)
   {
      Double minDistance = null;

      for (Atom atom1 : inResidue1.getAtoms())
      {
         for (Atom atom2 : inResidue2.getAtoms())
         {
            Double distance = atom1.distanceTo(atom2);
            if (null == minDistance
                || distance < minDistance)
            {
               minDistance = distance;
            }
         }
      }

      return minDistance;
   }
}
