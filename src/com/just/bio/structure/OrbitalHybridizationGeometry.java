package com.just.bio.structure;

/** ----------------------------------------------------------------------------------------------------
 * Orbital hybridization geometry values.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public enum OrbitalHybridizationGeometry
{
   sp,
   sp2,
   sp3,
   sp3d,
   sp3d2;
}
