package com.just.bio.structure;


import com.hfg.exception.InvalidValueException;
import com.hfg.units.length.Length;

/** ----------------------------------------------------------------------------------------------------
 * Represents a covalent bond between two atoms.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class CovalentBond implements Bond
{
   private float     mOrder; // 1 (-), 2 (=), 3 (≡)
   private Atom[]    mAtoms;
   private Length    mLength;


   //##########################################################################
   // CONSTRUCTORS
   //##########################################################################

   //--------------------------------------------------------------------------
   public CovalentBond(Atom inAtom1, int inBondOrder, Atom inAtom2)
   {
      mOrder = inBondOrder;
      mAtoms = new Atom[]{ inAtom1, inAtom2 };
   }

   //--------------------------------------------------------------------------
   public CovalentBond(Atom inAtom1, float inBondOrder, Atom inAtom2)
   {
      mOrder = inBondOrder;
      mAtoms = new Atom[]{ inAtom1, inAtom2 };
   }

   //##########################################################################
   // PUBLIC METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   /**
    * Returns the type of the bond.
    * @return the bond type
    */
   public BondType getType()
   {
      return BondType.COVALENT;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns the two atoms involved in the bond.
    * @return the two atoms involved in the bond as an Array of atoms.
    */
   public Atom[] getAtoms()
   {
      return mAtoms;
   }

   //--------------------------------------------------------------------------
   /**
    * Specifies the length of the bond.
    * @see <a href='https://en.wikipedia.org/wiki/Bond_order'>https://en.wikipedia.org/wiki/Bond_order</a>
    * @return this bond object to enable method chaining
    */
   private CovalentBond setBondOrder(float inValue)
   {
      if (inValue < 1
          || inValue > 3)
      {
         throw new InvalidValueException(inValue + " is an invalid bond order value. Value must be >= 1 and <= 3.");
      }
      
      mOrder = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns the bond order for the bond (1 = single bond, 2 = double bond, and 3 = triple bond).
    * @return the bond order value as an float
    */
   public float getBondOrder()
   {
      return mOrder;
   }

   //--------------------------------------------------------------------------
   /**
    * Specifies the length of the bond.
    * @param inValue the length of this bond
    * @return this bond object to enable method chaining
    */
   public CovalentBond setLength(Length inValue)
   {
      mLength = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns the length of the bond.
    * @return the bond length
    */
   public Length getLength()
   {
      return mLength;
   }
}
