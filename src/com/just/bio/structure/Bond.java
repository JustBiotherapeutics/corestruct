package com.just.bio.structure;

import com.hfg.units.length.Length;

/** ----------------------------------------------------------------------------------------------------
 * Represents a bond between two atoms.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public interface Bond
{
   //--------------------------------------------------------------------------
   /**
    * Returns the type of the bond.
    * @return the bond type
    */
   public BondType getType();

   //--------------------------------------------------------------------------
   /**
    * Returns the two atoms involved in the bond.
    * @return the two atoms involved in the bond as an Array of atoms.
    */
   public Atom[] getAtoms();

   //--------------------------------------------------------------------------
   /**
    * Returns the length of the bond.
    * @return the bond length
    */
   public Length getLength();
}
