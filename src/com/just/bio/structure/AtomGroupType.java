package com.just.bio.structure;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public enum AtomGroupType
{
   AMINO_ACID,
   HETERO_ATOM,
   NUCLEOTIDE
}
