package com.just.bio.structure.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

/**
 * Services allowing access to PDB Structure files accessible via a directory path
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class RemoteFileRepoService
{
   //todo comment all this stuff


   private File tempDir = new File("/web/tmp");  //todo cleanup


   private static RemoteFileRepoService sInstance;

   public synchronized static RemoteFileRepoService getInstance()
   {
      if (sInstance == null)
      {
         sInstance = new RemoteFileRepoService();
      }
      return sInstance;
   }

   private RemoteFileRepoService()
   {
   }


   public File getTempDir()
   {
      return tempDir;
   }

   public RemoteFileRepoService setTempDir(File tempDir)
   {
      this.tempDir = tempDir;
      return this;
   }


   //---------------------------------------------------------------------------------------------------------------------
   /**
    * taking in an existing remote structure file (possibly on a mirror on same computer) download a copy into our temp folder
    *
    * @param remoteFile the remote file
    * @return the downloaded local instance of the remote file
    * @throws IOException if an problem is encountered during download
    */
   public File downloadFileToLocal(File remoteFile)
         throws IOException
   {
      if (null == remoteFile)
      {
         throw new FileNotFoundException("Error retrieving Structure file content from: " + remoteFile.getAbsolutePath() + ". File content appears to be missing or null.");
      }

      File tempFile = new File(getTempDir(), remoteFile.getName());

      FileUtils.copyFile(remoteFile, tempFile);

      return tempFile;
   }


}
