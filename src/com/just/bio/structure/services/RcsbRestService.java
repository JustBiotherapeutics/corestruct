package com.just.bio.structure.services;

import com.hfg.util.FileUtil;
import com.hfg.util.StringUtil;

import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.pdb.StructureUtils;

import org.apache.commons.collections4.map.SingletonMap;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Services allowing access to PDB Structure files via RCSB REST APIs
 * <p>
 * http://www.rcsb.org/pdb/software/rest.do
 * http://www.rcsb.org/pdb/static.do?p=download/http/index.html
 */

//------------------------------------------------------------------------------------------------------
// Copyright (c) 2016 Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class RcsbRestService
{


   //todo comment all this stuff


   //urls to the file contents, and download - these will be overridden in the getInstance call
   private String restUrlPdbFile = "http://cftp.rcsb.org/view/";
   private String restUrlCifFile = "http://cftp.rcsb.org/view/";

   private String restUrlPdbDownload = "http://ftp.rcsb.org/download/";
   private String restUrlCIfDownload = "http://ftp.rcsb.org/download/";

   //setting to true means a bit quicker downloading
   private static boolean retrieveGzippedFile = true;
   private static String gzipExt = ".gz";


   private File tempDir = new File("/web/tmp");  //todo - should this be passsed in?


   private static RcsbRestService sInstance;

   /**
    * @param inRestUrlFile the URL for viewing files
    * @param inRestUrlDownload the URL for downloading files
    * @return the initialized RCSB REST service
    */
   public synchronized static RcsbRestService getInstance(String inRestUrlFile, String inRestUrlDownload)
   {
      if (sInstance == null)
      {
         sInstance = new RcsbRestService(inRestUrlFile, inRestUrlDownload);
      }
      return sInstance;
   }

   private RcsbRestService(String inRestUrlFile, String inRestUrlDownload)
   {
      this.restUrlPdbFile = inRestUrlFile;
      this.restUrlCifFile = inRestUrlFile;
      this.restUrlPdbDownload = inRestUrlDownload;
      this.restUrlCIfDownload = inRestUrlDownload;
   }


   public File getTempDir()
   {
      return tempDir;
   }

   public RcsbRestService setTempDir(File tempDir)
   {
      this.tempDir = tempDir;
      return this;
   }


   //---------------------------------------------------------------------------------------------------------------------
   /**
    * create the URL for a PDB file from RCSB
    *
    * @param pdbIdentifier PDB Identifier of the file to return
    * @return the URL for retrieving the specified PDB entry
    */
   public String getRestUrlPdbFile(String pdbIdentifier)
   {
      if (null == pdbIdentifier)
      {
         throw new NullPointerException("PDB Identifier is missing; must be present to create PDB File URL: " + restUrlPdbDownload);
      }
      StringBuilder url = new StringBuilder(restUrlPdbDownload);
      url.append(pdbIdentifier);
      url.append(".");
      url.append(StructureFormatType.PDB.extension());
      if (retrieveGzippedFile)
      {
         url.append(".gz");
      }
      return url.toString();
   }


   /**
    * create the URL for a mmCIF file from RCSB
    *
    * @param pdbIdentifier PDB Identifier of the file to return
    * @return the URL for retrieving the specified PDB entry
    */
   public String getRestUrlCifFile(String pdbIdentifier)
   {
      if (null == pdbIdentifier)
      {
         throw new NullPointerException("PDB Identifier is missing; must be present to create PDB File URL: " + restUrlCIfDownload);
      }
      StringBuilder url = new StringBuilder(restUrlCIfDownload);
      url.append(pdbIdentifier);
      url.append(".");
      url.append(StructureFormatType.CIF.extension());
      if (retrieveGzippedFile)
      {
         url.append(".gz");
      }
      return url.toString();
   }


   /**
    * based on a PDB Identifier and extension, create a link to the structure page from RCSB
    *
    * @param pdbIdentifier a PDB identifier
    * @param fileExtension the structure file type extension
    * @return the URL for viewing the specified PDB entry
    */
   public String createRestUrlFileLink(String pdbIdentifier, String fileExtension)
   {
      StringBuilder url = new StringBuilder();
      if (fileExtension.equalsIgnoreCase(StructureFormatType.CIF.extension()))
      {
         url.append(restUrlCifFile);
      }
      else
      {
         url.append(restUrlPdbFile);
      }
      url.append(pdbIdentifier);
      url.append(".");
      url.append(fileExtension);
      return url.toString();
   }

   /**
    * based on a PDB Identifier and extension, create a link to the structure download from RCSB
    *
    * @param pdbIdentifier a PDB identifier
    * @param fileExtension the structure file type extension
    * @return the URL for downloading the specified PDB entry
    */
   public String createRestUrlDownloadLink(String pdbIdentifier, String fileExtension)
   {
      StringBuilder url = new StringBuilder();
      if (fileExtension.equalsIgnoreCase(StructureFormatType.CIF.extension()))
      {
         url.append(restUrlCIfDownload);
      }
      else
      {
         url.append(restUrlPdbDownload);
      }
      url.append(pdbIdentifier);
      url.append(".");
      url.append(fileExtension);
      return url.toString();
   }


   /**
    * based on a PDB Identifier retrieve the content of a pdb file from RCSB (uses REST)
    *
    * @param pdbIdentifier a PDB identifier
    * @return a map containing urls and their corresponding downloaded files
    * @throws FileNotFoundException thrown if a problem is encountered during download
    */
   public SingletonMap<String, File> retrievePdbContentByPdbIdentifier(String pdbIdentifier)
         throws FileNotFoundException
   {
      File file;
      String restUrl;

      try
      {
         restUrl = getRestUrlPdbFile(pdbIdentifier);
         file = downloadFileFromUrl(restUrl);
      } catch (IOException ioePdb)
      {

         try
         {
            restUrl = getRestUrlCifFile(pdbIdentifier);
            file = downloadFileFromUrl(restUrl);
         } catch (IOException ioeCif)
         {

            throw new FileNotFoundException("Error retrieving RCSB file content from: " + getRestUrlPdbFile(pdbIdentifier) + " or " + getRestUrlCifFile(pdbIdentifier) + ". File/Structure at these URLs may no longer exist. \n" + "PDB file error: " + ioePdb.getMessage() + "; CIF file error: " + ioeCif.getMessage());
         }

      }

      if (null == file)
      {
         throw new FileNotFoundException("Error retrieving RCSB file content from: " + getRestUrlPdbFile(pdbIdentifier) + " or " + getRestUrlCifFile(pdbIdentifier) + ". File content appears to be missing or null.");
      }

      return new SingletonMap<>(restUrl, file);
   }


   public boolean pdbFileExistsInRcsb(String pdbIdentifier)
   {
      String pdbUrl = getRestUrlPdbFile(pdbIdentifier);
      String cifUrl = getRestUrlCifFile(pdbIdentifier);

      boolean exists = false;

      try
      {
         HttpURLConnection.setFollowRedirects(false);
         HttpURLConnection con = (HttpURLConnection) new URL(pdbUrl).openConnection();
         con.setRequestMethod("HEAD");
         exists = (con.getResponseCode() == HttpURLConnection.HTTP_OK);

         if(!exists)
         {
            HttpURLConnection con2 = (HttpURLConnection) new URL(cifUrl).openConnection();
            con2.setRequestMethod("HEAD");
            exists = (con2.getResponseCode() == HttpURLConnection.HTTP_OK);
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
         return false;
      }

      return exists;
   }

   /**
    * return a file that is found at a specific URL
    *
    * @param fullUrl full URL to REST API of structure file
    * @return file found at fullUrl
    * @throws IOException if the file is not found
    */
   private File downloadFileFromUrl(String fullUrl)
         throws IOException
   {

      InputStream inputStream = null;

      //define location where the file will be copied to
      File file = new File(getTempDir(), StructureUtils.getFileName(fullUrl));

      try
      {

         URL url = new URL(fullUrl);
         HttpURLConnection connection = (HttpURLConnection) url.openConnection();
         connection.setRequestMethod("GET");
         connection.setInstanceFollowRedirects(false);
         connection.connect();

         int code = connection.getResponseCode();
         if (code == 404)
         {
            throw new FileNotFoundException("FileNotFound - Error retrieving file content from: " + fullUrl);
         }


         inputStream = connection.getInputStream();

         String contentDisposition = connection.getHeaderField("Content-Disposition");
         if (StringUtil.isSet(contentDisposition) && !contentDisposition.endsWith(gzipExt))
         {
            //file is not yet gzipped; gzip the incoming file
            file = new File(file.getParentFile(), file.getName() + gzipExt);
            FileUtil.writeGzipped(file, inputStream);
         }
         else
         {
            //file from REST service is already gzipped
            FileUtil.write(file, inputStream);
         }


      } finally
      {
         IOUtils.closeQuietly(inputStream);
      }

      return file;

   }


}
