package com.just.bio.structure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.hfg.bio.Nucleotide;
import com.hfg.util.AttributeMgr;
import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;

import com.just.bio.structure.enums.ChainType;

/** ----------------------------------------------------------------------------------------------------
 * Chain contains AtomGroups (residues) atomic coordinates of a biological sequence (protein or nucleotide) in a Model.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class Chain
{
   private String    mId;   //aka: chain identifier,   aka: chain name,   aka: strand id
   private String    mName; //aka: chain identifier,   aka: chain name,   aka: strand id
   private String    mUnqualifiedName; // Chain designator. ex: 'A'
   private String    mDescription;
   private ChainType mType; //aka: assumed chain type based on the AtomGroup types
   private Model     mParentModel;
   private ArrayList<AtomGroup> mGroups;
   private List<SeqResidue> mSeqResidues; // List of all SEQRES residues
   private AttributeMgr mAdditionalFormatAttributeMgr;

   private String  mTheoreticalSeq = null;      //sequence created from the SEQRES (or related) records
   private Boolean mTheoreticalHasStandardAminoAcids = null;
   private Boolean mTheoreticalHasNonStandardAminoAcids = null;
   private Boolean mTheoreticalHasNucleotides = null;


   private boolean mHasNonStandardAminoAcidResidues = false;

   private String mSequenceWithAllCoordinates = null;
   private String mSequenceWithAnyCoordinates = null;
   private String mSequenceWithCaCoordinates = null;
   private String mSequenceWithBackboneCoordinates = null;
   private String mSequenceWithBackboneCbCoordinates = null;


   public final static Character NO_MATCHING_AMINO_ACID_CHAR = 'X';
   public final static Character NO_MATCHING_NUCLEIC_CHAR = 'n';


   private static final Double mMaxDistanceBetweenUngappedCarbonToNitrogenAtoms = 1.84;
   private static final Double mMaxDistanceBetweenUngappedAlphaCarbonAtoms = 4.00;

   private static final HashMap<String, Character> sNucleotideMap = new HashMap<>(12);

   static
   {
      //setup the nucleotideMap
      sNucleotideMap.put("DC", 'c');
      sNucleotideMap.put("DG", 'g');
      sNucleotideMap.put("DA", 'a');
      sNucleotideMap.put("DU", 'u');
      sNucleotideMap.put("DT", 't');
      sNucleotideMap.put("DI", 'i');
      sNucleotideMap.put("C", 'c');
      sNucleotideMap.put("G", 'g');
      sNucleotideMap.put("A", 'a');
      sNucleotideMap.put("U", 'u');
      sNucleotideMap.put("T", 't');
      sNucleotideMap.put("I", 'i');
   }

   //##########################################################################
   // CONSTRUCTORS
   //##########################################################################

   //--------------------------------------------------------------------------
   public Chain(String inId)
   {
      mId = inId;
   }

   //##########################################################################
   // PUBLIC METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   public Chain setName(String inValue)
   {
      mName = inValue;

      if (StringUtil.isSet(mName))
      {
         int index = mName.indexOf(".");
         if (index > 0 && index < mName.length() - 1)
         {
            mUnqualifiedName = mName.substring(index + 1);
         }
         else
         {
            mUnqualifiedName = mName;
         }
      }

      return this;
   }

   //--------------------------------------------------------------------------
   public String getUnqualifiedName()
   {
      return mUnqualifiedName;
   }

   //--------------------------------------------------------------------------
   public String name()
   {
      return mName;
   }

   //--------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return name() != null ? name() : mId;
   }

   //--------------------------------------------------------------------------
   public String getId()
   {
      return mId;
   }

   //--------------------------------------------------------------------------
   public Chain setDescription(String inValue)
   {
      mDescription = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   public String getDescription()
   {
      return mDescription;
   }

   //--------------------------------------------------------------------------
   public ChainType getType()
   {
      if (null == mType)
      {
         mType = ChainType.UNKNOWN;

         if (CollectionUtil.hasValues(getAtomGroups()))
         {
            Set<ChainType> chainTypes = new HashSet<>(3);
            for (AtomGroup atomGroup : getAtomGroups())
            {
               if (atomGroup.getType().equals(AtomGroupType.AMINO_ACID))
               {
                  chainTypes.add(ChainType.PROTEIN);
               }
               else if (atomGroup.getType().equals(AtomGroupType.NUCLEOTIDE))
               {
                  chainTypes.add(ChainType.NUCLEIC_ACID);
               }
            }

            if (1 == chainTypes.size())
            {
               mType = chainTypes.iterator().next();
            }
         }
      }

      return mType;
   }


   //--------------------------------------------------------------------------
   public Chain setParentModel(Model inValue)
   {
      mParentModel = inValue;
      return this;
   }

   //--------------------------------------------------------------------------
   public Model getParentModel()
   {
      return mParentModel;
   }

   //--------------------------------------------------------------------------
   public Chain trimToSize()
   {
      if (mGroups != null)
      {
         mGroups.trimToSize();
      }

      return this;
   }

   //--------------------------------------------------------------------------
   public List<AtomGroup> getAtomGroups()
   {
      return mGroups;
   }

   //--------------------------------------------------------------------------
   public void setAtomGroups(List<AtomGroup> inValues)
   {
      mGroups = CollectionUtil.hasValues(inValues) ? new ArrayList<>(inValues) : null;
   }

   //--------------------------------------------------------------------------
   public Chain addAtomGroup(AtomGroup inGroup)
   {
      if (null == mGroups)
      {
         mGroups = new ArrayList<>();
      }

      mGroups.add(inGroup);

      inGroup.setParentChain(this);

      return this;
   }

   //--------------------------------------------------------------------------
   public List<SeqResidue> getSeqResidues()
   {
      return mSeqResidues;
   }

   //--------------------------------------------------------------------------
   public Chain setSeqResidues(List<SeqResidue> inValues)
   {
      mSeqResidues = inValues;

      return this;
   }


   //--------------------------------------------------------------------------
   public String getTheoreticalSeq()
   {
      if(!StringUtil.isSet(mTheoreticalSeq))
      {
         mTheoreticalSeq = createTheoreticalSequence();
      }
      return mTheoreticalSeq;
   }

   //--------------------------------------------------------------------------
   public Boolean theoreticalHasNucleotides()
   {
      if(null==mTheoreticalHasNucleotides)
      {
         createTheoreticalSequence();
      }
      return mTheoreticalHasNucleotides;
   }

   //--------------------------------------------------------------------------
   public Boolean theoreticalHasNonStandardAminoAcids()
   {
      if(null==mTheoreticalHasNonStandardAminoAcids)
      {
         createTheoreticalSequence();
      }
      return mTheoreticalHasNonStandardAminoAcids;
   }

   //--------------------------------------------------------------------------
   public Boolean theoreticalHasStandardAminoAcids()
   {
      if(null==mTheoreticalHasStandardAminoAcids)
      {
         createTheoreticalSequence();
      }
      return mTheoreticalHasStandardAminoAcids;
   }


   //--------------------------------------------------------------------------
   /**
    * Returns sequence where residues have ALL the atom coordinates for their amino acid.
    * @return sequence where residues have ALL the atom coordinates for their amino acid
    */
   public String getSequenceWithAllCoordinates()
   {
      createSequenceVariations();
      return mSequenceWithAllCoordinates;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns sequence where residues have any coordinates.
    * @return sequence where residues have any coordinates
    */
   public String getSequenceWithAnyCoordinates()
   {
      createSequenceVariations();
      return mSequenceWithAnyCoordinates;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns sequence where residues have [at minimum] atom coordinates for the Carbon Alpha atom (CA).
    * @return sequence where residues have [at minimum] atom coordinates for the Carbon Alpha atom (CA)
    */
   public String getSequenceWithCaCoordinates()
   {
      createSequenceVariations();
      return mSequenceWithCaCoordinates;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns sequence where residues have [at minimum] atom coordinates for the backbone atoms (N, CA, C).
    * @return sequence where residues have [at minimum] atom coordinates for the backbone atoms (N, CA, C)
    */
   public String getSequenceWithBackboneCoordinates()
   {
      createSequenceVariations();
      return mSequenceWithBackboneCoordinates;
   }

   //--------------------------------------------------------------------------
   /**
    * Returns sequence where residues have [at minimum] atom coordinates for the backbone + CB atoms (N, CA, C, CB).
    * @return sequence where residues have [at minimum] atom coordinates for the backbone + CB atoms (N, CA, C, CB)
    */
   public String getSequenceWithBackboneCbCoordinates()
   {
      createSequenceVariations();
      return mSequenceWithBackboneCbCoordinates;
   }

   //--------------------------------------------------------------------------
   public Collection<String> getAdditionalFormatAttributeNames()
   {
      Collection<String> attrNames = null;
      if (mAdditionalFormatAttributeMgr != null)
      {
         attrNames = mAdditionalFormatAttributeMgr.getAttributeNames();
      }

      return attrNames;
   }

   //--------------------------------------------------------------------------
   public Object getAdditionalFormatAttribute(String inName)
   {
      Object attr = null;
      if (mAdditionalFormatAttributeMgr != null)
      {
         attr = mAdditionalFormatAttributeMgr.getAttribute(inName);
      }

      return attr;
   }

   //--------------------------------------------------------------------------
   public void setAdditionalFormatAttribute(String inName, Object inAttribute)
   {
      if (null == mAdditionalFormatAttributeMgr)
      {
         mAdditionalFormatAttributeMgr = new AttributeMgr();
      }

      mAdditionalFormatAttributeMgr.setAttribute(inName, inAttribute);
   }

   //##########################################################################
   // PRIVATE METHODS
   //##########################################################################

   //--------------------------------------------------------------------------
   /**
    * Creates the theoretical sequence (aka 'expected' sequence) from the SEQRES data
    * this iterates thru all the residue values (3 character abbreviation), and creates
    * the sequence by translating them into their 1 character abbreviation
    * @return the chain's sequence in 1-letter code
    */
   private String createTheoreticalSequence()
   {
      mTheoreticalHasNucleotides = false;
      mTheoreticalHasNonStandardAminoAcids = false;
      mTheoreticalHasStandardAminoAcids = false;

      StringBuilderPlus seq = null;
      if (CollectionUtil.hasValues(mSeqResidues))
      {
         seq = new StringBuilderPlus();

         for (SeqResidue seqResidue : mSeqResidues)
         {
            String seqResAbbrev = seqResidue.name();

            if (sNucleotideMap.containsKey(seqResAbbrev))
            {
               mTheoreticalHasNucleotides = true;
               seq.append(seqResAbbrev.toLowerCase());
            }
            else if (mTheoreticalHasNucleotides)
            {
               seq.append(NO_MATCHING_NUCLEIC_CHAR);
            }
            else
            {
               //add each residue's 1 char abbrev to the string sequence
               AminoAcid3D aa3d = AminoAcid3D.valueOf(seqResidue.name());

               seq.append(aa3d != null && aa3d.getAminoAcid() != null ? aa3d.getAminoAcid().getOneLetterCode() : NO_MATCHING_AMINO_ACID_CHAR);

               //additionally we can evaluate the diff residues, and flag what types of residues the sequence has
               if (aa3d != null)
               {
                  if (aa3d.isStandard())
                  {
                     mTheoreticalHasStandardAminoAcids = true;
                  }
                  else
                  {
                     mTheoreticalHasNonStandardAminoAcids = true;
                  }
               }
            }
         }
      }

      //if the sequence is a nucleic acid, change any 'X's to 'n's
      if(mTheoreticalHasNucleotides)
      {
         seq.replaceAll(NO_MATCHING_AMINO_ACID_CHAR, NO_MATCHING_NUCLEIC_CHAR);
      }

      return seq != null ? seq.toString() : null;
   }

   //--------------------------------------------------------------------------
   private void createSequenceVariations()
   {
      StringBuilder seqWithAllCoords = new StringBuilder();
      StringBuilder seqWithAnyCoords = new StringBuilder();
      StringBuilder seqWithCaCoords = new StringBuilder();
      StringBuilder seqWithBkbnCoords = new StringBuilder();
      StringBuilder seqWithBkbnCbCoords = new StringBuilder();

      AtomGroup prevAtomGroup = null;
      for(AtomGroup atomGroup : getAtomGroups())
      {
         Character residueChar = null;
         if (atomGroup.getType().equals(AtomGroupType.AMINO_ACID))
         {
            AminoAcid3D aa3d = AminoAcid3D.valueOf(atomGroup.name());
            residueChar = (aa3d != null && aa3d.getAminoAcid() != null ? aa3d.getAminoAcid().getOneLetterCode() : NO_MATCHING_AMINO_ACID_CHAR);
         }
         else if (atomGroup.getType().equals(AtomGroupType.NUCLEOTIDE))
         {
            Nucleotide nuc = Nucleotide.valueOf(atomGroup.name());
            residueChar = (nuc != null ? nuc.getOneLetterCode() : NO_MATCHING_NUCLEIC_CHAR);
         }
         else // Hetero atom group
         {
            if (atomGroup.getAtoms().size() > 3)
            {
               if (prevAtomGroup != null)
               {
                  // Is this group 'close' to the previous group?
                  // Test if this residue "connects" to the previous residue
                  if (DistanceUtil.minDistance(prevAtomGroup, atomGroup) < 5)
                  {
                     residueChar = NO_MATCHING_AMINO_ACID_CHAR;
                  }
               }
               else
               {
                  residueChar = NO_MATCHING_AMINO_ACID_CHAR;
               }
            }
         }

         // TODO: Test if this residue "connects" to the previous residue, else insert a gap
         if (prevAtomGroup != null
             && prevAtomGroup.getType().equals(atomGroup.getType()))
         {
            if (isGapBetweenResidues(prevAtomGroup, atomGroup))
            {
               seqWithAllCoords.append("-");
               seqWithAnyCoords.append("-");
               seqWithCaCoords.append("-");
               seqWithBkbnCoords.append("-");
               seqWithBkbnCbCoords.append("-");
            }
         }

         if (residueChar != null)
         {
            seqWithAllCoords.append(atomGroup.hasFullCoordinates() ? residueChar : "-");
            seqWithAnyCoords.append(atomGroup.hasAnyCoordinates() ? residueChar : "-");
            seqWithCaCoords.append(atomGroup.hasCaCoordinates() ? residueChar : "-");
            seqWithBkbnCoords.append(atomGroup.hasBackboneCoordinates() ? residueChar : "-");
            seqWithBkbnCbCoords.append(atomGroup.hasBackboneCbCoordinates() ? residueChar : "-");
         }

         prevAtomGroup = atomGroup;
      }

      mSequenceWithAllCoordinates = seqWithAllCoords.toString();
      mSequenceWithAnyCoordinates = seqWithAnyCoords.toString();
      mSequenceWithCaCoordinates = seqWithCaCoords.toString();
      mSequenceWithBackboneCoordinates = seqWithBkbnCoords.toString();
      mSequenceWithBackboneCbCoordinates = seqWithBkbnCbCoords.toString();
   }


   //--------------------------------------------------------------------------
   private boolean isGapBetweenResidues(AtomGroup inResidue1, AtomGroup inResidue2)
   {
      //first check distance from 'this C' to 'that N'
      Double distanceCarbonToNitrogen = DistanceUtil.distanceFromThisCarbonToThatNitrogen(inResidue1, inResidue2);

      if(distanceCarbonToNitrogen != null)
      {
         return distanceCarbonToNitrogen > mMaxDistanceBetweenUngappedCarbonToNitrogenAtoms;
      }

      //if coordinates weren't available for C to N measurement, check CA to CA
      Double distanceAlphaCarbons = DistanceUtil.distanceBetweenAlphaCarbons(inResidue1, inResidue2);

      if(distanceAlphaCarbons != null)
      {
         return distanceAlphaCarbons > mMaxDistanceBetweenUngappedAlphaCarbonAtoms;
      }

      return false;
   }

}
