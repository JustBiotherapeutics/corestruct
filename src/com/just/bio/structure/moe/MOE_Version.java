package com.just.bio.structure.moe;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hfg.util.StringUtil;

//======================================================================================================
/**
 Container for the MOE version information.
 <div>
 @author J. Alex Taylor
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class MOE_Version
{
   private int mYear;
   private String mMajorReleaseNum;
   private String mMinorReleaseNum;

   private static Pattern sPattern = Pattern.compile("(\\d{4})\\.(\\d+)");

   //------------------------------------------------------------------------
   // Ex: '2016.08'
   public MOE_Version(String inString)
   {
      Matcher m = sPattern.matcher(inString.trim());
      if (! m.matches())
      {
         throw new RuntimeException("The MOE version string " + StringUtil.singleQuote(inString) + " couldn't be parsed!");
      }

      mYear = Integer.parseInt(m.group(1));

      String releaseNum = m.group(2);
      mMajorReleaseNum = releaseNum.substring(0, 2);
      if (releaseNum.length() > 2)
      {
         mMinorReleaseNum = releaseNum.substring(2);
      }
   }

   //------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return mYear + "." + mMajorReleaseNum + (mMinorReleaseNum != null ? mMinorReleaseNum : "");
   }

   //------------------------------------------------------------------------
   public int getReleaseYear()
   {
      return mYear;
   }

   //------------------------------------------------------------------------
   public String getMajorReleaseNum()
   {
      return mMajorReleaseNum;
   }

   //------------------------------------------------------------------------
   public String getMinorReleaseNum()
   {
      return mMinorReleaseNum;
   }
}
