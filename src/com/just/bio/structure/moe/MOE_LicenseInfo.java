package com.just.bio.structure.moe;

import java.io.File;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.OrderedMap;

//======================================================================================================
/**
 * Container for MOE license information.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class MOE_LicenseInfo
{
   /*
      svl> LicenseInfo[];
      Path    : /Applications/moe2022_site/license.dat
      Feature : moe_site
      Version : 2025.01
      Expires : 31-jan-2025 (0 days left)
      Key     : 006C 4189 D672 ... 7BED 09E5 DD01
      Server  :
      Tokens  : 0 (0 used)
      Notice  : EvotecUK
      User    : USER=ataylor HOSTNAME=maleficent
      HostID  : a0781...
      Display : DISPLAY=
      Dongle  : 0
      FlexNet : v11.18.1.0 build 279526 arm64_mac11
      Daemon  :
      Borrow  : Off
    */

   Map<String, String> mInfoMap = new OrderedMap<>(20);

   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   //------------------------------------------------------------------------
   public MOE_LicenseInfo(String inString)
   {
      String[] lines = inString.split("[\r\n]+");

      for (String line : lines)
      {
         int index = line.indexOf(':');
         String key = line.substring(0, index).trim();
         String value = line.substring(index + 1).trim();
         mInfoMap.put(key, value);
      }
   }


   //###########################################################################
   // PUBLIC METHODS
   //###########################################################################

   //------------------------------------------------------------------------
   public int getNumDaysUntilExpiration()
   {
      int result = -1;
      String expiration = getExpires();
      if (StringUtil.isSet(expiration))
      {
         Pattern pattern = Pattern.compile("\\((\\d+) days left\\)");
         Matcher matcher = pattern.matcher(expiration);
         if (matcher.find())
         {
            result = Integer.parseInt(matcher.group(1));
         }
      }

      return result;
   }

   //------------------------------------------------------------------------
   @Override
   public String toString()
   {
      StringBuilderPlus buffer = new StringBuilderPlus().setDelimiter("\n");
      for (Map.Entry<String, String> entry : mInfoMap.entrySet())
      {
         buffer.delimitedAppend(entry.getKey() + " : " + entry.getValue());
      }

      return buffer.toString();
   }

   //------------------------------------------------------------------------
   /**
    * Returns the path to the license file in use.
    * @return the path to the license file in use
    */
   public File getPath()
   {
      return new File(mInfoMap.get("Path"));
   }

   //------------------------------------------------------------------------
   /**
    * Returns the name of the licensed application.
    * @return the name of the licensed application
    */
   public String getFeature()
   {
      return mInfoMap.get("Feature");
   }

   //------------------------------------------------------------------------
   /**
    * Returns the highest numbered version allowed.
    * @return the highest numbered version allowed
    */
   public String getVersion()
   {
      return mInfoMap.get("Version");
   }

   //------------------------------------------------------------------------
   /**
    * Returns the expiry date, given as dd-mmm-yyyy. If year is 0000, the feature never expires.
    * @return the expiry date, given as dd-mmm-yyyy. If year is 0000, the feature never expires
    */
   public String getExpires()
   {
      return mInfoMap.get("Expires");
   }

   //------------------------------------------------------------------------
   /**
    * Returns the hostname or IP address of the computer acting as license server.
    * @return the hostname or IP address of the computer acting as license server
    */
   public String getServer()
   {
      return mInfoMap.get("Server");
   }

   //------------------------------------------------------------------------
   /**
    * Returns the total number of tokens hosted by the license server and the
    * number of tokens currently in use. A value of 0 means unlimited.
    * @return the total number of tokens hosted by the license server and the
    *         number of tokens currently in use
    */
   public String getTokens()
   {
      return mInfoMap.get("Tokens");
   }
}
