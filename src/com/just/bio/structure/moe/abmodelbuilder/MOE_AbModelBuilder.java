package com.just.bio.structure.moe.abmodelbuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import com.hfg.bio.seq.Protein;
import com.hfg.bio.seq.format.FASTA;
import com.hfg.util.FileUtil;
import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.io.RuntimeIOException;
import com.hfg.util.io.StreamUtil;

import com.just.bio.structure.moe.MOE_Batch;


//======================================================================================================
/**
 * Wrapper for creating structure models with the MOE model builder.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class MOE_AbModelBuilder
{
   private MOE_AbModelBuilderSettings mSettings;

   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   //---------------------------------------------------------------------------
   public MOE_AbModelBuilder()
   {

   }

   //---------------------------------------------------------------------------
   public MOE_AbModelBuilder(MOE_AbModelBuilderSettings inSettings)
   {
      mSettings = inSettings;
   }

   //###########################################################################
   // PUBLIC METHODS
   //###########################################################################

   //---------------------------------------------------------------------------
   public MOE_AbModelBuilderSettings getSettings()
   {
      if (null == mSettings)
      {
         mSettings = new MOE_AbModelBuilderSettings();
      }

      return mSettings;
   }

   //---------------------------------------------------------------------------
   public File run(Protein inProtein)
      throws Exception
   {
      File workingDir = getSettings().getWorkingDir();

      File fastaFile = generateFastaFile(workingDir, inProtein);

      String baseFilename = FileUtil.getNameMinusExtension(fastaFile);

      File mdbFile = new File(workingDir, baseFilename + ".mdb");

      File modelFile = new File(workingDir, baseFilename + ".moe");


      // Invoke the modeler
      StringBuilderPlus modelSVL = new StringBuilderPlus()
              .appendln("// Build the model mdb")
              .appendln("opt = [Fv_model_type:" + StringUtil.singleQuote(mSettings.getFvModelType()) + ", fragment:" + StringUtil.singleQuote(mSettings.getFragment()) + ", output_mdb:" + StringUtil.singleQuote(mdbFile.getName()) + ", model_tag:" + StringUtil.singleQuote(inProtein.getID()) + ", cap_nterm:0, cap_cterm:0" + (getSettings().getRequireSS_Bond() ? ", require_ssbond:1" : "") + "];")
              .appendln("ab_Model[" + StringUtil.singleQuote(fastaFile.getName()) + ", opt];")
              .appendln("Close[force:1];")
              .appendln()
              .appendln("mdb = db_Open[" + StringUtil.singleQuote(mdbFile.getName()) + ", 'read'];")
              .appendln("   entry = db_NextEntry [mdb, 0];")
              .appendln("   mol = db_ReadFields[mdb, entry, 'model'];")
              .appendln("   _moe_Create[mol];")
              .appendln("   baseName = " + StringUtil.singleQuote(baseFilename) + ";")

              .appendln("   // Write the model to a file")
              .appendln("   WriteMOE[twrite ['{}.moe', baseName]];")

              .appendln("   Close[force:1];");


      File modelSvlScriptFile = new File(workingDir, baseFilename + "_script.svl");
      FileUtil.write(modelSvlScriptFile, modelSVL.toString());

      MOE_Batch moeBatch = new MOE_Batch()
            .setWorkingDir(workingDir)
            .setJavaHome(new File(System.getProperty("java.home")))
            .setStdMode(true)
            .setSvlScript(modelSvlScriptFile);

      moeBatch.execute();

      return modelFile;
   }


   //###########################################################################
   // PRIVATE METHODS
   //###########################################################################

   //------------------------------------------------------------------------
   // Create a FASTA file for the structure chains in the temp dir
   private File generateFastaFile(File inTempDir, Protein inProtein)
         throws Exception
   {
      FASTA formatObj = new FASTA();

      String seqName = inProtein.getID();
      if (!StringUtil.isSet(seqName))
      {
         seqName =  "model";
      }

      // Don't let wacky characters in the names screw up the scripts
      seqName = FileUtil.sanitizeFilename(seqName);

      File fastaFile = new File(inTempDir, seqName + ".fasta");
      Writer writer = null;
      try
      {
         if (! inTempDir.exists())
         {
            if (! inTempDir.mkdirs())
            {
               throw new RuntimeIOException("Couldn't create working dir " + StringUtil.singleQuote(inTempDir.getPath()) + "!");
            }
         }

         writer = new FileWriter(fastaFile);
         if (inProtein.hasChains())
         {
            for (Protein chain : inProtein.getChains())
            {
               formatObj.write(chain, writer);
            }
         }
         else
         {
            formatObj.write(inProtein, writer);
         }
      }
      finally
      {
         StreamUtil.close(writer);
      }

      return fastaFile;
   }
}
