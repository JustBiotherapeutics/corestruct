package com.just.bio.structure.moe.abmodelbuilder;

//======================================================================================================
/**
 Enumeration of fragment types for use with the MOE model builder.
 <div>
 @author J. Alex Taylor
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public enum Fragment
{
   Fv,
   seq,
   Fab,
   Fab2,
   rIg,
   Ig;
}
