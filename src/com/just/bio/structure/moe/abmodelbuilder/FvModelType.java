package com.just.bio.structure.moe.abmodelbuilder;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

//======================================================================================================
/**
 Enumeration of Fv model types for use with the MOE model builder.
 <div>
 @author J. Alex Taylor
 </div>
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class FvModelType
{
   private static final Map<String, FvModelType> VALUE_MAP = new HashMap<>(4);

   public static final FvModelType VL_VH = new FvModelType("VL, VH");
   public static final FvModelType VHH   = new FvModelType("VHH");

   private String mValue;


   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   //---------------------------------------------------------------------------
   private FvModelType(String inValue)
   {
      mValue = inValue;
      VALUE_MAP.put(mValue, this);
   }

   //###########################################################################
   // PUBLIC METHODS
   //###########################################################################

   //---------------------------------------------------------------------------
   @Override
   public String toString()
   {
      return mValue;
   }

   //---------------------------------------------------------------------------
   @Override
   public int hashCode()
   {
      return mValue.hashCode();
   }

   //---------------------------------------------------------------------------
   @Override
   public boolean equals(Object inObj2)
   {
      return inObj2 instanceof FvModelType
             && inObj2.toString().equals(toString());
   }

   //---------------------------------------------------------------------------
   public static Collection<FvModelType> values()
   {
      return Collections.unmodifiableCollection(VALUE_MAP.values());
   }

   //---------------------------------------------------------------------------
   public static FvModelType valueOf(String inString)
   {
      return VALUE_MAP.get(inString);
   }
}

