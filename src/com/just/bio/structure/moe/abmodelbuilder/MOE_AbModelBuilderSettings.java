package com.just.bio.structure.moe.abmodelbuilder;

import com.hfg.html.Select;
import com.hfg.setting.BooleanSetting;
import com.hfg.setting.Settings;
import com.hfg.setting.StringSetting;

import java.io.File;

//======================================================================================================
/**
 * Settings for creating structure models with the MOE model builder.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class MOE_AbModelBuilderSettings extends Settings implements Cloneable
{
   public static final String FV_MODEL_TYPE          = "fvModelType";
   public static final String FRAGMENT               = "fragment";
   public static final String WORKING_DIR            = "workingDir";
   public static final String REQUIRE_SS_BOND        = "requireSS_Bond";

   private static final FvModelType sDefaultFvModelType   = FvModelType.VL_VH;
   private static final Fragment    sDefaultFragment      = Fragment.Fab;
   private static final File        sDefaultWorkingDir    = new File("/tmp");
   private static final Boolean    sDefaultRequireSS_Bond = true;

   //###########################################################################
   // CONSTRUCTORS
   //###########################################################################

   //---------------------------------------------------------------------------
   public MOE_AbModelBuilderSettings()
   {
      init();
   }

   //---------------------------------------------------------------------------
   protected void init()
   {
      add(new StringSetting(FV_MODEL_TYPE, sDefaultFvModelType.toString()));
      add(new StringSetting(FRAGMENT, sDefaultFragment.name()));
      add(new StringSetting(WORKING_DIR, sDefaultWorkingDir.getPath()));
      add(new BooleanSetting(REQUIRE_SS_BOND, sDefaultRequireSS_Bond));
   }

   //###########################################################################
   // PUBLIC METHODS
   //###########################################################################

   //---------------------------------------------------------------------------
   @Override
   public MOE_AbModelBuilderSettings clone()
   {
      MOE_AbModelBuilderSettings cloneObj = (MOE_AbModelBuilderSettings) super.clone();

      return cloneObj;
   }


   //---------------------------------------------------------------------------
   @SuppressWarnings("unchecked")
   public MOE_AbModelBuilderSettings setFragment(Fragment inValue)
   {
      get(FRAGMENT).setValue(inValue != null ? inValue.name() : null);
      return this;
   }

   //---------------------------------------------------------------------------
   public Fragment getFragment()
   {
      String stringValue = (String) get(FRAGMENT).getValue();
      return stringValue != null ? Fragment.valueOf(stringValue) : null;
   }

   //---------------------------------------------------------------------------
   public Select generateFragmentMenu()
   {
      Fragment selectedValue = getFragment();
      Select menu = new Select(FRAGMENT);
      for (Fragment fragment : Fragment.values())
      {
         menu.addOption(fragment.name(), fragment.name(), fragment.equals(selectedValue));
      }

      return menu;
   }

   //---------------------------------------------------------------------------
   @SuppressWarnings("unchecked")
   public MOE_AbModelBuilderSettings setFvModelType(FvModelType inValue)
   {
      get(FV_MODEL_TYPE).setValue(inValue != null ? inValue.toString() : null);
      return this;
   }

   //---------------------------------------------------------------------------
   public FvModelType getFvModelType()
   {
      String stringValue = (String) get(FV_MODEL_TYPE).getValue();
      return stringValue != null ? FvModelType.valueOf(stringValue) : null;
   }

   //---------------------------------------------------------------------------
   public Select generateFvModelTypeMenu()
   {
      FvModelType selectedValue = getFvModelType();
      Select menu = new Select(FV_MODEL_TYPE);
      for (FvModelType value : FvModelType.values())
      {
         menu.addOption(value.toString(), value.toString(), value.equals(selectedValue));
      }

      return menu;
   }


   //---------------------------------------------------------------------------
   @SuppressWarnings("unchecked")
   public MOE_AbModelBuilderSettings setWorkingDir(File inValue)
   {
      get(WORKING_DIR).setValue(inValue != null ? inValue.getPath() : null);
      return this;
   }

   //---------------------------------------------------------------------------
   public File getWorkingDir()
   {
      String stringValue = (String) get(WORKING_DIR).getValue();
      return stringValue != null ? new File(stringValue) : null;
   }


   //---------------------------------------------------------------------------
   /**
    * Specifies the value of the require_ssbond option. A value of true (default)
    * requires that only templates that have normal disulfide bonds are considered.
    * @param inValue the value to specify for the require_ssbond option
    * @return this settings object to enable method chaining
    */
   @SuppressWarnings("unchecked")
   public MOE_AbModelBuilderSettings setRequireSS_Bond(Boolean inValue)
   {
      get(REQUIRE_SS_BOND).setValue(inValue);
      return this;
   }

   //---------------------------------------------------------------------------
   public Boolean getRequireSS_Bond()
   {
      return (Boolean) get(REQUIRE_SS_BOND).getValue();
   }
}
