package com.just.bio.structure.moe;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.hfg.util.Executor;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;

//======================================================================================================
/**
 * Wrapper for executing moebatch commands.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class MOE_Batch
{

   private File mExec;
   private File mWorkingDir;
   private File mJavaHome;
   private File mSvlLoadDir;
   private List<File> mSvlLoadScripts;
   private CharSequence mCmd;
   private File mSvlCmdScript;
   private boolean mStdMode;

   private static File sDefaultExec;

   //###########################################################################
   // PUBLIC METHODS
   //###########################################################################

   //------------------------------------------------------------------------
   public static File getDefaultMOE_BatchExe()
   {
      if (null == sDefaultExec)
      {
         File moeHome = MOE.getMOE_Dir();
         if (moeHome != null)
         {
            sDefaultExec = new File(moeHome, "bin/moebatch");
         }
      }

      return sDefaultExec;
   }

   //------------------------------------------------------------------------
   public static void setDefaultMOE_BatchExe(File inValue)
   {
      sDefaultExec = inValue;
   }


   //------------------------------------------------------------------------
   public File getMOE_BatchExe()
   {
      File exe = mExec;
      if (null == exe)
      {
         exe = getDefaultMOE_BatchExe();
      }

      return exe;
   }

   //------------------------------------------------------------------------
   public MOE_Batch setMOE_BatchExe(File inValue)
   {
      mExec = inValue;
      return this;
   }


   //------------------------------------------------------------------------
   /**
    * Used to specify the Java executable to use with moebatch (via the SVL_JVM environment variable).
    * @param inValue the root directory of the Java installation to use
    * @return this object to enable method chaining
    */
   public MOE_Batch setJavaHome(File inValue)
   {
      mJavaHome = inValue;
      return this;
   }

   //------------------------------------------------------------------------
   public File getJavaHome()
   {
      return mJavaHome;
   }


   //------------------------------------------------------------------------
   public MOE_Batch setWorkingDir(File inValue)
   {
      mWorkingDir = inValue;
      return this;
   }

   //------------------------------------------------------------------------
   public File getWorkingDir()
   {
      return mWorkingDir;
   }


   //------------------------------------------------------------------------
   public MOE_Batch setSvlLoadDir(File inValue)
   {
      mSvlLoadDir = inValue;
      return this;
   }

   //------------------------------------------------------------------------
   public File getSvlLoadDir()
   {
      return mSvlLoadDir;
   }


   //------------------------------------------------------------------------
   public MOE_Batch setSvlLoadScripts(List<File> inValue)
   {
      mSvlLoadScripts = inValue;
      return this;
   }

   //------------------------------------------------------------------------
   public MOE_Batch addSvlLoadScript(File inValue)
   {
      if (null == mSvlLoadScripts)
      {
         mSvlLoadScripts = new ArrayList<>(4);
      }

      mSvlLoadScripts.add(inValue);

      return this;
   }

   //------------------------------------------------------------------------
   public List<File> getSvlLoadScripts()
   {
      return mSvlLoadScripts;
   }


   //------------------------------------------------------------------------
   public MOE_Batch setSvlCommand(CharSequence inValue)
   {
      mCmd = inValue;
      return this;
   }

   //------------------------------------------------------------------------
   public CharSequence getSvlCommand()
   {
      return mCmd;
   }


   //------------------------------------------------------------------------
   public MOE_Batch setSvlScript(File inValue)
   {
      mSvlCmdScript = inValue;
      return this;
   }

   //------------------------------------------------------------------------
   public File getSvlScript()
   {
      return mSvlCmdScript;
   }

   //------------------------------------------------------------------------
   /**
    Ignore any customizations. In particular, ignore the $HOME/.moe-menus and $HOME/.moe-rc files and the environment
    variables MOE_SVL_LOAD and MOE_SVL_STARTSCRIPT.
    * @param inValue whether or not to add the '-std' flag as a command-line option.
    * @return this object to facilitate method chaining
    */
   public MOE_Batch setStdMode(boolean inValue)
   {
      mStdMode = inValue;
      return this;
   }

   //------------------------------------------------------------------------
   public String generateMoebatchCommand()
   {
      if (! StringUtil.isSet(getSvlCommand())
          && null == getSvlScript())
      {
         throw new RuntimeException("No command or script specified for moebatch to execute!");
      }

      StringBuilder cmd = new StringBuilder();
      if (mWorkingDir != null)
      {
         cmd.append("cd " + mWorkingDir.getPath() + "; ");
      }

      cmd.append(getMOE_BatchExe().getPath());
      cmd.append(" -setenv MOE=" + StringUtil.replaceAll(MOE.getMOE_Dir().getAbsolutePath(), " ", "\\ "));

      if (mJavaHome != null)
      {
         cmd.append(" -setenv SVL_JVM=" + StringUtil.replaceAll(mJavaHome.getPath() + "/bin/java", " ", "\\ "));
      }

      if (CollectionUtil.hasValues(mSvlLoadScripts))
      {
         for (File svlScript : mSvlLoadScripts)
         {
            cmd.append(" -load " + svlScript.getPath());
         }
      }
      else
      {
         cmd.append(" -setenv MOE_SVL_LOAD=" + StringUtil.replaceAll((mSvlLoadDir != null ? mSvlLoadDir : MOE.getMOE_SVL_Directory()).getPath(), " ", "\\ "));
      }

      if (StringUtil.isSet(getSvlCommand()))
      {
         cmd.append(" -exec " + (StringUtil.isQuoted(mCmd) ? mCmd : "\"" + mCmd + "\""));
      }
      else if (getSvlScript() != null)
      {
         cmd.append(" -script " + StringUtil.quote(getSvlScript()));
      }

      // Standard mode?
      if (mStdMode)
      {
         cmd.append(" -std");
      }

      cmd.append(" -licwait -exit");

      return cmd.toString();
   }

   //------------------------------------------------------------------------
   public String execute()
         throws IOException
   {
      String cmd = generateMoebatchCommand();

      Executor executor = new Executor(cmd);
      
      // MOE's svl scripts aren't always good about using STDERR only for real errors
      // so we'll only throw an exception if there's a non-zero exit value

      int exitValue = executor.exec();
      if (exitValue != 0)
      {
         if (StringUtil.isSet(executor.getSTDERR()))
         {
            throw new IOException(executor.getSTDERR() + "\n\nCommand: " + cmd + "\nExit value: " + exitValue);
         }
         else
         {
            throw new IOException(cmd + "\nexited with value " + exitValue + "!");
         }
      }

      String output = executor.getSTDOUT();
      if (! StringUtil.isSet(output))
      {
         // Sometimes what should be in stdout is in stderr
         output = executor.getSTDERR();
      }

      return output;
   }

}
