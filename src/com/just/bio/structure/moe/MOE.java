package com.just.bio.structure.moe;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.hfg.util.FileUtil;
import com.hfg.util.StringUtil;

//======================================================================================================
/**
 * Contains MOE environment and configuration functions.
*/
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class MOE
{
   private static File sMOE_Dir = new File("/apps/moe/current");
   private static File sMOE_SVL_Dir;


   //###########################################################################
   // PUBLIC FUNCTIONS
   //###########################################################################

   //------------------------------------------------------------------------
   public static File getMOE_Dir()
   {
      return sMOE_Dir;
   }

   //------------------------------------------------------------------------
   public static void setMOE_Dir(File inValue)
   {
      sMOE_Dir = inValue;
   }


   //------------------------------------------------------------------------
   public static File getMOE_SVL_Directory()
   {
      if (null == sMOE_SVL_Dir)
      {
         sMOE_SVL_Dir = new File(getMOE_Dir(), "moe_svl");
      }

      return sMOE_SVL_Dir;
   }

   //------------------------------------------------------------------------
   public static MOE_Version getMOE_Version()
         throws IOException
   {
      if (! getMOE_Dir().exists())
      {
         throw new IOException("The MOE directory " + StringUtil.singleQuote(getMOE_Dir()) + " doesn't exist!");
      }

      File versionFile = new File(getMOE_Dir(), "version.txt");
      if (! versionFile.exists())
      {
         throw new IOException("The MOE version file " + StringUtil.singleQuote(versionFile) + " doesn't exist!");
      }

      List<String> lines = FileUtil.readLines(versionFile);

      return new MOE_Version(lines.get(0).split("\\s+")[0]);
   }

   //------------------------------------------------------------------------
   /**
    * Returns information about the status of the current MOE license.
    * @return license information
    */
   public static MOE_LicenseInfo getLicenseInfo()
   {
      MOE_LicenseInfo licenseInfo;

      MOE_Batch moeBatch = new MOE_Batch()
            .setStdMode(true)
            .setSvlCommand("LicenseInfo [];");

      try
      {
         licenseInfo = new MOE_LicenseInfo(moeBatch.execute());
      }
      catch (Exception e)
      {
         throw new RuntimeException("Problem retrieving MOE license info!", e);
      }

      return licenseInfo;
   }

}
