package com.just.bio.structure;

import java.io.File;

import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.pdb.*;


/** ----------------------------------------------------------------------------------------------------
 * A Structure contains one or more biological sequence models.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public interface Structure
{
    //===================================================================================================================
    // Sections of the file
    //===================================================================================================================

    //---------------------------------------------------------------------------
    /**
     * pojo representing the metadata portion of a PDB file
     * @return metadata portion of a PDB file
     */
    public PdbSectionMetadata getMetadata();
    public void setMetadata(PdbSectionMetadata inValue);

    //---------------------------------------------------------------------------
    /**
     * pojo representing the Remark portion of a PDB file
     * @return Remark portion of a PDB file
     */
    public PdbSectionRemark getRemark();
    public void setRemark(PdbSectionRemark inValue);

    //---------------------------------------------------------------------------
    /**
     * pojo representing the Connectivity portion of a PDB file
     * @return Connectivity portion of a PDB file
     */
    public PdbSectionConnectivity getConnectivity();
    public void setConnectivity(PdbSectionConnectivity inValue);

    //---------------------------------------------------------------------------
    /**
     * pojo representing the Primary portion of a PDB file
     * @return Primary portion of a PDB file
     */
    public PdbSectionPrimary getPrimary();
    public void setPrimary(PdbSectionPrimary inValue);


    //===================================================================================================================
    // additional calculated info
    //===================================================================================================================

    //---------------------------------------------------------------------------
    public Structure setAtomicCoordinates(AtomicCoordinates inValue);

    //---------------------------------------------------------------------------
    public AtomicCoordinates getAtomicCoordinates();

    //===================================================================================================================
    // File metadata
    //===================================================================================================================

    //---------------------------------------------------------------------------
    /**
     * enum describing the file format of this file - only a few file formats currently supported
     * @return the file format for the file this structure was extracted from
     */
    public StructureFormatType getFileFormat();
    public void setFileFormat(StructureFormatType inValue);

    //---------------------------------------------------------------------------
    /**
     * either the PDB Identifier that was passed in for the REST service, or a PDB Identifier that was parsed from the file name
     * @return the PDB identifier associated with this structure
     */
    public String getPdbIdentifier();
    public void setPdbIdentifier(String inValue);

    //---------------------------------------------------------------------------
    /**
     * the actual url that this file came from (for all possible URLs see RcsbRestService class)
     * @return the URL where this structure was obtained
     */
    public String getFileRestUrl();
    public void setFileRestUrl(String inValue);

    //---------------------------------------------------------------------------
    /**
     * the local file path that was passed in - used to parse the file
     * @return the remote file path used to retrieve this structure
     */
    public String getFilePath();
    public void setFilePath(String inValue);

    //---------------------------------------------------------------------------
    /**
     * the local "temporary" place the file was copied to
     * @return the local file associated with this structure
     */
    public File getLocalFile();
    public void setLocalFile(File inValue);

    //---------------------------------------------------------------------------
    /**
     * the size of the structure in bytes (this is calculated when the file is being read, on the uncompressed bytes)
     * @return the size of the structure in bytes
     */
    public Long getUncompressedSizeInBytes();
    public void setUncompressedSizeInBytes(Long inValue);

    //---------------------------------------------------------------------------
    /**
     * MD5 checksum of the file (this is calculated when the file is being read, on the uncompressed bytes)
     * @return the MD5 checksum for this structure's file
     */
    public byte[] getMd5Checksum();
    public void setMd5Checksum(byte[] inValue);


}
