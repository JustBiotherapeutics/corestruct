package com.just.bio.structure;

import java.util.List;

import com.hfg.chem.Charge;
import com.hfg.chem.Element;

import com.just.bio.structure.format.pdb.PDB_AtomName;

/** ----------------------------------------------------------------------------------------------------
 * Interface representing an atom present in an atomic structure.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public interface Atom
{
   PDB_AtomName name();

   int getSerialNum();

   Atom setAtomGroup(AtomGroup inValue);
   AtomGroup getAtomGroup();

   Character getInsertCode();

   float[] getCoords();

   OrbitalHybridizationGeometry getGeometry();

   Float getOccupancy();

   Float getTempFactor();

   Element getElement();

   Charge getCharge();

   List<CovalentBond> getCovalentBonds();

   boolean isBackbone();

   //--------------------------------------------------------------------------
   /**
    * The distance formula states that the distance between 2 points in 3-dimensional space
    * is the square root of the sum of the squares of the differences between the corresponding
    * coordinates. That is, given P1 = (x1,y1,z1) and P2 = (x2,y2,z2), the distance between
    * P1 and P2 is given by d(P1,P2) = sqrt( (x2 - x1)² + (y2 - y1)² + (z2 - z1)² ).
    * @param inAtom2 the specified second atom
    * @return the distance in angstroms between this atom and the specified atom
    */
   default Double distanceTo(Atom inAtom2)
   {
      return Math.sqrt(Math.pow(getCoords()[0] - inAtom2.getCoords()[0], 2)
                     + Math.pow(getCoords()[1] - inAtom2.getCoords()[1], 2)
                     + Math.pow(getCoords()[2] - inAtom2.getCoords()[2], 2));
   }
}
