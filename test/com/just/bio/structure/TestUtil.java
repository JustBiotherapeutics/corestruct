package com.just.bio.structure;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import com.hfg.datetime.DateUtil;
import com.hfg.exception.ProgrammingException;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.hfg.util.io.HTTPFileLister;
import com.hfg.util.io.HTTPFileObj;
import com.hfg.util.io.RegexpFilenameFilter;

import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.format.BufferedStructureReader;
import com.just.bio.structure.format.ReadableStructureFormat;
import com.just.bio.structure.pdb.*;
import com.just.bio.structure.pdb.pojo.*;

//------------------------------------------------------------------------------
/**
 Utility class for tests.
 <div>
 @author J. Alex Taylor
 </div>
 */
//------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------

public class TestUtil
{
   private static File sProjectRootDir;
   private static File sTestOutputDir;
   private static File sTestCacheDir;

   //---------------------------------------------------------------------------
   /**
    * Locates the project root directory by searching for the build.gradle file.
    */
   public static File getProjectRootDir()
   {
      if (null == sProjectRootDir)
      {
         // The current working directory could be anywhere. Use this class file as a reference point.
         URL selfUrl = TestUtil.class.getResource(TestUtil.class.getSimpleName() + ".class");
         File file;
         try
         {
            file = new File(URLDecoder.decode(selfUrl.getFile(), "UTF-8"));
         }
         catch (UnsupportedEncodingException e)
         {
            throw new ProgrammingException(e);
         }

         File dir = file.getParentFile();
         FilenameFilter filter = new RegexpFilenameFilter(Pattern.compile("build.gradle"));
         while (dir != null
                && (null == dir.listFiles(filter)
                    || dir.listFiles(filter).length < 1))
         {
            dir = dir.getParentFile();
         }

         if (dir.listFiles(filter).length < 1)
         {
            throw new RuntimeException("Couldn't locate the project root dir! Where is the build.gradle file?");
         }

         sProjectRootDir = dir;
      }

      return sProjectRootDir;
   }

   //---------------------------------------------------------------------------

   /**
    * Returns the directory that should be used for test output.
    */
   public static File getTestOutputDir()
   {
      if (null == sTestOutputDir)
      {
         File outputDir = new File(getProjectRootDir(), "testout");
         if (!outputDir.exists())
         {
            outputDir.mkdirs();
         }

         sTestOutputDir = outputDir;
      }

      return sTestOutputDir;
   }

   //---------------------------------------------------------------------------
   /**
    * Returns the directory that should be used for downloaded test files.
    */
   public static File getTestCacheDir()
   {
      if (null == sTestCacheDir)
      {
         File dir = new File(getProjectRootDir(), "testcache");
         if (!dir.exists())
         {
            dir.mkdirs();
         }

         sTestCacheDir = dir;
      }

      return sTestCacheDir;
   }

   //---------------------------------------------------------------------------
   public static Structure createStructure(InputStream inStream, StructureFormatType structureFormatType)
         throws Exception
   {
      Structure structure = null;

      BufferedStructureReader<Structure> reader = null;
      try
      {
         ReadableStructureFormat formatObj = ReadableStructureFormat.allocateReader(structureFormatType);
         reader = new BufferedStructureReader(new BufferedReader(new InputStreamReader(inStream)),formatObj);

         structure = reader.next();
      }
      finally
      {
         if (reader != null)
         {
            reader.close();
         }
      }

      return structure;
   }

   //---------------------------------------------------------------------------
   /**
    * get a file at a specific location
    * @param localFile
    * @param structureFormatType
    * @return
    * @throws Exception
    */
   public static Structure createStructureFromFile(File localFile, StructureFormatType structureFormatType)
         throws Exception
   {
      long startTime = System.currentTimeMillis();

      Structure structure = createStructure(new FileInputStream(localFile), structureFormatType);

      System.out.println("Time to parse " + localFile.getName() + ": " + DateUtil.generateElapsedTimeString(startTime));

      return structure;
   }




   //---------------------------------------------------------------------------

   /**
    * get the Structure object based on a PDB Identifier
    * this will attempt to retrieve a cached version first, and if not available will go out to RCSB and download it
    * @param pdbId
    * @param structureFormatType
    * @return
    * @throws Exception
    */
   public static Structure getAsStructure(String pdbId, StructureFormatType structureFormatType) throws Exception
   {
      File localFile = getCachedStructureFile(pdbId, structureFormatType);
      Structure structure = null;

      long startTime = System.currentTimeMillis();
      BufferedStructureReader<Structure> reader = null;
      try
      {
         ReadableStructureFormat formatObj = ReadableStructureFormat.allocateReader(structureFormatType);
         reader = new BufferedStructureReader(new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(localFile)))),formatObj);

         structure = reader.next();
      }
      finally
      {
         if (reader != null)
         {
            reader.close();
         }
      }

      System.out.println("Time to parse " + localFile.getName() + ": " + DateUtil.generateElapsedTimeString(startTime));
      return structure;
   }



   //---------------------------------------------------------------------------
   private static File getCachedStructureFile(String inStructureId, StructureFormatType structureFormatType) throws Exception
   {
      String structureId = inStructureId.toUpperCase();

      File cacheDir = TestUtil.getTestCacheDir();

      File localFile = new File(cacheDir, structureId + "." + structureFormatType.extension() + ".gz");

      if (! localFile.exists())
      {
         System.out.println("Downloading " + localFile.getName() + " to cache...");

         HTTPFileLister fileLister = new HTTPFileLister("https://files.rcsb.org/download/" + structureId + "." + structureFormatType.extension() + ".gz");
         List<HTTPFileObj> remoteFileList = fileLister.getFilteredRemoteFileList();
         if (! CollectionUtil.hasValues(remoteFileList))
         {
            throw new RuntimeException("No remote RCSB file found for id " + StringUtil.singleQuote(structureId) + "!");
         }

         remoteFileList.get(0).writeToLocalDir(cacheDir);
      }

      return localFile;
   }




   //---------------------------------------------------------------------------
   public static void systemOutGeneralStructureData(Structure structure)
   {
      PdbSectionMetadata sectionMetadata = structure.getMetadata();
      PdbSectionRemark sectionRemark = structure.getRemark();
      PdbSectionPrimary sectionPrimary = structure.getPrimary();
      PdbSectionConnectivity sectionConnectivity = structure.getConnectivity();

      System.out.println("-----------------------------------------------------------------------------------------");
      System.out.println("sectionMetadata: ");
      if(sectionMetadata!=null) {
         System.out.println(" -   getPdbIdentifier(): " + sectionMetadata.getPdbIdentifier());
         System.out.println(" -  getClassification(): " + sectionMetadata.getClassification());
         System.out.println(" -  getDepositionDate(): " + sectionMetadata.getDepositionDate());
         System.out.println(" -           getTitle(): " + sectionMetadata.getTitle());
         System.out.println(" -          getCaveat(): " + sectionMetadata.getCaveat());
         System.out.println(" -          getExpdta(): " + sectionMetadata.getExpdta());
         System.out.println(" -          getNummdl(): " + sectionMetadata.getNummdl());
         System.out.println(" -          getMdltyp(): " + sectionMetadata.getMdltyp());
         System.out.println(" -         getKeyWrds(): " + sectionMetadata.getKeyWrds());
         System.out.println(" -         getAuthors(): " + sectionMetadata.getAuthors());
         System.out.println(" -         getCompnds(): " + sectionMetadata.getCompnds());
         System.out.println(" -         getSources(): " + sectionMetadata.getSources());
         System.out.println(" -      getPdbRevdats(): " + sectionMetadata.getPdbRevdats());
         System.out.println(" -        getPdbJrnls(): " + sectionMetadata.getPdbJrnls());
         System.out.println(" -          getSplits(): " + sectionMetadata.getSplits());
         System.out.println(" -      getObslteDate(): " + sectionMetadata.getObslteDate());
         System.out.println(" -         getObsltes(): " + sectionMetadata.getObsltes());
         System.out.println(" -      getSprsdeDate(): " + sectionMetadata.getSprsdeDate());
         System.out.println(" -         getSprsdes(): " + sectionMetadata.getSprsdes());
      } else {
         System.out.println(" -         sectionMetadata == null");
      }


      System.out.println("sectionRemark: ");
      if(sectionRemark != null) {
         System.out.println(" -  getResolutionUnit(): " + sectionRemark.getResolutionUnit());
         System.out.println(" -      getResolution(): " + sectionRemark.getResolution());
         if (CollectionUtil.hasValues(sectionRemark.getRemarks())) {
            for (PdbRemark remark : sectionRemark.getRemarks()) {
               System.out.println(" --        getRemarks(): " + remark);
            }
         } else {
            System.out.println(" -  no Remarks found ");
         }
      } else {
         System.out.println(" -         sectionRemark == null");
      }


      System.out.println("sectionPrimary: ");
      if(sectionPrimary != null) {
         if(CollectionUtil.hasValues(sectionPrimary.getPdbSeqress())){
            for(PdbSeqres seqres : sectionPrimary.getPdbSeqress()){
               System.out.println(" --     getPdbSeqress(): " + seqres);
            }
         } else {
            System.out.println(" -  no PdbSeqres found ");
         }
      } else {
         System.out.println(" -         sectionPrimary == null");
      }


      System.out.println("sectionConnectivity: ");
      if(sectionConnectivity != null) {
         if(CollectionUtil.hasValues(sectionConnectivity.getPdbSsbonds())){
            for(PdbSsbond ssbond : sectionConnectivity.getPdbSsbonds()){
               System.out.println(" --     getPdbSsbonds(): " + ssbond);
            }
         } else {
            System.out.println(" -  no PdbSsbond found ");
         }
      } else {
         System.out.println(" -         sectionConnectivity == null");
      }


      System.out.println("sectionCoordinate: ");
      if(structure.getAtomicCoordinates() != null)
      {
         System.out.println(" --  Num models: " + structure.getAtomicCoordinates().getModels().size());
         for (int modelIdx = 0; modelIdx < structure.getAtomicCoordinates().getModels().size(); modelIdx++)
         {
            Model model = structure.getAtomicCoordinates().getModels().get(modelIdx);
            System.out.println(" --  Chains in model " + (modelIdx + 1) + " : " + model.getChains().size());
            for (Chain chain : model.getChains())
            {
               System.out.println("   --  Chain " + chain.name() + " : " +  (chain.getAtomGroups()!=null ? chain.getAtomGroups().size() : "[null]") + " atom groups");
               System.out.println("                  " + chain.getTheoreticalSeq());
            }
         }
      }
   }


   //---------------------------------------------------------------------------
   public static void systemOutChainInfo(Structure structure, boolean verboseResidues)
   {
/*

        System.out.println("------------------------------------------------------------------------\nchain information: ");


        if (CollectionUtil.hasValues(structure.getChains())) {
            for (Chain chain : structure.getChains()) {

                System.out.println("   -       id.name.type: " + chain.getChainId() + "." + chain.getChainName() + "." + chain.getChainType());
                System.out.println("   -           sequence: " + chain.getTheoreticalSeq());


                if (CollectionUtil.hasValues(chain.getSeqResidues())) {
                    System.out.print("   -        SeqResidues: [ordnal.name.residueType] ");
                    for (SeqResidue seqResidue : chain.getSeqResidues()) {
                        System.out.print("[" + seqResidue.getOrdinal() + "." + seqResidue.getName() + "." + seqResidue.getResidueType() + "] ");
                    }
                    System.out.println("");
                }
                else
                {
                    System.out.println(" -  no seqResidues found ");
                }


                if (CollectionUtil.hasValues(chain.getAtomGroupings())) {
                    System.out.println("   -      AtomGroupings: ");
                    for (AtomGrouping atomGrouping : chain.getAtomGroupings()) {
                        System.out.println("   -                     [" + atomGrouping.getChainId() + "." + atomGrouping.getChainName() + "." + atomGrouping.getChainType() + ": " + atomGrouping.getSequenceWithAnyCoordinates() + "] ");
                        if(CollectionUtil.hasValues(atomGrouping.getAllResidues())) {
                            System.out.print("   -               residues: ");
                            for(Residue residue : atomGrouping.getAllResidues()){
                                System.out.print("[" + residue.getResidueType() + "." + residue.getName() + "." +residue.getAtomNames() + "] ");
                            }
                            System.out.println("");
                        } else {
                            System.out.println("   -               no residues");
                        }
                    }
                    System.out.println("");
                }
                else
                {
                    System.out.println(" -  no seqResidues found ");
                }


                System.out.println("");
            }
        }
        else
        {
            System.out.println(" -  no Chains found ");
        }


*/
   }



}
