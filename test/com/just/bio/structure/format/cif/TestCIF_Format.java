package com.just.bio.structure.format.cif;

import com.just.bio.structure.AtomicCoordinates;
import com.just.bio.structure.Model;
import com.just.bio.structure.Structure;
import com.just.bio.structure.TestUtil;
import com.just.bio.structure.enums.StructureFormatType;

import org.junit.Assert;
import org.junit.Test;


//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class TestCIF_Format
{


    //---------------------------------------------------------------------------
    @Test
    public void testStandardCifFile() throws Exception
    {
        //String pdbId = "4V4A";
        String pdbId = "6Q95";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.CIF);

        //assert..
        Assert.assertTrue(pdbId.equals(structure.getMetadata().getPdbIdentifier()));

        TestUtil.systemOutGeneralStructureData(structure);
    }


    //---------------------------------------------------------------------------
    @Test
    public void testExtraLargeCifFile() throws Exception
    {
        //3J3Q is around 250 MB uncompressed, and the largest RCSB file we are aware of
        String pdbId = "3J3Q";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.CIF);

        //assert..
        Assert.assertTrue(pdbId.equals(structure.getMetadata().getPdbIdentifier()));

        TestUtil.systemOutGeneralStructureData(structure);
    }


    //---------------------------------------------------------------------------
    @Test
    public void testParseError1() throws Exception
    {
        String pdbId = "3JCD";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.CIF);

        AtomicCoordinates coordinates = structure.getAtomicCoordinates();

        Model model = coordinates.getModels().get(0);

        Assert.assertEquals("BMT", model.getChain("B").getSeqResidues().get(4).name());
        Assert.assertEquals('X', model.getChain("B").getSequenceWithAnyCoordinates().charAt(4)); // full sequence: ALLVXXXLVLA

        TestUtil.systemOutGeneralStructureData(structure);
        TestUtil.systemOutChainInfo(structure, true);
    }



    //---------------------------------------------------------------------------
    @Test
    public void testParseError2() throws Exception
    {
        String pdbId = "2B62";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.CIF);

        //assert..
        Assert.assertTrue(pdbId.equals(structure.getMetadata().getPdbIdentifier()));

    }







}
