package com.just.bio.structure.format.pdb;

import java.io.*;
import java.util.zip.GZIPInputStream;

import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;

import com.just.bio.structure.AtomicCoordinates;
import com.just.bio.structure.Model;
import com.just.bio.structure.Structure;
import com.just.bio.structure.TestUtil;
import com.just.bio.structure.enums.ChainType;
import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.format.BufferedStructureReader;
import com.just.bio.structure.format.ReadableStructureFormat;
import com.just.bio.structure.format.StructureFactoryImpl;
import com.just.bio.structure.pdb.PdbFileParse;

import org.junit.Assert;
import org.junit.Test;


//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class TestPDB_Format
{
    //---------------------------------------------------------------------------
    @Test
    public void testParse1()
            throws Exception
    {
        String pdbId = "2DZ3";
        //String pdbId = "2B62";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.PDB);

        //assert pdb id matches
        Assert.assertEquals(pdbId, structure.getMetadata().getPdbIdentifier());

        //TestUtil.systemOutGeneralStructureData(structure);
    }



    //---------------------------------------------------------------------------
    @Test
    public void testStandardPdbFile()
          throws Exception
    {
        String pdbId = "4HHB";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.PDB);

        //assert pdb id matches
        Assert.assertEquals(pdbId, structure.getMetadata().getPdbIdentifier());

        AtomicCoordinates coordinates = structure.getAtomicCoordinates();

        Assert.assertEquals(1, coordinates.getModels().size());
        Assert.assertEquals(4, coordinates.getModels().get(0).getChains().size());
        Assert.assertEquals(ChainType.PROTEIN, coordinates.getModels().get(0).getChain("A").getType());
        Assert.assertEquals(ChainType.PROTEIN, coordinates.getModels().get(0).getChain("B").getType());
        Assert.assertEquals(ChainType.PROTEIN, coordinates.getModels().get(0).getChain("C").getType());
        Assert.assertEquals(ChainType.PROTEIN, coordinates.getModels().get(0).getChain("D").getType());
        Assert.assertEquals(136, coordinates.getModels().get(0).getChain("A").getAtomGroups().size());
        Assert.assertEquals(137, coordinates.getModels().get(0).getChain("B").getAtomGroups().size());
        Assert.assertEquals(134, coordinates.getModels().get(0).getChain("C").getAtomGroups().size());
        Assert.assertEquals(137, coordinates.getModels().get(0).getChain("D").getAtomGroups().size());

        Assert.assertEquals("VHLTPEEKSAVTALWGKVNVDEVGGEALGRLLVVYPWTQRFFESFGDLSTPDAVMGNPKVKAHGKKVLGAFSDGLAHLDNLKGTFATLSELHCDKLHVDPENFRLLGNVLVCVLAHHFGKEFTPPVQAAYQKVVAGVANALAHKYH",
              coordinates.getModels().get(0).getChain("D").getTheoreticalSeq());

        Assert.assertEquals("VHLTP-EKSAVTALWGKVNVDEV-GEALGR-L-VYPWTQR-FESFGDLSTPDAVMGNPKVKAHG-KVLGAFSDGLAHLDNLKGTFATLSELHCDKLHVDPENFR-LGNVLVCVLA-HFGKEFT-PVQ-AYQK-VAGVANALAHKYH",
              coordinates.getModels().get(0).getChain("D").getSequenceWithAnyCoordinates());

        Assert.assertEquals("VHLTP-EKSAVTALWGKVNVDEV-GEALGR-L-VYPWTQR-FESFGDLSTPDAVMGNPKVKAHG-KVLGAFSDGLAHLDNLKGTFATLSELHCDKLHVDPENFR-LGNVLVCVLA-HFGKEFT-PVQ-AYQK-VAGVANALAHKYH",
              coordinates.getModels().get(0).getChain("D").getSequenceWithCaCoordinates());

        Assert.assertEquals("VHLTP-EKSAVTALWGKVNVDEV-GEALGR-L-VYPWTQR-FESFGDLSTPDAVMGNPKVKAHG-KVLGAFSDGLAHLDNLKGTFATLSELHCDKLHVDPENFR-LGNVLVCVLA-HFGKEFT-PVQ-AYQK-VAGVANALAHKYH",
              coordinates.getModels().get(0).getChain("D").getSequenceWithBackboneCoordinates());

        Assert.assertEquals("VHLTP-EKSAVTALW-KVNVDEV--EAL-R-L-VYPWTQR-FESF-DLSTPDAVM-NPKVKAH--KVL-AFSD-LAHLDNLK-TFATLSELHCDKLHVDPENFR-L-NVLVCVLA-HF-KEFT-PVQ-AYQK-VA-VANALAHKYH",
              coordinates.getModels().get(0).getChain("D").getSequenceWithBackboneCbCoordinates());

        Assert.assertEquals("VHLTP-EKSAVTALWGKVNVDEV-GEALGR-L-VYPWTQR-FESFGDLSTPDAVMGNPKVKAHG-KVLGAFSDGLAHLDNLKGTFATLSELHCDKLHVDPENFR-LGNVLVCVLA-HFGKEFT-PVQ-AYQK-VAGVANALAHKYH",
              coordinates.getModels().get(0).getChain("D").getSequenceWithAllCoordinates());

        TestUtil.systemOutGeneralStructureData(structure);
    }


    //---------------------------------------------------------------------------
    // SSBONDS section does not have some values in it's columns. This tests that missing data in these columns
    // will be converted to a null Double, instead of null pointer ex
    // TODO
    @Test
    public void testSsbondsIncomplete()
          throws Exception
    {
        String pdbId = "2MTH";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.PDB);

        // assert pdb id matches
        Assert.assertEquals(pdbId, structure.getMetadata().getPdbIdentifier());

        TestUtil.systemOutGeneralStructureData(structure);
    }


    //---------------------------------------------------------------------------
    // The first residue in 2WPY's only chain is 'ACE' which is an unknown residue type and should be represented as such
    @Test
    public void testUnknownResidueType()
          throws Exception
    {
        String pdbId = "2WPY";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.PDB);
        AtomicCoordinates coordinates = structure.getAtomicCoordinates();
        Model model = coordinates.getModels().get(0);

        Assert.assertEquals("XRMKQLEDKVEELLSKVYHNENEVARLKKLVGER", model.getChain("A").getTheoreticalSeq());

        TestUtil.systemOutGeneralStructureData(structure);
    }


    //---------------------------------------------------------------------------
    // Chain B of ICXT is of type NUCLEIC_ACID - it has 2 unknowns and 2 nucleic acid residues
    @Test
    public void testNucleicAcidsInChain()
          throws Exception
    {
        String pdbId = "1CXT";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.PDB);
        AtomicCoordinates coordinates = structure.getAtomicCoordinates();
        Model model = coordinates.getModels().get(0);

        Assert.assertEquals(ChainType.NUCLEIC_ACID, model.getChain("B").getType());
        Assert.assertEquals("ngng", model.getChain("B").getTheoreticalSeq());

        TestUtil.systemOutGeneralStructureData(structure);
    }


    //---------------------------------------------------------------------------
    // 1MAG has STANDARD_AMINO_ACID, NON_STANDARD_AMINO_ACID, UNKNOWN residue types
    @Test
    public void testChainWithMultipleResidueTypes()
          throws Exception
    {
        String pdbId = "1MAG";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.PDB);
        AtomicCoordinates coordinates = structure.getAtomicCoordinates();
        Model model = coordinates.getModels().get(0);

        Assert.assertEquals("XGALAVVVWLWLWLWX", model.getChain("A").getSequenceWithAnyCoordinates());

        TestUtil.systemOutGeneralStructureData(structure);
        TestUtil.systemOutChainInfo(structure, false);
    }


    //---------------------------------------------------------------------------
    // 1DSI has 20 models (aka atomic coordinate sections).
    // 1DSI also has a ton of atom names that are not in our list. this will test they can all be dynamically created
    @Test
    public void testPdbWithMultipleModels()
          throws Exception
    {
         String pdbId = "1DSI";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.PDB);
        AtomicCoordinates coordinates = structure.getAtomicCoordinates();

        Assert.assertEquals(20, coordinates.getModels().size());

        Model firstModel = coordinates.getModels().get(0);
        Model lastModel = coordinates.getModels().get(coordinates.getModels().size() -1);

        // assert something so deep in our chain that only a correctly parsed structure could be performed.
        Assert.assertEquals(PDB_AtomName.N1, firstModel.getChain("B").getAtomGroups().get(1).getAtoms().get(11).name());
        Assert.assertEquals(PDB_AtomName.H6, lastModel.getChain("A").getAtomGroups().get(8).getAtoms().get(30).name());

        TestUtil.systemOutGeneralStructureData(structure);
        TestUtil.systemOutChainInfo(structure, true);
    }


    //---------------------------------------------------------------------------
    // 2RMA has BMT residues, which are in the amino acid map, but have NO single letter mapping. these should be translated to X
    @Test
    public void testPdbWithResiduesInMapButNoSingleLetter()
          throws Exception
    {
        String pdbId = "2RMA";
        Structure structure = TestUtil.getAsStructure(pdbId, StructureFormatType.PDB);

        AtomicCoordinates coordinates = structure.getAtomicCoordinates();

        Model model = coordinates.getModels().get(0);

        Assert.assertEquals("BMT", model.getChain("B").getSeqResidues().get(4).name());
        Assert.assertEquals('X', model.getChain("B").getSequenceWithAnyCoordinates().charAt(4)); // full sequence: ALLVXXXLVLA

        TestUtil.systemOutGeneralStructureData(structure);
        TestUtil.systemOutChainInfo(structure, true);
    }


    //---------------------------------------------------------------------------
    // test the PdbFileParse executeParseByRestServices(...) method
    @Test
    public void testExecuteParseByRestServices()
          throws Exception
    {
        String pdbIdentifier = "6YJR";
        String rcsbRestFileUrl = "http://cftp.rcsb.org/view/";
        String restUrlDownload = "http://ftp.rcsb.org/download/";

        PdbFileParse pdbFileParse = new PdbFileParse();
        Structure structure = pdbFileParse.executeParseByRestServices(pdbIdentifier, rcsbRestFileUrl, restUrlDownload);

        TestUtil.systemOutGeneralStructureData(structure);
        TestUtil.systemOutChainInfo(structure, true);
    }


    //---------------------------------------------------------------------------
    @Test
    public void testWriteSeqResSection1() throws Exception
    {
        String fileName = "4HHB.pdb.gz";

        Structure structure = getStructureFromRsrc(fileName, StructureFormatType.PDB);

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(byteStream);

        PDB_Format pdbFormatObj = new PDB_Format(new StructureFactoryImpl());

        pdbFormatObj.writeSeqResSection(structure, writer);

        writer.close();

        // Retrieve the original lines from the file
        String lines = getPDB_ResourceLines(fileName, 820, 865);

        // Compare the output to the original lines
        Assert.assertEquals(lines, new String(byteStream.toByteArray()));
    }


    //---------------------------------------------------------------------------
    @Test
    public void testWriteCoordinateSection1() throws Exception
    {
        String fileName = "4HHB.pdb.gz";

        Structure structure = getStructureFromRsrc(fileName, StructureFormatType.PDB);

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(byteStream);

        PDB_Format pdbFormatObj = new PDB_Format(new StructureFactoryImpl());

        pdbFormatObj.writeCoordinateSection(structure, writer);

        writer.close();

        // Retrieve the original lines from the file
        String lines = getPDB_ResourceLines(fileName, 940, 5722);

        // Compare the output to the original lines
        Assert.assertEquals(lines, new String(byteStream.toByteArray()));
    }





    //---------------------------------------------------------------------------
    private static String getPDB_ResourceLines(String inRsrcName, int inStartLine, int inEndLine)
          throws IOException
    {
        InputStream rsrcStream = getRsrcStream(inRsrcName);

        BufferedReader reader = new BufferedReader(new InputStreamReader(rsrcStream));

        StringBuilderPlus lines = new StringBuilderPlus();
        String line;

        int lineNumber = 1;

        while ((line = reader.readLine()) != null)
        {
            if(lineNumber >= inStartLine)
            {
                lines.appendln(line);
                if(lineNumber >= inEndLine)
                {
                    break;
                }
            }
            lineNumber++;

        }

        return lines.toString();
    }

    //---------------------------------------------------------------------------
    private static InputStream getRsrcStream(String inRsrcName) throws IOException
    {
        String rsrcName = "rsrc/" + inRsrcName;
        InputStream rsrcStream = TestPDB_Format.class.getResourceAsStream(rsrcName);

        if (null == rsrcStream)
        {
            throw new RuntimeException("The resource " + StringUtil.singleQuote(rsrcName) + " couldn't be found!");
        }

        if (rsrcName.endsWith(".gz"))
        {
            rsrcStream = new GZIPInputStream(rsrcStream);
        }

        return rsrcStream;
    }

    //---------------------------------------------------------------------------
    private static Structure getStructureFromRsrc(String inRsrcName, StructureFormatType inType) throws IOException
    {
        InputStream rsrcStream = getRsrcStream(inRsrcName);

        ReadableStructureFormat formatObj = ReadableStructureFormat.allocateReader(inType);
        BufferedStructureReader reader = new BufferedStructureReader(new BufferedReader(new InputStreamReader(rsrcStream)),formatObj);

        Structure structure = reader.next();

        reader.close();

        return structure;
    }

}
