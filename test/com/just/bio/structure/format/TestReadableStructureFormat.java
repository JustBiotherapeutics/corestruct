package com.just.bio.structure.format;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

import com.just.bio.structure.Structure;
import com.just.bio.structure.TestUtil;
import com.just.bio.structure.enums.StructureFormatType;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class TestReadableStructureFormat
{
   //---------------------------------------------------------------------------
   @Test
   public void testAllocateReader()
         throws Exception
   {
      File file = new File ("test.pdb");
      Assert.assertEquals(StructureFormatType.PDB, ReadableStructureFormat.allocateReader(file).getStructureFormatType());

      file = new File ("test.PDB");
      Assert.assertEquals(StructureFormatType.PDB, ReadableStructureFormat.allocateReader(file).getStructureFormatType());

      file = new File ("test.pdb.gz");
      Assert.assertEquals(StructureFormatType.PDB, ReadableStructureFormat.allocateReader(file).getStructureFormatType());

      // ENT should return PDB
      file = new File ("test.ent.gz");
      Assert.assertEquals(StructureFormatType.PDB, ReadableStructureFormat.allocateReader(file).getStructureFormatType());

      file = new File ("test.cif.gz");
      Assert.assertEquals(StructureFormatType.CIF, ReadableStructureFormat.allocateReader(file).getStructureFormatType());

      file = new File ("test.moe");
      Assert.assertEquals(StructureFormatType.MOE, ReadableStructureFormat.allocateReader(file).getStructureFormatType());


   }
}
