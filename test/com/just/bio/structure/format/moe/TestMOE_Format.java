package com.just.bio.structure.format.moe;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.zip.GZIPInputStream;

import com.hfg.util.collection.CollectionUtil;
import org.junit.Assert;
import org.junit.Test;

import com.hfg.util.StringUtil;
import com.hfg.util.io.StreamUtil;

import com.just.bio.structure.AtomGroup;
import com.just.bio.structure.AtomGroupType;
import com.just.bio.structure.AtomicCoordinates;
import com.just.bio.structure.Chain;
import com.just.bio.structure.Model;
import com.just.bio.structure.OrbitalHybridizationGeometry;
import com.just.bio.structure.Structure;
import com.just.bio.structure.TestUtil;
import com.just.bio.structure.enums.ChainType;
import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.format.StructureFactoryImpl;
import com.just.bio.structure.format.pdb.PDB_Format;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class TestMOE_Format
{

   //---------------------------------------------------------------------------
   @Test
   public void testMoeFile_1HZH()
         throws Exception
   {
      String fileName = "1HZH.moe.gz";
      InputStream rsrcStream = getMOE_Resource(fileName);

      Structure structure = TestUtil.createStructure(rsrcStream, StructureFormatType.MOE);

      AtomicCoordinates coordinates = structure.getAtomicCoordinates();

      Assert.assertEquals(1, coordinates.getModels().size());

      Model model = coordinates.getModels().get(0);

      Assert.assertEquals(10, model.getChains().size());

      Assert.assertEquals(ChainType.PROTEIN, model.getChain("12081").getType());
      Assert.assertEquals(ChainType.PROTEIN, model.getChain("12082").getType());
      Assert.assertEquals(ChainType.PROTEIN, model.getChain("12083").getType());
      Assert.assertEquals(ChainType.PROTEIN, model.getChain("12084").getType());
      Assert.assertEquals("1HZH", model.getChain("12084").getAdditionalFormatAttribute("cTag"));
      Assert.assertEquals(457, model.getChain("12081").getAtomGroups().size());
      Assert.assertEquals(457, model.getChain("12082").getAtomGroups().size());
      Assert.assertEquals(215, model.getChain("12083").getAtomGroups().size());
      Assert.assertEquals(215, model.getChain("12084").getAtomGroups().size());

      Assert.assertEquals("QVQLVQSGAEVKKPGASVKVSCQASGYRFSNFVIHWVRQAPGQRFEWMGWINPYNGNKEFSAKFQDRVTFTADTSANTAYMELRSLRSADTAVYYCARVGPYSWDDSPQDNYYMDVWGKGTTVIVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVLQSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKAEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK",
                          model.getChain("12081").getTheoreticalSeq());

      Assert.assertEquals("QVQLVQSGAEVKKPGASVKVSCQASGYRFSNFVIHWVRQAPGQRFEWMGWINPYNGNKEFSAKFQDRVTFTADTSANTAYMELRSLRSADTAVYYCARVGPYSWDDSPQDNYYMDVWGKGTTVIVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVLQSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKAEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK",
                          model.getChain("12081").getSequenceWithAnyCoordinates());

      Assert.assertEquals("QVQLVQSGAEVKKPGASVKVSCQASGYRFSNFVIHWVRQAPGQRFEWMGWINPYNGNKEFSAKFQDRVTFTADTSANTAYMELRSLRSADTAVYYCARVGPYSWDDSPQDNYYMDVWGKGTTVIVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVLQSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKAEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK",
                          model.getChain("12081").getSequenceWithCaCoordinates());

      Assert.assertEquals("QVQLVQSGAEVKKPGASVKVSCQASGYRFSNFVIHWVRQAPGQRFEWMGWINPYNGNKEFSAKFQDRVTFTADTSANTAYMELRSLRSADTAVYYCARVGPYSWDDSPQDNYYMDVWGKGTTVIVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVLQSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKAEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK",
                          model.getChain("12081").getSequenceWithBackboneCoordinates());

      Assert.assertEquals("QVQLVQS-AEVKKP-ASVKVSCQAS-YRFSNFVIHWVRQAP-QRFEWM-WINPYN-NKEFSAKFQDRVTFTADTSANTAYMELRSLRSADTAVYYCARV-PYSWDDSPQDNYYMDVW-K-TTVIVSSASTK-PSVFPLAPSSKSTS--TAAL-CLVKDYFPEPVTVSWNS-ALTS-VHTFPAVLQSS-LYSLSSVVTVPSSSL-TQTYICNVNHKPSNTKVDKKAEPKSCDKTHTCPPCPAPELL--PSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVD-VEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLN-KEYKCKVSNKALPAPIEKTISKAK-QPREPQVYTLPPSRDELTKNQVSLTCLVK-FYPSDIAVEWESN-QPENNYKTTPPVLDSD-SFFLYSKLTVDKSRWQQ-NVFSCSVMHEALHNHYTQKSLSLSP-K",
                          model.getChain("12081").getSequenceWithBackboneCbCoordinates());

      Assert.assertEquals("QVQLVQSGAEVKKPGASVKVSCQASGYRFSNFVIHWVRQAPGQRFEWMGWINPYNGNKEFSAKFQDRVTFTADTSANTAYMELRSLRSADTAVYYCARVGPYSWDDSPQDNYYMDVWGKGTTVIVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVLQSSGLYSLSSVVTVPSSSLGTQTYICNVNHKPSNTKVDKKAEPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK",
                          model.getChain("12081").getSequenceWithAllCoordinates());

      TestUtil.systemOutGeneralStructureData(structure);
      TestUtil.systemOutChainInfo(structure, true);
   }


   //---------------------------------------------------------------------------
   @Test
   public void testMoeFile_5EDN()
         throws Exception
   {
      String fileName = "5EDN.moe.gz";
      InputStream rsrcStream = getMOE_Resource(fileName);

      Structure structure = TestUtil.createStructure(rsrcStream, StructureFormatType.MOE);

      //assert..

      TestUtil.systemOutGeneralStructureData(structure);
      TestUtil.systemOutChainInfo(structure, true);
   }


   //---------------------------------------------------------------------------
   // Ribosome complex
   @Test
   public void testMoeFile_5JVG()
         throws Exception
   {
      String fileName = "5JVG.moe.gz";
      InputStream rsrcStream = getMOE_Resource(fileName);

      Structure structure = TestUtil.createStructure(rsrcStream, StructureFormatType.MOE);

      AtomicCoordinates coordinates = structure.getAtomicCoordinates();

      Assert.assertEquals(1, coordinates.getModels().size());

      Model model = coordinates.getModels().get(0);

      Assert.assertEquals(38, model.getChains().size());

      Chain chain = model.getChain("5JVG.X");
      Assert.assertEquals(ChainType.NUCLEIC_ACID, chain.getType());
      Assert.assertEquals("5JVG.X", chain.name());
      Assert.assertEquals(2877, chain.getAtomGroups().size());

      Assert.assertEquals("ggucaagauaguaaggguccacgguggaugcccuggcgcuggagccgaugaaggacgcgauuaccugcgaaa"
                        + "agccccgacgagcuggagauacgcuuugacucggggauguccgaauggggaaacccaccucguaagagguau"
                        + "ccgcaaggaugggaacucagggaacugaaacaucucaguaccugaaggagaagaaagagaauucgauuccgu"
                        + "uaguagcggcgagcgaacccggaucagcccaaaccgaaacgcuugcguuucgggguuguaggaccaguuuuu"
                        + "aagauucaaccccucaagccgaaguggcuggaaagcuacaccucagaaggugagaguccuguaggcgaacga"
                        + "gcgguugacuguacuggcaccugaguaggucguuguucgugaaacgaugacugaauccgcgcggaccaccgc"
                        + "gcaaggcuaaauacucccagugaccgauagcgcauaguaccgugagggaaaggugaaaagaaccccgggagg"
                        + "ggagugaaagagaaccugaaaccguggacuuacaagcagucauggcaccuuaugcguguuauggcgugccua"
                        + "uugaagcaugagccggcgacuuagaccugacgugcgagcuuaaguugaaaaacggaggcggagcgaaagcga"
                        + "guccgaauagggcggcauuaguacgucgggcuagacucgaaaccaggugagcuaagcaugaccagguugaaa"
                        + "cccccgugacagggggcggaggaccgaaccggugccugcugaaacagucucggaugaguuguguuuaggagu"
                        + "gaaaagcuaaccgaaccuggagauagcuaguucuccccgaaauguauugagguacagccucggauguugacc"
                        + "auguccuguagagcacucacaaggcuagggggccuaccagcuuaccaaaccuuaugaaacuccgaaggggca"
                        + "cgcguuuaguccgggagugaggcugcgagagcuaacuuccguagccgagagggaaacaacccagaccaucag"
                        + "cuaaggucccuaaaugaucgcucagugguuaaggaugugucgucgcauagacagccaggagguuggcuuaga"
                        + "agcagccacccuucaaagagugcguaauagcucacuggucgagugacgaugcgccgaaaaugaucggggcuc"
                        + "aagugaucuaccgaagcuauggauucaacucgcgaagcgaguugucugguaggggagcguucaguccgcgga"
                        + "gaagccauaccggaaggagugguggagccgacugaagugcggaugccggcaugaguaacgauaaaagaagug"
                        + "agaaucuucuucgccguaaggacaaggguuccuggggaagggucguccgcccagggaaagucgggaccuaag"
                        + "gugaggccgaacggcgcagccgauggacagcaggucaagauuccugcaccgaucauguggagugauggaggg"
                        + "acgcauuacgcuauccaaugccaagcuauggcuaugcugguugguacgcucaagggcgaucgggucagaaaa"
                        + "ucuaccggucacaugccucagacguaucgggagcuuccucggaagcgaaguuggaaacgcgacggugccaag"
                        + "aaaagcuucuaaacguugaaacaugauugcccguaccgcaaaccgacacagguguccgagugucaaugcacu"
                        + "aaggcgcgcgagagaacccucguuaaggaacuuugcaaucucaccccguaacuucggaagaaggggucccca"
                        + "cgcuucgcguggggcgcagugaauaggcccaggcgacuguuuaccaaaaucacagcacucugccaacacgaa"
                        + "caguggacguauagggugugacgccugcccggugccggaaggucaaguggagcggugcaagcugcgaaauga"
                        + "agccccggugaacggcgaccguaacuauaacgguccuaagguagcgaaauuccuugucggguaaguuccgac"
                        + "cugcacgaaaggcguaacgaucugggcgcugucucaacgagggacucggugaaauugaauuggcuguaaaga"
                        + "ugcggccuacccguagcaggacgaaaagaccccguggagcuuuacuauagucuggcauugggauucggguuu"
                        + "cucugcguaggauaggugggagccugcgaaacuggccuuuuggggucgguggaggcaacggugaaauaccac"
                        + "ccugagaaacuuggauuucuaaccugaaaaaucacuuucggggaccgugcuuggcggguaguuugacugggg"
                        + "cggucgccucccaaaauguaacggaggcgcccaaaggucaccucaagacgguuggaaaucgucuguagagcg"
                        + "caaagguagaagguggcuugacugcgagacugacacgucgagcagggaggaaacucgggcuuagugaaccgg"
                        + "ugguaccguguggaagggccaucgaucaacggauaaaaguuaccccggggauaacaggcugaucucccccga"
                        + "gaguccauaucggcggggagguuuggcaccucgaugucggcucgucgcauccuggggcugaagaagguccca"
                        + "aggguugggcuguucgcccauuaaagcggcacgcgagcuggguucagaacgucgugagacaguucggucucu"
                        + "auccgcuacgggcgcaggagaauugaggggaguugcuccuaguacgagaggaccggagugaacggaccgcug"
                        + "gucucccugcugucguaccaacggcacaugcaggguagcuauguccggaacggauaaccgcugaaagcaucu"
                        + "aagcgggaagccagccccaagaugaguucucccacuguuuaucagguaagacucccggaagaccaccggguu"
                        + "aagaggccaggcgugcacgcauagcaauguguucagcggacuggugcucaucagucgaggucuugacca",
                          chain.getTheoreticalSeq());


      chain = model.getChain("5JVG.A");
      Assert.assertEquals(ChainType.PROTEIN, chain.getType());
      Assert.assertEquals("5JVG.A", chain.name());
      Assert.assertEquals(275, chain.getAtomGroups().size());

      Assert.assertEquals("MAVKKYRPYTPSRRQMTTADFSGLTKKRPEKALTEALPKTGGRNNRGRITSRFIGGGHKRLYRIIDFKRRDKSGVNAKVAAIEYDPNRSARIALLHYADG"
                        + "EKRYILAPEGLTVGATVNAGPEAEPKLGNALPLRFVPVGAVVHALELVPGKGAQLARSAGTSVQVQGKESDYVIVRLPSGELRRVHSECYATIGAVGNAE"
                        + "HKNIVLGKAGRSRWLGRKPHQRGSAMNPVDHPHGGGEGRTGAGRVPVTPWGKPTKGLKTRRKRKTSDRFIVTRRK",
                          chain.getTheoreticalSeq());

      AtomGroup residue = chain.getAtomGroups().get(0);
      Assert.assertEquals("MET", residue.name());
      Assert.assertEquals(AtomGroupType.AMINO_ACID, residue.getType());
      Assert.assertEquals(3002, residue.getResidueNum().intValue());
      Assert.assertEquals(8, residue.getAtoms().size());
      Assert.assertEquals("N", residue.getAtoms().get(0).name().getName());
      Assert.assertEquals(75.781, residue.getAtoms().get(0).getCoords()[0], 0.001);
      Assert.assertEquals(183.964, residue.getAtoms().get(0).getCoords()[1], 0.001);
      Assert.assertEquals(152.957, residue.getAtoms().get(0).getCoords()[2], 0.001);
      Assert.assertEquals(OrbitalHybridizationGeometry.sp3, residue.getAtoms().get(0).getGeometry());
      Assert.assertTrue(residue.getAtoms().get(0).isBackbone());

      residue = chain.getAtomGroups().get(chain.getAtomGroups().size() - 1);
      Assert.assertEquals("LYS", residue.name());
      Assert.assertEquals(AtomGroupType.AMINO_ACID, residue.getType());
      Assert.assertEquals(3276, residue.getResidueNum().intValue());
      Assert.assertNull(residue.getAtoms());

      TestUtil.systemOutGeneralStructureData(structure);
      TestUtil.systemOutChainInfo(structure, true);
   }


   //---------------------------------------------------------------------------
   @Test
   public void testMoeFile_5TMI()
         throws Exception
   {
      String fileName = "5TMI.moe.gz";
      InputStream rsrcStream = getMOE_Resource(fileName);

      Structure structure = TestUtil.createStructure(rsrcStream, StructureFormatType.MOE);

      //assert..

      TestUtil.systemOutGeneralStructureData(structure);
      TestUtil.systemOutChainInfo(structure, true);

      ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
      Writer writer = new OutputStreamWriter(byteStream);

      PDB_Format pdbFormatObj = new PDB_Format(new StructureFactoryImpl());

      pdbFormatObj.writeSeqResSection(structure, writer);
      pdbFormatObj.writeCoordinateSection(structure, writer);

      writer.close();

      // Compare the output to the expected results
      String expectedResultFileName = "5TMI.pdb";
      rsrcStream = getMOE_Resource(expectedResultFileName);

      Assert.assertEquals(StreamUtil.inputStreamToString(rsrcStream), new String(byteStream.toByteArray()));
   }


   //---------------------------------------------------------------------------
   // Tests extraction of cGroup values. These values wer being places in a
   // separate attr block than the main chain attributes.
   @Test
   public void testExtractionOf_cGroupValues()
         throws Exception
   {
      String fileName = "belimumab_MBN.moe";
      InputStream rsrcStream = getMOE_Resource(fileName);

      Structure structure = TestUtil.createStructure(rsrcStream, StructureFormatType.MOE);

      Collection<Chain> chains = structure.getAtomicCoordinates().getModels().get(0).getChains();
      if (CollectionUtil.hasValues(chains))
      {
         for (Chain chain : chains)
         {
            Assert.assertEquals("MBN", chain.getAdditionalFormatAttribute("cGroup"));
         }
      }
   }

   //###########################################################################
   // STATIC PRIVATE METHODS
   //###########################################################################

   //---------------------------------------------------------------------------
   private static InputStream getMOE_Resource(String inRsrcName)
         throws IOException
   {
      String rsrcName = "rsrc/" + inRsrcName;
      InputStream rsrcStream = TestMOE_Format.class.getResourceAsStream(rsrcName);

      if (null == rsrcStream)
      {
         throw new RuntimeException("The resource " + StringUtil.singleQuote(rsrcName) + " couldn't be found!");
      }

      if (rsrcName.endsWith(".gz"))
      {
         rsrcStream = new GZIPInputStream(rsrcStream);
      }

      return rsrcStream;
   }








}
