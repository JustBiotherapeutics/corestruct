package com.just.bio.structure;

import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.pdb.StructureUtils;
import org.junit.Assert;
import org.junit.Test;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class TestStructureUtils
{

    //---------------------------------------------------------------------------
    @Test
    public void determineFileFormat()
    {
        String s1 = "pdbXXXX.ent.gz";
        String s2 = "XXXX.pdb";
        String s3 = "http://www.rcsb.org/pdb/files/XXXX.pdb";
        String s4 = "http://www.rcsb.org/pdb/files/XXXX.cif?headerOnly=YES";
        String s5 = "/data/pdb/mirror/.../structures/divided/pdb/zz/pdbXXXX.ent.gz";
        String s6 = "XXXX.xyz";

        StructureFormatType sft1_out = StructureUtils.determineFileFormatByNameUrlOrPath(s1);
        StructureFormatType sft2_out = StructureUtils.determineFileFormatByNameUrlOrPath(s2);
        StructureFormatType sft3_out = StructureUtils.determineFileFormatByNameUrlOrPath(s3);
        StructureFormatType sft4_out = StructureUtils.determineFileFormatByNameUrlOrPath(s4);
        StructureFormatType sft5_out = StructureUtils.determineFileFormatByNameUrlOrPath(s5);
        StructureFormatType sft6_out = StructureUtils.determineFileFormatByNameUrlOrPath(s6);

        Assert.assertEquals(StructureFormatType.ENT, sft1_out);
        Assert.assertEquals(StructureFormatType.PDB, sft2_out);
        Assert.assertEquals(StructureFormatType.PDB, sft3_out);
        Assert.assertEquals(StructureFormatType.CIF, sft4_out);
        Assert.assertEquals(StructureFormatType.ENT, sft5_out);
        Assert.assertNull(sft6_out);
    }



    //---------------------------------------------------------------------------
    @Test
    public void parsePdbIdentifierFromFileName()
    {
        String pdbFile1 = "pdb2rs3.ent.gz";
        String pdbFile2 = "pdb2rs3.ent";

        String pdbFile3 = "2RS3.pdb.gz";
        String pdbFile4 = "2RS3.pdb";

        String cifFile1 = "1yz7.cif";
        String cifFile2 = "1yz7.cif.gz";

        String pdbFile1_out = StructureUtils.parsePdbIdentifierFromFileName(pdbFile1);
        String pdbFile2_out = StructureUtils.parsePdbIdentifierFromFileName(pdbFile2);
        String pdbFile3_out = StructureUtils.parsePdbIdentifierFromFileName(pdbFile3);
        String pdbFile4_out = StructureUtils.parsePdbIdentifierFromFileName(pdbFile4);
        String cifFile1_out = StructureUtils.parsePdbIdentifierFromFileName(cifFile1);
        String cifFile2_out = StructureUtils.parsePdbIdentifierFromFileName(cifFile2);

        String expectedPdb = "2RS3";
        String expectedCif = "1YZ7";

        Assert.assertEquals(expectedPdb, pdbFile1_out);
        Assert.assertEquals(expectedPdb, pdbFile2_out);
        Assert.assertEquals(expectedPdb, pdbFile3_out);
        Assert.assertEquals(expectedPdb, pdbFile4_out);

        Assert.assertEquals(expectedCif, cifFile1_out);
        Assert.assertEquals(expectedCif, cifFile2_out);

    }


}
