package com.just.bio.structure.services;

import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.pdb.StructureUtils;
import org.junit.Assert;
import org.junit.Test;

//------------------------------------------------------------------------------------------------------
// Copyright (c) Just Biotherapeutics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//------------------------------------------------------------------------------------------------------

public class TestRcsbRestService
{

    //---------------------------------------------------------------------------
    @Test
    public void testPdbFileExistsInRcsb()
    {
        String restUrlPdbFile = "http://cftp.rcsb.org/view/";
        String restUrlCIfDownload = "http://ftp.rcsb.org/download/";

        RcsbRestService rcsbRestService = RcsbRestService.getInstance(restUrlPdbFile, restUrlCIfDownload);

        Assert.assertFalse(rcsbRestService.pdbFileExistsInRcsb("2GWY"));
        Assert.assertTrue(rcsbRestService.pdbFileExistsInRcsb("4HHB"));
    }




}
