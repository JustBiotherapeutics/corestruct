package com.just.bio.structure.moe;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

//======================================================================================================
/**
 * MOE-releated tests.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class TestMOE
{

   //---------------------------------------------------------------------------
   @Test
   public void testGetMOE_Version()
      throws Exception
   {
      MOE_Version version = MOE.getMOE_Version();
      Assert.assertTrue(version.getReleaseYear() >= 2014);
      System.out.println(version);
   }

   //---------------------------------------------------------------------------
   @Test
   public void testGetLicenseInfo()
   {
      MOE.setMOE_Dir(new File("/Applications/moe2022_site"));

      MOE_LicenseInfo licenseInfo = MOE.getLicenseInfo();
      Assert.assertNotNull(licenseInfo.getPath());
      Assert.assertNotNull(licenseInfo.getExpires());
      System.out.println(licenseInfo.getNumDaysUntilExpiration() + " days until expiration");
      System.out.println(licenseInfo);
   }

}
