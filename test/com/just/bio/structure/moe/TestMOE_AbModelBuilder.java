package com.just.bio.structure.moe;

import com.hfg.bio.seq.Protein;
import com.just.bio.structure.moe.abmodelbuilder.MOE_AbModelBuilder;
import com.just.bio.structure.moe.abmodelbuilder.MOE_AbModelBuilderSettings;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

//======================================================================================================
/**
 * Tests for MOE_ModelBuilder.
 */
//------------------------------------------------------------------------------------------------------
// Copyright (c) Just - Evotec Biologics, Inc.
// All rights reserved.
// See the LICENSE.txt file included with this library for license terms and conditions.
//======================================================================================================

public class TestMOE_AbModelBuilder
{
    //---------------------------------------------------------------------------
    @BeforeClass
    public static void setup()
    {
        MOE.setMOE_Dir(new File("/Applications/moe2022_site"));
    }

    //---------------------------------------------------------------------------
    @Test
    public void test1()
            throws Exception
    {
        Protein testFv = new Protein().setID("stamulumab");
        testFv.addChain(new Protein().setID("LC").setSequence("SYELTQPPSVSVSPGQTASITCSGHALGDKFVSWYQQKPGQSPVLVIYDDTQRPSGIPERFSGSNSGNTATLTISGTQAMDEADYYCQAWDSSFVFGGGTKVTVLG"));
        testFv.addChain(new Protein().setID("HC").setSequence("EVQLVQSGAEVKKPGASVKVSCKASGYTFTSYYMHWVRQAPGQGLEWMGIINPSGGSTSYAQKFQGRVTMTRDTSTSTVYMELSSLRSEDTAVYYCARDENWGFDPWGQGTLVTVSS"));

        MOE_AbModelBuilderSettings settings = new MOE_AbModelBuilderSettings()
              .setRequireSS_Bond(true);

        MOE_AbModelBuilder abModelBuilder = new MOE_AbModelBuilder(settings);
        File modelFile = abModelBuilder.run(testFv);

        Assert.assertTrue(modelFile.length() > 0);
    }
}
